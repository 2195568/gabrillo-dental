<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OldPatientAppointmentRequests;
class OldPatientAppointmentRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OldPatientAppointmentRequests::factory()->count(10)->create();
    }
}
