<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProcedureCategory;

class ProcedureCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProcedureCategory::factory()->count(10)->create();
    }
}