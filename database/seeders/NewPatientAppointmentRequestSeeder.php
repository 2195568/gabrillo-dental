<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\NewPatientAppointmentRequests;
class NewPatientAppointmentRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NewPatientAppointmentRequests::factory()->count(10)->create();
    }
}
