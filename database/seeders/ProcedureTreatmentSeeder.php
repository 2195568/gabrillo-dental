<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProcedureTreatment;
class ProcedureTreatmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProcedureTreatment::factory()->count(10)->create();
    }
}
