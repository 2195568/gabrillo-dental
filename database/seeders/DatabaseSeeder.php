<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Appointment;
use App\Models\Patient;
use App\Models\ProcedureTreatment;
use DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //run this once onle
        // $CATEGORIES = [
        //     'Costmetic Resotration', 'Jacket Crowns', 'Cementation', 'Edodontic Treatment', 
        //     'Prosthetic Procedures',
        //     'Surgical Procedures', 'Dentures', 'Others',
        // ];
        // //run this once onle
        // $PROCEDURES = [
        //     ['Oral Prophylaxis', 800, 8],
        //     ['Tooth Restoration', 800, 8],
        //     ['Tooth Extraction', 700, 8],
        //     ['Deep Scaling', 3000, 8],
        //     ['Pits and Fissures Sealant', 800, 8],
        //     ['Fluoride Treatment', 2500, 8],
        //     ['Intermediate Restoration', 500, 8],
        //     ['Direct Composite Veneer', 3000, 1],
        //     ['Direct Composite Class IV', 2000, 1],
        //     ['Diastema Closure (Bonding)', 500, 1],
        //     ['Ceramic/Porcelain Veneer', 15000, 1],
        //     ['Porcelain fused to Non-precious metal', 8000, 2],
        //     ['Porcelain fused to Semi-precious metal (Tillite)', 10000, 2],
        //     ['Porcelain fused to Gold', 18000, 2],
        //     ['All Ceramic Crown (Emax)', 18000, 2],
        //     ['Zirconia Crown', 20000, 2],
        //     ['Composite Crown', 6000, 2],
        //     ['Plastic Crown', 2500, 2],
        //     ['Full Metal Crown', 5000, 2],
        //     ['Full Metal Crown for Pediatric Patients', 3500, 2],
        //     ['Glass Ionomer', 800, 3],
        //     ['Resin Cement', 1500, 3],
        //     ['Temporary Cement (Non Eugenol)', 400, 3],
        //     ['Root Canal Therapy', 7000, 4],
        //     ['Pulpotomy', 3000, 4],
        //     ['Post and Core', 8000, 4],
        //     ['Denture Repair', 300, 5],
        //     ['Denture Reline (Laboratory Made)', 2000, 5],
        //     ['Denture Reline (Direct)', 2500, 5],
        //     ['Soft Reline', 3000, 5],
        //     ['Denture Teeth Replacement', 800, 5],
        //     ['Odontectomy', 8000, 6],
        //     ['Operculectomy', 3000, 6],
        //     ['Frenectomy', 10000, 6],
        //     ['Alveolectomy', 5000, 6],
        //     ['Gingivectomy or Contouring', 10000, 6],
        //     ['Apicoectomy', 10000, 6],
        //     ['Onlay/Inlay (Composite)', 5000, 8],
        //     ['Onlay/Inlay (Ceramic)', 15000, 8],
        //     ['Maryland Bridge (Composite)', 6000, 8],
        //     ['Maryland Bridge (Porcelain with metal)', 8000, 8],
        //     ['Maryland Bridge (All Ceramic)', 15000, 8],
        //     ['Maryland Bridge (Composite)', 6000, 8],
        //     ['Regular Plastic (Complete Denture)', 16000, 7],
        //     ['Hard Plastic (Complete Denture)', 20000, 7],
        //     ['Combination of Ceramic and Resin (Complete Denture)', 40000, 7],
        //     ['Ivocap (Complete Denture)', 50000, 7],
        //     ['1 Unit (Parial Denture Acrylic Base)', 1800, 7],
        //     ['2 Units (Parial Denture Acrylic Base)', 2500, 7],
        //     ['3 Units (Parial Denture Acrylic Base)', 2800, 7],
        //     ['4 Units (Parial Denture Acrylic Base)', 3500, 7],
        //     ['5 Units (Parial Denture Acrylic Base)', 4000, 7],
        //     ['6 Units (Parial Denture Acrylic Base)', 5000, 7],
        //     ['7 Units (Parial Denture Acrylic Base)', 7000, 7],
        //     ['8 Unit (Parial Denture Acrylic Base)', 8000, 7],
        //     ['9 Units (Parial Denture Acrylic Base)', 9000, 7],
        //     ['10 Units (Parial Denture Acrylic Base)', 1000, 7],
        //     ['1-4 Units (Flexible Denture)', 8000, 7],
        //     ['More than 5 missing teeth (Flexible Denture)', 15000, 7],
        //     ['Metal Frames RPD', 20000, 7],
        //     ['Combination of Flexible and metals', 25000, 7],
        // ];

        // //run this once onle
        // foreach($CATEGORIES as $cat){
        //     DB::table('procedure_categories')->insert(['name' =>$cat ]);
        // }

        // //run this once onle
        // foreach($PROCEDURES as $proc){
        //     DB::table('procedures')->insert(['name' =>$proc[0], 'amount_charged' =>$proc[1], 'procedure_category_id' =>$proc[2] ]);
        // }
        
       // Treatment::factory(10) ->create(10)-each(function($))
        //$this->call(PatientSeeder::class);
        //$this->call(UserSeeder::class);
       // $this->call(TreatmentSeeder::class);
       //$this->call(ProcedureCategorySeeder ::class);
       //$this->call(ProcedureTreatmentSeeder ::class);
       //$this->call(ProcedureSeeder ::class);
       //$this->call(NewPatientAppointmentRequestSeeder ::class);
       $this->call(OldPatientAppointmentRequestSeeder ::class);
        //$this->call(AppointmentSeeder::class);
       // $this->call(PaymentSeeder::class);
    }
}
