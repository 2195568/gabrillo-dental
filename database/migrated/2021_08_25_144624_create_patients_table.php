<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('middle_name', 50);
            $table->string('address', 255);
            $table->string('contact_no', 15)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('marital_status', 50)->nullable();
            $table->string('occupation', 255)->nullable();
            $table->string('birth_date', 50)->nullable();
            $table->integer('age')->nullable();
            $table->char('gender')->nullable();
            $table->string('medical_condition', 255)->nullable();
            $table->string('allergies', 255)->nullable();
            $table->string('account_status')->nullable();
            $table->double('balance', 8, 2)->default('0');

            $table->unsignedBigInteger('profile_picture_id');
            $table->foreign('profile_picture_id')->references('id')->on('profile_pictures')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
