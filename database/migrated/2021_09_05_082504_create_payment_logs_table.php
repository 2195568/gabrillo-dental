    <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_logs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('column', 50);
            $table->double('old_value', 8, 2);
            $table->double('new_value', 8, 2);

            $table->unsignedBigInteger('payment_id');
            $table->foreign('payment_id')->references('id')
                ->on('payments')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('changed_by');
            $table->foreign('changed_by')->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_logs');
    }
}
