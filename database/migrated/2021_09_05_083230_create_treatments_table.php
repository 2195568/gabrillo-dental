<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTreatmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('treatments')) {
            Schema::create('treatments', function (Blueprint $table) {
                $table->id();
                $table->time('time_start');
                $table->date('date');
                $table->double('total_amount');
                $table->double('balance')->default('0.0');
                $table->timestamps();
                $table->integer('tooth_no')->nullable();

                $table->unsignedBigInteger('patient_id');
                $table->foreign('patient_id')->references('id')
                    ->on('patients')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
                $table->unsignedBigInteger('payment_id');
                $table->unsignedBigInteger('payment_status_id');
                $table->foreign('payment_status_id')->references('id')
                    ->on('payment_status')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treatments');
    }
}