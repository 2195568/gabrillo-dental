<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewPatientAppointmentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_patient_appointment_requests', function (Blueprint $table) {
            $table->id();
            $table->boolean('appointment_status');
            $table->string('description');
            $table->date('date');
            $table->time('time_start');
            $table->time('time_end');
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('middle_name', 50);
            $table->string('address', 255);
            $table->string('contact_no', 15)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('marital_status', 50)->nullable();
            $table->string('occupation', 255)->nullable();
            $table->string('birth_date', 50)->nullable();
            $table->integer('age')->nullable();
            $table->char('gender')->nullable();
            $table->string('medical_condition', 255)->nullable();
            $table->string('allergies', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_patient_appointment_requests');
    }
}
