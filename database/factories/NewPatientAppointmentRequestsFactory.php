<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\NewPatientAppointmentRequests;
use DB;
class NewPatientAppointmentRequestsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NewPatientAppointmentRequests::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->name(),
            'last_name' => $this->faker->name(),
            'middle_name' => $this->faker->name(),
            'address' => $this->faker->address(),
            'contact_no'=> '09261238765',
            'email' => $this->faker->unique()->safeEmail(),
            'occupation' => $this->faker->jobTitle(),
            'birth_date' => $this->faker->date(),
            'age' => rand(10,80),
            'description' => $this->faker->sentence(2),
             'date' =>$this->faker->dateTimeBetween($startDate = 'now', $endDate = '+1 month'),
             'time_start' => $this->faker->time($format = 'H:i:s',$min='09:00:00', $max = '18:00:00'),
             'reference_id' =>  $this->faker->isbn10(),
            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),
            'allergies' => $this->faker->randomElement(['Food Allergy','Pet Allergy','Mold Allergy','Latex Allergy','Pollen Allergy','Null']),
            'medical_condition' => $this->faker->randomElement(['Mentally Disabled','Physically Disabled ','Broken Ribs','Special Child','Severly Wasted','Obese']),
            'gender' => $this->faker->randomElement(['Male','Female',]),
            'marital_status' => $this->faker->randomElement(['Single','Married',]),
            'appointment_status' => $this->faker->randomElement(['UP','DOWN',]),
        ];
    }
}
