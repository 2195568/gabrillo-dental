<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\ProcedureTreatment;
use DB;
class ProcedureTreatmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProcedureTreatment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $treatmentIDs = (DB::table('treatments')->pluck('id'))->random();
        $procedureIDs = (DB::table('procedures')->pluck('id'))->random();
        return [
            'procedure_id' => $procedureIDs,
            'treatment_id' => $treatmentIDs,
            'created_At' => '2021-05-19',
            
        ];
    }
}
