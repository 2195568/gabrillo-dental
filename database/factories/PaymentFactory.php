<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Payment;
use DB;
class PaymentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Payment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $treatmentIDs = (DB::table('treatments')->pluck('id'))->random();

        return [
            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),
            'payment_status' => rand(0,1),
            'treatment_id' =>  $treatmentIDs,
            'amount' => rand(0,900), 
            'receive' => rand(500,900), 
            
        ];
    }
}
