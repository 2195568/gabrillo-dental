<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Appointment;
use DB;
class AppointmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Appointment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $patientIDs = (DB::table('patients')->pluck('id'))->random();
        return [
            'title' => $this->faker->sentence(),
            'description' => $this->faker->sentence(2),
            'patient_id' =>  $patientIDs,
            'date' => $this->faker->date(),
            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),
            'time_start' => $this->faker->time(),
            'time_end' => $this->faker->time(),
        ];
    }
}
