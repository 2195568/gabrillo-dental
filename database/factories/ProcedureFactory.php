<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Procedure;
use DB;
class ProcedureFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Procedure::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        //$prodIDs = (DB::table('procedures')->pluck('id'))->random();

        return [
            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),
            'name'=> 'Jacket Crowns',
            //'procedure_category_id' => $prodIDs,
            'description' => $this->faker->sentence(2),
            'amount_charged' => rand(0,900),
            

        ];
    }
}
