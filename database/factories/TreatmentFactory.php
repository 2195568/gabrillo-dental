<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Treatment;
use DB;
class TreatmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Treatment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $patientIDs = (DB::table('patients')->pluck('id'))->random();

        return [
            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),
            'tooth_no' => rand(20,40),
            'patient_id' =>  $patientIDs,
            'date' => now(),
            'balance' => rand(200,1000),
            'payment_status_id' => rand(1,2),
            'time_start' => $this->faker->time(),
            'total_amount' => rand(0,900), 
            'payment_id' => rand(1, 10000),
            'reference_id' => rand(1, 10000),
           
        ];
    }
}
