<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\OldPatientAppointmentRequests;
use DB;
class OldPatientAppointmentRequestsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OldPatientAppointmentRequests::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        //$patientIDs = (DB::table('patients')->pluck('id'))->random();
        $patientID = (DB::table('patients')->pluck('id'))->random();
        return[
         
            
            
            //'patient_id' =>  $patientIDs,
            'description' => $this->faker->sentence(2),
             
            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),

           
            
            'patient_slug' => $this->faker->slug(),
            'description' => $this->faker->sentence(2),
             'date' =>$this->faker->dateTimeBetween($startDate = 'now', $endDate = '+1 month'),
             'time_start' => $this->faker->time($format = 'H:i:s',$min='09:00:00', $max = '18:00:00'),
             
            'reference_id' => $this->faker->uuid(),
            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),
            'patient_id' =>  $patientID,
            'appointment_status' => $this->faker->randomElement(['UP','DOWN',]),
        ];
    }
}
