<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Patient;
use DB;
class PatientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->name(),
            'last_name' => $this->faker->name(),
            'middle_name' => $this->faker->name(),
            'address' => $this->faker->address(),
            'contact_no'=> '09261238765',
            'email' => $this->faker->unique()->safeEmail(),
            'occupation' => $this->faker->jobTitle(),
            'birth_date' => $this->faker->date(),
            'age' => rand(10,80),
            'account_status'=> $this-> status(),
            'balance'=>  rand(0,900), 
            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),
            'last_appointment' =>$this->faker->date(),
           
        ];
    }

    public function status(){
        return rand(0, 1) ? 'Inactive' : 'Active';
    }
}
