-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: l8jet
-- ------------------------------------------------------
-- Server version	8.0.25-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointment_requests`
--

DROP TABLE IF EXISTS `appointment_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointment_requests` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `appointment_status` tinyint(1) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` bigint NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `patient_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment_requests`
--

LOCK TABLES `appointment_requests` WRITE;
/*!40000 ALTER TABLE `appointment_requests` DISABLE KEYS */;
INSERT INTO `appointment_requests` VALUES (1,1,'111','2021-10-01','13:30:00','14:30:00','Foo Test',1234,'2195568@slu.edu.ph',NULL,NULL,''),(2,1,'...','2021-10-01','13:32:00','14:32:00','Test',123,'2195568@slu.edu.ph',NULL,NULL,''),(3,1,'try','2021-10-01','13:38:00','14:38:00','Try',1234,'2195568@slu.edu.ph',NULL,NULL,''),(4,1,'Test','2021-10-01','14:01:00','15:01:00','Jake Manual',123,'2195568@slu.edu.ph',NULL,NULL,''),(5,1,'test','2021-10-01','14:39:00','15:39:00','Ac Palabay',124,'2195568@slu.edu.ph',NULL,NULL,''),(6,0,'Sobrang sakit ng ngipin ko.','2021-10-02','15:12:00','16:12:00','Naji Gabrillo',9123456789,'2195568@slu.edu.ph',NULL,NULL,'new'),(7,0,'Kulay yellow na may black.','2021-10-02','19:05:00','20:05:00','Mark John',9123456789,'2195568@slu.edu.ph',NULL,NULL,'new'),(8,0,'I need tooth extraction.','2021-10-02','19:07:00','20:07:00','Ney Net',9268925752,'2195568@slu.edu.ph',NULL,NULL,'new'),(9,1,'Test','2021-10-04','08:13:00','09:14:00','John Mark',9123456789,'2195568@slu.edu.ph',NULL,NULL,'new');
/*!40000 ALTER TABLE `appointment_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `patient_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `appointments_patient_id_foreign` (`patient_id`),
  CONSTRAINT `appointments_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (3,'Test1','...','2021-08-02','13:04:00','14:04:00','2021-08-25 15:04:13','2021-08-26 17:04:25',1),(5,'Sample123','...','2021-08-26','22:21:00','23:21:00','2021-08-26 14:21:31','2021-08-26 17:32:41',1),(6,'Aaa','...','2021-08-19','14:11:00','15:11:00','2021-08-27 06:11:24','2021-08-27 06:11:24',1),(10,'t1','.','2021-09-05','15:48:00','15:48:00',NULL,NULL,1),(11,'t12','.','2021-12-12','15:48:00','15:48:00',NULL,NULL,1),(12,'t12','.','2021-09-01','15:48:00','15:48:00',NULL,NULL,1),(13,'t12','.','2021-08-31','15:48:00','15:48:00',NULL,NULL,1),(14,'Last appointment','...','2021-08-01','16:21:00','17:21:00','2021-08-30 08:45:19','2021-08-30 08:45:19',1),(16,'T2','..','2021-08-04','15:04:00','16:04:00','2021-08-31 07:05:22','2021-08-31 07:05:22',1),(17,'3','..','2021-09-01','23:42:00','23:42:00','2021-09-01 15:42:30','2021-09-01 15:42:30',1),(18,'a','.','2021-09-06','23:44:00','12:44:00','2021-09-01 15:44:41','2021-09-01 15:44:41',1),(21,'d','d','2021-09-02','23:45:00','23:45:00','2021-09-01 15:45:32','2021-09-03 04:11:10',1),(22,'31','d','2021-08-31','23:45:00','23:45:00','2021-09-01 15:45:57','2021-09-01 15:45:57',1),(23,'d','d','2021-09-01','23:46:00','11:46:00','2021-09-01 15:46:14','2021-09-01 15:46:14',1),(24,'Sept 1','d','2021-09-01','23:47:00','23:47:00','2021-09-01 15:47:38','2021-09-01 15:47:38',1),(25,'d','d','2021-08-30','23:47:00','12:47:00','2021-09-01 15:47:59','2021-09-01 15:47:59',1),(31,'john mark','...','2021-09-11','00:41:00','01:41:00','2021-09-09 16:41:20','2021-09-09 16:41:20',3),(32,'Nigel Cacho','...','2021-09-12','14:26:00','15:26:00','2021-09-10 06:26:51','2021-09-10 06:26:51',12),(33,'Tests','dafasdfafd','2021-10-05','00:14:00','01:14:00','2021-10-04 16:14:50','2021-10-04 16:16:46',2);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1),(6,'2020_05_21_100000_create_teams_table',1),(7,'2020_05_21_200000_create_team_user_table',1),(8,'2020_05_21_300000_create_team_invitations_table',1),(9,'2021_08_11_173951_create_sessions_table',1),(10,'2021_08_25_144524_create_profile_pictures_table',2),(11,'2021_08_25_144624_create_patients_table',2),(12,'2021_08_25_144721_create_appointments_table',2),(13,'2021_08_27_143040_drop_multiple_columns_from_patients',3),(14,'2021_08_27_152214_drop_profile_picture_id_from_patients',4),(15,'2021_08_27_152556_drop_profile_picture_id_from_patients',5),(16,'2021_08_27_152739_drop_profile_picture_table',5),(17,'2021_08_29_155806_add_new_appointment_and_last_appointment_column_to_patient_table',6),(18,'2021_09_05_075939_create_payments_table',7),(19,'2021_09_05_082408_create_procedure_categories_table',7),(20,'2021_09_05_082504_create_payment_logs_table',7),(21,'2021_09_05_083225_create_procedures_table',8),(22,'2021_09_05_091319_add_time_start_and_time_end_to__treatments_table',9),(23,'2021_09_05_100401_remove_payment_id_to_treatments_table',10),(24,'2021_09_05_100847_add_treatments_id_to_payments_table',11),(25,'2021_09_05_101227_add_balance_to_treatments_table',12),(26,'2021_09_05_142116_add_date_column_to_treatments_table',13),(27,'2021_09_06_141431_delete_balance_column_to_treatments_table',14),(28,'2021_09_06_141709_create_total_amount_column_to_treatments_table',15),(29,'2021_09_08_065914_add_balance_column_to_treatments_table',16),(30,'2021_09_08_084839_create_user_roles_table',17),(31,'2021_09_27_064422_drop_procedures_and_procedure_categories_table',18),(32,'2021_09_27_065440_create_procedure_categories_table',19),(33,'2021_09_27_065450_create_procedures_table',19),(34,'2021_09_27_070509_remove_procedure_id_to_treatments_table',20),(35,'2021_09_27_072402_create_procedure_treatment_table',21),(36,'2021_10_01_042559_create_appointment_requests_table',22),(37,'2021_10_02_065936_add_patient_type_column_to_appointment_requests_table',23),(38,'2021_10_02_070243_modify_contact_no_column_to_appointment_requests_table',24),(39,'2021_10_02_070245_modify_contact_no_column_to_appointment_requests_table',25),(40,'2021_10_02_070246_modify_contact_no_column_to_appointment_requests_table',26),(41,'2021_10_04_164433_create_payment_status_table',27);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('fnotestado@gmail.com','$2y$10$eY3O6dFMZH.5G/pC1GZoau7w8qU41OIp8iE3lfTKyRTgGZL3TEiHW','2021-08-22 04:15:43');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int DEFAULT NULL,
  `gender` char(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergies` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` double(8,2) NOT NULL DEFAULT '0.00',
  `new_appointment` date DEFAULT NULL,
  `last_appointment` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (1,NULL,'2021-09-10 06:23:55','test','test','test','test',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'inactive',0.00,'2021-12-12','2021-09-05'),(2,'2021-08-27 15:31:29','2021-10-04 16:14:50','Foo','Test','ORDOÑA','Cuenca',NULL,'33@abc.com','None','2021-08-27',21,NULL,'egg','active',0.00,'2021-10-05',NULL),(3,NULL,'2021-09-14 06:53:41','john','mark','luke','secret','09','12',NULL,'2000-06-14',20,NULL,NULL,'active',0.00,NULL,NULL),(4,'2021-08-31 07:42:47','2021-09-01 15:39:10','FranzNico','Testado','ORDOÑA','Cuenca','09177515078','fnotestado@gmail.com','c','2021-08-31',21,NULL,'egg','inactive',0.00,NULL,NULL),(5,'2021-08-31 07:44:19','2021-09-01 15:41:05','Franz Nico0','Testado','ORDOÑA','Cuenca',NULL,'fnotestado@gmail.com','None','2021-08-31',21,NULL,'egg','inactive',0.00,NULL,NULL),(6,'2021-09-01 03:13:00','2021-09-01 03:13:00','FranzNico','Testado','ORDOÑA','Cuenca','09177515078','fnotestado@gmail.com','none','2021-09-01',21,NULL,'egg',NULL,0.00,NULL,NULL),(7,'2021-09-01 03:13:58','2021-09-01 03:13:58','Test','Test','Test','Cuenca','09177515078','fnotestado@gmail.com','none','2021-09-01',21,NULL,'egg',NULL,0.00,NULL,NULL),(8,'2021-09-01 03:16:20','2021-09-01 03:16:20','Test','Test','Test','Cuenca','09177515078','fnotestado@gmail.com','none','2021-09-01',21,NULL,'egg',NULL,0.00,NULL,NULL),(9,'2021-09-01 03:20:02','2021-09-01 03:20:19','Gab','Dent','Test','Cuenca','09177515078','fnotestado@gmail.com','none','2021-09-01',21,NULL,NULL,'inactive',0.00,NULL,NULL),(10,'2021-09-01 16:46:29','2021-09-01 16:46:40','FranzNico','Testado','ORDOÑA','Cuenca','09177515078','fnotestado@gmail.com','none','2021-09-02',21,NULL,'egg','inactive',0.00,NULL,NULL),(11,'2021-09-10 06:04:26','2021-09-10 06:04:26','Nico','Testado','Ordona','Cuenca','09177515078','fnotestado@gmail.com','None','2021-09-10',21,NULL,'Egg, peanuts','active',0.00,NULL,NULL),(12,'2021-09-10 06:21:25','2021-09-16 03:15:44','Nigel','Cacho','Ibay','Unino St., Iba, Zambales',NULL,'fnotestado@gmail.com','None','2021-09-10',21,NULL,'Egg','inactive',0.00,NULL,NULL);
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_logs`
--

DROP TABLE IF EXISTS `payment_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_logs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `column` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_value` double(8,2) NOT NULL,
  `new_value` double(8,2) NOT NULL,
  `payment_id` bigint unsigned NOT NULL,
  `changed_by` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_logs_payment_id_foreign` (`payment_id`),
  KEY `payment_logs_changed_by_foreign` (`changed_by`),
  CONSTRAINT `payment_logs_changed_by_foreign` FOREIGN KEY (`changed_by`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `payment_logs_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_logs`
--

LOCK TABLES `payment_logs` WRITE;
/*!40000 ALTER TABLE `payment_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_status`
--

DROP TABLE IF EXISTS `payment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_status` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_status`
--

LOCK TABLES `payment_status` WRITE;
/*!40000 ALTER TABLE `payment_status` DISABLE KEYS */;
INSERT INTO `payment_status` VALUES (1,'paid',NULL,NULL),(2,'pending',NULL,NULL);
/*!40000 ALTER TABLE `payment_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `receive` double(8,2) NOT NULL DEFAULT '0.00',
  `treatment_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_treatment_id_foreign` (`treatment_id`),
  CONSTRAINT `payments_treatment_id_foreign` FOREIGN KEY (`treatment_id`) REFERENCES `treatments` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedure_categories`
--

DROP TABLE IF EXISTS `procedure_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `procedure_categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedure_categories`
--

LOCK TABLES `procedure_categories` WRITE;
/*!40000 ALTER TABLE `procedure_categories` DISABLE KEYS */;
INSERT INTO `procedure_categories` VALUES (1,NULL,NULL,'Cosmetic restoration','...'),(2,NULL,NULL,'Jacket crowns','...'),(3,NULL,NULL,'Prosthetic procedures','Preprosthetic surgery is the term used for any procedure related to either hard/soft tissue correction or augmentation before prosthetic treatment.'),(4,NULL,NULL,'Surgical procedures','The act of performing surgery may be called a surgical procedure, operation, or simply \"surgery\". In this context, the verb \"operate\" means to perform surgery.'),(5,NULL,NULL,'Other resto. services','...');
/*!40000 ALTER TABLE `procedure_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedure_treatment`
--

DROP TABLE IF EXISTS `procedure_treatment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `procedure_treatment` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `procedure_id` bigint unsigned NOT NULL,
  `treatment_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedure_treatment`
--

LOCK TABLES `procedure_treatment` WRITE;
/*!40000 ALTER TABLE `procedure_treatment` DISABLE KEYS */;
INSERT INTO `procedure_treatment` VALUES (2,9,15),(3,1,16),(4,2,16),(5,3,16),(6,4,16),(7,1,17),(8,2,17),(9,8,17),(10,2,15),(11,3,15),(12,4,15),(14,9,18),(15,7,18),(16,8,18),(17,1,19),(18,2,19),(19,5,19),(20,8,19),(21,1,20),(22,2,20),(23,5,20),(24,6,20),(25,1,21),(26,2,21),(27,3,21),(28,4,21),(29,1,22),(30,2,22),(31,8,22),(32,3,23),(33,4,23),(34,8,23),(35,9,23);
/*!40000 ALTER TABLE `procedure_treatment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedures`
--

DROP TABLE IF EXISTS `procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `procedures` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_charged` double(8,2) NOT NULL DEFAULT '0.00',
  `procedure_category_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `procedures_procedure_category_id_foreign` (`procedure_category_id`),
  CONSTRAINT `procedures_procedure_category_id_foreign` FOREIGN KEY (`procedure_category_id`) REFERENCES `procedures` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedures`
--

LOCK TABLES `procedures` WRITE;
/*!40000 ALTER TABLE `procedures` DISABLE KEYS */;
INSERT INTO `procedures` VALUES (1,NULL,NULL,'Direct Composite Veneer','...',3000.00,1),(2,NULL,NULL,'Direct Composite Class IV','...',2000.00,1),(3,NULL,NULL,'Diastema Closure (Bonding)','...',2000.00,1),(4,NULL,NULL,'Ceramic/Porcelain Veneer','...',15000.00,1),(5,NULL,NULL,'Porcelain fused to Non-precious metal','...',8000.00,2),(6,NULL,NULL,'Porcelain fused to Gold','...',20000.00,2),(7,NULL,NULL,'All Ceramic Crown (Emax)','...',18000.00,2),(8,NULL,NULL,'Zirconia','...',20000.00,2),(9,NULL,NULL,'Composite Crown','...',6000.00,2),(10,NULL,NULL,'Plastic Crown','...',2500.00,2),(11,NULL,NULL,'Full Metal Crown','...',5000.00,2),(12,NULL,NULL,'Full Metal Crown for Pediatric Patients','...',3500.00,2);
/*!40000 ALTER TABLE `procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('bCOVvX09EHVEzBawfVkyIm2ru2kLXAXbxWIVNJjR',26,'192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36','YTo2OntzOjY6Il90b2tlbiI7czo0MDoiUEczQlJJblNkNkZISlhqa3pWQkt2NHF4SEpPeWtnbm5UOWJqY1UxMiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDA6Imh0dHA6Ly93d3cuZ2FicmlsbG9kZW50YWwuY29tL3RyZWF0bWVudHMiO31zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToyNjtzOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJGtYaUNCY2tFa1Zzd3d4R2oycjVwbU90alFHalR3dm92Q213Z29JNE5Qb1Nmb3VVYXZiUWcuIjtzOjIxOiJwYXNzd29yZF9oYXNoX3NhbmN0dW0iO3M6NjA6IiQyeSQxMCRrWGlDQmNrRWtWc3d3eEdqMnI1cG1PdGpRR2pUd3ZvdkNtd2dvSTROUG9TZm91VWF2YlFnLiI7fQ==',1633419008),('rE2NVYt08a9oQlYnufkN0YmB2PZsYmwrMAMVEMQC',26,'192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36','YTo2OntzOjY6Il90b2tlbiI7czo0MDoiemdYMGg5NjRlRHJnQlF1VmtHc25aOVEwNmd2TW81eEhmY3haU1JKNyI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjQwOiJodHRwOi8vd3d3LmdhYnJpbGxvZGVudGFsLmNvbS90cmVhdG1lbnRzIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MjY7czoxNzoicGFzc3dvcmRfaGFzaF93ZWIiO3M6NjA6IiQyeSQxMCRrWGlDQmNrRWtWc3d3eEdqMnI1cG1PdGpRR2pUd3ZvdkNtd2dvSTROUG9TZm91VWF2YlFnLiI7fQ==',1633408317);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_invitations`
--

DROP TABLE IF EXISTS `team_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `team_invitations` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_invitations_team_id_email_unique` (`team_id`,`email`),
  CONSTRAINT `team_invitations_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_invitations`
--

LOCK TABLES `team_invitations` WRITE;
/*!40000 ALTER TABLE `team_invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_user`
--

DROP TABLE IF EXISTS `team_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `team_user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_user_team_id_user_id_unique` (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_user`
--

LOCK TABLES `team_user` WRITE;
/*!40000 ALTER TABLE `team_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teams` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_team` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teams_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,1,'FranzNico\'s Team',1,'2021-08-11 17:50:30','2021-08-11 17:50:30'),(2,2,'Test\'s Team',1,'2021-08-16 06:39:49','2021-08-16 06:39:49'),(3,3,'FranzNico\'s Team',1,'2021-08-22 04:12:23','2021-08-22 04:12:23'),(4,25,'Admin\'s Team',1,'2021-10-02 03:46:33','2021-10-02 03:46:33'),(5,26,'Admin\'s Team',1,'2021-10-02 03:53:09','2021-10-02 03:53:09');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `treatments`
--

DROP TABLE IF EXISTS `treatments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `treatments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tooth_no` int DEFAULT NULL,
  `patient_id` bigint unsigned NOT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `date` date DEFAULT NULL,
  `total_amount` double(8,2) NOT NULL,
  `balance` double(8,2) NOT NULL,
  `payment_status_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `treatments`
--

LOCK TABLES `treatments` WRITE;
/*!40000 ALTER TABLE `treatments` DISABLE KEYS */;
INSERT INTO `treatments` VALUES (17,'2021-09-28 15:28:44','2021-10-02 11:20:07',26,3,NULL,NULL,'2021-09-30',2000.00,600.00,0),(21,'2021-10-01 06:46:37','2021-10-01 06:49:03',11,2,NULL,NULL,'2021-10-01',2000.00,999.00,0),(22,'2021-10-02 11:18:27','2021-10-02 11:18:27',11,3,NULL,NULL,'2021-10-02',900.00,900.00,0),(23,'2021-10-05 04:30:43','2021-10-05 04:30:43',47,3,NULL,NULL,'2021-10-05',999.00,999.00,2);
/*!40000 ALTER TABLE `treatments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,NULL,NULL,'admin'),(2,NULL,NULL,'admin_dentist'),(3,NULL,NULL,'dentist'),(4,NULL,NULL,'receptionist');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint unsigned DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'FranzNico Testado','fnotestado@gmail.com',NULL,'$2y$10$H.AQt7gKwxbFh7m/M.pp7efRDnPbaIF4qbMbiA2t4EojkASvX15sa',NULL,NULL,NULL,1,NULL,'2021-08-11 17:50:30','2021-09-06 05:00:24',0),(2,'Test','test@gmail.com',NULL,'$2y$10$7P7ST4lSuXGbsXDlI.aasu4.Rakurv2QFlAdYeAAHjiKrGNQMDEom',NULL,NULL,NULL,NULL,NULL,'2021-08-16 06:39:49','2021-08-16 06:46:27',0),(3,'FranzNico Testado','fnotestado1@gmail.com',NULL,'$2y$10$Pp17Ob2LFTlMWj5HyR9DMuzW2bhTgb1F6ls26xpF.g9AMTz.6Sxy.',NULL,NULL,NULL,NULL,'profile-photos/gnvv9sdDWIqU03zdqhO8HTHHkadGiCyTMR9DJuKx.jpg','2021-08-22 04:12:23','2021-08-22 07:45:11',0),(4,'Naji Gabrillo','ngabrillo@gabrillodental.com',NULL,'$2y$10$0dRMbOu6Y6YaoukZ1cgFG.dGuofS0tiPBnmeC9ih/fQQVsrl0CRYu',NULL,NULL,NULL,NULL,NULL,NULL,'2021-09-10 06:31:44',2),(18,'Test','test@test',NULL,'$2y$10$r4OEqtaet.8vlE1ZloO5HuLGp3E9vLaOwDco2uT8zEb76pLYujJjG',NULL,NULL,NULL,NULL,NULL,'2021-09-08 15:50:52','2021-09-08 15:50:52',1),(20,'Mamdouh Elssbiay','melssbiay@gabrillodental.com',NULL,'$2y$10$a7Sp3.4TpqM./NXR5DJIyOKfWPWiZV7wXMtmO8ktmPmBxKeSYePGO',NULL,NULL,NULL,NULL,NULL,'2021-09-08 16:01:24','2021-09-08 16:01:24',4),(21,'Admin Test','admin123@gdc.com',NULL,'$2y$10$DwZM5Mt7WXjgbHjLN.4YueYFc87efsS0oEIjmdMKGZYgnJz411/4K','eyJpdiI6IjZnQjQxMmFFaXpyWXIvNnh5c3lQYUE9PSIsInZhbHVlIjoiNVIvTkNzdWg2SVhGcWk2cFN4dE9SWHJMUmZlNEw3TnVVb05MSnU0OEd5Yz0iLCJtYWMiOiJhNjU3YTk1NTM0MzYyYmY0ZGUxMzI0YWIyMzQyMGEwYmQ5NjA1OTRjMGVmNTQyODdiNDU2N2NhNDgyMjFiMTlhIiwidGFnIjoiIn0=','eyJpdiI6Ild3UnFIVkFxT1JMOVJlSVJzTzRKT3c9PSIsInZhbHVlIjoiVElVSVpXQnVMT3JTQTY1MDk1NTM4SFErS28yV1ArcDZMZ1AxdGJ6YndoODEwM0xCTTgzZE9taVVtNVBaUWlIVm5VSm9EN3UrZjY1ZlZEb2RPVE45cVB3UU04ek11UUxDR2JvQUtzSnJDcC9HOVVqa09taHdDVlp4ZXJiSjU5ZVJqWXNEYjZEM042RU56SzEwRGlkWFVlTkQzUGdJUkx4bnkzSVIzSHM2SUl2ZzBkYi9UcnJkVFVheS9GUTBjVVdMSmw0VFBZZWI1MGlTaG9Od3czRmlPWnVuZFgyZ0duc09oaUVqbTkraGJiN3V3VHM0N0x2eWdBVlppbDRFTnFlZFJHQkJab3dQMWQwSmxsQWh3aGZYT3c9PSIsIm1hYyI6IjFhMTE1MjhjNzVlOTU2NWFkNzYwNTlhZTEwYzg2N2E0MGFmY2VlZTYzOTY3ZDA5MDM4YzNkN2U1ZWEzYmRjOGMiLCJ0YWciOiIifQ==',NULL,NULL,NULL,'2021-09-08 18:48:06','2021-10-01 06:53:17',1),(22,'Dentist','dentist@gdc.com',NULL,'$2y$10$tNZFK.nVi4S208eBCE4EROGx1e6ZRbqGzxmDSf2VkS/34aCVKIery','eyJpdiI6ImdHQVhTNmFpRkM5ZVVUOCtvZ3pmbGc9PSIsInZhbHVlIjoiNlR0dXZMOWVZVHl3a3djeENVWEtzdkZicytKaHoxQ1FSVDVnbDRpQkl3WT0iLCJtYWMiOiJjOTY4NTJkNmUyMzIyZTVhZGEwY2RjNThhNWMxYmIwYmUzY2ZmMWFhZDNiM2QxZWFmNGZmODY1YTkwZjY1OGQ0IiwidGFnIjoiIn0=','eyJpdiI6Im1SWHhMRVZWVy9EVUFyZWhrWXQ3emc9PSIsInZhbHVlIjoibVBzbFA3cktTUXYwYjBDU0p4QThUdGFmYlJzZGlHRnRrbHhoZkoyeW94UDZXZGZpL2VCc1JndG9SVzhUSDRVQXIrVllQYWdtUXc1V1BzRDBZKzlMMTBwYUViKzdJcnNocG04a2VWWDZDOWlWbzUzbFFJL3BTc2pRMTQ5RStxb2JMcmtYNWxEU0lzdVRSVEhKQ045c3JGOWdHMnY5Yk1mcjQyMzgrVURoZmQvbmhjYU9oc0lOTHJqRzBuK2d4S29mY21hQXhINDFrekdDYTNWUnFzbVpMWllWVVZpNWpYZ01hMDBuTEZKa2VDSkVpa2FYV0ZaWGhleFBJZVdialVEK0dUQ2x5S1dlN25RR3JNSm9NNGxnalE9PSIsIm1hYyI6ImRhYjA0ODlkNGY3OGExYTEzMmI1YmNmMjFiOTBlOTFkYjEyMjRmMzE0YzNiMzk5OWE2ZTUxOWY5NDA4ZmQzZTciLCJ0YWciOiIifQ==',NULL,NULL,NULL,'2021-09-08 18:48:45','2021-09-11 07:46:39',3),(23,'Receptionist','receptionist@gdc.com',NULL,'$2y$10$K.zxqLkYlcS1hjIlOd7Qqe75DWGuqgl7ZNBmHrvIhHnxmf2ofgO26',NULL,NULL,NULL,NULL,NULL,'2021-09-08 18:49:13','2021-09-08 18:49:13',4),(24,'admin test','atest@gmail.com',NULL,'atest@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(25,'Admin Test','ff@gmail.com',NULL,'$2y$10$wWgUcevWJKKO.KzmwnACg.6.oglaVqJWTSZD8UxdAvfVR23TdjWFm','eyJpdiI6Imt3R0NMSHFYY1FEUUdrdkdWcW0wRGc9PSIsInZhbHVlIjoiNUQ2ekhabHBocEkwOTRqZ0JJL2h1YWduWkVVdHJBK2lONkExOVRmYUkvcz0iLCJtYWMiOiJiZTgyY2MwZDFjNGUyZTA4MTEyYzBhMjBlMjI5OTBiNWM1MWUyZDFhNGQ1MDZhYjZhZWIwNDM4ZGNhYTUxM2JhIiwidGFnIjoiIn0=','eyJpdiI6IjhTM1Q0R2oveGp3NWpIa0R1dUo4aVE9PSIsInZhbHVlIjoiZXljZzIyYk00NjY4TUdaNmJvOEo1eDEyVE0wYWNYN0ttZDQrRHVzRms3cjhzajk4YS9va0dEZ3hSTHNyUE9kekxaOEZQS01SYlNzRGs5QkRLOHpsRTEzNlU4NUt3NUZWcDhNWkt6NDNvVkVpNzh6VS8xQy9nWWJxbkMrMDhpN3NWdGRPeklVa1VyYk1UanhtWW0rVlloUzlPVzV1dkNSd1dsYWs3ajVGZVFIdjB2alpoUmg4VGRlQWJkUkVoOEthc3crdXRNMW5JVDl3Sjd6b1ZxU04yQjRNRDJRSlFma1BUTTZLVXRSNExFYVlNU21aU0JWYkRqODNRSDFQRHpTTEtqbTI0WTd6NVdQYnlkQndnNUIwV3c9PSIsIm1hYyI6ImQyZGFmZjQ1NDZmNTEzYTg1N2I5ZjdmNTA3ZTlmMTgwNjE2NTFlN2Y2MDc2NjFmZjA0ZWRlOTg0ZWM0MjlhZDgiLCJ0YWciOiIifQ==',NULL,NULL,NULL,'2021-10-02 03:46:33','2021-10-02 03:48:08',1),(26,'Admin Test','admin456@gdc.com',NULL,'$2y$10$kXiCBckEkVswwxGj2r5pmOtjQGjTwvovCmwgoI4NPoSfouUavbQg.',NULL,NULL,NULL,NULL,NULL,'2021-10-02 03:53:09','2021-10-02 11:22:07',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-05  7:35:19
