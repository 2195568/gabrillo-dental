-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: l8jet
-- ------------------------------------------------------
-- Server version	8.0.25-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `patient_id` bigint unsigned NOT NULL,
  `procedures` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `appointments_patient_id_foreign` (`patient_id`),
  CONSTRAINT `appointments_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (31,'john mark','...','2021-09-11','00:41:00','01:41:00','2021-09-09 16:41:20','2021-09-09 16:41:20',3,''),(32,'Nigel Cacho','...','2021-09-12','14:26:00','15:26:00','2021-09-10 06:26:51','2021-09-10 06:26:51',12,''),(33,'Tests','dafasdfafd','0000-00-00','00:14:00','00:00:00','2021-10-04 16:14:50','2021-10-19 03:09:12',2,''),(34,'November 1','...','2021-11-01','08:30:00','09:30:00',NULL,NULL,12,''),(35,'November 1','...','2021-11-01','10:30:00','12:00:00',NULL,NULL,12,''),(36,'November 1','...','2021-11-01','13:00:00','14:00:00',NULL,NULL,12,''),(37,'October 31','...','2021-10-31','08:00:00','09:00:00',NULL,NULL,12,''),(38,'October 31','...','0000-00-00','10:00:00','11:00:00',NULL,'2021-10-19 03:34:01',12,'1'),(39,'','...','0000-00-00','13:00:00','14:00:00',NULL,'2021-10-19 03:32:06',12,'2'),(40,'','...','0000-00-00','16:00:00','17:00:00',NULL,'2021-10-19 03:30:04',12,'1'),(41,'','...','0000-00-00','13:00:00','09:00:00',NULL,'2021-10-19 03:31:47',12,''),(42,'October 31','...','2021-10-31','09:00:00','10:00:00',NULL,NULL,12,''),(44,'October 31','...','0000-00-00','09:00:00','10:00:00',NULL,'2021-10-19 03:35:34',12,'1'),(45,'November 2','...','2021-11-03','00:00:00','00:00:00',NULL,'2021-11-02 11:52:30',7,'2'),(46,'November 2','...','2021-11-02','09:00:00','00:00:00',NULL,NULL,7,''),(47,'November 2','...','2021-11-02','10:00:00','00:00:00',NULL,NULL,7,''),(49,'November 2','...','2021-11-02','12:00:00','00:00:00',NULL,NULL,7,''),(50,'November 2','...','2021-11-02','13:00:00','00:00:00',NULL,NULL,7,''),(51,'November 2','...','2021-11-02','14:00:00','00:00:00',NULL,NULL,7,''),(52,'November 2','...','2021-11-02','15:00:00','00:00:00',NULL,NULL,7,''),(53,'November 2','...','2021-11-07','13:00:00','14:00:00',NULL,'2021-11-07 07:46:34',80,''),(54,'November 2','...','2021-11-07','08:00:00','09:00:00',NULL,'2021-11-07 07:44:47',80,'5'),(55,'','kkk','0000-00-00','21:09:00','00:00:00','2021-10-18 13:11:41','2021-10-19 03:28:10',2,'1'),(56,'','asdfsadf','0000-00-00','21:12:00','00:00:00','2021-10-18 13:15:30','2021-10-19 03:26:12',2,'[\"1\"]'),(57,'','kkkk','0000-00-00','21:18:00','00:00:00','2021-10-18 13:20:54','2021-10-19 03:26:00',11,'[\"2\"]'),(58,'','kkk','0000-00-00','21:09:00','00:00:00',NULL,'2021-10-19 03:27:32',2,'1, 2, 5'),(59,'','dsfaasdsdf','0000-00-00','21:23:00','00:00:00','2021-10-18 13:24:02','2021-10-19 03:16:27',2,''),(60,'Test','afdsaf','0000-00-00','21:24:00','00:00:00','2021-10-18 13:24:42','2021-10-19 03:12:45',2,''),(61,'Test','afas','0000-00-00','21:25:00','00:00:00','2021-10-18 13:25:16','2021-10-19 03:11:06',2,''),(62,'Test','asdfasfa','0000-00-00','21:26:00','00:00:00','2021-10-18 13:26:20','2021-10-19 03:09:35',2,'1, 5, 62'),(63,'Sinatra','description','2021-11-04','08:00:00','09:00:00','2021-10-19 07:05:17','2021-11-11 05:31:03',13,'2'),(64,'Test','description','2021-11-04','08:00:00','00:00:00','2021-10-19 07:05:56','2021-10-19 07:05:56',7,'2'),(65,'Test','description','2021-11-04','08:00:00','00:00:00','2021-10-19 07:19:56','2021-10-19 07:19:56',7,' '),(69,'Test','description','2021-11-04','08:00:00','00:00:00','2021-10-19 08:05:14','2021-10-19 08:05:14',7,' '),(118,'Test','j;j;j;j;kj;','2021-10-22','08:00:00','09:00:00','2021-10-22 07:47:32','2021-10-22 07:47:32',2,'1 '),(119,'Test','j;kj;jlkjlk','2021-10-24','08:00:00','09:00:00','2021-10-22 07:48:25','2021-10-22 07:48:25',2,'1 '),(122,'John','Tooth extraction','2021-10-24','11:00:00','12:00:00','2021-10-23 12:28:36','2021-10-23 12:28:36',94,' '),(123,'Ney','Di ko alam','2021-11-01','10:00:00','11:00:00','2021-11-01 09:34:35','2021-11-01 09:34:35',102,'1 '),(124,'Sinatra','dfassdfassfdasdf','2021-11-02','08:00:00','09:00:00','2021-11-02 03:03:05','2021-11-02 03:03:05',13,'8 '),(128,'Doe','Jjas;ldkfj;alsjf','2021-11-02','08:00:00','09:00:00','2021-11-02 11:46:03','2021-11-02 11:46:03',79,'2 '),(129,'Sinatra','asdfasdfaf','2021-11-07','00:00:00','00:00:00','2021-11-07 03:47:42','2021-11-07 07:39:03',80,'1, 2'),(130,'Sinatra','asfdasdfsfad','2021-04-07','08:00:00','09:00:00','2021-11-07 04:18:19','2021-11-07 04:18:19',13,'1 '),(131,'Sinatra','afsadfasfdasf','2021-11-07','00:00:00','00:00:00','2021-11-07 06:58:27','2021-11-07 07:37:42',80,''),(132,'Sinatra','safasfasf','2021-11-07','00:00:00','00:00:00','2021-11-07 06:58:54','2021-11-07 07:37:24',13,''),(133,'Sinatra','test','2021-11-10','00:00:00','00:00:00','2021-11-10 07:42:07','2021-11-12 05:12:42',13,''),(134,'Testado','safasfasf','2021-11-12','13:00:00','14:00:00','2021-11-12 05:14:25','2021-11-12 05:14:46',80,'1'),(135,'Script','asdfsdf','2021-11-12','11:00:00','12:00:00','2021-11-12 05:16:55','2021-11-12 05:16:55',100,'1 '),(136,'Sinatra','asdfsdfadfasdf','2021-11-14','08:00:00','09:00:00','2021-11-14 08:40:06','2021-11-14 08:40:06',13,'2 '),(137,'Sinatra','adfsadfasdfsadf','2021-11-14','09:00:00','10:00:00','2021-11-14 08:43:01','2021-11-14 08:43:01',13,' '),(138,'Sinatra','asdfsadfasfasf','2021-11-14','10:00:00','11:00:00','2021-11-14 08:57:02','2021-11-14 08:57:02',13,' '),(139,'Sinatra','asdfasfafas','2021-11-14','11:00:00','12:00:00','2021-11-14 09:20:59','2021-11-14 09:20:59',13,' '),(140,'Sinatra','asdfasdf','2021-11-14','13:00:00','14:00:00','2021-11-14 09:23:47','2021-11-14 09:23:47',13,' '),(141,'Testado','asfasfasdf','2021-11-14','14:00:00','15:00:00','2021-11-14 09:24:36','2021-11-14 09:24:36',80,' '),(142,'Sinatra','asfsafd','2021-11-14','15:00:00','16:00:00','2021-11-14 09:25:44','2021-11-14 09:25:44',13,'4 '),(143,'Clear','JJJJJJJJJSLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW','2021-11-16','17:00:00','18:00:00','2021-11-16 08:48:02','2021-11-16 08:48:02',16,'1, 2, 3, 4 '),(144,'Sinatra','WWWWWWWWWWWWWWWWW','2021-11-16','08:00:00','09:00:00','2021-11-16 09:00:54','2021-11-16 09:00:54',13,'5 ');
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1),(6,'2020_05_21_100000_create_teams_table',1),(7,'2020_05_21_200000_create_team_user_table',1),(8,'2020_05_21_300000_create_team_invitations_table',1),(9,'2021_08_11_173951_create_sessions_table',1),(10,'2021_08_25_144524_create_profile_pictures_table',2),(11,'2021_08_25_144624_create_patients_table',2),(12,'2021_08_25_144721_create_appointments_table',2),(13,'2021_08_27_143040_drop_multiple_columns_from_patients',3),(14,'2021_08_27_152214_drop_profile_picture_id_from_patients',4),(15,'2021_08_27_152556_drop_profile_picture_id_from_patients',5),(16,'2021_08_27_152739_drop_profile_picture_table',5),(17,'2021_08_29_155806_add_new_appointment_and_last_appointment_column_to_patient_table',6),(18,'2021_09_05_075939_create_payments_table',7),(19,'2021_09_05_082408_create_procedure_categories_table',7),(20,'2021_09_05_082504_create_payment_logs_table',7),(21,'2021_09_05_083225_create_procedures_table',8),(22,'2021_09_05_091319_add_time_start_and_time_end_to__treatments_table',9),(23,'2021_09_05_100401_remove_payment_id_to_treatments_table',10),(24,'2021_09_05_100847_add_treatments_id_to_payments_table',11),(25,'2021_09_05_101227_add_balance_to_treatments_table',12),(26,'2021_09_05_142116_add_date_column_to_treatments_table',13),(27,'2021_09_06_141431_delete_balance_column_to_treatments_table',14),(28,'2021_09_06_141709_create_total_amount_column_to_treatments_table',15),(29,'2021_09_08_065914_add_balance_column_to_treatments_table',16),(30,'2021_09_08_084839_create_user_roles_table',17),(31,'2021_09_27_064422_drop_procedures_and_procedure_categories_table',18),(32,'2021_09_27_065440_create_procedure_categories_table',19),(33,'2021_09_27_065450_create_procedures_table',19),(34,'2021_09_27_070509_remove_procedure_id_to_treatments_table',20),(35,'2021_09_27_072402_create_procedure_treatment_table',21),(36,'2021_10_01_042559_create_appointment_requests_table',22),(37,'2021_10_02_065936_add_patient_type_column_to_appointment_requests_table',23),(38,'2021_10_02_070243_modify_contact_no_column_to_appointment_requests_table',24),(39,'2021_10_02_070245_modify_contact_no_column_to_appointment_requests_table',25),(40,'2021_10_02_070246_modify_contact_no_column_to_appointment_requests_table',26),(41,'2021_10_04_164433_create_payment_status_table',27),(42,'2021_10_05_041008_add_column_reference_id_to_treatments_table',28),(43,'2021_10_07_115115_drop_receive_and_payment_status_from_payments',29),(44,'2021_10_07_120859_add_treatment_reference_id_to_payments',30),(45,'2021_10_07_172935_drop_treatment_reference_column_id_to_payments',31),(46,'2021_10_07_173052_add_treatment_reference_id_again_to_payments',32),(47,'2021_10_07_183658_add_balance_to_payments',33),(48,'2021_10_12_095329_add_profile_bg_color_to_patients',34),(49,'2021_10_14_035832_add_slug_to_patients_table',35),(50,'2021_10_16_043821_drop_appointment_requests_table',36),(51,'2021_10_16_044913_create_old_patient_appointment_requests_table',37),(52,'2021_10_16_044936_create_new_patient_appointment_requests_table',37),(53,'2021_10_16_045939_add_column_slug_patients_table',38),(54,'2021_10_16_172422_add_email_verified_column_to_old_patient_appointment_requests_table',39),(55,'2021_10_16_175449_add_reference_id_to_old_patient_appointment_requests_table',40),(56,'2021_10_16_183631_modify_email_column_to_patients_table',41),(57,'2021_10_18_124508_add_procedures_to_appointments_table',42),(58,'2021_10_20_025724_add_reference_id_column_to_new_patient_appointment_requests_table',43),(59,'2021_10_20_031837_drop_email_address_from_patients_table',44),(60,'2021_10_20_083113_add_email_verified_id_to_new_patient_appointment_requests_table',45),(61,'2021_10_21_070042_add_profile_bg_color_to_new_patient_appointment_requests_table',46),(62,'2021_10_27_062334_add_timestamps_to_procedure_treatment_table',47);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `new_patient_appointment_requests`
--

DROP TABLE IF EXISTS `new_patient_appointment_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `new_patient_appointment_requests` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `appointment_status` tinyint(1) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int DEFAULT NULL,
  `gender` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medical_condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergies` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `reference_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified` tinyint(1) NOT NULL,
  `profile_bg_color` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `new_patient_appointment_requests`
--

LOCK TABLES `new_patient_appointment_requests` WRITE;
/*!40000 ALTER TABLE `new_patient_appointment_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `new_patient_appointment_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `old_patient_appointment_requests`
--

DROP TABLE IF EXISTS `old_patient_appointment_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `old_patient_appointment_requests` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `appointment_status` tinyint(1) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `patient_id` bigint unsigned NOT NULL,
  `patient_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified` tinyint(1) NOT NULL,
  `reference_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `old_patient_appointment_requests_patient_id_foreign` (`patient_id`),
  CONSTRAINT `old_patient_appointment_requests_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `old_patient_appointment_requests`
--

LOCK TABLES `old_patient_appointment_requests` WRITE;
/*!40000 ALTER TABLE `old_patient_appointment_requests` DISABLE KEYS */;
INSERT INTO `old_patient_appointment_requests` VALUES (2,0,'Mas','2021-10-17','09:00:00','08:00:00',NULL,NULL,8,'21018',0,'');
/*!40000 ALTER TABLE `old_patient_appointment_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('fnotestado@gmail.com','$2y$10$eY3O6dFMZH.5G/pC1GZoau7w8qU41OIp8iE3lfTKyRTgGZL3TEiHW','2021-08-22 04:15:43');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int DEFAULT NULL,
  `gender` char(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergies` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` double(8,2) NOT NULL DEFAULT '0.00',
  `new_appointment` date DEFAULT NULL,
  `last_appointment` date DEFAULT NULL,
  `profile_bg_color` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (1,NULL,'2021-11-02 08:27:09','Batttt','Testado','test','Cuenca','+639177515078',NULL,'2019-01-20',2,'male',NULL,'inactive',0.00,'2021-12-12','2021-09-05','','1','afdsdo@gmail.com'),(2,'2021-08-27 15:31:29','2021-11-01 08:59:07','Foo','Test','ORDOÑA','Cuenca, Batangas','0932245689','None','2021-08-27',21,NULL,'egg','inactive',8000.00,NULL,NULL,'','21272',''),(3,NULL,'2021-10-14 04:46:50','john','mark','luke','secret','09',NULL,'2000-06-14',20,NULL,NULL,'inactive',0.00,NULL,NULL,'','3',''),(4,'2021-08-31 07:42:47','2021-10-14 04:46:50','FranzNico','Testado','ORDOÑA','Cuenca','09177515078','c','2021-08-31',21,NULL,'egg','inactive',0.00,NULL,NULL,'','21314',''),(5,'2021-08-31 07:44:19','2021-10-14 04:46:50','Franz Nico0','Testado','ORDOÑA','Cuenca',NULL,'None','2021-08-31',21,NULL,'egg','inactive',0.00,NULL,NULL,'','21315',''),(7,'2021-09-01 03:13:58','2021-10-19 07:04:34','Test','Test','Test','Cuenca','09177515078','none','2021-09-01',21,NULL,'egg',NULL,0.00,'2021-11-04',NULL,'','21017',''),(8,'2021-09-01 03:16:20','2021-10-14 04:46:50','Test','Test','Test','Cuenca','09177515078','none','2021-09-01',21,NULL,'egg',NULL,0.00,NULL,NULL,'','21018',''),(9,'2021-09-01 03:20:02','2021-10-14 04:46:50','Gab','Dent','Test','Cuenca','09177515078','none','2021-09-01',21,NULL,NULL,'inactive',0.00,NULL,NULL,'','21019',''),(10,'2021-09-01 16:46:29','2021-10-14 04:46:50','FranzNico','Testado','ORDOÑA','Cuenca','09177515078','none','2021-09-02',21,NULL,'egg','inactive',0.00,NULL,NULL,'','210110',''),(11,'2021-09-10 06:04:26','2021-11-20 07:00:57','Juan','','Ordona','San Jacinto, Ilocos Sur','0922245689','none','2021-09-10',21,NULL,'Egg, peanuts','active',76000.00,NULL,NULL,'','211011',''),(12,'2021-09-10 06:21:25','2021-11-02 03:04:44','Nigel','Cacho','Ibay','Unino St., Iba, Zambales',NULL,'None','2021-09-10',21,NULL,'Egg','inactive',0.00,NULL,NULL,'','211012',''),(13,'2021-10-09 10:02:54','2021-11-17 04:41:01','Frank','Sinatra','World','Oradaga, Bacalod','0912345689','none','2021-03-09',30,NULL,NULL,'active',77000.00,NULL,NULL,'','210913',''),(14,'2021-10-10 17:47:28','2021-11-02 09:32:50','FranzNico','Testado','k','Cuenca','177515078',NULL,'2021-10-11',8,NULL,NULL,'inactive',0.00,NULL,NULL,'','211014',''),(15,'2021-10-12 09:53:29','2021-11-02 03:04:43','Tue','Oct','Naa','Poblacion East, Rosario','09123456781',NULL,'2021-10-12',21,NULL,NULL,'inactive',0.00,NULL,NULL,'','211215',''),(16,'2021-10-12 09:53:29','2021-11-17 04:41:01','James','Clear','Then','Poblacion East, Rosario','09123456789',NULL,'2021-10-12',70,NULL,NULL,'active',58000.00,NULL,NULL,'','211216',''),(17,'2021-10-12 17:11:10','2021-10-15 10:49:32','Rastaman','Rastaman','Rastaman','Concepcion, Cross','09123456789',NULL,'2021-10-13',21,NULL,NULL,'active',48000.00,NULL,NULL,'00ffff','211217',''),(18,'2021-10-12 17:46:46','2021-11-04 08:43:18','Faster','Faster','Faster','Cuenca, Cuenca','09177515078',NULL,'2021-10-13',21,NULL,NULL,'inactive',0.00,NULL,NULL,'ff0000','211218',''),(19,'2021-10-14 04:37:50','2021-10-14 04:46:50','James','James','James','Test, Concepcion','09187482844',NULL,'2021-10-14',20,NULL,NULL,'active',0.00,NULL,NULL,'ffff00','211419',''),(20,'2021-10-14 04:43:52','2021-11-04 08:41:34','first_name','last_name','middle_name','home_address','contact_number','occupation','birth_date',0,'gender','allergies','inactive',0.00,NULL,NULL,'profil','211420',''),(21,'2021-10-14 04:44:42','2021-11-04 08:43:17','first_name','last_name','middle_name','home_address','contact_number','occupation','birth_date',0,'gender','allergies','inactive',0.00,NULL,NULL,'profil','211421',''),(22,'2021-10-14 04:45:04','2021-10-14 04:45:04','first_name','last_name','middle_name','home_address','contact_number','occupation','birth_date',0,'gender','allergies','active',0.00,NULL,NULL,'profil','211422',''),(79,'2021-10-23 03:47:47','2021-11-04 08:37:44','John','Doe','Form','sfajasjflajsfd;lajsf','111111111111111','None','2021-10-23',55,NULL,'Egg, peanuts','active',0.00,NULL,NULL,'ffb6c1','','295568@slu.edu.ph'),(80,'2021-10-23 04:04:14','2021-11-17 04:39:21','FranzNico','Testado','Ordona','Cuenca','09177515078',NULL,'2021-10-23',21,NULL,NULL,'active',0.00,NULL,NULL,'000000','212380','fnotestado@gmail.com'),(81,'2021-10-23 04:05:05','2021-10-23 04:05:05','FranzNico','Testado','Ordona','Cuenca','09177515078',NULL,'2021-10-23',21,NULL,NULL,'active',0.00,NULL,NULL,'000000','212381','fnotestado@gmail.com'),(82,'2021-10-23 04:10:34','2021-10-23 04:10:35','FranzNico','Testado','Ford','Cuenca','09177515078',NULL,'2021-10-23',21,NULL,NULL,'active',0.00,NULL,NULL,'add8e6','212382','fnotestado@gmail.com'),(83,'2021-10-23 04:16:10','2021-11-20 07:02:42','FranzNico','Testado','Ordona','Cuenca','09177515078',NULL,'2021-10-23',21,NULL,NULL,'active',22000.00,NULL,NULL,'800080','212383','fnotestado@gmail.com'),(84,'2021-10-23 04:16:44','2021-10-23 04:16:44','FranzNico','Testado','Ordona','Cuenca','09177515078',NULL,'2021-10-23',21,NULL,NULL,'active',0.00,NULL,NULL,'d3d3d3','212384','fnotestado@gmail.com'),(85,'2021-10-23 04:17:34','2021-10-23 06:52:19','FranzNico','Testado','Ordona','Cuenca','09177515078',NULL,'2021-10-23',21,'male',NULL,'active',0.00,NULL,NULL,'f0e68c','212385','fnotestado@gmail.com'),(86,'2021-10-23 04:26:31','2021-10-23 06:51:38','FranzNico','Testado','Ordona','Cuenca',NULL,NULL,'2021-10-23',21,'male',NULL,'active',0.00,NULL,NULL,'ff00ff','212386','fnotestado@gmail.com'),(87,'2021-10-23 04:27:07','2021-10-23 06:51:11','FranzNico','Testado','Ordona','Cuenca',NULL,NULL,'2021-10-23',55,'male',NULL,'active',0.00,NULL,NULL,'ffffe0','212387','fnotestado@gmail.com'),(88,'2021-10-23 06:53:26','2021-10-23 06:53:27','FranzNico','Testado','ORDOÑA','Cuenca','09177515078',NULL,'2021-10-23',21,NULL,NULL,'active',0.00,NULL,NULL,'00ff00','212388','try@gmail'),(89,'2021-10-23 06:54:34','2021-10-23 06:54:34','FranzNico','Testado','ORDOÑA','Cuenca','09177515078',NULL,'2021-10-23',55,NULL,NULL,'active',0.00,NULL,NULL,'f0e68c','212389','adsfasdf@gmail.com'),(90,'2021-10-23 06:55:50','2021-10-23 06:55:50','FranzNico','Testado','Ordona','Cuenca','09177515078',NULL,'2021-10-23',55,'male',NULL,'active',0.00,NULL,NULL,'808000','212390','fnoo@gmail.com'),(91,'2021-10-23 06:57:22','2021-10-23 06:57:23','FranzNico','Testado','Ordona','Cuenca','09177515078',NULL,'2021-10-23',21,'male',NULL,'active',0.00,NULL,NULL,'800000','212391','fnotedo@gmail.com'),(92,'2021-10-23 06:59:38','2021-10-23 06:59:38','FranzNico','Testado','Ordona','Cuenca','09177515078',NULL,'2021-10-23',21,'male',NULL,'active',0.00,NULL,NULL,'ff00ff','212392','hlhlj@gmail.com'),(93,'2021-10-23 11:58:40','2021-10-23 11:58:40','John','Doe','Ford','Cuenca, San Jose','09177515078','None','2021-10-23',55,'male','Egg, peanuts','active',0.00,NULL,NULL,'ffc0cb','212393','2195568@slu.edu.ph'),(94,'2021-10-23 12:28:34','2021-10-25 02:52:15','Mark','John','Paul','Cuenca',NULL,'None','2021-10-23',55,'male',NULL,'active',0.00,NULL,NULL,'ffffe0','','nicon9708@gmail.com'),(95,'2021-10-26 09:27:27','2021-10-26 09:27:28','FranzNico','Testado','Ordona','Cuenca','09177515078',NULL,'2021-04-26',21,'male',NULL,'active',0.00,NULL,NULL,'00ff00','212695','asdfasfd@gmail.com'),(96,'2021-10-29 03:27:51','2021-10-29 03:27:51','FranzNico','Testado','Ordona','Cuenca',NULL,NULL,'2021-04-08',21,'male',NULL,'active',0.00,NULL,NULL,'ffffff','212996','afdsawf@gmail.com'),(97,'2021-10-29 10:26:27','2021-10-29 10:26:27','FranzNico','Testado','Ordona','Cuenca',NULL,NULL,'2000-06-04',21,'male',NULL,'active',0.00,NULL,NULL,'f5f5dc','212997','asafdstado@gmail.com'),(98,'2021-10-30 01:38:43','2021-10-30 01:38:43','FranzNico','Testado','Ordona','Cuenca',NULL,NULL,'2000-06-14',21,'male',NULL,'active',0.00,NULL,NULL,'ffffff','213098','fnot@gmail.com'),(99,'2021-11-01 01:52:04','2021-11-01 01:52:04','Test','Ng','Ff','Cuenca','+639+6390000000',NULL,'2000-01-01',21,'female',NULL,'active',0.00,NULL,NULL,'000080','210199','fno@gmail.com'),(100,'2021-11-01 01:54:47','2021-11-14 09:43:54','Java','Script','Php','Secret, Secret','+639+6392222222',NULL,'2020-01-01',1,'male',NULL,'active',0.00,NULL,NULL,'0000ff','2101100','fasdf@gdc.com'),(101,'2021-11-01 01:56:15','2021-11-01 01:56:15','Py','Thon','Ji','Jskensk, Kjskn',NULL,NULL,'2020-01-01',1,'male',NULL,'active',0.00,NULL,NULL,'00ff00','2101101','afsjkj@gdc.com'),(102,'2021-11-01 01:59:12','2021-11-02 01:55:04','Ney','Ney','Ney','Neeey','+639222222222','Ney','2020-01-02',1,'female','Ney','active',98000.00,NULL,NULL,'f0e68c','2101102','ney@gdc.com'),(103,'2021-11-02 11:39:39','2021-11-02 11:40:37','Jack','And','Jill','Cuenca','+639123333333',NULL,'2019-01-01',2,'male',NULL,'active',0.00,NULL,NULL,'c0c0c0','2102103','sadfado@gmail.com');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_logs`
--

DROP TABLE IF EXISTS `payment_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_logs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `column` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_value` double(8,2) NOT NULL,
  `new_value` double(8,2) NOT NULL,
  `payment_id` bigint unsigned NOT NULL,
  `changed_by` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_logs_payment_id_foreign` (`payment_id`),
  KEY `payment_logs_changed_by_foreign` (`changed_by`),
  CONSTRAINT `payment_logs_changed_by_foreign` FOREIGN KEY (`changed_by`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `payment_logs_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_logs`
--

LOCK TABLES `payment_logs` WRITE;
/*!40000 ALTER TABLE `payment_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_status`
--

DROP TABLE IF EXISTS `payment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_status` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_status`
--

LOCK TABLES `payment_status` WRITE;
/*!40000 ALTER TABLE `payment_status` DISABLE KEYS */;
INSERT INTO `payment_status` VALUES (1,'paid',NULL,NULL),(2,'pending',NULL,NULL);
/*!40000 ALTER TABLE `payment_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `treatment_id` bigint unsigned NOT NULL,
  `treatment_reference_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_treatment_id_foreign` (`treatment_id`),
  CONSTRAINT `payments_treatment_id_foreign` FOREIGN KEY (`treatment_id`) REFERENCES `treatments` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (24,'2021-10-08 21:59:53','2021-10-08 21:59:53',0.00,33,'T-bXnVnEYb',31000),(25,'2021-10-09 10:34:01','2021-10-09 10:34:01',0.00,34,'T-i07JEuIj',38000),(26,'2021-10-09 10:35:17','2021-10-09 10:35:17',100.00,33,'T-bXnVnEYb',30900),(27,'2021-10-09 15:05:22','2021-10-09 15:05:22',0.00,35,'T-l3kiV4Xo',20000),(28,'2021-10-09 15:10:37','2021-10-09 15:10:37',38000.00,34,'T-i07JEuIj',0),(29,'2021-10-09 15:10:50','2021-10-09 15:10:50',900.00,35,'T-l3kiV4Xo',19100),(30,'2021-10-09 15:32:40','2021-10-09 15:32:40',0.00,42,'T-dwW3lZ8G',40000),(31,'2021-10-09 15:34:58','2021-10-09 15:34:58',0.00,43,'T-8K3T1kwe',18000),(32,'2021-10-09 16:43:25','2021-10-09 16:43:25',0.00,44,'T-vaIS5Fu2',19000),(33,'2021-10-10 06:47:15','2021-10-10 06:47:15',100.00,42,'T-dwW3lZ8G',39900),(34,'2021-10-15 05:56:02','2021-10-15 05:56:02',0.00,45,'T-98738',11000),(35,'2021-10-15 06:11:36','2021-10-15 06:11:36',0.00,46,'T-4XrKG',8000),(36,'2021-10-15 10:40:09','2021-10-15 10:40:09',0.00,47,'T-F7Unp',20000),(37,'2021-10-15 10:49:32','2021-10-15 10:49:32',0.00,48,'T-WpHAm',28000),(39,'2021-10-22 10:35:16','2021-10-22 10:35:16',18000.00,43,'T-8K3T1kwe',0),(40,'2021-10-23 07:41:42','2021-10-23 07:41:42',19000.00,44,'T-vaIS5Fu2',0),(41,'2021-11-01 09:01:03','2021-11-01 09:01:03',0.00,50,'T-Uf2b6',41000),(42,'2021-11-01 09:05:47','2021-11-01 09:05:47',0.00,51,'T-lN4OB',46000),(43,'2021-11-01 09:06:39','2021-11-01 09:06:39',0.00,52,'T-kf274',8000),(44,'2021-11-01 09:10:30','2021-11-01 09:10:30',0.00,53,'T-2Fsni',3000),(45,'2021-11-01 09:15:49','2021-11-01 09:15:49',0.00,54,'T-4fJUK',2000),(46,'2021-11-02 02:30:35','2021-11-02 02:30:35',0.00,55,'T-bX6mN',20000),(47,'2021-11-02 02:42:06','2021-11-02 02:42:06',0.00,56,'T-AqXWc',8000),(48,'2021-11-02 02:56:15','2021-11-02 02:56:15',0.00,57,'T-oAyDn',20000),(49,'2021-11-17 04:37:28','2021-11-17 04:37:28',20000.00,47,'T-F7Unp',0),(50,'2021-11-17 04:37:55','2021-11-17 04:37:55',8000.00,46,'T-4XrKG',0),(51,'2021-11-17 04:52:13','2021-11-17 04:52:13',20000.00,57,'T-oAyDn',0),(52,'2021-11-17 04:52:20','2021-11-17 04:52:20',19100.00,35,'T-l3kiV4Xo',0),(53,'2021-11-20 06:56:24','2021-11-20 06:56:24',0.00,58,'T-Hl5gq',7500),(54,'2021-11-20 07:00:57','2021-11-20 07:00:57',0.00,59,'T-LUt72',7500),(55,'2021-11-20 07:02:42','2021-11-20 07:02:42',0.00,60,'T-XVhNh',22000);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedure_categories`
--

DROP TABLE IF EXISTS `procedure_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `procedure_categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedure_categories`
--

LOCK TABLES `procedure_categories` WRITE;
/*!40000 ALTER TABLE `procedure_categories` DISABLE KEYS */;
INSERT INTO `procedure_categories` VALUES (1,NULL,NULL,'Cosmetic restoration','...'),(2,NULL,NULL,'Jacket crowns','...'),(3,NULL,NULL,'Prosthetic procedures','Preprosthetic surgery is the term used for any procedure related to either hard/soft tissue correction or augmentation before prosthetic treatment.'),(4,NULL,NULL,'Surgical procedures','The act of performing surgery may be called a surgical procedure, operation, or simply \"surgery\". In this context, the verb \"operate\" means to perform surgery.'),(5,NULL,NULL,'Other resto. services','...');
/*!40000 ALTER TABLE `procedure_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedure_treatment`
--

DROP TABLE IF EXISTS `procedure_treatment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `procedure_treatment` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `procedure_id` bigint unsigned NOT NULL,
  `treatment_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedure_treatment`
--

LOCK TABLES `procedure_treatment` WRITE;
/*!40000 ALTER TABLE `procedure_treatment` DISABLE KEYS */;
INSERT INTO `procedure_treatment` VALUES (2,9,15,NULL,NULL),(3,1,16,NULL,NULL),(4,2,16,NULL,NULL),(5,3,16,NULL,NULL),(6,4,16,NULL,NULL),(7,1,17,NULL,NULL),(8,2,17,NULL,NULL),(9,8,17,NULL,NULL),(10,2,15,NULL,NULL),(11,3,15,NULL,NULL),(12,4,15,NULL,NULL),(14,9,18,NULL,NULL),(15,7,18,NULL,NULL),(16,8,18,NULL,NULL),(17,1,19,NULL,NULL),(18,2,19,NULL,NULL),(19,5,19,NULL,NULL),(20,8,19,NULL,NULL),(21,1,20,NULL,NULL),(22,2,20,NULL,NULL),(23,5,20,NULL,NULL),(24,6,20,NULL,NULL),(25,1,21,NULL,NULL),(26,2,21,NULL,NULL),(27,3,21,NULL,NULL),(28,4,21,NULL,NULL),(29,1,22,NULL,NULL),(30,2,22,NULL,NULL),(31,8,22,NULL,NULL),(32,3,23,NULL,NULL),(33,4,23,NULL,NULL),(34,8,23,NULL,NULL),(35,9,23,NULL,NULL),(36,8,24,NULL,NULL),(37,8,25,NULL,NULL),(38,9,25,NULL,NULL),(39,10,26,NULL,NULL),(40,5,27,NULL,NULL),(41,7,28,NULL,NULL),(42,8,28,NULL,NULL),(43,5,29,NULL,NULL),(44,8,30,NULL,NULL),(46,8,32,NULL,NULL),(47,9,32,NULL,NULL),(48,8,33,NULL,NULL),(49,9,33,NULL,NULL),(50,10,33,NULL,NULL),(51,10,28,NULL,NULL),(52,7,34,NULL,NULL),(53,8,34,NULL,NULL),(54,8,35,NULL,NULL),(55,8,36,NULL,NULL),(56,8,37,NULL,NULL),(57,8,38,NULL,NULL),(58,8,39,NULL,NULL),(59,8,40,NULL,NULL),(60,8,41,NULL,NULL),(61,8,42,NULL,NULL),(62,7,43,NULL,NULL),(63,2,44,NULL,NULL),(64,3,44,NULL,NULL),(65,4,44,NULL,NULL),(66,10,45,NULL,NULL),(67,11,45,NULL,NULL),(68,12,45,NULL,NULL),(69,5,46,NULL,NULL),(70,6,47,NULL,NULL),(71,5,48,NULL,NULL),(72,6,48,NULL,NULL),(73,6,49,NULL,NULL),(74,7,49,NULL,NULL),(75,8,49,NULL,NULL),(76,4,50,NULL,NULL),(77,8,50,NULL,NULL),(78,9,50,NULL,NULL),(79,5,51,NULL,NULL),(80,6,51,NULL,NULL),(81,7,51,NULL,NULL),(82,5,52,NULL,NULL),(83,1,53,NULL,NULL),(84,2,54,NULL,NULL),(85,6,55,NULL,NULL),(86,5,56,NULL,NULL),(87,8,57,NULL,NULL),(88,1,58,NULL,NULL),(89,2,58,NULL,NULL),(90,10,58,NULL,NULL),(91,1,59,NULL,NULL),(92,2,59,NULL,NULL),(93,10,59,NULL,NULL),(94,1,60,NULL,NULL),(95,2,60,NULL,NULL),(96,3,60,NULL,NULL),(97,4,60,NULL,NULL),(98,9,31,NULL,NULL);
/*!40000 ALTER TABLE `procedure_treatment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedures`
--

DROP TABLE IF EXISTS `procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `procedures` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_charged` double(8,2) NOT NULL DEFAULT '0.00',
  `procedure_category_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `procedures_procedure_category_id_foreign` (`procedure_category_id`),
  CONSTRAINT `procedures_procedure_category_id_foreign` FOREIGN KEY (`procedure_category_id`) REFERENCES `procedures` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedures`
--

LOCK TABLES `procedures` WRITE;
/*!40000 ALTER TABLE `procedures` DISABLE KEYS */;
INSERT INTO `procedures` VALUES (1,NULL,NULL,'Direct Composite Veneer','...',3000.00,1),(2,NULL,NULL,'Direct Composite Class IV','...',2000.00,1),(3,NULL,NULL,'Diastema Closure (Bonding)','...',2000.00,1),(4,NULL,NULL,'Ceramic/Porcelain Veneer','...',15000.00,1),(5,NULL,NULL,'Porcelain fused to Non-precious metal','...',8000.00,2),(6,NULL,NULL,'Porcelain fused to Gold','...',20000.00,2),(7,NULL,NULL,'All Ceramic Crown (Emax)','...',18000.00,2),(8,NULL,NULL,'Zirconia','...',20000.00,2),(9,NULL,NULL,'Composite Crown','...',6000.00,2),(10,NULL,NULL,'Plastic Crown','...',2500.00,2),(11,NULL,NULL,'Full Metal Crown','...',5000.00,2),(12,NULL,NULL,'Full Metal Crown for Pediatric Patients','...',3500.00,2);
/*!40000 ALTER TABLE `procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('MWI9tWUBaEmEZP9aB9goT9ny1r3taXJBYY7Kgdih',26,'192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36','YTo2OntzOjY6Il90b2tlbiI7czo0MDoiM0dvMWJjTW1nVUp1QlZhdU91Tm9VeFZCbng4NDU5Yld2S1NQZTY4SiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzc6Imh0dHA6Ly93d3cubWVyZ2VicmFuY2guY29tL3RyZWF0bWVudHMiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToyNjtzOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJGtYaUNCY2tFa1Zzd3d4R2oycjVwbU90alFHalR3dm92Q213Z29JNE5Qb1Nmb3VVYXZiUWcuIjtzOjIxOiJwYXNzd29yZF9oYXNoX3NhbmN0dW0iO3M6NjA6IiQyeSQxMCRrWGlDQmNrRWtWc3d3eEdqMnI1cG1PdGpRR2pUd3ZvdkNtd2dvSTROUG9TZm91VWF2YlFnLiI7fQ==',1637395914);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_invitations`
--

DROP TABLE IF EXISTS `team_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `team_invitations` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_invitations_team_id_email_unique` (`team_id`,`email`),
  CONSTRAINT `team_invitations_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_invitations`
--

LOCK TABLES `team_invitations` WRITE;
/*!40000 ALTER TABLE `team_invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_user`
--

DROP TABLE IF EXISTS `team_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `team_user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_user_team_id_user_id_unique` (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_user`
--

LOCK TABLES `team_user` WRITE;
/*!40000 ALTER TABLE `team_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teams` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_team` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teams_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,1,'FranzNico\'s Team',1,'2021-08-11 17:50:30','2021-08-11 17:50:30'),(2,2,'Test\'s Team',1,'2021-08-16 06:39:49','2021-08-16 06:39:49'),(3,3,'FranzNico\'s Team',1,'2021-08-22 04:12:23','2021-08-22 04:12:23'),(4,25,'Admin\'s Team',1,'2021-10-02 03:46:33','2021-10-02 03:46:33'),(5,26,'Admin\'s Team',1,'2021-10-02 03:53:09','2021-10-02 03:53:09');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `treatments`
--

DROP TABLE IF EXISTS `treatments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `treatments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tooth_no` int DEFAULT NULL,
  `patient_id` bigint unsigned NOT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `date` date DEFAULT NULL,
  `total_amount` double(8,2) NOT NULL,
  `balance` double(8,2) NOT NULL,
  `payment_status_id` bigint unsigned NOT NULL,
  `reference_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `treatments`
--

LOCK TABLES `treatments` WRITE;
/*!40000 ALTER TABLE `treatments` DISABLE KEYS */;
INSERT INTO `treatments` VALUES (33,'2021-10-08 21:59:53','2021-11-20 08:11:17',24,2,NULL,NULL,'2021-10-09',31000.00,30900.00,2,'T-bXnVnEYb'),(34,'2021-10-09 10:34:00','2021-10-09 15:10:36',34,2,NULL,NULL,'2021-10-09',38000.00,0.00,1,'T-i07JEuIj'),(35,'2021-10-09 15:05:22','2021-11-17 04:52:20',21,2,NULL,NULL,'2021-10-09',20000.00,0.00,1,'T-l3kiV4Xo'),(36,'2021-10-09 15:25:42','2021-10-09 15:25:42',33,13,NULL,NULL,'2021-10-09',20000.00,20000.00,2,'T-d3z4q0eP'),(37,'2021-10-09 15:25:58','2021-10-09 15:25:58',33,13,NULL,NULL,'2021-10-09',20000.00,20000.00,2,'T-l073W0Sf'),(38,'2021-10-09 15:27:45','2021-10-09 15:27:45',33,13,NULL,NULL,'2021-10-09',20000.00,20000.00,2,'T-MaGUlDJx'),(39,'2021-10-09 15:28:37','2021-10-09 15:28:37',33,13,NULL,NULL,'2021-10-09',20000.00,20000.00,2,'T-wdBHHPnq'),(40,'2021-10-09 15:28:51','2021-10-09 15:28:51',33,13,NULL,NULL,'2021-10-09',20000.00,20000.00,2,'T-KarFA7Ob'),(41,'2021-10-09 15:31:26','2021-10-09 15:31:26',16,13,NULL,NULL,'2021-10-09',40000.00,40000.00,2,'T-dfDdpqbX'),(42,'2021-10-09 15:32:40','2021-10-10 06:47:15',16,13,NULL,NULL,'2021-10-09',40000.00,39900.00,2,'T-dwW3lZ8G'),(43,'2021-10-09 15:34:58','2021-10-22 10:35:15',42,13,NULL,NULL,'2021-10-09',18000.00,0.00,1,'T-8K3T1kwe'),(44,'2021-10-09 16:43:24','2021-10-23 07:41:41',42,13,NULL,NULL,'2021-10-10',19000.00,0.00,1,'T-vaIS5Fu2'),(45,'2021-10-15 05:56:02','2021-10-15 05:56:02',36,11,NULL,NULL,'2021-10-15',11000.00,11000.00,2,'T-98738'),(46,'2021-10-15 06:11:36','2021-11-17 04:37:55',23,2,NULL,NULL,'2021-10-15',8000.00,0.00,1,'T-4XrKG'),(47,'2021-10-15 10:40:09','2021-11-17 04:37:27',NULL,17,NULL,NULL,'2021-10-15',20000.00,0.00,1,'T-F7Unp'),(48,'2021-10-15 10:49:32','2021-10-15 10:49:32',21,17,NULL,NULL,'2021-10-15',28000.00,28000.00,2,'T-WpHAm'),(49,'2021-10-15 11:29:11','2021-11-20 07:56:07',21,16,NULL,NULL,'2021-10-15',58000.00,50000.00,2,'T-8rfOr'),(50,'2021-11-01 09:01:03','2021-11-01 09:01:03',12,102,NULL,NULL,'2021-11-01',41000.00,41000.00,2,'T-Uf2b6'),(51,'2021-11-01 09:05:47','2021-11-01 09:05:47',NULL,102,NULL,NULL,'2021-11-01',46000.00,46000.00,2,'T-lN4OB'),(52,'2021-11-01 09:06:39','2021-11-01 09:06:39',NULL,102,NULL,NULL,'2021-11-01',8000.00,8000.00,2,'T-kf274'),(53,'2021-11-01 09:10:30','2021-11-01 09:10:30',28,102,NULL,NULL,'2021-11-01',3000.00,3000.00,2,'T-2Fsni'),(54,'2021-11-01 09:15:49','2021-11-01 09:15:49',NULL,11,NULL,NULL,NULL,2000.00,2000.00,2,'T-4fJUK'),(55,'2021-11-02 02:30:35','2021-11-02 02:30:35',NULL,11,NULL,NULL,'2021-11-17',20000.00,20000.00,2,'T-bX6mN'),(56,'2021-11-02 02:42:06','2021-11-02 02:42:06',22,11,NULL,NULL,'2021-11-02',8000.00,8000.00,2,'T-AqXWc'),(57,'2021-11-02 02:56:15','2021-11-17 04:52:12',12,11,NULL,NULL,'2021-11-02',20000.00,0.00,1,'T-oAyDn'),(58,'2021-11-20 06:56:22','2021-11-20 06:56:22',17,11,NULL,NULL,'2021-11-20',7500.00,7500.00,2,'T-Hl5gq'),(59,'2021-11-20 07:00:57','2021-11-20 07:00:57',17,11,NULL,NULL,'2021-11-20',7500.00,7500.00,2,'T-LUt72'),(60,'2021-11-20 07:02:42','2021-11-20 07:02:42',46,83,NULL,NULL,'2021-11-20',22000.00,22000.00,2,'T-XVhNh');
/*!40000 ALTER TABLE `treatments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,NULL,NULL,'admin'),(2,NULL,NULL,'admin_dentist'),(3,NULL,NULL,'dentist'),(4,NULL,NULL,'receptionist');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint unsigned DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'FranzNico Testado','fnotestado@gmail.com',NULL,'$2y$10$H.AQt7gKwxbFh7m/M.pp7efRDnPbaIF4qbMbiA2t4EojkASvX15sa',NULL,NULL,NULL,1,NULL,'2021-08-11 17:50:30','2021-09-06 05:00:24',0),(2,'Test','test@gmail.com',NULL,'$2y$10$7P7ST4lSuXGbsXDlI.aasu4.Rakurv2QFlAdYeAAHjiKrGNQMDEom',NULL,NULL,NULL,NULL,NULL,'2021-08-16 06:39:49','2021-08-16 06:46:27',0),(3,'FranzNico Testado','fnotestado1@gmail.com',NULL,'$2y$10$Pp17Ob2LFTlMWj5HyR9DMuzW2bhTgb1F6ls26xpF.g9AMTz.6Sxy.',NULL,NULL,NULL,NULL,'profile-photos/gnvv9sdDWIqU03zdqhO8HTHHkadGiCyTMR9DJuKx.jpg','2021-08-22 04:12:23','2021-08-22 07:45:11',0),(4,'Naji Gabrillo','ngabrillo@gabrillodental.com',NULL,'$2y$10$0dRMbOu6Y6YaoukZ1cgFG.dGuofS0tiPBnmeC9ih/fQQVsrl0CRYu',NULL,NULL,NULL,NULL,NULL,NULL,'2021-09-10 06:31:44',2),(23,'Receptionist','receptionist@gdc.com',NULL,'$2y$10$K.zxqLkYlcS1hjIlOd7Qqe75DWGuqgl7ZNBmHrvIhHnxmf2ofgO26',NULL,NULL,NULL,NULL,NULL,'2021-09-08 18:49:13','2021-09-08 18:49:13',4),(26,'Admin','admin456@gdc.com',NULL,'$2y$10$kXiCBckEkVswwxGj2r5pmOtjQGjTwvovCmwgoI4NPoSfouUavbQg.',NULL,NULL,NULL,NULL,NULL,'2021-10-02 03:53:09','2021-10-23 07:23:02',1),(27,'Dentist','dentista@gdc.com',NULL,'$2y$10$73.eua4niH6wDNo4dpgqXORDgU43jrzO9qiJj6cAn6nTlLjD8/bni',NULL,NULL,NULL,NULL,NULL,'2021-10-23 07:24:10','2021-10-28 03:25:39',3),(29,'FranzNico Testa','fn@gmail.com',NULL,'$2y$10$VlDyYaMBN1Em2QsZS6mAnOm8yUBynNUtn6auZMAT2V7RCzv9cVpPy',NULL,NULL,NULL,NULL,NULL,'2021-10-28 02:50:49','2021-10-28 02:50:49',1),(33,'FranzNico Testa','fnodo@gmail.com',NULL,'$2y$10$mPb3bn1ljx2TVuMPQK2ao.JY30Rr.UOHJFHyW5.zxrnySYrQirpiK',NULL,NULL,NULL,NULL,NULL,'2021-10-28 04:38:24','2021-10-28 04:38:24',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'l8jet'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-20 14:55:22
