
# Gabrillo Dental

Gabrillo Dental is a web-based electronic dental record and online appointment system, which addresses most dental clinic problems, including the storage problem and security of patient data, chaotic appointment scheduling and revenue computation.



## Features

- Online Appointment
- Electronic Dental Records (EDR)
- Tooth Number Chart
- Payments
- Report Generation


## Installation

*  See installation process: `docs/dev-manual/GabrilloDentalInstallation.pdf` 
    