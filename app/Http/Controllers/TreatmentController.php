<?php

namespace App\Http\Controllers;
use App\Models\Treatment;
use App\Models\Procedures;
use App\Models\Payment;
use App\Models\Patient;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\TreatmentFormRequest;

use Illuminate\Http\Request;

class TreatmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $treatments = DB::table('treatments')
                        ->select(array('tooth_no', 'patient_id', 'date', 'total_amount', 'treatments.id as treatment_id', 'patients.first_name', 'patients.last_name'))
                        ->join('patients', 'treatments.patient_id', '=', 'patients.id')
                        ->get();

                        
        
        // return json_encode($treatments);
        $treatments = Treatment::with('patient')->get();
        $result = array();

        foreach ($treatments as $treatment) {
            
            $treatmentArray = array($treatment);
            $procedures = $treatment->procedures;
            array_push($treatmentArray, $procedures);
            
            array_push($result, $treatmentArray);
            
        }
        
        return json_encode($result);
    }



    public function getData()
    {
        $treatments = Treatment::with('patient')->get();
        $result = array();

        foreach ($treatments as $treatment) {
            
            $treatmentArray = array($treatment);
            $procedures = $treatment->procedures;
            array_push($treatmentArray, $procedures);
            
            array_push($result, $treatmentArray);
            
        }
        
        return json_encode($result);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    function gen_uuid($len=5) {

        $hex = md5("9@brIll0" . uniqid("", true));

        $pack = pack('H*', $hex);
        $tmp =  base64_encode($pack);

        $uid = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);

        $len = max(4, min(128, $len));

        while (strlen($uid) < $len)
            $uid .= $this->gen_uuid(22);

        return substr($uid, 0, $len);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TreatmentFormRequest $request)
    {
        // Retrieve the validated input data
      
        $validated = $request->validated();
        $pending = '2';
        $procedures = $request->get('procedures');

        $treatment = new Treatment(array(
            'reference_id' => 'T-' . $this->gen_uuid(),
            'payment_status_id' => $pending,
            'tooth_no' => $request->get('tooth_no'),
            'procedure_id' => $request->get('procedure_id'),
            'patient_id' => $request->get('patient_id'),
            'time_start' => $request->get('time_start'),
            'time_end' => $request->get('time_end'),
            'total_amount' => $request->get('total_amount'),
            'balance' => $request->get('total_amount'),
            'date' => $request->get('date')
        ));
        
        $treatment->save();

        $treatment->procedures()->attach($procedures);

        // Update the balance of patient
        $patient = Patient::find($treatment->patient_id);
        $patient->balance = $patient->balance + 0 + $treatment->total_amount;
        $patient->save();

        // Create new empty payment for reference only
        $payment = new Payment(array(
            'treatment_id' => $treatment->id,
            'treatment_reference_id' => $treatment->reference_id,
            'amount' => '0',
            'balance' => $treatment->balance,
        ));

        $payment->save();
         return redirect($request->server('HTTP_REFERER'))->with('status', 'New treatment has been saved!');
    }

    

 
 //changes dito
    public function saveMulti($tooth, $procid, $cost,  $date, $patientid)
    {   
        echo $tooth;
        echo "<br/>";
        echo $procid;
        echo "<br/>";
        echo $cost;
        echo "<br/>";
        echo $patientid;
        echo "<br/>";
        echo "oy isave mo na ako";

        $referenceID = 'T-' . $this->gen_uuid();
        // Retrieve the validated input data
        // $validated = $request->validated();
        $pending = '2';
       

            $procedures =  $procid;
            $treatment = new Treatment(array(
                'reference_id' => $referenceID,
                'payment_status_id' => $pending,
                'tooth_no' =>  $tooth,
                'procedure_id' =>  $procid,
                'patient_id' =>$patientid,
                'balance' => $procid,
                'total_amount' => $cost,

                'date' => $date,
             
            
            ));
            
            $treatment->save();
    
            $treatment->procedures()->attach($procedures);
    
            // Update the balance of patient
            $patient = Patient::find($treatment->patient_id);
            $patient->balance = $patient->balance + 0 + $treatment->total_amount;
            $patient->save();
    
            // Create new empty payment for reference only
            $payment = new Payment(array(
                'treatment_id' => $treatment->id,
                'treatment_reference_id' => $treatment->reference_id,
                'amount' => '0',
                'balance' => $treatment->balance,
            ));
    
            $payment->save();
        
            $treamentID = DB::table('treatments')
            ->select(DB::raw('id'))
            ->whereYear('reference_id', $referenceID)
            ->get();
           
            DB::table('procedure_treatment')->insert([
                'procedure_id' => $procid,
                'treatment_id' => $treamentID,
            ]);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $treatment = DB::table('treatments')
                        ->where('treatments.id', $id)
                        ->select(array('tooth_no', 'patients.id as patient_id', 'date', 'total_amount', 'treatments.id as treatment_id', 'treatments.reference_id as treatment_reference_id', 'patients.first_name', 'patients.last_name', 'patients.email'))
                        ->join('patients', 'treatments.patient_id', '=', 'patients.id')
                        ->get();

        return json_encode($treatment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TreatmentFormRequest $request)
    {
        $treatment = Treatment::find($request->get('treatment_id'));

        $procedures = $request->get('procedures');

        $treatment->tooth_no = $request->get('tooth_no');
        $treatment->patient_id = $request->get('patient_id');
        $treatment->date = $request->get('date');
        $treatment->total_amount = $request->get('total_amount');

        $treatment->save();
        $treatment->procedures()->sync($procedures);

        return redirect('/treatments')->with('status', 'Changes have been saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // Delete first payments
        $payments = Payment::where('treatment_reference_id', $request->get('treatment_reference_id'))->delete();
        
        $treatment = Treatment::find($request->get('id'));
        $patientName = $request->get('patient_name');

        $treatment->delete();
        
        return redirect('/treatments')->with('status', 'Treatment has been deleted!');
    }

     /**
     * Search treatment
     */
    public function search(Request $request) 
    {
        $search = $request->get('query');

        // $treatments = Treatment::with('patient')
                        // ->where('reference_id', 'LIKE', $search . '%')
                        // ->orWhere('patients.first_name', 'LIKE', $search . '%')
                        // ->orWhere('patients.last_name', 'LIKE', $search . '%')
                        // ->orWhere('date', 'LIKE', $search . '%')
                        // ->orWhere('tooth_no', 'LIKE', $search . '%')
        //                 ->select(array('treatments.id as treatment_id', 'patients.first_name', 'patients.last_name', 'reference_id', 'tooth_no', 'date', 'total_amount', 'payment_status_id'))
        //                 ->join('patients', 'treatments.patient_id', '=', 'patients.id')
        //                 ->get();
        // $result = array();

        // foreach ($treatments as $treatment) {
        //     $id = $treatment->id;


        //     $procedures = DB::table('procedure_treatment')
        //                 ->where('treatment_id', $id)
        //                 ->join('procedures', 'procedures.id', '=', 'procedure_id')
        //                 ->get();

        //     $procedureCategory = array(
        //         'procedures' => $procedures
        //     );
            
        //     $treatmentArray = array($treatment);
           
        //     array_push($treatmentArray, $procedureCategory);
            
        //     array_push($result, $treatmentArray);
        // }

        $treatments = Treatment::with('patient' , 'procedures')
                                ->join('patients', 'treatments.patient_id', '=', 'patients.id')
                                ->where('reference_id', 'LIKE', $search . '%')
                                ->orWhere('first_name', 'LIKE', $search . '%')
                                ->orWhere('last_name', 'LIKE', $search . '%')
                                ->orWhere('date', 'LIKE', $search . '%')
                                ->orWhere('tooth_no', 'LIKE', $search . '%')
                                // ->select(array('treatments.id as treatment_id', 'patients.first_name', 'patients.last_name', 'reference_id', 'tooth_no', 'date', 'total_amount', 'payment_status_id'))
                                ->get();
        
        return json_encode($treatments);
    }

     /**
     * Get procedures by treatment id.
     */
    public function getProceduresByTreatmentId($id) 
    {
        $procedures = Treatment::with('procedures')
                        ->where('id', $id)
                        ->get();

        return json_encode($procedures);
    }

    /**
     * Get treatments by patient id.
     */
    public function getTreatmentsByPatientId($patient_id, $tooth_no) 
    {
        $treatments = '';
        
        if ($tooth_no == 'all') {
            $treatments = Treatment::with('procedures', 'patient')->where('patient_id', $patient_id)->get();
        } else {
            $treatments = Treatment::with('procedures', 'patient')->where([['patient_id', $patient_id], ['tooth_no', $tooth_no]])->get();
        }
        return json_encode($treatments);
    }


    public function showSelectedProcedures($id){
        $data = DB::table('procedures')
        ->select( 'name','amount_charged', 'id')
        ->where('id',[$id])
        ->get();
        return json_encode($data);
    }

    public function saveProcedure(Request $req){

        $procedures = new Procedure;
        $procedures ->id=$request->id;
        $procedures ->name=$request->name;
        $procedures ->amount_charged=$request->amount_charged;
        $procedures ->save();
        return redirect('/treatments')->with('status', 'New treatment has been saved!');
    }
}
