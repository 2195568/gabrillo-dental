<?php

namespace App\Http\Controllers;


use App\Models\Appointment;
use App\Models\Patient;
use Illuminate\Http\Request;
use App\Http\Requests\AppointmentFormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
// use App\Http\Controllers\Config;

class AppointmentController extends Controller
{
    /**
     * Displays the calendar of the appointment feature.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('appointment.calendar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppointmentFormRequest $request)
    {
        // Retrieve the validated input data
        $validated = $request->validated();

        // Patient's id
        $id = $request->get('id');

        $patient = Patient::find($id);
        $new_appointment = $request->get('date');

        $pastAppointments = array();

        // If appointment is behind today's date
        if (date('Y-m-d') > $new_appointment) {
            $appointments = $patient->appointments()->get();

            for ($i = 0; $i < sizeof($appointments); $i++) {
                if (date('Y-m-d') > $appointments[$i]->date) {
                    $pastAppointments[] = $appointments[$i]->date;
                }
            }

            usort($pastAppointments, function($a, $b) {
                if ($a == $b) {
                    return 0;
                }

                return $a < $b ? -1 : 1;
            });

            if (sizeof($pastAppointments) == 0) {
                $patient->last_appointment = NULL;
            } else {
                $patient->last_appointment = end($pastAppointments);
            }

            $patient->save();
            
            $title = $patient->last_name;
            $date = $request->get('date');
            $time_start = $request->get('time_start');
            $time_end = $request->get('time_end');
            $description = $request->get('description');
            $procedures = $request->get('procedures') == '' ? $request->get('procedures') : implode(', ',$request->get('procedures'));
            $patient_id = $id;

            $this->saveAppointment($title, $date, $time_start, $time_end, $description, $patient_id, $procedures);
            return redirect('/appointments')->with('status', 'New appointment has been saved!');
        }


        if (is_null($patient->new_appointment)) {
            $patient->new_appointment = $new_appointment;
            $patient->save();
        } else {
            // If patient's upcoming new appointment is behind today's day
            // Set previous appointment as an old appointment
            if (date('Y-m-d') > $patient->new_appointment) {
                $patient->last_appointment = $patient->new_appointment;
                $patient->new_appointment = $new_appointment;
                $patient->save();
            } else {
                $oldAppointment = strtotime($patient->new_appointment);
                $newAppointment = strtotime($new_appointment);
                $dateToday = strtotime(date('Y-m-d'));

                $daysBetweenOldAppointmentAndToday = ceil(($oldAppointment - $dateToday) / 86400);
                $daysBetweenNewAppointmentAndToday = ceil(($newAppointment - $dateToday) / 86400);

                if($daysBetweenNewAppointmentAndToday < $daysBetweenOldAppointmentAndToday) {
                    $patient->new_appointment = $new_appointment;
                    $patient->save();
                }

            }
        }

        $title = $patient->last_name;
        $date = $request->get('date');
        $time_start = $request->get('time_start');
        $time_end = $request->get('time_end');
        $description = $request->get('description');
        $procedures = $request->get('procedures') == '' ? $request->get('procedures') : implode(', ',$request->get('procedures'));

        // print_r(gettype($procedures));
        // return;

        $this->saveAppointment($title, $date, $time_start, $time_end, $description, $id, $procedures);
        
        return redirect('/appointments')->with('status', 'New appointment has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // TODO: This method will be used for search operation for the appointment feature. 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $appointment = Appointment::find($request->get('id'));
        $patient = Patient::find($request->get('patient_id'));

        $appointment->date = $request->get('date');
        $appointment->title = $patient->last_name;
        $appointment->patient_id = $request->get('patient_id');
        $appointment->time_start = $request->get('time_start');
        $appointment->time_end = $request->get('time_end');
        $appointment->description = $request->get('description');
        $appointment->procedures = $request->get('procedures') == '' ? $request->get('procedures') : implode(', ',$request->get('procedures'));

        $appointment->save();

        return redirect('/appointments')->with('status', 'Changes have been saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $appointment = Appointment::find($request->get('id'));

        $appointment->delete();
        
        return redirect('/appointments')->with('status', 'Appointment has been deleted!');
    }

    /**
     * Returns all the events from storage.
     */
    public function events()
    {
        $events = Appointment::all();

        return json_encode($events);
    }

    /**
     * 
     */
    public function requestApprovalAccept(AppointmentFormRequest $request)
    {
        // Retrieve the validated input data
        $validated = $request->validated();

        // Patient's id
        $id = $request->get('id');

        $patient = Patient::find($id);
        $new_appointment = $request->get('date');

        $pastAppointments = array();

        // If appointment is behind today's date
        if (date('Y-m-d') > $new_appointment) {
            $appointments = $patient->appointments()->get();

            for ($i = 0; $i < sizeof($appointments); $i++) {
                if (date('Y-m-d') > $appointments[$i]->date) {
                    $pastAppointments[] = $appointments[$i]->date;
                }
            }

            usort($pastAppointments, function($a, $b) {
                if ($a == $b) {
                    return 0;
                }

                return $a < $b ? -1 : 1;
            });

            if (sizeof($pastAppointments) == 0) {
                $patient->last_appointment = NULL;
            } else {
                $patient->last_appointment = end($pastAppointments);
            }

            $patient->save();
            
            $title = $patient->last_name;
            $date = $request->get('date');
            $time_start = $request->get('time_start');
            $time_end = $request->get('time_end');
            $description = $request->get('description');
            $procedures = $request->get('procedures') == '' ? $request->get('procedures') : implode(', ',$request->get('procedures'));
            $patient_id = $id;

            $this->saveAppointment($title, $date, $time_start, $time_end, $description, $patient_id, $procedures);
        }

        if (is_null($patient->new_appointment)) {
            $patient->new_appointment = $new_appointment;
            $patient->save();
        } else {
            // If patient's upcoming new appointment is behind today's day
            // Set previous appointment as an old appointment
            if (date('Y-m-d') > $patient->new_appointment) {
                $patient->last_appointment = $patient->new_appointment;
                $patient->new_appointment = $new_appointment;
                $patient->save();
            } else {
                $oldAppointment = strtotime($patient->new_appointment);
                $newAppointment = strtotime($new_appointment);
                $dateToday = strtotime(date('Y-m-d'));

                $daysBetweenOldAppointmentAndToday = ceil(($oldAppointment - $dateToday) / 86400);
                $daysBetweenNewAppointmentAndToday = ceil(($newAppointment - $dateToday) / 86400);

                if($daysBetweenNewAppointmentAndToday < $daysBetweenOldAppointmentAndToday) {
                    $patient->new_appointment = $new_appointment;
                    $patient->save();
                }

            }
        }

        $title = $patient->last_name;
        $date = $request->get('date');
        $time_start = $request->get('time_start');
        $time_end = $request->get('time_end');
        $description = $request->get('description');
        $procedures = $request->get('procedures') == '' ? $request->get('procedures') : implode(', ',$request->get('procedures'));

        $this->saveAppointment($title, $date, $time_start, $time_end, $description, $id, $procedures);
        
        $data = array(
            'date' => $request->get('date'),
            'time_start' => $request->get('time_start'),
            'time_end' => $request->get('time_end')
        );

        // DELETE THE APPOINTMENT REQUEST
        DB::table('old_patient_appointment_requests')->where('patient_id', $id)->delete();

        // SEND AN EMAIL
        $emailTo = $request->get('email');
        $patientName = $request->get('first_name') . ' ' . $request->get('last_name');

        Mail::send('emails.appointment-approval-accept', $data, function ($message) use ($emailTo) {
            $message->from('fnotestado@gmail.com', 'Gabrillo Dental Clinic');
            $message->to($emailTo)->subject('Gabrillo Dental Clinic - Online Appointment');
        });

        return redirect('/appointments/request-approval')->with('status', 'The appointment of ' . $patientName . ' has been accepted!');
    }

    public function requestApprovalReject(Request $request)
    {
        if ($request->get('patient_type') == 'old') {
             // DELETE THE APPOINTMENT REQUEST
        DB::table('old_patient_appointment_requests')->where('patient_id', $request->get('id'))->delete();

        // SEND AN EMAIL
        $emailTo = $request->get('email');
        $patientName = $request->get('first_name') . ' ' . $request->get('last_name');

        $data = array(
            'url_address_book_appointment' => $request->server->get('SERVER_NAME') . '/online-appointment#appointments',
            'date' => $request->get('date'),
            'time_start' => $request->get('time'),
            'receptionistMessage' => $request->get('message'),
        );

        Mail::send('emails.appointment-approval-reject', $data, function ($message) use ($emailTo) {
            $message->from('fnotestado@gmail.com', 'Gabrillo Dental Clinic');
            $message->to($emailTo)->subject('Gabrillo Dental Clinic - Online Appointment');
        });

        return redirect('/appointments/request-approval')->with('status', 'The appointment of ' . $patientName . ' has been rejected!');
        }
        
       
    }

    public function search(Request $request) 
    {
        $search = $request->get('query');
        $appointments = DB::select('SELECT appointments.id as appointment_id, title, first_name, last_name, slug, email, date, time_start, description FROM appointments JOIN patients ON patients.id = appointments.patient_id WHERE (patients.first_name LIKE "' . $search . '%"  OR patients.last_name LIKE "' . $search . '%" OR patients.slug LIKE "' . $search . '%");');
        

        return json_encode($appointments);
    }

    public function getEventById($id)
    {
        $appointment = DB::select('SELECT appointments.id as appointment_id, patients.id as patient_id, first_name, last_name, email, slug, date, time_start,  time_end, procedures, description FROM appointments JOIN patients ON patients.id = appointments.patient_id WHERE appointments.id = "' . $id .'"');
        
        return json_encode($appointment);
    }

    public function saveAppointment($title, $date, $time_start, $time_end, $description, $patient_id, $procedures)
    {
        $appointment = new Appointment(array(
            'title' => $title,
            'date' => $date,
            'time_start' => $time_start,
            'time_end' => $time_end,
            'description' => $description,
            'patient_id' => $patient_id,
            'procedures' => $procedures . ' '
        ));
            
        $appointment->save();
    }

    public function getAppointmentsByDate($date) 
    {
        $appointments = Appointment::with('patient')->where('date', $date)->get();

        return json_encode($appointments);
    }

    public function onlineAppointmentSubmit(Request $request)
    {
        if($request->get('patient_type') == 'old') {
            $slug = $request->get('patient_slug');

            $patient = Patient::where('slug', $slug)->get();

            $first_name = json_encode($patient[0]->first_name);
            $last_name = json_encode($patient[0]->last_name);
            $email = json_encode($patient[0]->email);
            $contact_no = json_encode($patient[0]->contact_no);
            $id = json_encode($patient[0]->id);

            $confirmAppointmentRequest = DB::table('old_patient_appointment_requests')
                                ->where('patient_slug', $slug)
                                ->where('appointment_status', '0')
                                ->get();
            
            $hasPendingAppointment = sizeof($confirmAppointmentRequest) > 0;

            if($hasPendingAppointment) {
                return view('online-appointment.old-patient-information-form', [
                "id" => $id,
                "patient_slug" => $slug,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "email" => $email,
                "contact_no" => $contact_no
            ])->with('hasAlreadyAppointment', 'Oooops you have already requested for an appointment. Do you want to cancel your initial appointment?');
            } else {
                $referenceId = $this->gen_uuid();
                
                 DB::table('old_patient_appointment_requests')->insert(
                    array('appointment_status' => '0',
                        'description' => $request->get('description'),
                        'date' => $request->get('date'),
                        'time_start' => $request->get('time_start'),
                        'time_end' => $request->get('time_end'),
                        'patient_id' => $request->get('patient_id'),
                        'patient_slug' => $request->get('patient_slug'),
                        'email_verified' => '0',
                        'reference_id' => $referenceId
                ));

                $patient = Patient::where('email', $email);

                $data = array(
                    'url_address' => $request->server->get('SERVER_NAME') . '/online-appointment/confirm/' . $referenceId . '/old',
                    'url_address_cancel_appointment' => $request->server->get('SERVER_NAME') . '/get-patient-by-slug?slug=' . $slug,
                    'date' => $request->get('date'),
                    'time_start' => $request->get('time_start'),
                    'time_end' => $request->get('time_end')
                );

                $emailTo = str_replace('"', "", $email);

                Mail::send('emails.confirm-appointment', $data, function ($message) use ($emailTo) {
                    $message->from('fnotestado@gmail.com', 'Gabrillo Dental Clinic');
                    $message->to($emailTo)->subject('Gabrillo Dental Clinic - Online Appointment');
                });

                return view('online-appointment.old-patient-information-form', [
                    "id" => $id,
                    "patient_slug" => $slug,
                    "first_name" => $first_name,
                    "last_name" => $last_name,
                    "email" => $email,
                    "contact_no" => $contact_no
                    ])->with('success', '');
                }
        } else {
            $referenceId = $this->gen_uuid();
                
            DB::table('new_patient_appointment_requests')->insert(
            array('appointment_status' => '0',
                'description' => $request->get('description'),
                'profile_bg_color' => $request->get('profile_bg_color'),
                'date' => $request->get('date'),
                'time_start' => $request->get('time_start'),
                'time_end' => $request->get('time_end'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'middle_name' => $request->get('middle_name'),
                'address' => $request->get('address'),
                'contact_no' => $request->get('contact_no'),
                'email' => $request->get('email'),
                'occupation' => $request->get('occupation'),
                'birth_date' => $request->get('birth_date'),
                'age' => $request->get('age'),
                'gender' => $request->get('gender'),
                'allergies' => $request->get('allergies'),
                'email_verified' => '0',
                'reference_id' => $referenceId
            ));

            $data = array(
                    'url_address' => $request->server->get('SERVER_NAME') . '/online-appointment/confirm/' . $referenceId . '/new',
                    'url_address_cancel_appointment' => $request->server->get('SERVER_NAME') . '/online-appointment/new-patient-form/update/' . $referenceId,
                    'date' => $request->get('date'),
                    'time_start' => $request->get('time_start'),
                    'time_end' => $request->get('time_end')
            );

            $emailTo = str_replace('"', "", $request->get('email'));

            Mail::send('emails.confirm-appointment', $data, function ($message) use ($emailTo) {
                $message->from('fnotestado@gmail.com', 'Gabrillo Dental Clinic');
                $message->to($emailTo)->subject('Gabrillo Dental Clinic - Online Appointment');
            });

            return view('online-appointment.new-patient-form')->with('success', '');
        }
    }

    function gen_uuid($len=5) {

        $hex = md5("9@brIll0" . uniqid("", true));

        $pack = pack('H*', $hex);
        $tmp =  base64_encode($pack);

        $uid = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);

        $len = max(4, min(128, $len));

        while (strlen($uid) < $len)
            $uid .= $this->gen_uuid(22);

        return substr($uid, 0, $len);
    }

    public function cancelAppointment(Request $request)
    {
        if($request->get('patient_type') == 'old') {
            $patientId = $request->get('patient_id');
            DB::table('old_patient_appointment_requests')->where('patient_id', $patientId)->delete();
        } else {
            $email = $request->get('email');
        }
        
    }

    public function confirmAppointment($referenceId, $patientType) 
    {
        if($patientType == 'old') {
            $confirmReferenceIdValidity = DB::table('old_patient_appointment_requests')
                                ->where('reference_id', $referenceId)
                                ->where('email_verified', '0')
                                ->get();
            
            $isReferenceIdValid = sizeof($confirmReferenceIdValidity) > 0;

            if($isReferenceIdValid) {
                $oldPatient = DB::table('old_patient_appointment_requests')->where('reference_id', '7OQwN')->first();
                
                // Patient's id
                $id = $oldPatient->patient_id;

                $patient = Patient::find($id);
                $new_appointment = $oldPatient->date;

                $pastAppointments = array();

                // If appointment is behind today's date
                if (date('Y-m-d') > $new_appointment) {
                    $appointments = $patient->appointments()->get();

                    for ($i = 0; $i < sizeof($appointments); $i++) {
                        if (date('Y-m-d') > $appointments[$i]->date) {
                            $pastAppointments[] = $appointments[$i]->date;
                        }
                    }

                    usort($pastAppointments, function($a, $b) {
                        if ($a == $b) {
                            return 0;
                        }

                        return $a < $b ? -1 : 1;
                    });

                    if (sizeof($pastAppointments) == 0) {
                        $patient->last_appointment = NULL;
                    } else {
                        $patient->last_appointment = end($pastAppointments);
                    }

                    $patient->save();
                    
                    $title = $patient->last_name;
                    $date = $oldPatient->date;
                    $time_start = $oldPatient->time_end;
                    $time_end = $oldPatient->time_start;
                    $description = $oldPatient->description;
                    $procedures = '';
                    $patient_id = $id;

                    $this->saveAppointment($title, $date, $time_start, $time_end, $description, $patient_id, $procedures);
                }

                if (is_null($patient->new_appointment)) {
                    $patient->new_appointment = $new_appointment;
                    $patient->save();
                } else {
                    // If patient's upcoming new appointment is behind today's day
                    // Set previous appointment as an old appointment
                    if (date('Y-m-d') > $patient->new_appointment) {
                        $patient->last_appointment = $patient->new_appointment;
                        $patient->new_appointment = $new_appointment;
                        $patient->save();
                    } else {
                        $oldAppointment = strtotime($patient->new_appointment);
                        $newAppointment = strtotime($new_appointment);
                        $dateToday = strtotime(date('Y-m-d'));

                        $daysBetweenOldAppointmentAndToday = ceil(($oldAppointment - $dateToday) / 86400);
                        $daysBetweenNewAppointmentAndToday = ceil(($newAppointment - $dateToday) / 86400);

                        if($daysBetweenNewAppointmentAndToday < $daysBetweenOldAppointmentAndToday) {
                            $patient->new_appointment = $new_appointment;
                            $patient->save();
                        }

                    }
                }

                $title = $patient->last_name;
                $date = $oldPatient->date;
                $time_start = $oldPatient->time_end;
                $time_end = $oldPatient->time_start;
                $description = $oldPatient->description;
                $procedures = '';

                $this->saveAppointment($title, $date, $time_start, $time_end, $description, $id, $procedures);
                
                $data = array(
                    'date' => $oldPatient->date,
                    'time_start' => $oldPatient->time_end,
                    'time_end' => $oldPatient->time_start
                );

                // DELETE THE APPOINTMENT REQUEST
                DB::table('old_patient_appointment_requests')->where('patient_id', $id)->delete();

                // SEND AN EMAIL
                $emailTo = $patient->email;
                $patientName = $patient->first_name . ' ' . $patient->last_name;

                Mail::send('emails.appointment-approval-accept', $data, function ($message) use ($emailTo) {
                    $message->from('fnotestado@gmail.com', 'Gabrillo Dental Clinic');
                    $message->to($emailTo)->subject('Gabrillo Dental Clinic - Online Appointment');
                });
                
                DB::statement("UPDATE old_patient_appointment_requests SET email_verified = '1' where reference_id = '" . $referenceId . "'");
                return view('online-appointment.success');
            } else {
                return view('online-appointment.reference-id-expired');
            }
            
        } else {
             $confirmReferenceIdValidity = DB::table('new_patient_appointment_requests')
                                ->where('reference_id', $referenceId)
                                ->where('email_verified', '0')
                                ->get();
            
            $isReferenceIdValid = sizeof($confirmReferenceIdValidity) > 0;

            if($isReferenceIdValid) {
                DB::statement("UPDATE new_patient_appointment_requests SET email_verified = '1' where reference_id = '" . $referenceId . "'");
                return view('online-appointment.success');
            } else {
                return view('online-appointment.reference-id-expired');
            }
        }
    }

    public function newPatientForm(Request $request)
    {
        return view('online-appointment.new-patient-form');
    }

    public function checkEmailAvailability($email)
    {
        $oldPatientEmailSimilarity = DB::table('patients')
                        ->where('patients.email', $email)
                        ->get();
        
        if(sizeof($oldPatientEmailSimilarity) > 0) {
            return '1';
        } 

        $newPatientEmailSimilarity = DB::table('new_patient_appointment_requests')
                        ->where('new_patient_appointment_requests.email', $email)
                        ->get();

        if(sizeof($newPatientEmailSimilarity) > 0) {
            return '1';
        } else {
            return '0';
        }
    }

    public function updateNewPatientAppointmentGet($referenceId)
    {
        $result = DB::table('new_patient_appointment_requests')
                                ->where('reference_id', $referenceId)
                                ->where('email_verified', '0')
                                ->get();

        $isReferenceIdValid = sizeof($result) > 0;

        if($isReferenceIdValid) {
            return view('online-appointment.update-new-patient-information-form', ['data' => $result]);
        } else {
                return view('online-appointment.reference-id-expired');
        }
    }
    
    public function updateNewPatientAppointmentPost(Request $request)
    {
        $result = DB::table('new_patient_appointment_requests')
                                ->where('reference_id', $request->get('reference_id'))
                                ->where('email_verified', '0')
                                ->get();

        $isReferenceIdValid = sizeof($result) > 0;
                                
        if($isReferenceIdValid) {
             DB::table('new_patient_appointment_requests')->insert(
            array('appointment_status' => '0',
                'description' => $request->get('description'),
                'profile_bg_color' => $request->get('description'),
                'date' => $request->get('date'),
                'time_start' => $request->get('time_start'),
                'time_end' => $request->get('time_end'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'middle_name' => $request->get('middle_name'),
                'address' => $request->get('address'),
                'contact_no' => $request->get('contact_no'),
                'email' => $request->get('email'),
                'occupation' => $request->get('occupation'),
                'birth_date' => $request->get('birth_date'),
                'age' => $request->get('age'),
                'gender' => $request->get('gender'),
                'allergies' => $request->get('allergies'),
                'email_verified' => '0',
                'reference_id' => $request->get('reference_id')
            ));
            
            DB::table('new_patient_appointment_requests')->where('id', $request->get('id'))->delete();

            $data = array(
                    'url_address' => $request->server->get('SERVER_NAME') . '/online-appointment/confirm/' . $request->get('reference_id') . '/new',
                    'url_address_cancel_appointment' => $request->server->get('SERVER_NAME') . '/online-appointment/new-patient-form/update/' . $request->get('reference_id'),
                    'date' => $request->get('date'),
                    'time_start' => $request->get('time_start'),
                    'time_end' => $request->get('time_end')
            );

            $emailTo = str_replace('"', "", $request->get('email'));

            Mail::send('emails.confirm-appointment', $data, function ($message) use ($emailTo) {
                $message->from('fnotestado@gmail.com', 'Gabrillo Dental Clinic');
                $message->to($emailTo)->subject('Gabrillo Dental Clinic - Online Appointment');
            });
            
            return view('online-appointment.new-patient-form')->with('success', '');
        } else {
                return view('online-appointment.reference-id-expired');
        }
        
    }

    public function requestApprovalReviewAccept(Request $request)
    {
        $patient = new Patient(array(
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'middle_name' => $request->get('middle_name'),
            'birth_date' => $request->get('birth_date'),
            'age' => $request->get('age'),
            'gender' => $request->get('gender'),
            'email' => $request->get('email'),
            'contact_no' => $request->get('contact_number'),
            'address' => $request->get('address'),
            'occupation' => $request->get('occupation'),
            'allergies' => $request->get('allergies'),
            'profile_bg_color' => $request->get('profile_bg_color'),
            'account_status' => 'active'
        ));

        // Create the slug using the combination of year and day and patient's id
        $datetime = $patient->created_at;

        $date_suffix = substr($datetime, 2, 2);

        $day = substr($datetime, 8, 2);
        
        $slug = $date_suffix . $day . $patient->id;

        $patient->slug = $slug;
        
        $patient->save();
        
        $new_appointment = $request->get('date');

        $pastAppointments = array();

        // If appointment is behind today's date
        if (date('Y-m-d') > $new_appointment) {
            $appointments = $patient->appointments()->get();

            for ($i = 0; $i < sizeof($appointments); $i++) {
                if (date('Y-m-d') > $appointments[$i]->date) {
                    $pastAppointments[] = $appointments[$i]->date;
                }
            }

            usort($pastAppointments, function($a, $b) {
                if ($a == $b) {
                    return 0;
                }

                return $a < $b ? -1 : 1;
            });

            if (sizeof($pastAppointments) == 0) {
                $patient->last_appointment = NULL;
            } else {
                $patient->last_appointment = end($pastAppointments);
            }

            $patient->save();
            
            $title = $patient->last_name;
            $date = $request->get('date');
            $time_start = $request->get('time_start');
            $time_end = $request->get('time_end');
            $description = $request->get('description');
            $procedures = $request->get('procedures') == '' ? $request->get('procedures') : implode(', ',$request->get('procedures'));
            $patient_id = $patient->id;

            $data = array(
            'date' => $request->get('date'),
            'time_start' => $request->get('time_start'),
            'time_end' => $request->get('time_end')
            );

            // DELETE THE APPOINTMENT REQUEST
            DB::table('new_patient_appointment_requests')->where('reference_id', $request->get('reference_id'))->delete();

            // SEND AN EMAIL
            $emailTo = $request->get('email');
            $patientName = $request->get('first_name') . ' ' . $request->get('last_name');

            Mail::send('emails.appointment-approval-accept', $data, function ($message) use ($emailTo) {
                $message->from('fnotestado@gmail.com', 'Gabrillo Dental Clinic');
                $message->to($emailTo)->subject('Gabrillo Dental Clinic - Online Appointment');
            });

            $this->saveAppointment($title, $date, $time_start, $time_end, $description, $patient_id, $procedures);
            return redirect('/appointments/request-approval')->with(['tab2' => '1', 'success' => '1', 'patientName' => $patientName]);
        }


        if (is_null($patient->new_appointment)) {
            $patient->new_appointment = $new_appointment;
            $patient->save();
        } else {
            // If patient's upcoming new appointment is behind today's day
            // Set previous appointment as an old appointment
            if (date('Y-m-d') > $patient->new_appointment) {
                $patient->last_appointment = $patient->new_appointment;
                $patient->new_appointment = $new_appointment;
                $patient->save();
            } else {
                $oldAppointment = strtotime($patient->new_appointment);
                $newAppointment = strtotime($new_appointment);
                $dateToday = strtotime(date('Y-m-d'));

                $daysBetweenOldAppointmentAndToday = ceil(($oldAppointment - $dateToday) / 86400);
                $daysBetweenNewAppointmentAndToday = ceil(($newAppointment - $dateToday) / 86400);

                if($daysBetweenNewAppointmentAndToday < $daysBetweenOldAppointmentAndToday) {
                    $patient->new_appointment = $new_appointment;
                    $patient->save();
                }

            }
        }

        $title = $patient->last_name;
        $date = $request->get('date');
        $time_start = $request->get('time_start');
        $time_end = $request->get('time_end');
        $description = $request->get('description');
        $procedures = $request->get('procedures') == '' ? $request->get('procedures') : implode(', ',$request->get('procedures'));

        $data = array(
            'date' => $request->get('date'),
            'time_start' => $request->get('time_start'),
            'time_end' => $request->get('time_end')
        );

        // DELETE THE APPOINTMENT REQUEST
        DB::table('new_patient_appointment_requests')->where('reference_id', $request->get('reference_id'))->delete();

        // SEND AN EMAIL
        $emailTo = $request->get('email');
        $patientName = $request->get('first_name') . ' ' . $request->get('last_name');

        Mail::send('emails.appointment-approval-accept', $data, function ($message) use ($emailTo) {
            $message->from('fnotestado@gmail.com', 'Gabrillo Dental Clinic');
            $message->to($emailTo)->subject('Gabrillo Dental Clinic - Online Appointment');
        });

        $this->saveAppointment($title, $date, $time_start, $time_end, $description, $patient->id, $procedures);
        // return redirect('/appointments/request-approval')->with(['tab2' => '1', 'success' => '1', 'patientName' => $patientName]);
        return redirect('/appointments/request-approval')->with(['tab2' => '1', 'success' => '1', 'patientName' => $patientName]);
    }
}




