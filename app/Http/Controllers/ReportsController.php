<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{

    //get appointments
    /**
     * Display the specified resource.
     *
     * @param  date  $start
     * @param  date  $end
     * @return \Illuminate\Http\Response
     */
    public function getAppointmentsByday($start, $end)
    {
        $appointments = DB::table('appointments')
            ->select('date')
            ->wherebetween('date', [$start, $end])
            ->orderBy('date')
            ->get();
        return json_encode($appointments);
    }

    public function getAppointmentsByMonth($monthc)
    {
        $month = $monthc[0] . $monthc[1];
        $year = $monthc[3] . $monthc[4] . $monthc[5] . $monthc[6];

        $appointments = DB::table('appointments')
            ->select('date')
            ->whereYear('date', $year)
            ->whereMonth('date', $month)
            ->orderBy('date')
            ->get();

        return json_encode($appointments);
    }

    public function getAppointmentsByYear($yearc)
    {

        $appointments = DB::table('appointments')
            ->select('date')
            ->whereYear('date', $yearc)
            ->orderBy('date')
            ->get();

        return json_encode($appointments);
    }

    public function getTopProceduresName($year)
    {
        $procedures = DB::table('procedure_treatment')
            ->select(DB::raw('COUNT(procedure_id) as number_of_treatments, procedure_id, MONTH(created_at) as Month'))
            ->whereYear('created_at', $year)
            ->groupBy(DB::raw('MONTH(created_at),  procedure_id'))
            ->orderByRaw('Month ASC, number_of_treatments DESC')
            ->get();

        $ids = array();
        $names = array();

        for ($i = 0; $i < 5; $i++) {
            array_push($ids, $procedures[$i]->procedure_id);
        }

        for ($i = 0; $i < sizeof($ids); $i++) {

            $name = DB::table('procedures')
                ->select(DB::raw('name'))
                ->where('id', $ids[$i])
                ->get();

            array_push($names, $name);
        }
        $arr = [$procedures, $names, $ids];

        return json_encode($arr);
    }

    //Active and Inactive
    public function pieGraph2()
    {
        $status = DB::select(DB::raw("select count(*) as total_account_status, account_status FROM patients group by account_status;"));
        $chartData = "";
        foreach ($status as $list) {
            $chartData .= "['" . $list->account_status . "', " . $list->total_account_status . "],";
        }
        $arr['chartData'] = rtrim($chartData, ",");
        return json_encode($status);
    }

    public function getIncome($year)
    {
        $income = DB::table('payments')
            ->select(DB::raw('MONTH(`updated_at`) as Month,SUM(receive) as Income'))
            ->whereYear('created_at', $year)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get();

        return json_encode($income);
    }

    //Top Procedure
    public function index()
    {
        $data['year_list'] = $this->fetch_year();
        return view('/reports/index')->with($data);
    }

    public function fetch_year()
    {
        $data = DB::table('procedure_treatment')
            ->select(DB::raw('YEAR(created_at) as year'))
            ->groupBy(DB::raw('YEAR(created_at)'))
            ->orderBy('year', 'DESC')
            ->get();
        return $data;
    }

    public function fetch_data(Request $request)
    {
        if ($request->input('year')) {
            $procedure_treatment = $this->fetch_chart_data($request->input('year'));

            foreach ($procedure_treatment->toArray() as $row) {
                $output[] = array(
                    'procedure_id' => $row->procedure_id,
                    'treatment_id' => $row->treatment_id,
                    'created_at' => $row->created_at
                );
            }
            echo json_encode($output);
        }
    }

    public function fetch_chart_data($year)
    {
        $data =  DB::table('procedure_treatment')
            ->groupBy(DB::raw('YEAR(created_at)'))
            ->whereYear('created_at', $year)
            ->orderBy('year', 'ASC')
            ->get();
        return $data;
    }
}
