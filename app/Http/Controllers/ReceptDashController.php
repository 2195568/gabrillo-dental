<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReceptDashController extends Controller
{
     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getReceptionistDashData()
    {
        $date = Carbon::now();
        $dateToday = $date->format('Y-m-d');
        $data = DB::table('appointments')
                        ->join('patients','appointments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->select('title','description','time_start','patients.*')
                        ->orderBy('time_end','asc')
                        ->get();

        $pending = DB::table('appointment_requests')
                        ->select('appointment_requests.*')
                        ->where('appointment_status', '=', '0')
                        ->orderBy('date', 'desc')
                        ->get();
        
        $count = DB::table('appointments')
                        ->join('patients','appointments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->select('title','description','time_start','patients.*')
                        ->count();

        return view('receptionistdash.index',[  'data'=>$data, 
                                                'pending' => $pending,
                                                'count' => $count]);  
      
    }


}
