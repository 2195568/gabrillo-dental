<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RequestApprovalController extends Controller
{
    public function oldAppointmentRequests()
    {
        $requests = DB::table('old_patient_appointment_requests')
                        ->where('email_verified', '=', '1')
                        ->join('patients', 'old_patient_appointment_requests.patient_id', 'patients.id')
                        ->orderBy('date', 'desc')
                        ->get();

        return json_encode($requests);
    }

    public function newAppointmentRequests()
    {
        $requests = DB::table('new_patient_appointment_requests')
                        ->where('email_verified', '=', '1')
                        ->orderBy('date', 'desc')
                        ->get();

        return json_encode($requests);
    }

    public function store(Request $request)
    {
        $procedures = $request->get('procedures');

        DB::table('appointment_requests')->insert(
            array('appointment_status' => '0',
            'description' => $request->get('description'),
            'time_start' => $request->get('time_start'),
            'time_end' => $request->get('time_end'),
            'date' => $request->get('date'),
            'name' => $request->get('name'),
            'contact_no' => $request->get('contact_no'),
            'patient_type' => $request->get('patient_type'),
            'email' => $request->get('email')
            )
        );

        return redirect('/landing')->with('status', 'Request has been saved!');
    }
    
    public function getRequestsByDate($date)
    {
        $requests = DB::table('appointment_requests')
                        ->select('apointment_requests.*')
                        ->where('date', '=', $date)
                        ->get();

        return json_encode($requests);
    }
}
