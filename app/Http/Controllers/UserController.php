<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests\StoreUserFormRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')
                        ->select(array('users.id', 'users.name AS name', 'email', 'email_verified_at', 'user_roles.name AS role'))
                        ->join('user_roles', 'users.user_role_id', '=', 'user_roles.id')
                        ->get();
        
        return json_encode($users);
    }

    public function getData()
    {
        $users = DB::table('users')
                        ->select(array('users.id', 'users.name AS name', 'email', 'email_verified_at', 'user_roles.name AS role'))
                        ->join('user_roles', 'users.user_role_id', '=', 'user_roles.id')
                        ->get();
        
        return json_encode($users);
    }

    public function view()
    {
        return view('user.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserFormRequest $request)
    {
        $validated = $request->validated();
        
        $user = new User(array(
            'name' => $request->get('name'),
            'user_role_id' => $request->get('role'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password'))
            // 'password' => $request->get('password')
        ));

        $user->save();
        
        return redirect('/users')->with('status', 'New user "' . $request->get('name') . '" has been saved!');
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = User::find($request->get('id'));

        $user->delete();
        
        return redirect('/users')->with('status', 'The user has been deleted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return json_encode($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserFormRequest $request)
    {
        $validated = $request->validated();
        $user = User::find($request->get('id'));

        
        // Validate old password
        if (!(Hash::check($request->get('old_password'), $user->password))) {
            return redirect('/users')->with('err', 'Old password does not match.');
        }

        // Validate length of new password 
        if($request->get('new_password'))

        $user->id = $request->get('id');
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('new_password'));
        // $user->password = $request->get('new_password');

        $user->save();

        return redirect('/users')->with('status', 'Changes have been saved.');
    }

    /**
     * Search user
     */
    public function search(Request $request) 
    {
        $search = $request->get('query');

        $users = DB::table('users')
                        ->select(array('users.id', 'users.name AS name', 'email', 'email_verified_at', 'user_roles.name AS role'))
                        ->orWhere('users.id', 'LIKE', $search . '%')
                        ->orWhere('email', 'LIKE', $search . '%')
                        ->join('user_roles', 'users.user_role_id', '=', 'user_roles.id')
                        ->get();

                    

        return json_encode($users);
    }
}
