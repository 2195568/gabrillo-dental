<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use Illuminate\Http\Request;
use App\Http\Requests\PatientFormRequest;
use Illuminate\Support\Facades\DB;
use \DateTime;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::all();
        
        foreach ($patients as $patient) {
            $incomingAppointments = array();
            $appointments = $patient->appointments()->get();

            for ($i = 0; $i < sizeof($appointments); $i++) {
                if (date('Y-m-d') < $appointments[$i]->date) {
                    $incomingAppointments[] = $appointments[$i]->date;
                }
            }

            usort($incomingAppointments, function($a, $b) {
                if ($a == $b) {
                    return 0;
                }

                return $a < $b ? -1 : 1;
            });

            if (date('Y-m-d') > $patient->new_appointment) {
                $patient->last_appointment = $patient->new_appointment;

                if (sizeof($incomingAppointments) == 0) {
                    $patient->new_appointment = NULL;
                } else {
                    $patient->new_appointment = $incomingAppointments[0];
                }

                $patient->save();
            } 
        }

        echo Auth::user();

        return json_encode($patients);
    }

    public function getData(){

        $patients = DB::select('SELECT * FROM patients WHERE account_status="active"');
        // $patients = Patient::all();
      
        
        return json_encode($patients);
    }


    public function getInactive(){

        $patients = DB::select('SELECT * FROM patients WHERE account_status="inactive"');
        // $patients = Patient::all();
      
        
        return json_encode($patients);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientFormRequest $request)
    {
        // Retrieve the validated input data
        $validated = $request->validated();

        // Validate email
        $patient = Patient::where('email', $request->get('email_address'))->get();
        $hasDuplicateEmail = sizeof($patient) > 0;

        if($hasDuplicateEmail) {
            return redirect('/patients')->with('err', 'Email address is already in use!');
        }
        
        $patient = new Patient(array(
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'middle_name' => $request->get('middle_name'),
            'birth_date' => $request->get('birth_date'),
            'age' => $request->get('age'),
            'gender' => $request->get('gender'),
            'email' => $request->get('email_address'),
            'contact_no' => $request->get('contact_no'),
            'address' => $request->get('home_address'),
            'occupation' => $request->get('occupation'),
            'allergies' => $request->get('allergies'),
            'profile_bg_color' => $request->get('profile_bg_color'),
            'account_status' => 'active'
        ));

        $patient->save();
        
        // Create the slug using the combination of year and day and patient's id
        $datetime = $patient->created_at;
        
        $date_suffix = substr($datetime, 2, 2);
        
        $day = substr($datetime, 8, 2);
        
        $slug = $date_suffix . $day . $patient->id;
        
        $patient->slug = $slug;
        $patient->save();

        return redirect('/patients')->with('status', 'New patient "' . $request->get('first_name') . ' ' . $request->get('last_name') . '" has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PatientsController  $patientsController
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = Patient::with('futureAppointments')->find($id);

        return json_encode($patient);
    }

    public function showPatientWithAppointments($id)
    {
        $patient = DB::table('patients')
                    ->join('appointments', 'patients.id', 'appointments.patient_id')
                    ->where('date', '>=', date("Y-m-d"))
                    ->where('patients.id', '=', $id)
                    ->get();

        return json_encode($patient);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PatientsController  $patientsController
     * @return \Illuminate\Http\Response
     */
    public function edit(PatientsController $patientsController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PatientsController  $patientsController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $patient = Patient::find($request->get('id'));

        $patient->first_name = $request->get('first_name');
        $patient->last_name = $request->get('last_name');
        $patient->middle_name = $request->get('middle_name');
        $patient->birth_date = $request->get('birth_date');
        $patient->age = $request->get('age');
        $patient->gender = $request->get('gender');
        $patient->contact_no = $request->get('contact_no');
        $patient->email = $request->get('email_address');
        $patient->address = $request->get('home_address');
        $patient->occupation = $request->get('occupation');
        $patient->allergies = $request->get('allergies');

        $patient->save();

        return redirect($request->server('HTTP_REFERER'))->with('status', 'Changes have been saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PatientsController  $patientsController
     * @return \Illuminate\Http\Response
     */
    public function destroy(PatientsController $patientsController)
    {
        //
    }

    function date_sort($a, $b) {
        return strtotime($a) - strtotime($b);
    }

    public function appointments($id)
    {
    }

    public function patients($status) 
    {
        $patients = '';
        
        if ($status == 'active') {
            $patients = Patient::where('account_status', 'active')->get();
        } else {
            $patients = Patient::where('account_status', 'inactive')->get();
        }
        
        foreach ($patients as $patient) {
            $incomingAppointments = array();
            $appointments = $patient->appointments()->get();

            for ($i = 0; $i < sizeof($appointments); $i++) {
                if (date('Y-m-d') < $appointments[$i]->date) {
                    $incomingAppointments[] = $appointments[$i]->date;
                }
            }

            usort($incomingAppointments, function($a, $b) {
                if ($a == $b) {
                    return 0;
                }

                return $a < $b ? -1 : 1;
            });

            if (date('Y-m-d') > $patient->new_appointment) {
                $patient->last_appointment = $patient->new_appointment;

                if (sizeof($incomingAppointments) == 0) {
                    $patient->new_appointment = NULL;
                } else {
                    $patient->new_appointment = $incomingAppointments[0];
                }

                $patient->save();
            } 
        }

        return json_encode($patients);
    }

    public function deactivatePatient(Request $request)
    {
        $patient = Patient::find($request->get('id'));
        $patient->account_status = 'inactive';

        // Reset balance and delete all appointments
        $patient->balance = '0';
        $appointments = DB::table('appointments')
                        ->where('patient_id', '=', $patient->id)
                        ->delete();
        
        $patient->save();

        return redirect('/inactive-patients')->with('status', 'Patient "'.$request->get('name').'" has been deactivated.');
    }

    public function activatePatient(Request $request)
    {
        $patient = Patient::find($request->get('id'));
        $patient->account_status = 'active';
        $patient->save();

        return redirect('/patients')->with('status', 'Patient "'.$request->get('name').'" has been activated.');
    }

    /**
     * Search patient
     */
    public function search(Request $request) 
    {
        $status = $request->get('status');
        $search = $request->get('query');
        $patients = '';

        if ($status == '') {
            $patients = DB::select('SELECT * FROM patients WHERE (first_name LIKE "' . $search . '%"  OR last_name LIKE "' . $search . '%" OR slug LIKE "' . $search . '%");');
        } else {
            $patients = DB::select('SELECT * FROM patients WHERE (first_name LIKE "' . $search . '%"  OR last_name LIKE "' . $search . '%" OR slug LIKE "' . $search . '%") AND account_status="'. $status . '";');
        }

        return json_encode($patients);
    }

    public function getPatientBySlug(Request $request) 
    {
        $slug = $request->get('slug');

        $patient = Patient::where('slug', $slug)->get();

        if (sizeof($patient) == 0) {
            return redirect('online-appointment/patient-type')->with('err', 'Patient ID ' . $slug . ' was not found.');
        } 

        $appointmentRequest = DB::table('old_patient_appointment_requests')
                                ->where('patient_slug', $slug)
                                ->get();

        $verifiedAppointment = DB::table('old_patient_appointment_requests')
                                ->where('patient_slug', $slug)
                                ->where('email_verified', '1')
                                ->get();

        $hasVerifiedAppointment = sizeof($verifiedAppointment) > 0;
                            
        $first_name = json_encode($patient[0]->first_name);
        $last_name = json_encode($patient[0]->last_name);
        $email = json_encode($patient[0]->email);
        $contact_no = json_encode($patient[0]->contact_no);
        $id = json_encode($patient[0]->id);
        
        $isAlreadyRequestedAnAppointment = sizeof($appointmentRequest) > 0;
        
        if($hasVerifiedAppointment){
            $appointmentDate = $verifiedAppointment[0]->date;
            $isCurrentAppointmentFinish = date('Y-m-d') > $appointmentDate;
            if($isCurrentAppointmentFinish) {
                // Delete the verified appointment requests since its already behind the date
                DB::table('old_patient_appointment_requests')->delete($verifiedAppointment[0]->id);
                return view('online-appointment.old-patient-information-form', [
                    "id" => $id,
                    "patient_slug" => $slug,
                    "first_name" => $first_name,
                    "last_name" => $last_name,
                    "email" => $email,
                    "contact_no" => $contact_no
                ]);
            } else {
                return view('online-appointment.old-patient-information-form', [
                "id" => $id,
                "patient_slug" => $slug,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "email" => $email,
                "contact_no" => $contact_no
            ])
            ->with('hasVerifiedAppointment', 'Ooops! You have currently request that is being processed. Please wait until further notice.');
            }
        } else if ($isAlreadyRequestedAnAppointment) {
            return view('online-appointment.old-patient-information-form', [
                "id" => $id,
                "patient_slug" => $slug,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "email" => $email,
                "contact_no" => $contact_no
            ])
            ->with('hasAlreadyAppointment', 'Oooops you have already requested for an appointment. Do you want to cancel your initial appointment?');
        } else {
            return view('online-appointment.old-patient-information-form', [
                "id" => $id,
                "patient_slug" => $slug,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "email" => $email,
                "contact_no" => $contact_no
            ]);
        }
    }
}
