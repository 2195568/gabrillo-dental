<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Patient;

class DentistDashController extends Controller
{
     /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function getTodaysAppointmentData()
    {
        $date = Carbon::now();
        $dateToday = $date->format('Y-m-d');
        $data = DB::table('appointments')
                        ->join('patients','appointments.patient_id','=','patients.id')
                        //-> where('date',[$dateToday])
                        -> where('date',"2021-09-01")
                        ->select('title','description','time_start','patients.*')
                        ->orderBy('time_end','asc')
                        ->get();

        
        return view('admindash.index',['data'=>$data]);  
      
    }



}
