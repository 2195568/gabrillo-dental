<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Patient;

class DashController extends Controller
{
    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function getTodaysAppointmentData()
    {
        $date = Carbon::now();
        $dateToday = $date->format('Y-m-d');
        $data = DB::table('appointments')
                        ->join('patients','appointments.patient_id','=','patients.id')
                        -> where('date',[$dateToday])
                        ->select('title','description','time_start','patients.*')
                        ->orderBy('time_end','asc')
                        ->get();
        $procedurecount = DB::table('procedure_treatment')
                        ->join('procedures','procedure_treatment.procedure_id','=', 'procedures.id')
                        ->select('procedure_treatment.procedure_id','procedures.name' ,DB::raw('COUNT(procedure_id) as count'))
                        ->groupBy('procedure_id')
                        ->orderBy('count','desc')
                        ->get();

        return view('dentistdash.index',['data'=>$data,'procedurecount'=>$procedurecount]);   
    }

    public function getAdminDashData()
    {
        $date = Carbon::now();
        $dateToday = $date->format('Y-m-d');
        $data = DB::table('appointments')
                        ->join('patients','appointments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->select('title','description','time_start','patients.*')
                        ->orderBy('time_end','asc')
                        ->get();

        $payments = DB::table('treatments')
                        ->join('patients','treatments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->select('*')
                        ->where('payment_status_id','=','1')
                        ->get();

        $paymentcount = DB::table('treatments')
                        ->join('patients','treatments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->select('*')
                        ->where('payment_status_id','=','1')
                        ->count();

        $procedurecount = DB::table('procedure_treatment')
                        ->join('procedures','procedure_treatment.procedure_id','=', 'procedures.id')
                        ->select('procedure_treatment.procedure_id','procedures.name' ,DB::raw('COUNT(procedure_id) as count'))
                        ->groupBy('procedure_id')
                        ->orderBy('count','desc')
                        ->get();
        
        $count = DB::table('appointments')
                        ->join('patients','appointments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->count();
        $patientcount = DB::table('patients')
                        ->count();
        $inactivepatients = DB::table('patients')
                        ->select('*')
                        ->where('account_status','=','inactive')
                        ->count();
        $listInactivePatients = DB::table('patients')
                        ->select('*')
                        ->where('account_status','=','inactive')
                        ->get();                
        $activepatients = DB::table('patients')
                        ->select('*')
                        ->where('account_status','=','active')
                        ->count();
        $unpaidcount = DB::table('treatments')
                        ->select('*')
                        ->where('payment_status_id','=','2')
                        ->count();
        $patients = DB::table('patients')
                        ->select('*')
                        ->get();
        return view('admindash.index',[  'data'=>$data, 
                                         'count' => $count,
                                         'paymentcount' => $paymentcount,
                                         'inactivepatients' => $inactivepatients,
                                         'activepatients' => $activepatients,
                                         'patients' => $patients,
                                         'payments' => $payments,
                                         'patientcount' => $patientcount,
                                        'listInactivePatients'=>$listInactivePatients,
                                        'unpaidcount'=>$unpaidcount,
                                    'procedurecount'=>$procedurecount]);  
      
    }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getReceptionistDashData()
    {
        $date = Carbon::now();
        $dateToday = $date->format('Y-m-d');
        $data = DB::table('appointments')
                        ->join('patients','appointments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->select('title','description','time_start','patients.*')
                        ->orderBy('time_end','asc')
                        ->get();
                        
        $count = DB::table('appointments')
                        ->join('patients','appointments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->select('title','description','time_start','patients.*')
                        ->count();

        $payments = DB::table('treatments')
                        ->join('patients','treatments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->select('*')
                        ->where('payment_status_id','=','1')
                        ->get();

        $newpatientreq = DB::table('new_patient_appointment_requests')
                        ->select('*')
                        ->where('email_verified', '=', '1')
                        ->count();
        $oldpatientreq = DB::table('old_patient_appointment_requests')
                        ->select('*')
                        ->where('email_verified', '=', '1')
                        ->count();
                    

        $paymentcount = DB::table('treatments')
                        ->join('patients','treatments.patient_id','=','patients.id')
                        ->where('date',[$dateToday])
                        ->select('*')
                        ->where('payment_status_id','=','1')
                        ->count();

        $patientcount = DB::table('patients')
                        ->count();
        $inactivepatients = DB::table('patients')
                        ->select('*')
                        ->where('account_status','=','inactive')
                        ->count();
        $listInactivePatients = DB::table('patients')
                        ->select('*')
                        ->where('account_status','=','inactive')
                        ->get();                
        $activepatients = DB::table('patients')
                        ->select('*')
                        ->where('account_status','=','active')
                        ->count();
        $patients = DB::table('patients')
                        ->select('*')
                        ->get();
        return view('receptionistdash.index',[  'data'=>$data, 
                                                'count' => $count,
                                                'payments'=>$payments,
                                                'paymentcount'=>$paymentcount,
                                                'inactivepatients' => $inactivepatients,
                                                'activepatients' => $activepatients,
                                                'patients' => $patients,
                                                'payments' => $payments,
                                                'patientcount' => $patientcount,
                                                'newpatientreq' => $newpatientreq,
                                                'oldpatientreq' => $oldpatientreq,
                                               'listInactivePatients'=>$listInactivePatients,
                                            ]);  
      
    }

     /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function getTodaysAppointmentDataAdmin()
    {
        $date = Carbon::now();
        $dateToday = $date->format('Y-m-d');
        $data = DB::table('appointments')
                        ->join('patients','appointments.patient_id','=','patients.id')
                        //-> where('date',[$dateToday])
                        -> where('date',"2021-09-01")
                        ->select('title','description','time_start','patients.*')
                        ->orderBy('time_end','asc')
                        ->get();

        
        return view('admindash.index',['data'=>$data]);  
      
    }

}
