<?php

namespace App\Http\Controllers;
use App\Models\Payment;
use App\Models\Treatment;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = DB::table('payments')
                        ->select(array('treatments.date as treatment_date','treatments.patient_id', 'treatments.created_at', 'patients.first_name', 'patients.last_name', 'treatments.reference_id', 'treatments.id as treatment_id', 'treatments.total_amount as treatment_total_amount', 'payment_status.name as payment_status_name', 'treatments.balance as treatment_balance', DB::raw('SUM(payments.amount) AS total_payment')))
                        ->join('treatments', 'treatments.id', '=', 'treatment_id')
                        ->join('patients', 'patients.id', '=', 'patient_id')
                        ->join('payment_status', 'payment_status.id', '=', 'treatments.payment_status_id')
                        ->groupBy('treatments.id')
                        ->orderBy('total_payment')
                        ->get();

        return json_encode($payments);
    }

    public function getData()
    {
        $payments = DB::table('payments')
                        ->select(array('treatments.date as treatment_date','treatments.patient_id', 'treatments.reference_id as treatment_reference_id', 'treatments.created_at', 'patients.first_name', 'patients.last_name', 'treatments.reference_id', 'treatments.id as treatment_id', 'treatments.total_amount as treatment_total_amount', 'payment_status.name as payment_status_name', 'treatments.balance as treatment_balance', DB::raw('SUM(payments.amount) AS total_payment')))
                        ->join('treatments', 'treatments.id', '=', 'treatment_id')
                        ->join('patients', 'patients.id', '=', 'patient_id')
                        ->join('payment_status', 'payment_status.id', '=', 'treatments.payment_status_id')
                        ->groupBy('treatments.id')
                        ->orderBy('total_payment')
                        ->get();

        return json_encode($payments);
    }
    
    public function getLogs()
    {
        $logs = DB::table('payments')
                        ->select(array('payments.id as payment_id', 'patients.slug', 'treatments.patient_id', 'patients.first_name', 'patients.last_name', 'treatments.reference_id', 'treatments.id as treatment_id', 'treatments.total_amount as treatment_total_amount', 'payments.amount', 'payment_status.name as payment_status_name', 'treatments.balance as treatment_balance', 'payments.balance as payment_balance', 'payments.created_at'))
                        ->join('treatments', 'treatments.id', '=', 'treatment_id')
                        ->join('patients', 'patients.id', '=', 'patient_id')
                        ->join('payment_status', 'payment_status.id', '=', 'treatments.payment_status_id')
                        ->get();

        return json_encode($logs);
    }


    public function logs()
    {
        $logs = DB::table('payments')
                        ->select(array('payments.id as payment_id', 'treatments.patient_id', 'treatments.date as treatment_date', 'payments.amount', 'patients.first_name', 'patients.last_name', 'treatments.reference_id', 'treatments.id as treatment_id', 'treatments.total_amount as treatment_total_amount', 'payments.amount', 'payment_status.name as payment_status_name', 'treatments.balance as treatment_balance', 'payments.balance as payment_balance', 'payments.created_at'))
                        ->join('treatments', 'treatments.id', '=', 'treatment_id')
                        ->join('patients', 'patients.id', '=', 'patient_id')
                        ->join('payment_status', 'payment_status.id', '=', 'treatments.payment_status_id')
                        ->where('payments.amount', '>', '0')
                        ->get();

        return json_encode($logs);
    }

    public function store(Request $request)
    {
        $treatment = Treatment::where('reference_id', $request->get('treatment_reference_id'))->get();

        $balance = $treatment[0]->balance - $request->get('amount');

        if ($balance < 0) {
            return redirect('/payments')->with('err', 'The amount you are trying to pay is invalid!');
        } else if ($balance == 0) {
            $treatment[0]->payment_status_id = '1'; 
        }
        
        $treatment[0]->balance = $balance;
        $treatment[0]->save();
        
        $payment = new Payment(array(
            'treatment_id' => $treatment[0]->id,
            'treatment_reference_id' => $request->get('treatment_reference_id'),
            'amount' => $request->get('amount'),
            'balance' => $balance,
        ));

        $payment->save();

        return redirect('/payments')->with('status', 'New payment has been saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('payments')->where('treatment_reference_id', $request->get('id'))->delete();
        
        return redirect('/payments/logs')->with('status', 'Payment has been deleted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = DB::table('payments')
                        ->where('payments.id', $id)
                        ->get();

        return json_encode($payment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $payment = Payment::find($request->get('payment_id'));
        $treatments = Treatment::where('reference_id', $request->get('treatment_reference_id'))->get();
        $treatment = Treatment::find($treatments[0]->id);

        $balance = $treatment->balance - $request->get('amount');

        if ($balance < 0) {
            return redirect('/payments/logs')->with('err', 'The amount you are trying to change is invalid!');
        } else if ($balance == 0) {
            $treatment->payment_status_id = '1'; 
        } else {
            $treatment->payment_status_id = '0'; 
        }

        $treatment->balance = $balance;
        $treatment->save();

        $payment->amount = $request->get('amount');
        $payment->balance = $balance;
        $payment->save();

        return redirect('/payments/logs')->with('status', 'Changes have been saved.');
    }

}
