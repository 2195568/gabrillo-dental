<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class Admin
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        if(!Auth::check()) {
            return redirect('/login');
        } else {
            // $userHasRoleAdmin = Auth::user()->user_role_id <= 2;
            if(Auth::user()->user_role_id <= 2)
            {
                return $next($request);
            } else {
                return response('Unauthorized ka.', 401);
            }
        }
    }
}
