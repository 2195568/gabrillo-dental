<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'date', 'time_start', 'time_end', 'created_at', 'updated_at', 'patient_id', 'procedures']; 

    /**
     * Patient that owns the appointment.
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
}