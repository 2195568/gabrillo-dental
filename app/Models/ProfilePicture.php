<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfilePicture extends Model
{
    use HasFactory;

    /**
     * Get the patient that owns the profile picture.
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
}