<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    protected $guarded = ['id']; 
    
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function futureAppointments()
    {
        return $this->hasMany(Appointment::class)->whereDate('date', '>=', date("Y-m-d"));
    }

    public function treatments()
    {
        return $this->hasMany(Treatment::class);
    }
}