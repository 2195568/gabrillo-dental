<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{
    use HasFactory;

    public function treatments()
    {
        return $this->belongsToMany(Treatment::class);
    }

    public function category()
    {
        return $this->belongsTo(ProcedureCategory::class);
    }
    public function treatment()
    {
        return $this->belongsTo(ProcedureTreatment::class);
    }
}
