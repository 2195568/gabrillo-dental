// Change the active item on the main navigation  
function toggleMainNavActiveClass(htmlId) {
    $(htmlId).on("click", function() {
        $("li a").removeClass("active");
        $(this).addClass("active");
        $.ajax()
    });
}

function mainNavOnclick(url) {
    $.ajax({
        type: "get",
        url: url,
        success: function(html) {
            $("#content").html(html);
        }
    });

    if (url.includes('appointment')) {
        history.replaceState('data to be passed', 'Title of the page', '/appointments');
        $("#appointmentsNavItem").addClass("active");
        // $("li #appointmentsNavItem").on("click", function() {
        //     $.ajax()
        // });
    } else if (url.includes('dashboard')) {
        history.replaceState('data to be passed', 'Title of the page', '/dashboard');
        toggleMainNavActiveClass("#dashboardNavItem");
    }
}

$(document).ready(function() {
    $("#content").fadeIn("slow", 0.33);

    // // Search patients
    // $('#patient-input').keyup(function(e) {
    //     console.log('en')
    //     var query = $(this).val();

    //     if (!(query == '')) {
    //         $('#patientSearchResults').empty();

    //         $.ajax({
    //             url: '/patients/search',
    //             method: 'post',
    //             data: {
    //                 "_token": $('meta[name="csrf-token"]').attr('content'),
    //                 "query": query
    //             },
    //             dataType: 'json',
    //             success: function(data) {
    //                 if (data.length > 0) {
    //                     $('#patientSearchResults').addClass('show');

    //                     let html = "<p class='pl - 3 text - muted '>Search results for \"" + query + "\"" + ":</p>";

    //                     for (let i = 0; i < data.length; i++) {
    //                         html += "<a id='" + data[i].id + "' class='dropdown-item' href='#'>" + data[i].first_name + " " + data[i].last_name + "</a>";
    //                     }

    //                     $('#patientSearchResults').html(html);
    //                 } else {
    //                     $('#patientSearchResults').addClass('show');
    //                     let html = "<p class='pl - 3 text - muted '>No search results for \"" + query + "\"" + ".</p>";
    //                     $('#patientSearchResults').html(html);
    //                 }
    //             }
    //         })
    //     }
    // })

    // $('#patient-input').focusout(function(e) {
    //     $(".dropdown-item").click(function(e) {
    //         $('#patient-input').val($(this).text());
    //         $('#patientAppointmentId').val($(this).attr('id'));
    //         $('#patientSearchResults').removeClass('show');

    //     })
    // })

    document.addEventListener('DOMContentLoaded', function() {
        /* 1/3. create reference DOM Element 
        The DOM node used as reference of the tooltip (it can be a jQuery element).
        */
        var referenceElement = document.getElementById("hello-world");
        /* 2/3. Create a new Tooltip.js instance */
        var instance = new Tooltip(referenceElement, {
            title: referenceElement.getAttribute('data-tooltip'),
            trigger: "hover",
            placement: "top",
            /* more option her */
            /* #list of options: */
            /* https://popper.js.org/tooltip-documentation.html#new_Tooltip_new*/
        });
        /* 3/3. methods */
        // instance.show();
        // instance.hide();
        // instance.toggle();
        // instance.dispose()
        // instance.updateTitleContent("update title")
    });

    var url = window.location.href;

    // Configure the active item on the main navigation
    if (url.includes('appointment')) {
        $("#appointmentsNavItem").addClass("active");
    } else if (url.includes('dashboard')) {
        $("#dashboardNavItem").addClass("active");;
    } else if (url.includes('treatment')) {
        $("#treatmentsNavItem").addClass("active");;
    } else if (url.includes('patient')) {
        $("#patientsNavItem").addClass("active");;
    } else if (url.includes('payment')) {
        $("#paymentsNavItem").addClass("active");;
    } else if (url.includes('user')) {
        $("#usersNavItem").addClass("active");;
    }

    // Change the active item on the main navigation  
    $("li a").on("click", function() {
        $("li a").removeClass("active");
        $(this).addClass("active");
        $.ajax()
    });

    // Change the active item on the secondary navigation
    $("#appointmentSecondaryNav a").on("click", function() {
        $("#appointmentSecondaryNav a").removeClass("active");
        $(this).addClass("active");
        $.ajax();
    })

    create_custom_dropdowns();
});

// Wait for window load
$(window).load(function() {
    // Animate loader off screen
    $("#loading").fadeOut("slow");
    // $("#content").fadeOut("slow");
});

//