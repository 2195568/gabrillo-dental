$(document).ready(function() {
    // Current path
    url = window.location.href;

    // Fetch patients
    function fetchPatients() {
        if (url.includes('inactive-patients')) {
            getPatients('inactive')
        } else {
            getPatients('active')
        }
    }

    // Renders patient's table
    function renderTable(data) {
        // let html =
        //     '<table class="table ">' +
        //     '<thead>' +
        //     '<th class="text-center" scope="col">Date</th>' +
        //     '<th class="text-center" scope="col">Name</th>' +
        //     '<th class="text-center" scope="col">Treatment ID</th>' +
        //     '<th class="text-center" scope="col">Amount Charged</th>' +
        //     '<th class="text-center" scope="col">Total Payment</th>' +
        //     '<th class="text-center" scope="col">Balance</th>' +
        //     '<th class="text-center" scope="col">Actions</th>' +
        //     '</tr>' +
        //     '</thead>' +
        //     '<tbody id="treatmentBodyTable">';

        // for (i = 0; i < data.length; i++) {
        //     let paymentStatus = '';

        //     if (data[i].payment_status_name == 'paid') {
        //         paymentStatus = "<span class='badge rounded-pill bg-success'>" + data[i].payment_status_name + "</span>"
        //     } else {
        //         paymentStatus = "<span class='badge rounded-pill bg-danger'>" + data[i].payment_status_name + "</span>"
        //     }

        //     html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
        //     html += "<th class='text-center' scope='row'>" + data[i].treatment_date + '</th>';
        //     html += "<td class='text-center'>" + data[i].first_name + ' ' + data[i].last_name + "</td>";
        //     html += "<td class='text-center'>" + data[i].reference_id + "</td>";
        //     html += "<td class='text-center'>" + data[i].treatment_total_amount + "</td>";
        //     html += "<td class='text-center'>" + data[i].total_payment + "</td>";
        //     html += "<td class='text-center'>" + data[i].payment_balance + "</td>";
        //     html += '<td class="text-center"><button id="' + data[i].payment_id + '"class="btn btn-danger btn-sm rounded-0 mr-2" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a></button>';
        //     html += '<button id="' + data[i].payment_id + '" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a></button></td>';
        //     html += '</tr>';
        // }

        // html += '</tbody>';

        // return html;
        return;
    }

    function renderProcedures(data) {
        let html = '<option selected disabled>Select procedure</option>';

        for (i = 0; i < data.length; i++) {
            html += '<option ' + 'id="' + data[i].amount_charged + '" data-procedure-id="' + data[i].id + '">' + data[i].name + '</option>';
        }

        return html;
    }

    // Retrieve patient records from the database
    function getPayments() {
        $.ajax({
            dataType: 'json',
            url: '/payments/logs/index',
            success: function(data) {
                console.log
                let html = renderTable(data);
                $("#main-content").html(html);
            }
        })
    }

    // Retrieve procedures records from the database
    function getProcedures() {
        $.ajax({
            dataType: 'json',
            url: '/procedures/index',
            success: function(data) {
                let html = renderProcedures(data);
                $("#select-procedures").html(html);
                $("#edit-select-procedures").html(html);
            }
        })
    }

    function editPayment(payment) {
        var reference_id = payment.treatment_reference_id,
            amount = payment.amount
        payment_id = payment.id;

        // Edit form values
        $('#edit-payment-id').val(payment_id);
        $('#edit-treatment-reference-input').val(reference_id);
        $('#edit-amount').val(amount);
        $('#editModal').modal('toggle');
    }

    // Edit button on click
    $(document).on('click', ".btn-success", function() {
        // $('#searchResult').addClass('show');
        // $('#searchResult').removeClass('show');
        let id = this.id;

        $.ajax({
            dataType: 'json',
            url: '/payments/' + id + '/show',
            success: function(data) {
                editPayment(data[0]);
            }
        });

    });

    $('#select-procedures').change(function(e) {
        var id = $(this).children(":selected").attr("id");
        var procedure_id = $(this).children(":selected").attr("data-procedure-id");
        $('#total_amount').val(id);
        $('#procedure_id').val(procedure_id);
    })

    $('#edit-select-procedures').change(function(e) {
        var id = $(this).children(":selected").attr("id");
        var procedure_id = $(this).children(":selected").attr("data-procedure-id");
        $('#edit-total_amount').val(id);
        $('#edit-procedure_id').val(procedure_id);
    })

    // Delete payment (button action)
    $(document).on('click', ".btn-danger", function() {
        let id = $(this).attr('id');
        let referenceId = $(this).data('treatment_reference_id');

        Swal.fire({
            title: 'Oops...',
            text: 'Once you delete the log, the payment will automatically be disregarded. Are you sure you want to delete the log?',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            showConfirmButton: true,
            cancelButtonText: 'No',
            confirmButtonText: 'Yes',
        }).then((result) => {
            if (result.value) {
                $('#deleteReferenceId').val(referenceId)
                $('#deletePaymentId').val(referenceId);
                $("#deletePaymentForm")[0].submit();
            }
        });

        return;
    });

    // Search box
    // $('#searchBox').keyup(function(event) {
    //     var query = $(this).val();
    //     console.log(query)

    //     if (!(query == '')) {
    //         $.ajax({
    //             url: '/treatments/search',
    //             method: 'post',
    //             data: {
    //                 "_token": $('meta[name="csrf-token"]').attr('content'),
    //                 "query": query
    //             },
    //             dataType: 'json',
    //             success: function(data) {
    //                 if (data.length) {
    //                     let html = renderTable(data);
    //                     $('#main-content').html(html);
    //                     $('#main-content').prepend('<p>Search results for "' + query + '":</p>');
    //                 } else {
    //                     $('#main-content').html('<p>No search results for "' + query + '".</p>');
    //                 }

    //             }
    //         })
    //     } else {
    //         // Fetch patients
    //         getTreatments();

    //         // Fetch procedures
    //         getProcedures();
    //     }
    // })

    // // Search patients
    // $('#patient-input').keyup(function(e) {
    //     var query = $(this).val();

    //     if (!(query == '')) {
    //         $('#patientSearchResults').empty();

    //         $.ajax({
    //             url: '/patients/search',
    //             method: 'post',
    //             data: {
    //                 "_token": $('meta[name="csrf-token"]').attr('content'),
    //                 "query": query
    //             },
    //             dataType: 'json',
    //             success: function(data) {
    //                 if (data.length > 0) {
    //                     $('#patientSearchResults').addClass('show');

    //                     let html = "<p class='pl - 3 text - muted '>Search results for \"" + query + "\"" + ":</p>";

    //                     for (let i = 0; i < data.length; i++) {
    //                         html += "<a id='" + data[i].id + "' class='dropdown-item' href='#'>" + data[i].first_name + " " + data[i].last_name + "</a>";
    //                     }

    //                     $('#patientSearchResults').html(html);
    //                 } else {
    //                     $('#patientSearchResults').addClass('show');
    //                     let html = "<p class='pl - 3 text - muted '>No search results for \"" + query + "\"" + ".</p>";
    //                     $('#patientSearchResults').html(html);
    //                 }
    //             }
    //         })
    //     }
    // })

    // $('#patient-input').focusout(function(e) {
    //     $(".dropdown-item").click(function(e) {
    //         $('#patient-input').val($(this).text());
    //         $('#patient_id').val($(this).attr('id'));
    //         $('#patientSearchResults').removeClass('show');

    //     })
    // })

    // $('#patient-input').focusout(function(e) {
    //     $(".dropdown-item").click(function(e) {
    //         $('#patient-input').val($(this).text());
    //         $('#patientAppointmentId').val($(this).attr('id'));
    //         $('#patientSearchResults').removeClass('show');

    //     })
    // })

    // Search box
    // $('#searchBox').keyup(function(event) {
    //     var query = $(this).val();

    //     if (!(query == '')) {
    //         $.ajax({
    //             url: '/treatments/search',
    //             method: 'post',
    //             data: {
    //                 "_token": $('meta[name="csrf-token"]').attr('content'),
    //                 "query": query
    //             },
    //             dataType: 'json',
    //             success: function(data) {
    //                 if (data.length) {
    //                     let html = renderTable(data);
    //                     $('#main-content').html(html);
    //                     $('#main-content').prepend('<p>Search results for "' + query + '":</p>');
    //                 } else {
    //                     $('#main-content').html('<p>No search results for "' + query + '".</p>');
    //                 }

    //             }
    //         })
    //     } else {
    //         // Fetch patients
    //         getPayments();

    //         // Fetch procedures
    //         getProcedures();
    //     }
    // })


    // Fetch patients
    getPayments();

    // Fetch procedures
    getProcedures();
})