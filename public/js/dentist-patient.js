Colors = {};
Colors.names = {
    aqua: "00ffff",
    azure: "f0ffff",
    beige: "f5f5dc",
    black: "000000",
    blue: "0000ff",
    brown: "a52a2a",
    cyan: "00ffff",
    fuchsia: "ff00ff",
    gold: "ffd700",
    green: "008000",
    indigo: "4b0082",
    khaki: "f0e68c",
    lightblue: "add8e6",
    lightcyan: "e0ffff",
    lightgreen: "90ee90",
    lightgrey: "d3d3d3",
    lightpink: "ffb6c1",
    lightyellow: "ffffe0",
    lime: "00ff00",
    magenta: "ff00ff",
    maroon: "800000",
    navy: "000080",
    olive: "808000",
    orange: "ffa500",
    pink: "ffc0cb",
    purple: "800080",
    violet: "800080",
    red: "ff0000",
    silver: "c0c0c0",
    white: "ffffff",
    yellow: "ffff00"
};

Colors.random = function() {
    var result;
    var count = 0;
    for (var prop in this.names)
        if (Math.random() < 1 / ++count)
            result = prop;
    return { name: result, rgb: this.names[result] };
};

$(document).ready(function() {
    // Current path
    url = window.location.href;

    // Fetch patients
    function fetchPatients() {
        if (url.includes('inactive-patients')) {
            getPatients('inactive')
        } else {
            getPatients('active')
        }
    }

    function resetTreatmentHistoryToothNoButtons() {
        $('.new-treatment-tooth-no-btn').removeClass('btn-warning');
    }

    // Renders treatment history table
    function renderTreatmentHistoryTable(data) {
        let html =
            '<table class="table ">' +
            '<thead>' +
            '<tr class="p-3 mb-5 rounded">' +
            '<td class="text-center">Treatment ID</td>' +
            '<td class="text-center">Name</td>' +
            '<td class="text-center">Date</td>' +
            '<td class="text-center">Tooth No</td>' +
            '<td class="text-center">Procedure/s</td>' +
            '<td class="text-center">Total Amount</td>' +
            // '<td class="text-center">Action</td>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="treatmentBodyTable">';


        if (data.length > 0) {
            for (i = 0; i < data.length; i++) {
                html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
                html += "<td class='text-center'>" + data[i].reference_id + "1</td>";
                html += "<th class='text-center' scope='row'>" + data[i].patient.first_name + ' ' + data[i].patient.last_name + '</th>';
                html += "<td class='text-center'>" + data[i].date + "</td>";
                html += "<td class='text-center'>" + data[i].tooth_no + "</td>";
                // console.log(data[i][0].reference_id)

                $("button:contains(" + data[i].tooth_no + ")").removeClass('btn-light').addClass('btn-warning').attr('disabled', false);

                let procedures = '';


                // console.log(data[i][0].procedures[0].name)

                // for (k = 0; i < data[i][0].procedures.length; k++) {
                //     console.log(data[i][0].procedures)
                //     if (k == data[i][0].procedures.length) {
                //         // procedures += data[i][0].procedures[k].name + '';
                //     } else {
                //         // procedures += data[i][0].procedures[k].name + ', ';
                //     }
                // }

                for (let val of data[i].procedures) {
                    procedures += ' - ' + val.name + '<br>';
                }

                // console.log(procedures)

                procedures = $.trim(procedures);


                html += "<td class='text-center'><div>" + procedures + "</div></td>";
                html += "<td class='text-center'>" + data[i].total_amount + "</td>";
                // html += '<td class="text-center"><button id="' + data[i][0].id + '"class="btn btn-danger btn-sm rounded-0 mr-2" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a></button>';
                // html += '<button id="' + data[i][0].id + '" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a></button></td>';
                // html += '</tr>';
            }
            html += '</tbody></table>';
        } else {
            html = '<div class="d-flex justify-content-center"><div class="d-flex flex-column justify-content-center"><div class="d-flex flex-column justify-content-center pl-3"><img src="img/no-data.png" style="height: 500px;"></div></div></div>';
            return html;
        }

        return html;
    }

    function getTreatmentHistory(patient_id, tooth_number = 'all') {
        $.ajax({
            dataType: 'json',
            url: '/treatments/' + patient_id + '/' + tooth_number + '/getTreatments',
            success: function(data) {
                let html = renderTreatmentHistoryTable(data);
                $("#treatmentsHistoryContainer").html(html);
            }
        })
    }

    // Renders patient's table
    function renderTable(data) {
        if (data.length > 0) {
            let html =
                '<table class="table ">' +
                '<thead>' +
                '<tr class="p-3 mb-5 rounded">' +
                '<td class="pl-3">Name</td>' +
                '<td>Contact No</td>' +
                '<td>Address</td>' +
                '<td>New Appointment</td>' +
                '<td>Last Appointment</td>' +
                '<td>Action</td>' +
                '</tr>' +
                '</thead>' +
                '<tbody id="patientBodyTable">';

            for (i = 0; i < data.length; i++) {
                let new_appointment = data[i].new_appointment;
                let last_appointment = data[i].last_appointment;
                let created_at = new Date(data[i].created_at);

                if (new_appointment === null) {
                    new_appointment = '<span class="text-center">-</span>';
                }

                if (last_appointment === null) {
                    last_appointment = '<span class="text-center">-</span>';
                }

                html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
                html += "<th scope='row'><img class='h-8 w-8 rounded-full object-cover' style='float: left;' src='https://ui-avatars.com/api/?name=" + data[i].first_name + '+' + data[i].last_name + "&background=" + data[i].profile_bg_color + "&bold=true' alt='Admin'><span class='th-title ml-2 text-capitalize'>" + data[i].first_name + ' ' + data[i].last_name + ' </span>' + '<u><em>    ' + data[i].slug + '</em></u><br>';
                html += "<span class='td-email ml-2'>" + data[i].email + '</span></th>';
                html += "<td>" + data[i].contact_no + "</td>";
                html += "<td>" + data[i].address + "</td>";
                html += "<td>" + new_appointment + "</td>";
                html += "<td>" + last_appointment + "</td>";
                html += '<td><button id="' + data[i].id + '"class="btn btn-view btn-sm rounded-0 mr-3" style="background-color: #7764CA; color: white;" type="button" data-toggle="tooltip" data-placement="top" title="View patient"><i class="fa fa-user-circle"></i></a></button>';
                html += '</td>';
                html += '</tr>';
            }

            html += '</tbody>';

            return html;
        }
    }

    // Retrieve patient records from the database
    function getPatients(accountStatus) {
        var url = '';

        if (accountStatus == 'active') {
            url = '/patients/active';
        } else {
            url = '/patients/inactive';
        }

        $.ajax({
            dataType: 'json',
            url: url,
            success: function(data) {
                // let html = renderTable(data);
                // $("#main-content").html(html);
            }
        })
    }

    function editPatient(patient) {
        var firstName = patient.first_name,
            lastName = patient.last_name,
            middleName = patient.middle_name,
            birthDate = patient.birth_date,
            age = patient.age,
            contactNo = patient.contact_no,
            email = patient.email,
            address = patient.address,
            occupation = patient.occupation,
            allergies = patient.allergies,
            gender = patient.gender,
            id = patient.id;

        console.log(lastName)

        // Delete form values
        $('#deactivatePatientId').val(id);
        $('#deactivatePatientName').val(firstName + ' ' + lastName);

        // Edit form values
        $('#editPatientId').val(id);
        $('#edit_first_name').val(firstName).prop('readonly', 'true');
        $('#edit_last_name').val(lastName);
        $('#edit_middle_name').val(middleName);
        $('#edit_birth_date').val(birthDate).prop('readonly', 'true');
        $('#edit_age').val(age).prop('readonly', 'true');;
        $('#edit_contact_number').val(contactNo);
        $('#edit_email_address').val(email);
        $('#edit_home_address').val(address);
        $('#edit_occupation').val(occupation);
        $('#edit_allergies').val(allergies);
        $('#edit_gender').val(gender).prop('readonly', 'true');
        $('#editModal').modal('toggle');
    }

    function viewPatient(patient) {
        var firstName = patient.first_name,
            lastName = patient.last_name,
            email = patient.email,
            balance = patient.balance,
            gender = patient.gender,
            birthDate = patient.birth_date,
            address = patient.address,
            age = patient.age,
            contactNo = patient.contact_no,
            occupation = patient.occupation,
            allergies = patient.allergies,
            registeredDate = patient.created_at,
            id = patient.id,
            profile_bg_color = patient.profile_bg_color,
            patientStatus = '';

        if (patient.account_status == 'active') {
            patientStatus = '<span class = "badge rounded-pill bg-success"> Active </span>';
        } else {
            patientStatus = '<span class="badge rounded-pill bg-danger">Inactive</span>';
        }

        // Edit form values
        // 
        $('#view_patient_img').html('<img class="img-responsive rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + firstName + '+' + lastName + '&amp;background=' + profile_bg_color + '&amp;bold=true" alt="User profile image">');
        $('#view_patient_name').text(firstName + ' ' + lastName);
        $('#view_patient_email').text(email);
        $('#view_patient_balance').text(balance);
        $('#view_patient_gender').text(gender);
        $('#view_patient_birthday').text(birthDate);
        $('#view_patient_phone_no').text(contactNo);
        $('#view_patient_address').text(address);
        $('#view_patient_age').text(age);
        $('#view_patient_allergies').text(allergies);
        $('#view_patient_occupation').text(occupation);
        $('#view_patient_patient_status').html(patientStatus);
        $('#view_patient_registered_date').text(registeredDate);
        $('#viewModal').modal('toggle');
    }

    // Retrieve procedures records from the database
    function getProcedures() {
        $.ajax({
            dataType: 'json',
            url: '/procedures/index',
            success: function(data) {
                let html = renderProcedures(data);
                $(".procedures_container").html(html);
                $("#edit-select-procedures").html(html);
            }
        })
    }

    function renderProcedures(data) {
        let html = '';
        let currentCategoryId = '';

        for (i = 0; i < data.length; i++) {
            if (currentCategoryId == '') {
                currentCategoryId = data[i].procedure_category_id;
                html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend><hr>';
            }

            if (data[i].procedure_category_id == currentCategoryId) {
                html += '<div class="form-check"><input class="form-check-input" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>'
            } else {
                html += '</fieldset></div>';
                html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend>';
                html += '<hr><div class="form-check"><input class="form-check-input" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>';

                currentCategoryId = data[i].procedure_category_id;
            }
        }

        html += '</div>';

        // let html = '<fieldset><legend>' + data.category + '</legend> <hr> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="1"> <label class="form-check-label" for="flexCheckDefault"> Tooth Extraction </label> </div> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="Pasta"> <label class="form-check-label" for="flexCheckChecked"> Pasta </label> </div> </fieldset>';

        // for (i = 0; i < data.length; i++) {
        //     html += '<option ' + 'id="' + data[i].amount_charged + '" data-procedure-id="' + data[i].id + '">' + data[i].name + '</option>';
        // }

        return html;
    }

    // Renders search result for patient
    function renderPatientSearchResult(data) {
        let html =
            '<table id="patientTableResult" class="table patientTableResult">' +
            '<thead>' +
            '<tr>' +
            '<td></td>' +
            '<td></td>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="patientTableBody">';


        for (i = 0; i < data.length; i++) {
            html += "<tr id='patientTableRow-" + data[i].id + "' class='shadow-sm p-3 mb-5 bg-white rounded'>";
            html += "<th class='patientResultTh' role='button' scope='row'><img class='h-8 w-8 rounded-full object-cover' style='float: left;' src='https://ui-avatars.com/api/?name=" + data[i].first_name + '+' + data[i].last_name + "&background=random&bold=true' alt='Admin'><span class='th-title ml-2 text-capitalize'>" + data[i].first_name + ' ' + data[i].last_name + '</span><u><em>    ' + data[i].id + '</em></u><br>';
            html += "<span class='td-email ml-2'>" + data[i].email + '</span></th>';
            html += '<td><span class="d-flex justify-content-end"><button type="button" class="btn btn-light select-btn" data-patient-id="' + data[i].id + '">Select</button></span></td>';
            html += '</tr>';
        }

        html += '</tbody>';


        return html;
    }

    $('#addPaymentButton').click(function() {
        $('#viewModal').modal('hide');
    })


    // Add submit button ON CLICK 

    //changes dito
    $(document).on('click', "#add_submit_button", function(event) {
        document.getElementById("date").required = true;

        var valueDate = $('#date').val();

        if (!Date.parse(valueDate)) {
            alert('Please Select Date');
        } else {

            for (var i = 0; i < selectedItems[0].length; i++) {
                $.ajax({
                    dataType: 'json',

                    url: '/treatments/' + selectedItems[0][i] + '/' + selectedItems[1][i] + '/' + selectedItems[2][i] + '/' + $('#date').val() + '/' + patientID + '/save',
                    success: function(data) {
                        console.log('saved');
                    }
                });
                location.reload();
            }

        }


    });

    // Select button on patient search results
    $(document).on('click', ".select-btn", function(event) {
        let patientId = $(this).attr('data-patient-id');
        let patientTableRow = $('#patientTableRow-' + patientId)[0].outerHTML;
        $('#patientTableBody').html(patientTableRow);
        $('#patientSearchBox').hide();
        $('#editPatientSearchBox').hide();
        $('#patientTableBody').find('button').replaceWith('<button type="button" class="btn-close btn-close-td" aria-label="Close"></button>');
        $('#searchTag').hide();
        $('#patientTableResult').find('thead').remove();
        $('#patientTableResult').find('button').removeClass('select-btn');

        $('#add_patient_id').val(patientId);
        $('#edit_patient_id').val(patientId);
    });

    $('#patientSearchBox').keyup(function(event) {
        var query = $(this).val();

        if (!(query == '')) {
            $.ajax({
                url: '/patients/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query,
                    "status": "active"
                },
                dataType: 'json',
                success: function(data) {
                    if (data.length) {
                        let html = renderPatientSearchResult(data);
                        $('#patientSearchResultBox').html(html);
                        $('#patientSearchResultBox').prepend('<p id="searchTag">SEARCH</p>');
                    } else {
                        $('#patientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
                    }

                }
            })
        } else {
            $('#patientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
        }
    })



    //Delete Selected
    $("#selected_table").on('click', '#btnDelete', function() {
        var $amnt = $(this).closest("tr")
            .find(".nr")
            .text();
        document.getElementById("total_amount").value = document.getElementById("total_amount").value - $amnt
        $(this).closest('tr').remove();


    });

    //changes dito
    //eto yung mga lumalabas pag may ciniclick na mga procedures AT treatments
    var selectedItems = [
        [],
        [],
        []
    ];


    /////////////////////////////////////////////////////////////////////inedit ko
    // Tooth number buttons
    $(".btn-purple").click(function(event) {



        let tooth_no = $(this).text();
        $('#add_tooth_no').val(tooth_no);
        $('#edit_tooth_no').val(tooth_no);
        $('.btn-clicked').addClass('btn-purple') //.removeClass('btn-clicked');
            //   $(this).removeClass('btn-purple');
        $(this).addClass('btn-clicked');

        var x = document.getElementById("editPatientSearchBox");


        var tbody = $('#selected_table').children('tbody');
        var table = tbody.length ? tbody : $('#selected_table');
        var items = new Array();
        var selected = new Array();
        var tls = new Array();

        $('.form-check-input:checkbox:checked').each(function() {
            selected.push(this.value);
            $('.btn-clicked').addClass('btn-purple') //.removeClass('btn-clicked');
        });



        //var tdContent = $(this).text();
        for (i = 1; i < document.getElementById('selected_table').rows.length; i++) {
            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
            var objCells = document.getElementById('selected_table').rows.item(i).cells;
            // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
            tls.push(objCells.item(0).innerHTML);
            //  console.log(tooth_num);
        }

        var ls = ['11', '12', '13', '14', '15', '16', '17', '18', '21', '22', '23', '24', '25', '26', '27', '28', '41', '42', '43', '44', '45', '46', '47', '48', '31', '32', '33', '34', '35', '36', '37', '38'];

        var person;
        for (i = 0; i < ls.length; i++) {
            j = ls.indexOf(tooth_no); //3
            k = tls.indexOf(tooth_no); //0
            if (tooth_no == ls[j]) {
                //naremove si 14
                ls.splice(j, 1);
                //na add si 14
                tls.push(tooth_no);
                //show data ni 14  
                let findDuplicates = arr => arr.filter((item, index) => arr.indexOf(tooth_no) != index)
                duplicateElementa = findDuplicates(tls);
                console.log(duplicateElementa);
                if (tls[k] != tooth_no) {
                    console.log(tls);

                    for (i = 0; i < selected.length; i++) {
                        $.ajax({
                            dataType: 'json',
                            url: '/treatments/' + selected[i] + '/procedures',
                            type: 'get',
                            success: function(data) {
                                items.push(data[0].name);

                                sleLength = items.length;
                                if (selected.length == sleLength) {
                                    console.log("umabot");

                                    //changes dito
                                    selectedItems[0].push(document.getElementById('add_tooth_no').value);
                                    selectedItems[1].push(data[0].id);
                                    selectedItems[2].push(document.getElementById('selected_total_amount').value);
                                    table.append(
                                        '<tr scope="row">' +

                                        '<td>' + document.getElementById('add_tooth_no').value + '</td>' +
                                        '<td>' + items + '</td>' +
                                        '<td class = "nr">' + document.getElementById('selected_total_amount').value + '</td>' +
                                        '<td>' + "<button id= 'btnDelete' class='btn btn-danger'>  Remove  </button>" + '</td>' +
                                        '</tr>'
                                    )
                                    document.getElementById("selected_total_amount").value = 0

                                    $('.form-check-input:checkbox').removeAttr('checked');
                                    $('.btn-clicked').addClass('btn-purple') //.removeClass('btn-clicked');
                                    $(this).removeClass('btn-purple');
                                }
                            }
                        });

                    };
                } else {
                    alert('Tooth Number already selected')
                    break;


                }


                break;
            }


            // $.ajax({
            //     dataType: 'json',
            //    url: '/treatments/'+selectedItems+'/store',
            //    type:'get',
            //    success: function(data) {         
            //         console.log("eto yung request");
            //       console.log(data);


            //    }
            // });

            //document.getElementById("selected_total_amount").value = "" 
        }


        console.log('My array');
        console.log(selectedItems);
    });




    //////////////////////////////////////////////////////////////////////////////



    // Procedure upon clicking
    $(document).on('click', ".form-check-input", function() {

        let procedureAmountCharged = $(this).attr('data-amount-charged');
        let selected_total = $('#selected_total_amount').val();
        let total_amount_val = $('#total_amount').val();
        if ($(this).prop('checked') == true) {
            console.log('check');
            $('#selected_total_amount').val(+procedureAmountCharged + +selected_total);
            $('#total_amount').val(+procedureAmountCharged + +total_amount_val);
            $('#add_total_amount').val(+procedureAmountCharged + +total_amount_val);
            $('#edit_total_amount_display').val(+procedureAmountCharged + +total_amount_val);
            $('#edit_total_amount').val(+procedureAmountCharged + +total_amount_val);
        } else if ($(this).prop('checked') == false) {
            console.log('uncheck');
            $('#selected_total_amount').val(-procedureAmountCharged - -selected_total);
            $('#total_amount').val(-procedureAmountCharged - -total_amount_val);
            $('#add_total_amount').val(-procedureAmountCharged - -total_amount_val);
            $('#edit_total_amount_display').val(-procedureAmountCharged - -total_amount_val);
            $('#edit_total_amount').val(-procedureAmountCharged - -total_amount_val);
        }
        // console.log($('#total_amount').val(+procedureAmountCharged + +total_amount_val));

    });

    // Tooth number buttons for treatment history
    $(".treatment-history-tooth-btn").click(function() {
        let tooth_no = $(this).text();
        let patient_id = $('#viewTreatmentHistoryButton').data('patient_id');
        getTreatmentHistory(patient_id, tooth_no)
    });

    $('#addPaymentButton').click(function() {
        $('#viewModal').modal('hide');
    })

    $('#viewTreatmentHistoryButton').click(function() {
        let id = $('#viewTreatmentHistoryButton').data('patient_id');
        getTreatmentHistory(id);
        $('#viewModal').modal('hide');
    })

    $('#plus-button').click(function() {
        var color = Colors.random();
        $('#profile_bg_color').val(color.rgb);
    })

    $('#addTreatmentButton').click(function() {
        var first_name = $(this).data('first-name');
        var last_name = $(this).data('last-name');
        var email = $(this).data('email');
        $('#patientSearchResultBox').html(
            '<table id="patientTableResult" class="table patientTableResult"><tbody id="patientTableBody"><tr id="patientTableRow-3" class="shadow-sm p-3 mb-5 bg-white rounded"><th class="patientResultTh" role="button" scope="row"><img class="h-8 w-8 rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + first_name + '' + last_name + '&amp;background=random&amp;bold=true"><span class="th-title ml-2 text-capitalize">' + first_name + '' + last_name + '</span><br><span class="td-email ml-2">' + email + '</span></th><td><span class="d-flex justify-content-end"><button type="button" class="btn-close btn-close-td" aria-label="Close"></button></span></td></tr></tbody></table>'
        );
        $('#viewModal').modal('hide');
    })

    $(document).on('click', ".btn-close-td", function(event) {
        $('#editPatientSearchBox').show();
        $('#patientSearchBox').show();
        $('#patientSearchBox').val('');
        $('#editPatientSearchBox').val('');
        // $('#searchTag').show();
        $('#patientTableBody').remove();

    });

    // View button on click
    $(document).on('click', ".btn-view", function() {
        // $('#searchResult').addClass('show');
        // $('#searchResult').removeClass('show');
        let id = this.id;
        $('#addTreatmentButton').data('patient-id', id);
        $('#add_patient_id').val(id);
        $('#viewTreatmentHistoryButton').data('patient_id', id);

        $.ajax({
            dataType: 'json',
            url: '/get-patient/' + id,
            success: function(data) {
                $('#addTreatmentButton').data('first-name', data.first_name);
                $('#addTreatmentButton').data('last-name', data.last_name);
                $('#addTreatmentButton').data('email', data.email);
                viewPatient(data);
            }
        });

    });

    // Edit button on click
    $(document).on('click', ".btn-success", function() {
        // $('#searchResult').addClass('show');
        // $('#searchResult').removeClass('show');
        let id = this.id;

        $.ajax({
            dataType: 'json',
            url: '/get-patient/' + id,
            success: function(data) {
                editPatient(data);
            }
        });

    });

    // Delete appointment (button action)
    $("#deactivatePatientButton").click(function() {
        $("#deactivatePatientForm")[0].submit();
    })

    // Search box
    $('#searchBox').keyup(function(event) {
        var query = $(this).val();

        if (!(query == '')) {
            $.ajax({
                url: '/patients/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query,
                    "status": ""
                },
                dataType: 'json',
                success: function(data) {
                    if (data.length) {
                        let html = renderTable(data);
                        $('#main-content').html(html);
                        $('#main-content').prepend('<p>Search results for "' + query + '":</p>');
                    } else {
                        $('#main-content').html('<p>No search results for "' + query + '".</p>');
                    }

                }
            })
        } else {
            fetchPatients();
        }
    })


    // Fetch patients
    fetchPatients();

    getProcedures();
})