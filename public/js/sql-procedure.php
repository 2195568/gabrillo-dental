<!-- COSMETIC RESTORATION -->
INSERT INTO procedures 
    (name, description, amount_charged, procedure_category_id)
VALUES
    ('Direct Composite Veneer', '...', '3000.00', 1),
    ('Direct Composite Class IV', '...', '2000.00', 1),
    ('Diastema Closure (Bonding)', '...', '2000.00', 1),
    ('Ceramic/Porcelain Veneer', '...', '15000.00', 1);

<!-- JACKET CROWNS -->
INSERT INTO procedures 
    (name, description, amount_charged, procedure_category_id)
VALUES
    ('Porcelain fused to Non-precious metal', '...', '8000.00', 2),
    ('Porcelain fused to Gold', '...', '20000.00', 2),
    ('All Ceramic Crown (Emax)', '...', '18000.00', 2),
    ('Zirconia', '...', '20000.00', 2),
    ('Composite Crown', '...', '6000.00', 2),
    ('Plastic Crown', '...', '2500.00', 2),
    ('Full Metal Crown', '...', '5000.00', 2),
    ('Full Metal Crown for Pediatric Patients', '...', '3500.00', 2);

<!-- CEMENTATION -->
INSERT INTO procedures 
    (name, description, amount_charged, procedure_category_id)
VALUES
    ('Glass Ionomer', '...', '800.00', 2),
    ('Resin Cement', '...', '1500.00', 2),
    ('Temporary Cement Non Eugenol()', '...', '400.00', 2);

<!-- ENDODONTIC TREATMENT -->
INSERT INTO procedures 
    (name, description, amount_charged, procedure_category_id)
VALUES
    ('Root Canal Theraphy', '...', '7000.00', 3),
    ('Pulpotomy', '...', '3000.00', 3),
    ('Post and Core', '...', '8000.00', 3);

<!-- PROSTHETIC PROCEDURES -->
INSERT INTO procedures 
    (name, description, amount_charged, procedure_category_id)
VALUES
    ('Denture Repair', '...', '300.00', 4),
    ('Denture Reline', '...', '2500.00', 4),
    ('Soft Reline', '...', '3000.00', 4),
    ('Denture Teeth Replacement', '...', '800.00', 4),
    ('Pulpotomy', '...', '3000.00', 4),
    ('Post and Core', '...', '8000.00', 4);

INSERT INTO procedure_categories
    (name, description)
VALUES
    ('Cosmetic restoration', '...'),
    ('Jacket crowns', '...'),
    ('Prosthetic procedures', 'Preprosthetic surgery is the term used for any procedure related to either hard/soft tissue correction or augmentation before prosthetic treatment.'),
    ('Surgical procedures', 'The act of performing surgery may be called a surgical procedure, operation, or simply "surgery". In this context, the verb "operate" means to perform surgery.'),
    ('Other resto. services', '...');