const form = document.getElementById('form');
const desc = document.getElementById('description');
const datepicker = document.getElementById('datepicker');
const stime = document.getElementById('stime');
const otime = document.getElementById('otime');

const patientFirstName = document.getElementById('patientFirstName');
const patientLastName = document.getElementById('patientLastName');
const patientMiddleName = document.getElementById('patientMiddleName');
const patientBirthDate = document.getElementById('patientBirthDate');
const patientAge = document.getElementById('patientAge');
const patientGender = document.getElementById('patientGender');
const patientContactNumber = document.getElementById('patientContactNumber');
const patientEmailAddress = document.getElementById('patientEmailAddress');
const patientHomeAddress = document.getElementById('patientHomeAddress');
const patientOccupation = document.getElementById('patientOccupation');
const patientAllergies = document.getElementById('patientAllergies');

Colors = {};
Colors.names = {
    aqua: "00ffff",
    azure: "f0ffff",
    beige: "f5f5dc",
    black: "000000",
    blue: "0000ff",
    brown: "a52a2a",
    cyan: "00ffff",
    fuchsia: "ff00ff",
    gold: "ffd700",
    green: "008000",
    indigo: "4b0082",
    khaki: "f0e68c",
    lightblue: "add8e6",
    lightcyan: "e0ffff",
    lightgreen: "90ee90",
    lightgrey: "d3d3d3",
    lightpink: "ffb6c1",
    lightyellow: "ffffe0",
    lime: "00ff00",
    magenta: "ff00ff",
    maroon: "800000",
    navy: "000080",
    olive: "808000",
    orange: "ffa500",
    pink: "ffc0cb",
    purple: "800080",
    violet: "800080",
    red: "ff0000",
    silver: "c0c0c0",
    white: "ffffff",
    yellow: "ffff00"
};

Colors.random = function() {
    var result;
    var count = 0;
    for (var prop in this.names)
        if (Math.random() < 1 / ++count)
            result = prop;
    return { name: result, rgb: this.names[result] };
};

function checkInputs() {
    // trim to remove the whitespaces
    const patientFirstNameValue = patientFirstName.value.trim();
    const patientLastNameValue = patientLastName.value.trim();
    const patientMiddleNameValue = patientMiddleName.value.trim();
    const patientBirthDateValue = patientBirthDate.value.trim();
    const patientAgeValue = patientAge.value.trim();
    const patientGenderValue = patientGender.value.trim();
    const patientContactNumberValue = patientContactNumber.value.trim();
    const patientEmailAddressValue = patientEmailAddress.value.trim();
    const patientOccupationValue = patientOccupation.value.trim();
    const patientHomeAddressValue = patientHomeAddress.value.trim();
    const patientAllergiesValue = patientAllergies.value.trim();

    const descValue = desc.value.trim();
    const datepickerValue = datepicker.value.trim();
    const stimeValue = stime.value.trim();
    const otimeValue = otime.value.trim();

    var add = 0;

    const regex = /^[a-zA-Z ]+$/;

    if (patientFirstNameValue === '') {
        setErrorFor(patientFirstName, 'First Name cannot be blank!');
    } else if (!(patientFirstNameValue.match(regex))) {
        setErrorFor(patientFirstName, 'It must not contain numbers!')
    } else if (patientFirstNameValue.length <= 1) {
        setErrorFor(patientFirstName, 'Invalid Input!')
    } else {
        setSuccessFor(patientFirstName);
        add++;
    }

    if (patientLastNameValue === '') {
        setErrorFor(patientLastName, 'Last Name cannot be blank!');
    } else if (!(patientLastNameValue.match(regex))) {
        setErrorFor(patientLastName, 'It must not contain numbers!')
    } else if (patientLastNameValue.length <= 1) {
        setErrorFor(patientLastName, 'Invalid Input!')
    } else {
        setSuccessFor(patientLastName);
        add++;
    }

    if (patientMiddleNameValue === '') {
        setErrorFor(patientMiddleName, 'Middle Name cannot be blank!');
    } else if (!patientMiddleNameValue.match(regex)) {
        setErrorFor(patientMiddleName, 'It must not contain numbers!')
    } else if (patientMiddleNameValue.length <= 1) {
        setErrorFor(patientMiddleName, 'Invalid Input!')
    } else {
        setSuccessFor(patientMiddleName);
        add++;
    }

    if (patientBirthDateValue === '') {
        setErrorFor(patientBirthDate, 'Birth Date cannot be blank!');
    } else if (patientBirthDateValue.length <= 5) {
        setErrorFor(patientBirthDate, 'Invalid Input');
    } else {
        setSuccessFor(patientBirthDate);
        add++;
    }

    if (patientAgeValue === '') {
        setErrorFor(patientAge, 'Age cannot be blank!');
    } else {
        setSuccessFor(patientAge);
        add++;
    }

    if (patientGenderValue == '') {
        setErrorFor(patientGender, 'Gender cannot be blank!');
    } else {
        setSuccessFor(patientGender);
        add++;
    }

    if (patientContactNumberValue === '') {
        setErrorFor(patientContactNumber, 'Contact Number cannot be blank!');
    } else if (patientContactNumberValue.length <= 10) {
        setErrorFor(patientContactNumber, 'Length of number Invalid!');
    } else {
        setSuccessFor(patientContactNumber);
        add++;
    }

    if (patientEmailAddressValue === '') {
        setErrorFor(patientEmailAddress, 'Email cannot be blank!');
    } else if (!isEmail(patientEmailAddressValue)) {
        setErrorFor(patientEmailAddress, 'Invalid Email!')
    } else if (patientEmailAddressValue.length <= 6) {
        setErrorFor(patientEmailAddress, 'Invalid Email!')
    } else {
        setSuccessFor(patientEmailAddress);
        add++;
    }

    if (patientHomeAddressValue === '') {
        setErrorFor(patientHomeAddress, 'Address cannot be blank!');
    } else {
        setSuccessFor(patientHomeAddress);
        add++;
    }

    if (patientAllergiesValue === '') {
        setErrorFor(patientAllergies, 'Allergies cannot be blank!');
    } else if (patientAllergiesValue.length <= 1) {
        setErrorFor(patientAllergies, 'Invalid Input!');
    } else {
        setSuccessFor(patientAllergies);
        add++;
    }


    if (descValue === '') {
        setErrorFor(desc, 'Description cannot be blank!');
    } else if (descValue.length <= 3) {
        setErrorFor(desc, 'Invalid Input!');
    } else {
        setSuccessFor(desc);
        add++;
    }


    if (datepickerValue === '') {
        setErrorFor(datepicker, 'Date cannot be blank!');
    } else {
        setSuccessFor(datepicker);
        add++;
    }

    if (stimeValue === '') {
        setErrorFor(stime, 'Invalid!');
    } else {
        setSuccessFor(stime);
        add++;
    }

    if (otimeValue === '') {
        setErrorFor(otime, 'Invalid!');
    } else {
        setSuccessFor(otime);
        add++;
    }

    if (add >= 14) {
        var color = Colors.random();
        $('#profile_bg_color').val(color.rgb);
        $('form#form').submit();
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'You left an invalid input'
        })
    }

}

function setErrorFor(input, message) {
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');
    formControl.className = 'form-con error';
    small.innerText = message;
}


function setSuccessFor(input) {
    const formControl = input.parentElement;
    formControl.className = 'form-con success';
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

function updateAvailableSchedules(data) {
    var appointments = [];

    for (i = 0; i < data.length; i++) {
        let time_start = data[i].time_start
        time_start = time_start.slice(0, -3);

        let time_end = data[i].time_end
        time_end = time_end.slice(0, -3);

        appointments.push({
            start_time: time_start,
            end_time: time_end
        })
    }

    appointments.sort(function(a, b) {
        return a.start_time > b.start_time ? 1 : -1;
    });

    let availableSchedules = getAvailableSchedules(appointments)

    var html = ''

    if (availableSchedules.length == 0 || $('#datePicker').val() == '') {
        html += '<tr> <th scope="col">#</th> <th scope="col">No Available Schedules</th> <th scope="col"></th></tr>';
        $('#schedules-head').html(html)
        $('#schedules-body').html('')
    } else {
        var i = 1
        for (const schedule of availableSchedules) {
            html += '<tr><td>' + i + '</td><td><b>' + schedule + '</b></td> <td class="text-center"><button type="button" class="btn btn-primary btn-select" data-schedule="' + schedule + '">Select</button></td></tr>'
            i++
        }
        $('#schedules-body').html(html)
    }
}

function getAvailableSchedules(appointments) {
    availableSchedules = ['08:00-09:00', '09:00-10:00', '10:00-11:00', '11:00-12:00', '13:00-14:00', '14:00-15:00', '15:00-16:00', '16:00-17:00', '17:00-18:00'];

    for (const appointment of appointments) {
        for (const schedule of availableSchedules) {
            scheduleTimeStart = schedule.split('-')
            if (appointment.start_time == scheduleTimeStart[0]) {
                let i = availableSchedules.indexOf(schedule);
                if (i > -1) {
                    availableSchedules.splice(i, 1);
                }
            }
        }
    }

    return availableSchedules;
}

$(document).ready(function() {
    $('#datepicker').datepicker({
        minDate: new Date(),
        maxDate: 31,
        dateFormat: 'yy-mm-dd',
        onSelect: function() {
            $.ajax({
                url: '/online-appointment/' + $(this).val() + '/schedules',
                dataType: 'json',
                success: function(data) {
                    updateAvailableSchedules(data)
                }
            })
        }
    });

    $(document).on('click', '.btn-select', function() {
        schedule = $(this).data('schedule')
        schedule = schedule.split('-')
        $('#stime').val(schedule[0])
        $('#time_start').val(schedule[0])
        $('#otime').val(schedule[1])
        $('#time_end').val(schedule[1])
    })

    $('#sweetalert').click(function() {
        checkInputs()
    })

    $('#patientEmailAddress').focusout(function() {
        $.ajax({
            url: '/online-appointment/new-patient-form/check-email-availability/' + $('#patientEmailAddress').val(),
            success: function(data) {
                console.log(data)
                console.log($('#patientEmailAddress').val())
                if (data == '1') {
                    setErrorFor(patientEmailAddress, 'Email is already in use!')
                    $('#sweetalert').prop('disabled', true);
                } else {
                    setSuccessFor(patientEmailAddress);
                    $('#sweetalert').prop('disabled', false);
                }
            }
        })
    });

    $('#stime').timepicker({
        interval: 60,
        minTime: '7:00am',
        maxTime: '5:00pm',
        format: 'h:i',
        dropdown: true,
        scrollbar: true,
        use24hours: true,
        change: function() {
            let time = convertTime12to24($('#stime').val())
            let endTIme = time + '01:00'
            if (isTimeAvailable(time)) {
                setSuccessFor(stime);
                let startTime =
                    $("#otime").val(addTimes('12:13', '01:42'))
            } else {
                setErrorFor(stime, 'Time is already taken!');
            }
        }
    });

});