const form = document.getElementById('form');
const fname = document.getElementById('fname');
const lname = document.getElementById('lname');
const number = document.getElementById('number');
const email = document.getElementById('email');
const desc = document.getElementById('description');
const datepicker = document.getElementById('datepicker');
const stime = document.getElementById('stime');
const otime = document.getElementById('otime');


function subtractMinute(time) {
    var h = +time.substr(0, 2);
    var m = +time.substr(3, 2);

    if (m > 0) {
        m -= 1;
    } else {
        if (h > 0) {
            h -= 1;
        } else {
            return false;
        }
        m = 59;
    }

    if (h < 10)
        h = '0' + h;

    if (m < 10)
        m = '0' + m;

    return h + ':' + m + ':00';
}

function addMinute(time) {
    var h = +time.substr(0, 2);
    var m = +time.substr(3, 2);

    if (m < 59) {
        m += 1;
    } else {
        if (h < 22) {
            h += 1;
        } else {
            return false;
        }
        m = 0;
    }

    if (h < 10)
        h = '0' + h;

    if (m < 10)
        m = '0' + m;

    return h + ':' + m + ':00';
}

// Convert a time in hh:mm format to minutes
function timeToMins(time) {
    var b = time.split(':');
    return b[0] * 60 + +b[1];
}

// Convert minutes to a time in format hh:mm
// Returned value is in range 00  to 24 hrs
function timeFromMins(mins) {
    function z(n) { return (n < 10 ? '0' : '') + n; }
    var h = (mins / 60 | 0) % 24;
    var m = mins % 60;
    return z(h) + ':' + z(m);
}

// Add two times in hh:mm format
function addTimes(t0, t1) {
    return timeFromMins(timeToMins(t0) + timeToMins(t1));
}

function isTimeAvailable(time) {
    for (const appointment of appointments) {
        console.log(appointment.start_time + ' == ' + time)
        if (appointment.start_time == time) {
            return false
        }
    }
    return true
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function getAvailableSchedules(appointments) {
    availableSchedules = ['08:00-09:00', '09:00-10:00', '10:00-11:00', '11:00-12:00', '13:00-14:00', '14:00-15:00', '15:00-16:00', '16:00-17:00', '17:00-18:00'];

    for (const appointment of appointments) {
        for (const schedule of availableSchedules) {
            scheduleTimeStart = schedule.split('-')
            if (appointment.start_time == scheduleTimeStart[0]) {
                let i = availableSchedules.indexOf(schedule);
                if (i > -1) {
                    availableSchedules.splice(i, 1);
                }
            }
        }
    }

    return availableSchedules;
}

function checkInputs() {
    // trim to remove the whitespaces
    const numberValue = number.value.trim();
    const fnameValue = fname.value.trim();
    const lnameValue = lname.value.trim();
    const emailValue = email.value.trim();
    const descValue = desc.value.trim();
    const datepickerValue = datepicker.value.trim();
    const stimeValue = stime.value.trim();
    const otimeValue = otime.value.trim();

    var hasError = false;
    var s = 0;

    const regex = /^[a-zA-Z]+$/;

    if (fnameValue === '') {
        setErrorFor(fname, 'First Name cannot be blank!');
        hasError = true;
    } else if (!fnameValue.match(regex)) {
        setErrorFor(fname, 'First Name must not contain numbers!')
        hasError = true;
    } else {
        setSuccessFor(fname);
        hasError = false;
        s++;
    }

    if (lnameValue === '') {
        setErrorFor(lname, 'Last Name cannot be blank!');
        hasError = true;
    } else if (!lnameValue.match(regex)) {
        setErrorFor(lname, 'Last Name must not contain numbers!')
        hasError = true;
    } else {
        setSuccessFor(lname);
        hasError = false;
        s++;
    }

    if (emailValue === '') {
        setErrorFor(email, 'Email cannot be blank');
        hasError = true;
    } else if (!isEmail(emailValue)) {
        setErrorFor(email, 'Not a valid email');
        hasError = true;
    } else {
        setSuccessFor(email);
        hasError = false;
        s++;
    }

    if (numberValue === '') {
        setErrorFor(number, 'Number cannot be blank!');
        hasError = true;
    } else {
        setSuccessFor(number);
        hasError = false;
        s++;
    }

    if (descValue == '') {
        setErrorFor(desc, 'Description cannot be blank!');
        hasError = true;
    } else {
        setSuccessFor(desc);
        hasError = false;
        s++;
    }


    if (datepickerValue === '') {
        setErrorFor(datepicker, 'Date cannot be blank!');
        hasError = true;
    } else {
        setSuccessFor(datepicker);
        hasError = false;
        s++;
    }

    if (stimeValue === '') {
        setErrorFor(stime, 'Invalid!');
        hasError = true;
    } else {
        setSuccessFor(stime);
        hasError = false;
        s++;
    }

    if (otimeValue === '') {
        setErrorFor(otime, 'Invalid!');
        hasError = true;
    } else {
        setSuccessFor(otime);
        hasError = false;
        s++;
    }

    if (descValue === '') {
        setErrorFor(desc, 'Description cannot be blank!');
    } else if (descValue.length <= 5) {
        setErrorFor(desc, 'Descripion should not be less than 5 characters!');
    } else {
        setSuccessFor(desc);
        s++;
    }

    if (s >= 8) {
        $('form#form').submit();
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'You have left an invalid input'
        })
    }

}

function setErrorFor(input, message) {
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');
    formControl.className = 'form-con error';
    small.innerText = message;
}

function getTime(button) {
    button = button.parentElement;
    const bold = button.querySelector('b');
    let time = bold.innerText;
    return time;
}

function updateAvailableSchedules(data) {
    var appointments = [];

    for (i = 0; i < data.length; i++) {
        let time_start = data[i].time_start
        time_start = time_start.slice(0, -3);

        let time_end = data[i].time_end
        time_end = time_end.slice(0, -3);

        appointments.push({
            start_time: time_start,
            end_time: time_end
        })
    }

    appointments.sort(function(a, b) {
        return a.start_time > b.start_time ? 1 : -1;
    });

    let availableSchedules = getAvailableSchedules(appointments)

    var html = ''

    if (availableSchedules.length == 0 || $('#datePicker').val() == '') {
        html += '<tr> <th scope="col">#</th> <th scope="col">No Available Schedules</th> <th scope="col"></th></tr>';
        $('#schedules-head').html(html)
        $('#schedules-body').html('')
    } else {
        var i = 1
        for (const schedule of availableSchedules) {
            html += '<tr><td>' + i + '</td><td><b>' + schedule + '</b></td> <td class="text-center"><button type="button" class="btn btn-primary btn-select" data-schedule="' + schedule + '">Select</button></td></tr>'
            i++
        }
        $('#schedules-body').html(html)
    }
}


function setSuccessFor(input) {
    const formControl = input.parentElement;
    formControl.className = 'form-con success';
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

const convertTime12to24 = (time12h) => {
    const [time, modifier] = time12h.split(' ');

    let [hours, minutes] = time.split(':');

    if (hours === '12') {
        hours = '00';
    }

    if (modifier === 'PM') {
        hours = parseInt(hours, 10) + 12;
    }

    return `${hours}:${minutes}`;
}

function init() {
    fnameVal = $('#fname').val()
    fnameVal = fnameVal.replace(/^"|"$/g, '');
    $('#fname').val(fnameVal)

    lnameVal = $('#lname').val()
    lnameVal = lnameVal.replace(/^"|"$/g, '');
    $('#lname').val(lnameVal)

    emailVal = $('#email').val()
    emailVal = emailVal.replace(/^"|"$/g, '');
    $('#email').val(emailVal)

    numberVal = $('#number').val()
    numberVal = numberVal.replace(/^"|"$/g, '');
    $('#number').val(numberVal)

    patientIdVal = $('#patient_id').val()
    patientIdVal = patientIdVal.replace(/^"|"$/g, '');
    $('#patient_id').val(patientIdVal)
}

$(document).on('click', '.btn-select', function() {
    schedule = $(this).data('schedule')
    schedule = schedule.split('-')
    $('#stime').val(schedule[0])
    $('#time_start').val(schedule[1])
    $('#otime').val(schedule[1])
    $('#time_end').val(schedule[0])
})

$(document).ready(function() {
    $('#datepicker').datepicker({
        minDate: new Date(),
        maxDate: 31,
        dateFormat: 'yy-mm-dd',
        onSelect: function() {
            console.log($(this).val())
            $.ajax({
                url: '/online-appointment/' + $(this).val() + '/schedules',
                dataType: 'json',
                success: function(data) {
                    updateAvailableSchedules(data)
                }
            })
        }
    });

    $('#sweetalert').click(function() {
        checkInputs()
    })

    $('#stime').timepicker({
        interval: 60,
        minTime: '7:00am',
        maxTime: '5:00pm',
        format: 'h:i',
        dropdown: true,
        scrollbar: true,
        use24hours: true,
        change: function() {
            let time = convertTime12to24($('#stime').val())
            let endTIme = time + '01:00'
            if (isTimeAvailable(time)) {
                setSuccessFor(stime);
                let startTime =
                    $("#otime").val(addTimes('12:13', '01:42'))
            } else {
                setErrorFor(stime, 'Time is already taken!');
            }
        }
    });

    init()

});