var editEvent;
var procedureCheckButtonIsClicked = false;

$(document).ready(function() {

    function getEvents() {
        $.ajax({
            dataType: 'json',
            url: '/events',
            success: function(data) {
                for (let i = 0; i < data.length; i++) {
                    data[i].allDay = false;
                    data[i].start = new Date(data[i].date + ' ' + data[i].time_start);
                    data[i].end = new Date(data[i].date + ' ' + data[i].time_end);
                    data[i].d = new Date(data[i].date);
                    data[i].className = 'info';
                }

                $("#calendar").fullCalendar('removeEvents');
                $("#calendar").fullCalendar('addEventSource', data);
                $("#calendar").fullCalendar('rerenderEvents');
            }
        });
    }

    function editEvent(event) {
        var date = event.d;

        var day = date.getDate(),
            month = date.getMonth() + 1,
            year = date.getFullYear(),
            startHour = event.start.getHours(),
            startMin = event.start.getMinutes(),
            // endHour = event.end.getHours(),
            // endMin = event.end.getMinutes();

            startHour = (startHour < 10 ? "0" : "") + startHour;
        startMin = (startMin < 10 ? "0" : "") + startMin;
        endHour = (endHour < 10 ? "0" : "") + endHour;
        // endMin = (endMin < 10 ? "0" : "") + endMin;

        $('#editAppointmentHeading').text('Appointment "' + event.title + '"');
        // $('#edit_time').val()
        $('#editAppointmentId').val(event.id);
        $('#deleteAppointmentId').val(event.id);
        $('#editAppointmentTitle').val(event.title);
        $('#deleteAppointmentTitle').val(event.title);
        $('#editAppointmentDate').val(event.d.toISOString().substr(0, 10));
        $('#editAppointmentTimeStart').val(startHour + ':' + startMin);
        // $('#editAppointmentTimeEnd').val(endHour + ':' + endMin);
        $('#editAppointmentDescription').val(event.description);
        $('#editModal').modal('toggle');
    }

    // Renders appointmnets's table
    function renderTable(data) {
        if (data.length > 0) {
            let html =
                '<table class="table container-fluid" style="word-wrap:break-word;">' +
                '<thead>' +
                '<tr class="p-3 mb-5 rounded">' +
                '<td class="pl-3">Patient</td>' +
                '<td>Date</td>' +
                '<td>Time</td>' +
                '<td>Description</td>' +
                '<td>Action</td>' +
                '</tr>' +
                '</thead>' +
                '<tbody id="patientBodyTable">';


            for (i = 0; i < data.length; i++) {
                html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
                html += "<th scope='row'><img class='h-8 w-8 rounded-full object-cover' style='float: left;' src='https://ui-avatars.com/api/?name=" + data[i].first_name + '+' + data[i].last_name + "&background=" + data[i].profile_bg_color + "&bold=true' alt='Admin'><span class='th-title ml-2 text-capitalize'>" + data[i].first_name + ' ' + data[i].last_name + ' </span>' + '<u><em>    ' + data[i].slug + '</em></u><br>';
                html += "<span class='td-email ml-2'>" + data[i].email + '</span></th>';
                html += "<td>" + data[i].date + "</td>";
                html += "<td>" + data[i].time_start + "</td>";
                html += "<td>" + data[i].description + "</td>";
                html += '<td><button type="button" id="' + data[i].appointment_id + '" class="btn pay-btn btn-primary view-button">View</button>';
                html += '</td>';
                html += '</tr>';
            }

            html += '</tbody>';

            return html;
        }
    }

    /* initialize the calendar
    -----------------------------------------------------------------*/
    var calendar = $('#calendar').fullCalendar({
        eventRender: function(event, element, view) {
            element.popover({
                title: event.title,
                content: "<b>Datetime: </b>" + event.d + "<br>" + "<b>Description: </b>" + event.description,
                delay: {
                    show: "800",
                    hide: "50"
                },
                trigger: 'hover',
                placement: 'top',
                html: true,
                container: 'body'
            })
        },
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,next today'
        },
        editable: true,
        disableDragging: true,
        disableResizing: false,
        firstDay: 0, //  1(Monday) this can be changed to 0(Sunday) for the USA system
        selectable: true,
        defaultView: 'month',
        axisFormat: 'h:mm',
        columnFormat: {
            month: 'ddd', // Mon
            week: 'ddd d', // Mon 7
            day: 'dddd M/d', // Monday 9/7
            agendaDay: 'dddd d'
        },
        titleFormat: {
            month: 'MMMM yyyy', // September 2009
            week: "MMMM yyyy", // September 2009
            day: 'MMMM yyyy' // Tuesday, Sep 8, 2009
        },
        allDaySlot: false,
        selectHelper: true,
        select: function(start, end, allDay, event) {
            // var title = prompt('Event Title:');
            // if (title) {
            //     calendar.fullCalendar('renderEvent', {
            //             title: title,
            //             start: start,
            //             end: end,
            //             allDay: allDay
            //         },
            //         true // make the event "stick"
            //     );
            // }
            // console.log(event.title)
            calendar.fullCalendar('unselect');
        },
        eventClick: function(event) {
            $.ajax({
                dataType: 'json',
                url: '/event/' + event.id,
                success: function(data) {
                    console.log('ddddd')
                    console.log(data)
                    procedures = data[0].procedures.split(',');
                    for (i = 0; i < procedures.length; i++) {
                        $('input[value="' + procedures[i].trim() + '"]').attr("checked", true);
                        // let val = $('input[value="' + data[i].id + '"]').val();
                        // console.log(val)
                    }

                    let timeStart = data[0].time_start
                    let timeEnd = data[0].time_end

                    let timeValue = timeStart.substring(0, timeStart.length - 3) + '-' + timeEnd.substring(0, timeEnd.length - 3)

                    $('#edit_time').val(timeValue)

                    $('#editPatientSearchResultBox').html(
                        '<table id="patientTableResult" class="table patientTableResult"><tbody id="patientTableBody"><tr id="patientTableRow-3" class="shadow-sm p-3 mb-5 bg-white rounded"><th class="patientResultTh" role="button" scope="row"><img class="h-8 w-8 rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + data[0].first_name + '' + data[0].last_name + '&amp;background=random&amp;bold=true"><span class="th-title ml-2 text-capitalize">' + data[0].first_name + '' + data[0].last_name + '</span><br><span class="td-email ml-2">' + data[0].email + '</span></th><td><span class="d-flex justify-content-end"><button type="button" class="btn-close btn-close-td" aria-label="Close"></button></span></td></tr></tbody></table>'
                    );
                    $('#editPatientSearchBox').hide();
                    $('#editPatientSearchResultBox').find('button').replaceWith('<button type="button" class="btn-close btn-close-td" aria-label="Close"></button>');

                    $('#editAppointmentHeading').text('Appointment "' + event.title + '"');
                    $('#editAppointmentId').val(event.id);
                    $('#edit_patient_id').val(data[0].patient_id);
                    $('#deleteAppointmentId').val(event.id);
                    $('#editAppointmentTitle').val(event.title);
                    $('#deleteAppointmentTitle').val(event.title);
                    $('#editAppointmentDate').val(event.d.toISOString().substr(0, 10));
                    // $('#editAppointmentTimeEnd').val(endHour + ':' + endMin);
                    $('#editAppointmentDescription').val(event.description);
                    $('#editModal').modal('toggle');
                }
            });
        }
    });

    function renderProcedures(data) {
        let html = '';
        let currentCategoryId = '';

        for (i = 0; i < data.length; i++) {
            if (currentCategoryId == '') {
                currentCategoryId = data[i].procedure_category_id;
                html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend><hr>';
            }

            if (data[i].procedure_category_id == currentCategoryId) {
                html += '<div class="form-check"><input class="form-check-input" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>'
            } else {
                html += '</fieldset></div>';
                html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend>';
                html += '<hr><div class="form-check"><input class="form-check-input" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>';

                currentCategoryId = data[i].procedure_category_id;
            }
        }

        html += '</div>';

        // let html = '<fieldset><legend>' + data.category + '</legend> <hr> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="1"> <label class="form-check-label" for="flexCheckDefault"> Tooth Extraction </label> </div> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="Pasta"> <label class="form-check-label" for="flexCheckChecked"> Pasta </label> </div> </fieldset>';

        // for (i = 0; i < data.length; i++) {
        //     html += '<option ' + 'id="' + data[i].amount_charged + '" data-procedure-id="' + data[i].id + '">' + data[i].name + '</option>';
        // }

        return html;
    }

    // Retrieve procedures records from the database
    function getProcedures() {
        $.ajax({
            dataType: 'json',
            url: '/procedures/index',
            success: function(data) {
                let html = renderProcedures(data);
                $(".procedures_container").html(html);
                $("#edit-select-procedures").html(html);
            }
        })
    }

    function setErrorFor(input, message) {
        const formControl = input.parentElement;
        const small = formControl.querySelector('small');
        formControl.className = 'form-con error';
        small.innerText = message;
    }


    function setSuccessFor(input) {
        const formControl = input.parentElement;
        formControl.className = 'form-con success';
    }

    function updateAvailableSchedules(data) {
        var appointments = [];

        for (i = 0; i < data.length; i++) {
            let time_start = data[i].time_start
            time_start = time_start.slice(0, -3);

            let time_end = data[i].time_end
            time_end = time_end.slice(0, -3);

            appointments.push({
                start_time: time_start,
                end_time: time_end
            })
        }

        appointments.sort(function(a, b) {
            return a.start_time > b.start_time ? 1 : -1;
        });

        let availableSchedules = getAvailableSchedules(appointments)

        var html = ''
        console.log(availableSchedules)

        if (availableSchedules.length == 0 || $('#datePicker').val() == '') {
            setErrorFor(add_time, 'No available schedules! Please select another date.')
        } else {
            optionHtml = '<option selected="" disabled="">Select time</option>';

            for (i = 0; i < availableSchedules.length; i++) {
                optionHtml += '<option value="' + availableSchedules[i] + '">' + availableSchedules[i].substring(0, 5) + '</option>'
            }

            $('#add_time').html(optionHtml)
                // var i = 1
                // for (const schedule of availableSchedules) {
                //     html += '<tr><td>' + i + '</td><td><b>' + schedule + '</b></td> <td class="text-center"><button type="button" class="btn btn-primary btn-select" data-schedule="' + schedule + '">Select</button></td></tr>'
                //     i++
                // }
                // $('#schedules-body').html(html)
        }
    }

    function getAvailableSchedules(appointments) {
        availableSchedules = ['08:00-09:00', '09:00-10:00', '10:00-11:00', '11:00-12:00', '13:00-14:00', '14:00-15:00', '15:00-16:00', '16:00-17:00', '17:00-18:00'];

        for (const appointment of appointments) {
            for (const schedule of availableSchedules) {
                scheduleTimeStart = schedule.split('-')
                if (appointment.start_time == scheduleTimeStart[0]) {
                    let i = availableSchedules.indexOf(schedule);
                    if (i > -1) {
                        availableSchedules.splice(i, 1);
                    }
                }
            }
        }

        return availableSchedules;
    }

    // Retrieve events 
    getEvents();

    // Retrieve procedures
    getProcedures();

    // Appointment date
    $("#appointmentDate").on('change', function() {
        $.ajax({
            url: '/online-appointment/' + $(this).val() + '/schedules',
            dataType: 'json',
            success: function(data) {
                updateAvailableSchedules(data)
            }
        })
    });

    // Plus button on click
    $("#plus-button").click(function() {
        procedureCheckButtonIsClicked = false;
    })

    // Add new appointment (Save button)
    $("#save-changes").click(function(event) {
        event.preventDefault();

        if ($("#patientSearchBox").val() == "") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Patient is required!'
            })
            return
        }

        if ($("#appointmentDate").val() == "") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Date is required!'
            })
            return;
        }

        console.log('add_time')
        console.log($("#add_time").val())

        if ($("#add_time").val() == "" || $("#add_time").val() == null) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Time is required!'
            })
            return;
        }

        if ($("#appointmentDescription").val() == "") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Description is required!'
            })
            return;
        }

        appointmentTitle = $("#appointmentTitle").val();
        appointmentDate = $("#appointmentDate").val();
        appointmentDescription = $("#appointmentDescription").val();
        appointmentTimeStart = $("#appointmentTimeStart").val();
        appointmentTimeEnd = $("#appointmentTimeEnd").val();

        appointmentTimeDateStart = new Date(appointmentDate + " " + appointmentTimeStart);
        appointmentTimeDateEnd = new Date(appointmentDate + " " + appointmentTimeEnd);


        var eventData = {
            title: appointmentTitle,
            start: appointmentTimeDateStart,
            end: appointmentTimeDateEnd,
            description: appointmentDescription,
            allDay: false,
        };

        // $("#addModal").modal('toggle');
        $("#calendar").fullCalendar("renderEvent", eventData, true);

        $("form#addAppointmentForm").submit();
    })

    // Procedure upon clicking
    $(document).on('click', ".form-check-input", function() {
        console.log('oo')
        procedureCheckButtonIsClicked = true;
    });

    // Delete appointment (button action)
    $("#deleteAppointmentSubmit").click(function() {
        $("#deleteAppointmentForm")[0].submit();
    })

    $('#editPatientSearchBox').keyup(function(event) {
        var query = $(this).val();

        if (!(query == '')) {
            $.ajax({
                url: '/patients/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query,
                    "status": "active"
                },
                dataType: 'json',
                success: function(data) {
                    if (data.length) {
                        let html = renderPatientSearchResult(data);
                        $('#editPatientSearchResultBox').html(html);
                        $('#editPatientSearchResultBox').prepend('<p id="searchTag">SEARCH</p>');
                    } else {
                        $('#editPatientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
                    }

                }
            })
        } else {

        }
    })

    // Edit appointment
    $(document).on('click', ".view-button", function(event) {
        let appointmentId = $(this).attr('id');

        $.ajax({
            dataType: 'json',
            url: '/event/' + appointmentId,
            success: function(data) {
                procedures = data[0].procedures.split(',');
                for (i = 0; i < procedures.length; i++) {
                    $('input[value="' + procedures[i].trim() + '"]').attr("checked", true);
                }

                let timeStart = data[0].time_start
                let timeEnd = data[0].time_end

                let timeValue = timeStart.substring(0, timeStart.length - 3) + '-' + timeEnd.substring(0, timeEnd.length - 3)

                $('#edit_time').val(timeValue)

                $('#editPatientSearchResultBox').html(
                    '<table id="patientTableResult" class="table patientTableResult"><tbody id="patientTableBody"><tr id="patientTableRow-3" class="shadow-sm p-3 mb-5 bg-white rounded"><th class="patientResultTh" role="button" scope="row"><img class="h-8 w-8 rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + data[0].first_name + '' + data[0].last_name + '&amp;background=random&amp;bold=true"><span class="th-title ml-2 text-capitalize">' + data[0].first_name + '' + data[0].last_name + '</span><br><span class="td-email ml-2">' + data[0].email + '</span></th><td><span class="d-flex justify-content-end"><button type="button" class="btn-close btn-close-td" aria-label="Close"></button></span></td></tr></tbody></table>'
                );
                $('#editPatientSearchBox').hide();
                $('#editPatientSearchResultBox').find('button').replaceWith('<button type="button" class="btn-close btn-close-td" aria-label="Close"></button>');

                $('#editAppointmentHeading').text('Appointment "' + data[0].last_name + '"');
                $('#editAppointmentId').val(data[0].appointment_id);
                $('#edit_patient_id').val(data[0].patient_id);
                $('#deleteAppointmentId').val(data[0].appointment_id);
                $('#editAppointmentTitle').val(data[0].title);
                $('#deleteAppointmentTitle').val(data[0].title);
                $('#editAppointmentDate').val(data[0].date);
                // $('#editAppointmentTimeEnd').val(endHour + ':' + endMin);
                $('#editAppointmentDescription').val(data[0].description);
                $('#wrap').show();
                $('#searchResultContainer').hide();
                $('#editModal').modal('toggle');
            }
        });
    });

    // Save button (edit appointment)
    $('#editSaveChanges').click(function(event) {
        event.preventDefault();

        console.log('$("#edit_time").val()')
        console.log($("#edit_time").val())

        if ($(".patientResultTh").length == 0) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Patient is required!'
            })
            return
        }

        if ($("#editAppointmentDate").val() == "") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Date is required!'
            })
            return;
        }

        if ($("#edit_time").val() == "" || $("#edit_time").val() == null) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Time is required!'
            })
            return;
        }

        if ($("#editAppointmentDescription").val() == "") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Description is required!'
            })
            return;
        }

        $("form#editAppointmentForm").submit()
    })

    // Search box
    $('#searchBox').keyup(function(event) {
        var query = $(this).val();

        if (!(query == '')) {
            $.ajax({
                url: '/appointments/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query
                },
                dataType: 'json',
                success: function(data) {
                    if (data.length) {
                        let html = renderTable(data);
                        $('#searchResultContainer').show();
                        $('#wrap').hide();
                        $('#searchResultContainer').html(html);
                        $('#searchResultContainer').prepend('<p>Search results for "' + query + '":</p>');
                    } else {
                        $('#searchResultContainer').html('<p>No search results for "' + query + '".</p>');
                    }
                }
            })
        } else {
            // $('#searchResult').removeClass('show');
            $('#wrap').show();
            $('#searchResultContainer').hide();
        }
    })

    // Search patients
    $('#patient-input').keyup(function(e) {
        console.log('en')
        var query = $(this).val();

        if (!(query == '')) {
            $('#patientSearchResults').empty();

            $.ajax({
                url: '/patients/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query
                },
                dataType: 'json',
                success: function(data) {
                    if (data.length > 0) {
                        $('#patientSearchResults').addClass('show');

                        let html = "<p class='pl - 3 text - muted '>Search results for \"" + query + "\"" + ":</p>";

                        for (let i = 0; i < data.length; i++) {
                            html += "<a id='" + data[i].id + "' class='dropdown-item' href='#'>" + data[i].first_name + " " + data[i].last_name + "</a>";
                        }

                        $('#patientSearchResults').html(html);
                    } else {
                        $('#patientSearchResults').addClass('show');
                        let html = "<p class='pl - 3 text - muted '>No search results for \"" + query + "\"" + ".</p>";
                        $('#patientSearchResults').html(html);
                    }
                }
            })
        }
    })

    $('#patient-input').focusout(function(e) {
        $(".dropdown-item").click(function(e) {
            $('#patient-input').val($(this).text());
            $('#patientAppointmentId').val($(this).attr('id'));
            $('#patientSearchResults').removeClass('show');

        })
    })

    $('#searchBox').focusout(function(e) {
        var elementClicked = false;
        $(".dropdown-item").click(function() {
            elementClicked = true;
            $('#searchResult').removeClass('show');
            let id = this.id;

            $.ajax({
                dataType: 'json',
                url: '/event/' + id,
                success: function(data) {
                    console.log('data')
                    console.log(data)
                    data.allDay = false;
                    data.start = new Date(data.date + ' ' + data.time_start);
                    data.end = new Date(data.date + ' ' + data.time_end);
                    data.d = new Date(data.date);
                    data.className = 'info';

                    editEvent(data);
                }
            });
        });
        if (elementClicked = false) {
            $('#searchResult').removeClass('show');
        }

    });

    // PATIENT SEARCH BOX

    // Remove the patient from the patient search box
    $(document).on('click', ".btn-close-td", function(event) {
        $('#editPatientSearchBox').show();
        $('#patientSearchBox').show();
        $('#patientSearchBox').val('');
        $('#editPatientSearchBox').val('');
        // $('#searchTag').show();
        $('#patientTableBody').remove();

    });

    // Renders search result for patient
    function renderPatientSearchResult(data) {
        let html =
            '<table id="patientTableResult" class="table patientTableResult">' +
            '<thead>' +
            '<tr>' +
            '<td></td>' +
            '<td></td>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="patientTableBody">';


        for (i = 0; i < data.length; i++) {
            html += "<tr id='patientTableRow-" + data[i].id + "' class='shadow-sm p-3 mb-5 bg-white rounded'>";
            html += "<th class='patientResultTh' role='button' scope='row'><img class='h-8 w-8 rounded-full object-cover' style='float: left;' src='https://ui-avatars.com/api/?name=" + data[i].first_name + '+' + data[i].last_name + "&background=random&bold=true' alt='Admin'><span class='th-title ml-2 text-capitalize'>" + data[i].first_name + ' ' + data[i].last_name + '</span><u><em>    ' + data[i].slug + '</em></u><br>';
            html += "<span class='td-email ml-2'>" + data[i].email + '</span></th>';
            html += '<td><span class="d-flex justify-content-end"><button type="button" class="btn btn-light select-btn" data-patient-id="' + data[i].id + '">Select</button></span></td>';
            html += '</tr>';
        }

        html += '</tbody>';


        return html;
    }
    // Select button on patient search results
    $(document).on('click', ".select-btn", function(event) {
        let patientId = $(this).attr('data-patient-id');
        let patientTableRow = $('#patientTableRow-' + patientId)[0].outerHTML;
        $('#patientTableBody').html(patientTableRow);
        $('#patientSearchBox').hide();
        $('#editPatientSearchBox').hide();
        $('#patientTableBody').find('button').replaceWith('<button type="button" class="btn-close btn-close-td" aria-label="Close"></button>');
        $('#searchTag').hide();
        $('#patientTableResult').find('thead').remove();
        $('#patientTableResult').find('button').removeClass('select-btn');
        // $('#patientTableBody').remove();

        $('#add_patient_id').val(patientId);
        $('#edit_patient_id').val(patientId);
    });

    $('#patientSearchBox').keyup(function() {
        var query = $(this).val();

        if (!(query == '')) {
            $.ajax({
                url: '/patients/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query,
                    "status": "active"
                },
                dataType: 'json',
                success: function(data) {
                    if (data.length) {
                        let html = renderPatientSearchResult(data);
                        $('#patientSearchResultBox').html(html);
                        $('#patientSearchResultBox').prepend('<p id="searchTag">SEARCH</p>');
                    } else {
                        $('#patientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
                    }

                }
            })
        } else {
            $('#patientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
        }
    })

    $('#add_time').on('change', function() {
        let time = $(this).val().split('-')

        $('#add_time_start').val(time[0]);
        $('#add_time_end').val(time[1]);
    })

    $('#edit_time').on('change', function() {
        let time = $(this).val().split('-')

        $('#edit_time_start').val(time[0]);
        $('#edit_time_end').val(time[1]);
    })
});