$(document).ready(function() {
    // Current path
    url = window.location.href;

    // Verify passwords
    function verifyPassword() {
        const password = document.querySelector('input[name=password]');
        const confirm = document.querySelector('input[name=confirm]');
        if (confirm.value === password.value) {
            confirm.setCustomValidity('');
        } else {
            confirm.setCustomValidity('Passwords do not match');
        }
    }

    // Renders patient's table
    function renderTable(data) {
        // if (data.length > 0) {
        //     let html =
        //         '<table class="table ">' +
        //         '<thead>' +
        //         '<tr class="p-3 mb-5 rounded">' +
        //         '<td>ID</td>' +
        //         '<td>Name</td>' +
        //         '<td>Email</td>' +
        //         // '<td>Email Verified At</td>' +
        //         '<td>Role</td>' +
        //         '<td>Action</td>' +
        //         '</tr>' +
        //         '</thead>' +
        //         '<tbody id="treatmentBodyTable">';

        //     for (i = 0; i < data.length; i++) {
        //         html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
        //         html += "<td>" + data[i].id + "</td>";
        //         html += "<th scope='row'>" + data[i].name + '</th>';
        //         html += "<td>" + data[i].email + "</td>";
        //         // html += "<td>" + data[i].email_verified_at + "</td>";
        //         html += "<td><span class='badge rounded-pill bg-info text-dark'>" + data[i].role + "</span></td>";
        //         html += '<td><button id="' + data[i].id + '"class="btn btn-danger btn-sm rounded-0 mr-2" type="button" data-toggle="tooltip" data-placement="top" title="Delete user"><i class="fa fa-trash"></i></a></button>';
        //         html += '<button id="' + data[i].id + '" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit user"><i class="fa fa-edit"></i></a></button></td>';
        //         html += '</tr>';
        //     }

        //     html += '</tbody>';

        //     return html;
        // }
        return;
    }

    // Retrieve patient records from the database
    function getUsers() {
        console.log('called')
        $.ajax({
            dataType: 'json',
            url: '/users/index',
            success: function(data) {
                console.log('index')

                let html = renderTable(data);
                $("#main-content").html(html);
            }
        })
    }

    function editUser(user) {
        var id = user.id,
            name = user.name,
            email = user.email,
            user_role_id = user.user_role_id;

        // Edit form values
        $('#edit_id').val(id);
        $('#edit_name').val(name);
        $('#edit_email').val(email);
        $('#edit-select-roles').val(user_role_id);
        $('#editModal').modal('toggle');

    }

    // Edit button on click
    $(document).on('click', ".btn-success", function() {
        // $('#searchResult').addClass('show');
        // $('#searchResult').removeClass('show');
        let id = this.id;

        $.ajax({
            dataType: 'json',
            url: '/users/' + id + '/show',
            success: function(data) {
                editUser(data);
            }
        });

    });

    $('#select-procedures').change(function(e) {
        var id = $(this).children(":selected").attr("id");
        var procedure_id = $(this).children(":selected").attr("data-procedure-id");
        $('#total_amount').val(id);
        $('#procedure_id').val(procedure_id);
    })

    $('#edit-select-procedures').change(function(e) {
        var id = $(this).children(":selected").attr("id");
        var procedure_id = $(this).children(":selected").attr("data-procedure-id");
        $('#edit-total_amount').val(id);
        $('#edit-procedure_id').val(procedure_id);
    })

    $('#save-changes').click(function(event) {
        if ($('#add_password').val() == $('#add_confirm_password').val()) {
            $('form#add_user_form').submit();
        } else {
            event.preventDefault();
            alert('Passwords do not match!');
        }

    })

    $('#editSaveChanges').click(function(event) {
        event.preventDefault();
        if ($('#edit_confirm_new_password').val() == $('#edit_cnew_password').val()) {
            $('form#editForm').submit();
        } else {
            alert('New passwords do not match!');
        }

    })

    // Delete user (button action)
    $(document).on('click', ".btn-danger", function() {
        let id = $(this).attr('id');
        if (confirm('Are you sure you want to delete this user?')) {
            $('#deleteFormId').val(id)
            $('#deleteForm').submit()
        }
    });

    // Search box
    // $('#searchBox').keyup(function(event) {
    //     var query = $(this).val();

    //     if (!(query == '')) {
    //         $.ajax({
    //             url: '/users/search',
    //             method: 'post',
    //             data: {
    //                 "_token": $('meta[name="csrf-token"]').attr('content'),
    //                 "query": query
    //             },
    //             dataType: 'json',
    //             success: function(data) {
    //                 console.log(data)
    //                 if (data.length) {
    //                     let html = renderTable(data);
    //                     $('#main-content').html(html);
    //                     $('#main-content').prepend('<p>Search results for "' + query + '":</p>');
    //                 } else {
    //                     $('#main-content').html('<p>No search results for "' + query + '".</p>');
    //                 }

    //             }
    //         })
    //     } else {
    //         // Fetch patients
    //         getUsers();
    //     }
    // })

    // Search patients
    // $('#patient-input').keyup(function(e) {
    //     var query = $(this).val();

    //     if (!(query == '')) {
    //         $('#patientSearchResults').empty();

    //         $.ajax({
    //             url: '/patients/search',
    //             method: 'post',
    //             data: {
    //                 "_token": $('meta[name="csrf-token"]').attr('content'),
    //                 "query": query
    //             },
    //             dataType: 'json',
    //             success: function(data) {
    //                 if (data.length > 0) {
    //                     $('#patientSearchResults').addClass('show');

    //                     let html = "<p class='pl - 3 text - muted '>Search results for \"" + query + "\"" + ":</p>";

    //                     for (let i = 0; i < data.length; i++) {
    //                         html += "<a id='" + data[i].id + "' class='dropdown-item' href='#'>" + data[i].first_name + " " + data[i].last_name + "</a>";
    //                     }

    //                     $('#patientSearchResults').html(html);
    //                 } else {
    //                     $('#patientSearchResults').addClass('show');
    //                     let html = "<p class='pl - 3 text - muted '>No search results for \"" + query + "\"" + ".</p>";
    //                     $('#patientSearchResults').html(html);
    //                 }
    //             }
    //         })
    //     }
    // })

    $('#patient-input').focusout(function(e) {
        $(".dropdown-item").click(function(e) {
            $('#patient-input').val($(this).text());
            $('#patient_id').val($(this).attr('id'));
            $('#patientSearchResults').removeClass('show');

        })
    })

    // Fetch users
    getUsers();
})