$(document).ready(function() {
    var today = new Date().toISOString().split('T')[0];
    document.getElementsByName("treatmentDate")[0].setAttribute('min', today);
    document.getElementsByName("treatmentEditDate")[0].setAttribute('min', today);


    function leadingZeros(input) {
        if (!isNaN(input.value) && input.value.length === 1) {
            input.value = '0' + input.value;
        }
    }

    // Renders treatments table
    function renderTable(data) {
        let html =
            '<table class="table ">' +
            '<thead>' +
            '<tr class="p-3 mb-5 rounded">' +
            '<td class="text-center">Treatment ID</td>' +
            '<td class="text-center">Name</td>' +
            '<td class="text-center">Date</td>' +
            '<td class="text-center">Tooth No</td>' +
            '<td class="text-center">Procedure/s</td>' +
            '<td class="text-center">Total Amount</td>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="treatmentBodyTable">';


        if (data.length > 0) {
            for (i = 0; i < data.length; i++) {
                html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
                html += "<td class='text-center'>" + data[i][0].reference_id + "1</td>";
                html += "<th class='text-center' scope='row'>" + data[i][0].patient.first_name + ' ' + data[i][0].patient.last_name + '</th>';
                html += "<td class='text-center'>" + data[i][0].date + "</td>";
                html += "<td class='text-center'>" + data[i][0].tooth_no + "</td>";

                let procedures = '';


                // console.log(data[i][0].procedures[0].name)

                // for (k = 0; i < data[i][0].procedures.length; k++) {
                //     console.log(data[i][0].procedures)
                //     if (k == data[i][0].procedures.length) {
                //         // procedures += data[i][0].procedures[k].name + '';
                //     } else {
                //         // procedures += data[i][0].procedures[k].name + ', ';
                //     }
                // }

                for (let val of data[i][0].procedures) {
                    procedures += ' - ' + val.name + '<br>';
                }

                // console.log(procedures)

                procedures = $.trim(procedures);




                // html += '<td><button id="' + data[i][0].id + '"class="btn btn-danger btn-sm rounded-0 mr-2" type="button" data-toggle="tooltip" data-placement="top" title="View patient"><i class="fa fa-trash"></i></a></button>';
                html += "<td class='text-center'><div>" + procedures + "</div></td>";
                html += "<td class='text-center'>" + data[i][0].total_amount + "</td>";
                html += '</tr>';
            }
            html += '</tbody>';
        } else {
            html = '<div class="d-flex justify-content-center"><div class="d-flex flex-column justify-content-center"><div class="d-flex flex-column justify-content-center pl-3"><img src="img/no-data.png" style="height: 500px;"></div></div></div>';
            return html;
        }

        return html;
    }

    // Renders treatments table
    function renderSearchTable(data) {
        console.log('se')
        console.log(data)
        let html =
            '<table class="table ">' +
            '<thead>' +
            '<tr class="p-3 mb-5 rounded">' +
            '<td class="text-center">Reference ID</td>' +
            '<td class="text-center">Name</td>' +
            '<td class="text-center">Date</td>' +
            '<td class="text-center">Tooth No</td>' +
            '<td class="text-center">Procedure/s</td>' +
            '<td class="text-center">Amount Chared</td>' +
            '<td class="text-center">Action</td>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="treatmentBodyTable">';


        if (data.length > 0) {
            for (i = 0; i < data.length; i++) {
                html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
                html += "<td class='text-center'>" + data[i].reference_id + "</td>";
                html += "<th class='text-center' scope='row'>" + data[i].first_name + ' ' + data[i].last_name + '</th>';
                html += "<td class='text-center'>" + data[i].date + "</td>";
                html += "<td class='text-center'>" + data[i].tooth_no + "</td>";
                // console.log(data[i].reference_id)

                let procedures = '';

                let id = data[i].treatment_id

                $.ajax({
                    dataType: 'json',
                    url: '/treatments/' + data[i].id + '/getProcedures',
                    success: function(data) {
                        // let html = renderEditProcedures(data);
                        console.log('id')
                        console.log(id)
                        console.log(data[0])
                            // renderEditProcedures(data)
                            // $(".procedures_container").html(html);
                            // $("#edit-select-procedures").html(html);
                    }
                })


                // console.log(data[i][0].procedures[0].name)

                // for (k = 0; i < data[i][0].procedures.length; k++) {
                //     console.log(data[i][0].procedures)
                //     if (k == data[i][0].procedures.length) {
                //         // procedures += data[i][0].procedures[k].name + '';
                //     } else {
                //         // procedures += data[i][0].procedures[k].name + ', ';
                //     }
                // }

                // for (let val of data[i][0].procedures) {
                //     procedures += ' - ' + val.name + '<br>';
                // }

                // console.log(procedures)

                // procedures = $.trim(procedures);


                html += "<td class='text-center'><div>" + procedures + "</div></td>";
                html += "<td class='text-center'>" + data[i][0].total_amount + "</td>";
                html += '<td class="text-center"><button id="' + data[i][0].id + '"class="btn btn-danger btn-sm rounded-0 mr-2" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a></button>';
                html += '<button id="' + data[i][0].id + '" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a></button></td>';
                html += '</tr>';
            }
            html += '</tbody>';
        } else {
            html = '<div class="d-flex justify-content-center"><div class="d-flex flex-column justify-content-center"><div class="d-flex flex-column justify-content-center pl-3"><img src="img/no-data.png" style="height: 500px;"></div></div></div>';
            return html;
        }

        return html;
    }

    // Renders search result for patient
    function renderPatientSearchResult(data) {
        let html =
            '<table id="patientTableResult" class="table patientTableResult">' +
            '<thead>' +
            '<tr>' +
            '<td></td>' +
            '<td></td>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="patientTableBody">';


        for (i = 0; i < data.length; i++) {
            html += "<tr id='patientTableRow-" + data[i].id + "' class='shadow-sm p-3 mb-5 bg-white rounded'>";
            html += "<th class='patientResultTh' role='button' scope='row'><img class='h-8 w-8 rounded-full object-cover' style='float: left;' src='https://ui-avatars.com/api/?name=" + data[i].first_name + '+' + data[i].last_name + "&background=" + data[i].profile_bg_color + "&bold=true' alt='Admin'><span class='th-title ml-2 text-capitalize'>" + data[i].first_name + ' ' + data[i].last_name + '</span><u><em>    ' + data[i].id + '</em></u><br>';
            html += "<span class='td-email ml-2'>" + data[i].email + '</span></th>';
            html += '<td><span class="d-flex justify-content-end"><button type="button" class="btn btn-light select-btn" data-patient-id="' + data[i].id + '">Select</button></span></td>';
            html += '</tr>';
        }

        html += '</tbody>';


        return html;
    }

    function renderProcedures(data) {
        let html = '';
        let currentCategoryId = '';

        for (i = 0; i < data.length; i++) {
            if (currentCategoryId == '') {
                currentCategoryId = data[i].procedure_category_id;
                html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend><hr>';
            }

            if (data[i].procedure_category_id == currentCategoryId) {
                html += '<div class="form-check"><input class="form-check-input" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>'
            } else {
                html += '</fieldset></div>';
                html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend>';
                html += '<hr><div class="form-check"><input class="form-check-input" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>';

                currentCategoryId = data[i].procedure_category_id;
            }
        }

        html += '</div>';

        // let html = '<fieldset><legend>' + data.category + '</legend> <hr> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="1"> <label class="form-check-label" for="flexCheckDefault"> Tooth Extraction </label> </div> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="Pasta"> <label class="form-check-label" for="flexCheckChecked"> Pasta </label> </div> </fieldset>';

        // for (i = 0; i < data.length; i++) {
        //     html += '<option ' + 'id="' + data[i].amount_charged + '" data-procedure-id="' + data[i].id + '">' + data[i].name + '</option>';
        // }

        return html;
    }

    // Retrieve patient records from the database
    function getTreatments() {
        $.ajax({
            dataType: 'json',
            url: '/treatments/index',
            success: function(data) {
                // let html = renderTable(data);
                // $("#main-content").html(html);
            }
        })
    }

    // Retrieve procedures records from the database
    function getProcedures() {
        $.ajax({
            dataType: 'json',
            url: '/procedures/index',
            success: function(data) {
                let html = renderProcedures(data);
                $(".procedures_container").html(html);
                $("#edit-select-procedures").html(html);
            }
        })
    }

    function renderEditProcedures(data) {
        console.log('rend')
        console.log(data)
        data = data[0].procedures
        for (i = 0; i < data.length; i++) {
            $('input[value="' + data[i].id + '"]').attr("checked", true);
            // let val = $('input[value="' + data[i].id + '"]').val();
            // console.log(val)
        }
    }

    function getProceduresById(id) {
        $.ajax({
            dataType: 'json',
            url: '/treatments/' + id + '/getProcedures',
            success: function(data) {
                // let html = renderEditProcedures(data);
                console.log('data')
                console.log(data)
                renderEditProcedures(data)
                    // $(".procedures_container").html(html);
                    // $("#edit-select-procedures").html(html);
            }
        })
    }


    function editTreatment(treatment) {
        var patient_id = treatment.patient_id,
            first_name = treatment.first_name,
            last_name = treatment.last_name,
            email = treatment.email,
            date = treatment.date,
            tooth_no = treatment.tooth_no,
            total_amount = treatment.total_amount,
            treatment_id = treatment.treatment_id;

        // Edit form values
        getProceduresById(treatment_id)
        $('#edit_patient_id').val(patient_id);
        $('#editPatientSearchResultBox').html(
            '<table id="patientTableResult" class="table patientTableResult"><tbody id="patientTableBody"><tr id="patientTableRow-3" class="shadow-sm p-3 mb-5 bg-white rounded"><th class="patientResultTh" role="button" scope="row"><img class="h-8 w-8 rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + first_name + '' + last_name + '&amp;background=random&amp;bold=true"><span class="th-title ml-2 text-capitalize">' + first_name + '' + last_name + '</span><br><span class="td-email ml-2">' + email + '</span></th><td><span class="d-flex justify-content-end"><button type="button" class="btn-close btn-close-td" aria-label="Close"></button></span></td></tr></tbody></table>'
        );
        $('#editPatientSearchBox').hide();
        $('#editPatientSearchResultBox').find('button').replaceWith('<button type="button" class="btn-close btn-close-td" aria-label="Close"></button>');
        $('#searchTag').hide();
        $('#editPatientTableResult').find('thead').remove();
        $('#editPatientTableResult').find('button').removeClass('select-btn');

        // 

        $('#edit_treatment_id').val(treatment_id);
        $('#edit_date_display').val(date);
        $('#edit_date').val(date);
        $('#edit_tooth_no').val(tooth_no);
        $("button:contains(" + tooth_no + ")").removeClass('btn-purple').addClass('btn-clicked');
        $('#edit_total_amount_display').val(total_amount);
        $('#edit_total_amount').val(total_amount);

        $('#editModal').modal('toggle');

    }

    // Edit button on click
    $(document).on('click', ".btn-success", function() {
        let id = this.id;

        $.ajax({
            dataType: 'json',
            url: '/treatments/' + id + '/show',
            success: function(data) {
                console.log(data[0].date)
                editTreatment(data[0]);
            }
        });

    });

    // Select button on patient search results
    $(document).on('click', ".select-btn", function(event) {
        let patientId = $(this).attr('data-patient-id');
        let patientTableRow = $('#patientTableRow-' + patientId)[0].outerHTML;
        $('#patientTableBody').html(patientTableRow);
        $('#patientSearchBox').hide();
        $('#editPatientSearchBox').hide();
        $('#patientTableBody').find('button').replaceWith('<button type="button" class="btn-close btn-close-td" aria-label="Close"></button>');
        $('#searchTag').hide();
        $('#patientTableResult').find('thead').remove();
        $('#patientTableResult').find('button').removeClass('select-btn');

        $('#add_patient_id').val(patientId);
        $('#edit_patient_id').val(patientId);
    });

    // Add submit button ON CLICK 
    $(document).on('click', "#add_submit_button", function(event) {
        $('#add_date').val($('#date').val());
        $('#add_total_amount').val($('#total_amount').val());
        $('#add_treatment_form').submit();

    });

    // Edit submit button ON CLICK 
    $(document).on('click', "#edit_submit_button", function(event) {
        $('#edit_date').val($('#edit_date_display').val());
        $('#edit_total_amount').val($('#edit_total_amount_display').val());
        $('#edit_treatment_form').submit();

    });

    $(document).on('click', ".btn-close-td", function(event) {
        $('#editPatientSearchBox').show();
        $('#patientSearchBox').show();
        $('#patientSearchBox').val('');
        $('#editPatientSearchBox').val('');
        // $('#searchTag').show();
        $('#patientTableBody').remove();

    });

    $('#select-procedures').change(function(e) {
        var id = $(this).children(":selected").attr("id");
        var procedure_id = $(this).children(":selected").attr("data-procedure-id");
        $('#total_amount').val(id);
        $('#procedure_id').val(procedure_id);
    })

    $('#edit-select-procedures').change(function(e) {
        var id = $(this).children(":selected").attr("id");
        var procedure_id = $(this).children(":selected").attr("data-procedure-id");
        $('#edit-total_amount').val(id);
        $('#edit-procedure_id').val(procedure_id);
    })

    // Delete appointment (button action)
    $(document).on('click', ".btn-danger", function() {
        let id = $(this).attr('id');

        $.ajax({
            dataType: 'json',
            url: '/treatments/' + id + '/show',
            success: function(data) {
                data = data[0];
                // Delete form values
                $('#deleteTreatmentId').val(data.treatment_id);
                $('#deletePatientName').val(data.first_name + ' ' + data.last_name);
                $("#deleteTreatmentForm")[0].submit();
            }
        });
    });

    // Search box
    $('#searchBox').keyup(function(event) {
        var query = $(this).val();
        console.log(query)

        if (!(query == '')) {
            $.ajax({
                url: '/treatments/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query
                },
                dataType: 'json',
                success: function(data) {
                    console.log('dito')
                    console.log(data)

                    if (data.length) {
                        let html = renderSearchTable(data);
                        $('#main-content').html(html);
                        $('#main-content').prepend('<p>Search results for "' + query + '":</p>');
                    } else {
                        $('#main-content').html('<p>No search results for "' + query + '".</p>');
                    }

                }
            })
        } else {
            // Fetch patientst
            getTreatments();

            // Fetch procedures
            getProcedures();
        }
    })

    // Tooth number buttons
    $(".btn-purple").click(function() {
        let tooth_no = $(this).text();
        $('#add_tooth_no').val(tooth_no);
        $('#edit_tooth_no').val(tooth_no);
        $('.btn-clicked').addClass('btn-purple').removeClass('btn-clicked');
        $(this).removeClass('btn-purple');
        $(this).addClass('btn-clicked');
    });

    // Procedure upon clicking
    $(document).on('click', ".form-check-input", function() {
        console.log('te')
        let procedureAmountCharged = $(this).attr('data-amount-charged');
        let total_amount_val = $('#total_amount').val();

        $('#total_amount').val(+procedureAmountCharged + +total_amount_val);
        $('#add_total_amount').val(+procedureAmountCharged + +total_amount_val);
        $('#edit_total_amount_display').val(+procedureAmountCharged + +total_amount_val);
        $('#edit_total_amount').val(+procedureAmountCharged + +total_amount_val);


    });

    // Search patients
    $('#patient-input').keyup(function(e) {
        var query = $(this).val();

        if (!(query == '')) {
            $('#patientSearchResults').empty();

            $.ajax({
                url: '/patients/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query
                },
                dataType: 'json',
                success: function(data) {
                    if (data.length > 0) {
                        $('#patientSearchResults').addClass('show');

                        let html = "<p class='pl - 3 text - muted '>Search results for \"" + query + "\"" + ":</p>";

                        for (let i = 0; i < data.length; i++) {
                            html += "<a id='" + data[i].id + "' class='dropdown-item' href='#'>" + data[i].first_name + " " + data[i].last_name + "</a>";
                        }

                        $('#patientSearchResults').html(html);
                    } else {
                        $('#patientSearchResults').addClass('show');
                        let html = "<p class='pl - 3 text - muted '>No search results for \"" + query + "\"" + ".</p>";
                        $('#patientSearchResults').html(html);
                    }
                }
            })
        }

    })

    $('#patientSearchBox').keyup(function(event) {
        var query = $(this).val();

        if (!(query == '')) {
            $.ajax({
                url: '/patients/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query,
                    "status": "active"
                },
                dataType: 'json',
                success: function(data) {
                    if (data.length) {
                        let html = renderPatientSearchResult(data);
                        $('#patientSearchResultBox').html(html);
                        $('#patientSearchResultBox').prepend('<p id="searchTag">SEARCH</p>');
                    } else {
                        $('#patientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
                    }

                }
            })
        } else {
            $('#patientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
        }
    })

    $('#editPatientSearchBox').keyup(function(event) {
        var query = $(this).val();

        if (!(query == '')) {
            $.ajax({
                url: '/patients/search',
                method: 'post',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "query": query,
                    "status": "active"
                },
                dataType: 'json',
                success: function(data) {
                    if (data.length) {
                        let html = renderPatientSearchResult(data);
                        $('#editPatientSearchResultBox').html(html);
                        $('#editPatientSearchResultBox').prepend('<p id="searchTag">SEARCH</p>');
                    } else {
                        $('#editPatientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
                    }

                }
            })
        } else {
            $('#editPatientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
        }
    })

    $('#patient-input').focusout(function(e) {
        $(".dropdown-item").click(function(e) {
            $('#patient-input').val($(this).text());
            $('#patient_id').val($(this).attr('id'));
            $('#patientSearchResults').removeClass('show');

        })
    })

    // $('#patient-input').focusout(function(e) {
    //     $(".dropdown-item").click(function(e) {
    //         $('#patient-input').val($(this).text());
    //         $('#patientAppointmentId').val($(this).attr('id'));
    //         $('#patientSearchResults').removeClass('show');

    //     })
    // })

    // Search box
    // $('#searchBox').keyup(function(event) {
    //     var query = $(this).val();

    //     if (!(query == '')) {
    //         $.ajax({
    //             url: '/treatments/search',
    //             method: 'post',
    //             data: {
    //                 "_token": $('meta[name="csrf-token"]').attr('content'),
    //                 "query": query
    //             },
    //             dataType: 'json',
    //             success: function(data) {
    //                 if (data.length) {
    //                     let html = renderTable(data);
    //                     $('#main-content').html(html);
    //                     $('#main-content').prepend('<p>Search results for "' + query + '":</p>');
    //                 } else {
    //                     $('#main-content').html('<p>No search results for "' + query + '".</p>');
    //                 }

    //             }
    //         })
    //     } else {
    //         // Fetch patients
    //         getTreatments();

    //         // Fetch procedures
    //         getProcedures();
    //     }
    // })


    // Fetch patients
    getTreatments();

    // Fetch procedures
    getProcedures();
})