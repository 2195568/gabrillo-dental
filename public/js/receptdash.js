$(document).on('click', '.btn[data-id]', function () {
    var id = $(this).attr('data-id');
     
        $.ajax({
            dataType: 'json',
            url: '/get-patient/' + id,
            success: function(patient) {

                var firstName = patient.first_name,
                lastName = patient.last_name,
                email = patient.email,
                balance = patient.balance,
                gender = patient.gender,
                birthDate = patient.birth_date,
                address = patient.address,
                age = patient.age,
                contactNo = patient.contact_no,
               // occupation = patient.occupation,
                allergies = patient.allergies,
                //registeredDate = patient.created_at,
                id = patient.id,
                profile_bg_color = patient.profile_bg_color,
                patientStatus = '';
    
            if (patient.account_status == 'active') {
                patientStatus = '<span class = "badge rounded-pill bg-success"> Active </span>';
            } else {
                patientStatus = '<span class="badge rounded-pill bg-danger">Inactive</span>';
            }
 
            
            $('#view_patient_img').html('<img class="img-responsive rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + firstName + '+' + lastName  + profile_bg_color + '&amp;bold=true" alt="User profile image">');
            $('#view_patient_name').text(firstName + ' ' + lastName);
            $('#view_patient_email').text(email);
            $('#view_patient_balance').text(balance);
            $('#view_patient_gender').text(gender);
            $('#view_patient_birthday').text(birthDate);
            $('#view_patient_phone_no').text(contactNo);
            $('#view_patient_address').text(address);
            $('#view_patient_age').text(age);
            $('#view_patient_allergies').text(allergies);
          //  $('#view_patient_occupation').text(occupation);
            $('#view_patient_patient_status').html(patientStatus);
          //  $('#view_patient_registered_date').text(registeredDate);


            }
        });


        $('#viewTreatmentHistoryButton').click(function() {
            getTreatmentHistory(id);

        })

        // Tooth number buttons for treatment history
    $(".treatment-history-tooth-btn").click(function() {
        let tooth_no = $(this).text();
        let patient_id = $('#viewTreatmentHistoryButton').data('patient_id');
        console.log(patient_id);
        getTreatmentHistory(patient_id, tooth_no);
    });

    function getTreatmentHistory(patient_id, tooth_number = 'all') {
        $.ajax({
            dataType: 'json',
            url: '/treatments/' + patient_id + '/' + tooth_number + '/getTreatments',
            success: function(data) {              
                let html = renderTreatmentHistoryTable(data);
                $("#treatmentsHistoryContainer").html(html);
            }
        })
    }

    // Renders treatment history table
    function renderTreatmentHistoryTable(data) {      
        let html =
            '<table class="table ">' +
            '<thead>' +
            '<tr class="p-3 mb-5 rounded">' +
            '<td class="text-center">Treatment ID</td>' +
            '<td class="text-center">Name</td>' +
            '<td class="text-center">Date</td>' +
            '<td class="text-center">Tooth No</td>' +
            '<td class="text-center">Procedure/s</td>' +
            '<td class="text-center">Total Amount</td>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="treatmentBodyTable">';

        if (data.length > 0) {
            for (i = 0; i < data.length; i++) {
                html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
                html += "<td class='text-center'>" + data[i].reference_id + "1</td>";
                html += "<th class='text-center' scope='row'>" + data[i].patient.first_name + ' ' + data[i].patient.last_name + '</th>';
                html += "<td class='text-center'>" + data[i].date + "</td>";
                html += "<td class='text-center'>" + data[i].tooth_no + "</td>";


                $("button:contains(" + data[i].tooth_no + ")").removeClass('btn-light').addClass('btn-warning').attr('disabled', false);

                let procedures = '';


                for (let val of data[i].procedures) {
                    procedures += ' - ' + val.name + '<br>';
                }


                procedures = $.trim(procedures);


                html += "<td class='text-center'><div>" + procedures + "</div></td>";
                html += "<td class='text-center'>" + data[i].total_amount + "</td>";

            }
            html += '</tbody></table>';
        } else {
            html = '<div class="d-flex justify-content-center"><div class="d-flex flex-column justify-content-center"><div class="d-flex flex-column justify-content-center pl-3"><img src="img/no-data.png" style="height: 500px;"></div></div></div>';
            return html;
        }

        return html;
    }

    $('#closeModal').on('click', function() {
        $('.modal').modal('hide');
    });
});