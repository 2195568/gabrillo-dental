Colors = {};
Colors.names = {
    aqua: "00ffff",
    azure: "f0ffff",
    beige: "f5f5dc",
    black: "000000",
    blue: "0000ff",
    brown: "a52a2a",
    cyan: "00ffff",
    fuchsia: "ff00ff",
    gold: "ffd700",
    green: "008000",
    indigo: "4b0082",
    khaki: "f0e68c",
    lightblue: "add8e6",
    lightcyan: "e0ffff",
    lightgreen: "90ee90",
    lightgrey: "d3d3d3",
    lightpink: "ffb6c1",
    lightyellow: "ffffe0",
    lime: "00ff00",
    magenta: "ff00ff",
    maroon: "800000",
    navy: "000080",
    olive: "808000",
    orange: "ffa500",
    pink: "ffc0cb",
    purple: "800080",
    violet: "800080",
    red: "ff0000",
    silver: "c0c0c0",
    white: "ffffff",
    yellow: "ffff00"
};

Colors.random = function() {
    var result;
    var count = 0;
    for (var prop in this.names)
        if (Math.random() < 1 / ++count)
            result = prop;
    return { name: result, rgb: this.names[result] };
};

const numberRegex = /^[a-zA-Z ]+$/;
var newTreatmentToothNoButtonIsClicked = false;
var procedureCheckButtonIsClicked = false;
var hasFutureAppointments = false;
var hasPendingBalance = false;

$(document).ready(function() {
    let maximumDate = new Date();
    maximumDate.setYear(maximumDate.getFullYear() - 1);

    let minimumDate = new Date();
    minimumDate.setYear(minimumDate.getFullYear() - 120);

    $("#patientBirthDate").prop("min", minimumDate.getFullYear() + "-" + ("0" + (minimumDate.getMonth() + 1)).slice(-2) + "-" + ("0" + minimumDate.getDate()).slice(-2))
    $("#patientBirthDate").prop("max", maximumDate.getFullYear() + "-" + ("0" + (maximumDate.getMonth() + 1)).slice(-2) + "-" + ("0" + maximumDate.getDate()).slice(-2))
    $("#edit_birth_date").prop("min", minimumDate.getFullYear() + "-" + ("0" + (minimumDate.getMonth() + 1)).slice(-2) + "-" + ("0" + minimumDate.getDate()).slice(-2))
    $("#edit_birth_date").prop("max", maximumDate.getFullYear() + "-" + ("0" + (maximumDate.getMonth() + 1)).slice(-2) + "-" + ("0" + maximumDate.getDate()).slice(-2))

    // Current path
    url = window.location.href;

    // Fetch patients
    function fetchPatients() {
        if (url.includes('inactive-patients')) {
            getPatients('inactive')
        } else {
            getPatients('active')
        }
    }

    function resetTreatmentHistoryToothNoButtons() {
        $('.new-treatment-tooth-no-btn').removeClass('btn-warning').addClass('btn-light');
        $('.treatment-history-tooth-btn').removeClass('btn-warning').addClass('btn-light');
    }

    // Renders treatment history table
    function renderTreatmentHistoryTable(data) {
        let html =
            '<table class="table ">' +
            '<thead>' +
            '<tr class="p-3 mb-5 rounded">' +
            '<td class="text-center">Treatment ID</td>' +
            '<td class="text-center">Date</td>' +
            '<td class="text-center">Tooth No</td>' +
            '<td class="text-center">Procedure/s</td>' +
            '<td class="text-center">Total Amount</td>' +
            // '<td class="text-center">Action</td>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="treatmentBodyTable">';


        if (data.length > 0) {
            for (i = 0; i < data.length; i++) {
                html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
                html += "<td class='text-center'>" + data[i].reference_id + "1</td>";
                html += "<td class='text-center'>" + data[i].date + "</td>";
                html += "<td class='text-center'>" + data[i].tooth_no + "</td>";
                // console.log(data[i][0].reference_id)

                $("button:contains(" + data[i].tooth_no + ")").removeClass('btn-light').addClass('btn-warning').attr('disabled', false);

                let procedures = '';


                // console.log(data[i][0].procedures[0].name)

                // for (k = 0; i < data[i][0].procedures.length; k++) {
                //     console.log(data[i][0].procedures)
                //     if (k == data[i][0].procedures.length) {
                //         // procedures += data[i][0].procedures[k].name + '';
                //     } else {
                //         // procedures += data[i][0].procedures[k].name + ', ';
                //     }
                // }

                for (let val of data[i].procedures) {
                    procedures += ' - ' + val.name + '<br>';
                }

                // console.log(procedures)

                procedures = $.trim(procedures);


                html += "<td class='text-center'><div>" + procedures + "</div></td>";
                html += "<td class='text-center'>" + data[i].total_amount + "</td>";
                // html += '<td class="text-center"><button id="' + data[i][0].id + '"class="btn btn-danger btn-sm rounded-0 mr-2" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a></button>';
                // html += '<button id="' + data[i][0].id + '" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a></button></td>';
                // html += '</tr>';
            }
            html += '</tbody></table>';
        } else {
            html = '<div class="d-flex justify-content-center"><div class="d-flex flex-column justify-content-center"><div class="d-flex flex-column justify-content-center pl-3"><img src="img/no-data.png" style="height: 500px;"></div></div></div>';
            return html;
        }

        return html;
    }

    function getTreatmentHistory(patient_id, tooth_number = 'all') {
        $.ajax({
            dataType: 'json',
            url: '/treatments/' + patient_id + '/' + tooth_number + '/getTreatments',
            success: function(data) {
                console.log(data)
                let html = renderTreatmentHistoryTable(data);
                console.log('getteraetemnthis')
                console.log(html)
                $("#treatmentsHistoryContainer").html(html);
            }
        })
    }

    // Renders patient's table
    function renderTable(data) {
        // if (data.length > 0) {
        //     let html =
        //         '<table class="table ">' +
        //         '<thead>' +
        //         '<tr class="p-3 mb-5 rounded">' +
        //         '<td class="pl-3">Name</td>' +
        //         '<td>Contact No</td>' +
        //         '<td>Address</td>' +
        //         '<td>New Appointment</td>' +
        //         '<td>Last Appointment</td>' +
        //         '<td>Created At</td>' +
        //         '<td>Action</td>' +
        //         '</tr>' +
        //         '</thead>' +
        //         '<tbody id="patientBodyTable">';

        //     for (i = 0; i < data.length; i++) {
        //         let new_appointment = data[i].new_appointment;
        //         let last_appointment = data[i].last_appointment;
        //         let created_at = new Date(data[i].created_at);

        //         if (new_appointment === null) {
        //             new_appointment = '<span class="text-center">-</span>';
        //         }

        //         if (last_appointment === null) {
        //             last_appointment = '<span class="text-center">-</span>';
        //         }

        //         html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
        //         html += "<th scope='row'><img class='h-8 w-8 rounded-full object-cover' style='float: left;' src='https://ui-avatars.com/api/?name=" + data[i].first_name + '+' + data[i].last_name + "&background=" + data[i].profile_bg_color + "&bold=true' alt='Admin'><span class='th-title ml-2 text-capitalize'>" + data[i].first_name + ' ' + data[i].last_name + ' </span>' + '<u><em>    ' + data[i].slug + '</em></u><br>';
        //         html += "<span class='td-email ml-2'>" + data[i].email + '</span></th>';
        //         html += "<td>" + data[i].contact_no + "</td>";
        //         html += "<td>" + data[i].address + "</td>";
        //         html += "<td>" + new_appointment + "</td>";
        //         html += "<td>" + last_appointment + "</td>";
        //         html += "<td>" + created_at.toISOString().split('T')[0] + "</td>";
        //         html += '<td><button id="' + data[i].id + '"class="btn btn-view btn-sm rounded-0 mr-3" style="background-color: #7764CA; color: white;" type="button" data-toggle="tooltip" data-placement="top" title="View patient"><i class="fa fa-user-circle"></i></a></button>';
        //         html += '</td>';
        //         html += '</tr>';
        //     }

        //     html += '</tbody>';

        //     return html;
        // }
        return;
    }

    // Retrieve patient records from the database
    function getPatients(accountStatus) {
        var url = '';

        if (accountStatus == 'active') {
            url = '/patients/active';
        } else {
            url = '/patients/inactive';
        }

        $.ajax({
            dataType: 'json',
            url: url,
            success: function(data) {
                let html = renderTable(data);
                $("#main-content").html(html);
            }
        })
    }

    function editPatient(patient) {
        var firstName = patient.first_name,
            lastName = patient.last_name,
            middleName = patient.middle_name,
            birthDate = patient.birth_date,
            balance = patient.balance,
            age = patient.age,
            contactNo = patient.contact_no,
            email = patient.email,
            address = patient.address,
            occupation = patient.occupation,
            allergies = patient.allergies,
            gender = patient.gender,
            id = patient.id,
            futureAppointments = patient.future_appointments;

        if (balance > 0) {
            hasPendingBalance = true;
        }

        if (futureAppointments.length > 0) {
            hasFutureAppointments = true;
        }

        // Delete form values
        $('#activatePatientId').val(id);
        $('#deactivatePatientId').val(id);
        $('#activatePatientName').val(firstName + ' ' + lastName);
        $('#deactivatePatientName').val(firstName + ' ' + lastName);

        // Edit form values
        $('#editPatientId').val(id);
        $('#edit_first_name').val(firstName);
        $('#edit_last_name').val(lastName);
        $('#edit_middle_name').val(middleName);
        $('#edit_birth_date').val(birthDate);
        $('#edit_age').val(age);
        $('#edit_contact_number').val(contactNo);
        $('#edit_email_address').val(email);
        $('#edit_home_address').val(address);
        $('#edit_occupation').val(occupation);
        $('#edit_allergies').val(allergies);
        $('#edit_gender').val(gender);
        $('#editModal').modal('toggle');
    }

    function viewPatient(patient) {
        var firstName = patient.first_name,
            lastName = patient.last_name,
            email = patient.email,
            balance = patient.balance,
            gender = patient.gender,
            birthDate = patient.birth_date,
            address = patient.address,
            age = patient.age,
            contactNo = patient.contact_no,
            occupation = patient.occupation,
            allergies = patient.allergies,
            registeredDate = patient.created_at,
            id = patient.id,
            profile_bg_color = patient.profile_bg_color,
            patientStatus = '';

        if (patient.account_status == 'active') {
            patientStatus = '<span class = "badge rounded-pill bg-success"> Active </span>';
        } else {
            patientStatus = '<span class="badge rounded-pill bg-danger">Inactive</span>';
        }

        // Edit form values
        // 
        $('#view_patient_img').html('<img class="img-responsive rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + firstName + '+' + lastName + '&amp;background=' + profile_bg_color + '&amp;bold=true" alt="User profile image">');
        $('#view_patient_name').html(firstName + ' ' + lastName + '<button id="' + id + '" class="btn btn-light btn-sm rounded-0" style="" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a></button>');
        $('#viewTreatmentHistoryButton').attr("patient-id", id);
        $('#view_patient_email').text(email);
        $('#view_patient_balance').text(balance);
        $('#view_patient_gender').text(gender);
        $('#view_patient_birthday').text(birthDate);
        $('#view_patient_phone_no').text(contactNo);
        $('#view_patient_address').text(address);
        $('#view_patient_age').text(age);
        $('#view_patient_allergies').text(allergies);
        $('#view_patient_occupation').text(occupation);
        $('#view_patient_patient_status').html(patientStatus);
        $('#view_patient_registered_date').text(registeredDate);
        $('#viewModal').modal('toggle');
    }

    // Retrieve procedures records from the database
    function getProcedures() {
        $.ajax({
            dataType: 'json',
            url: '/procedures/index',
            success: function(data) {
                let html = renderProcedures(data);
                $(".procedures_container").html(html);
                $("#edit-select-procedures").html(html);
            }
        })
    }

    function renderProcedures(data) {
        let html = '';
        let currentCategoryId = '';

        for (i = 0; i < data.length; i++) {
            if (currentCategoryId == '') {
                currentCategoryId = data[i].procedure_category_id;
                html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend><hr>';
            }

            if (data[i].procedure_category_id == currentCategoryId) {
                html += '<div class="form-check"><input class="form-check-input" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>'
            } else {
                html += '</fieldset></div>';
                html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend>';
                html += '<hr><div class="form-check"><input class="form-check-input procedureCheckButton" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>';

                currentCategoryId = data[i].procedure_category_id;
            }
        }

        html += '</div>';

        // let html = '<fieldset><legend>' + data.category + '</legend> <hr> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="1"> <label class="form-check-label" for="flexCheckDefault"> Tooth Extraction </label> </div> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="Pasta"> <label class="form-check-label" for="flexCheckChecked"> Pasta </label> </div> </fieldset>';

        // for (i = 0; i < data.length; i++) {
        //     html += '<option ' + 'id="' + data[i].amount_charged + '" data-procedure-id="' + data[i].id + '">' + data[i].name + '</option>';
        // }

        return html;
    }

    // Renders search result for patient
    // function renderPatientSearchResult(data) {
    //     let html =
    //         '<table id="patientTableResult" class="table patientTableResult">' +
    //         '<thead>' +
    //         '<tr>' +
    //         '<td></td>' +
    //         '<td></td>' +
    //         '</tr>' +
    //         '</thead>' +
    //         '<tbody id="patientTableBody">';


    //     for (i = 0; i < data.length; i++) {
    //         html += "<tr id='patientTableRow-" + data[i].id + "' class='shadow-sm p-3 mb-5 bg-white rounded'>";
    //         html += "<th class='patientResultTh' role='button' scope='row'><img class='h-8 w-8 rounded-full object-cover' style='float: left;' src='https://ui-avatars.com/api/?name=" + data[i].first_name + '+' + data[i].last_name + "&background=random&bold=true' alt='Admin'><span class='th-title ml-2 text-capitalize'>" + data[i].first_name + ' ' + data[i].last_name + '</span><u><em>    ' + data[i].id + '</em></u><br>';
    //         html += "<span class='td-email ml-2'>" + data[i].email + '</span></th>';
    //         html += '<td><span class="d-flex justify-content-end"><button type="button" class="btn btn-light select-btn" data-patient-id="' + data[i].id + '">Select</button></span></td>';
    //         html += '</tr>';
    //     }

    //     html += '</tbody>';


    //     return html;
    // }

    // Computes age given the birthdate
    function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    function setErrorFor(input, message) {
        const formControl = input.parentElement;
        const small = formControl.querySelector('small');
        formControl.className = 'form-con error';
        small.innerText = message;
    }


    function setSuccessFor(input) {
        const formControl = input.parentElement;
        formControl.className = 'form-con success';
    }

    // Checks if value is a number
    function isNum(val) {
        let isnum = /^\d+$/.test(val);
        return isnum;
    }

    function isEmail(email) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }

    $('#addPaymentButton').click(function() {
        $('#viewModal').modal('hide');
    })

    // Add new treatment button ON CLICK 
    $(document).on('click', ".new-treatment-tooth-no-btn", function(event) {
        newTreatmentToothNoButtonIsClicked = true;
        let tooth_no = $(this).text();
        $('#add_tooth_no').val(tooth_no);
        $('.new-treatment-tooth-no-btn').removeClass('btn-warning').addClass('btn-light')
        $(this).removeClass('btn-light').addClass('btn-warning')
            // $('#add_date').val($('#date').val());
            // $('#add_total_amount').val($('#total_amount').val());
            // $('#add_treatment_form').submit();

    });

    // Select button on patient search results
    // $(document).on('click', ".select-btn", function(event) {
    //     let patientId = $(this).attr('data-patient-id');
    //     let patientTableRow = $('#patientTableRow-' + patientId)[0].outerHTML;
    //     $('#patientTableBody').html(patientTableRow);
    //     $('#patientSearchBox').hide();
    //     $('#editPatientSearchBox').hide();
    //     $('#patientTableBody').find('button').replaceWith('<button type="button" class="btn-close btn-close-td" aria-label="Close"></button>');
    //     $('#searchTag').hide();
    //     $('#patientTableResult').find('thead').remove();
    //     $('#patientTableResult').find('button').removeClass('select-btn');

    //     $('#add_patient_id').val(patientId);
    //     $('#edit_patient_id').val(patientId);
    // });

    // $('#patientSearchBox').keyup(function(event) {
    //     var query = $(this).val();

    //     if (!(query == '')) {
    //         $.ajax({
    //             url: '/patients/search',
    //             method: 'post',
    //             data: {
    //                 "_token": $('meta[name="csrf-token"]').attr('content'),
    //                 "query": query,
    //                 "status": "active"
    //             },
    //             dataType: 'json',
    //             success: function(data) {
    //                 if (data.length) {
    //                     let html = renderPatientSearchResult(data);
    //                     $('#patientSearchResultBox').html(html);
    //                     $('#patientSearchResultBox').prepend('<p id="searchTag">SEARCH</p>');
    //                 } else {
    //                     $('#patientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
    //                 }

    //             }
    //         })
    //     } else {
    //         $('#patientSearchResultBox').html('<p class="text-justify text-muted">No patient found</p>');
    //     }
    // })

    // Procedure upon clicking
    $(document).on('click', ".form-check-input", function() {
        procedureCheckButtonIsClicked = true;
        let procedureAmountCharged = $(this).attr('data-amount-charged');
        let total_amount_val = $('#total_amount').val();

        if ($(this).is(":checked")) {
            $('#total_amount').val((+procedureAmountCharged) + (+total_amount_val));
        } else {
            $('#total_amount').val((+total_amount_val) - (+procedureAmountCharged));
        }

        $('#add_total_amount').val(+procedureAmountCharged + +total_amount_val);
        $('#edit_total_amount_display').val(+procedureAmountCharged + +total_amount_val);
        $('#edit_total_amount').val(+procedureAmountCharged + +total_amount_val);
    });

    $(document).on('click', '.close', function() {
        $('.modal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    })

    // Add modal save changes on click
    $('#save-changes').click(function(event) {
        var successCounter = 0;

        let patientFirstNameValue = $("#patientFirstName").val()
        let patientLastNameValue = $("#patientLastName").val()
        let patientMiddleNameValue = $("#patientMiddleName").val()
        let patientBirthDateValue = $("#patientBirthDate").val()
        let patientAgeValue = $("#patientAge").val()
        let patientGenderValue = $("#patientGender").val()
        let patientContactNumberValue = $("#patientContactNumber").val()
        let patientEmailAddressValue = $("#patientEmailAddress").val()
        let patientHomeAddressValue = $("#patientHomeAddress").val()
        let patientOccupationValue = $("#patientOccupation").val()
        let patientAllergiesValue = $("#patientAllergies").val()

        let isContactNoIsNum = /^\d+$/.test(patientContactNumberValue);

        event.preventDefault();
        // Validate first name
        if (patientFirstNameValue === '') {
            setErrorFor(patientFirstName, 'First name cannot be blank!');
        } else if (!(patientFirstNameValue.match(numberRegex))) {
            setErrorFor(patientFirstName, 'It must not contain numbers!')
        } else if (patientFirstNameValue.length <= 1) {
            setErrorFor(patientFirstName, 'Invalid Input!')
        } else {
            setSuccessFor(patientFirstName)
            successCounter++;
        }

        // Validate last name
        if (patientLastNameValue === '') {
            setErrorFor(patientLastName, 'Last name cannot be blank!');
        } else if (!(patientLastNameValue.match(numberRegex))) {
            setErrorFor(patientLastName, 'It must not contain numbers!')
        } else if (patientLastNameValue.length <= 1) {
            setErrorFor(patientLastName, 'Invalid Input!')
        } else {
            setSuccessFor(patientLastName)
            successCounter++;
        }

        // Validate middle name
        if (patientMiddleNameValue === '') {
            setErrorFor(patientMiddleName, 'Middle name cannot be blank!');
        } else if (!(patientMiddleNameValue.match(numberRegex))) {
            setErrorFor(patientMiddleName, 'It must not contain numbers!')
        } else if (patientMiddleNameValue.length <= 1) {
            setErrorFor(patientMiddleName, 'Invalid Input!')
        } else {
            setSuccessFor(patientMiddleName)
            successCounter++;
        }

        // Validate birth date
        if (patientBirthDateValue === '') {
            setErrorFor(patientBirthDate, 'Birth date cannot be blank!');
        } else {
            setSuccessFor(patientBirthDate)
            successCounter++;
        }

        // Validate age
        if (patientAgeValue == "") {
            setErrorFor(patientAge, 'Age cannot be blank!');
        } else if (!(isNum(patientAgeValue))) {
            setErrorFor(patientAge, 'Age must be numberic!');
        } else if (patientAgeValue != getAge(patientBirthDateValue)) {
            setErrorFor(patientAge, 'Age does not match the given birth date!');
        } else {
            setSuccessFor(patientAge);
            successCounter++;
        }

        // Validate gender
        if (patientGenderValue == "") {
            setErrorFor(patientGender, 'Gender cannot be blank!');
        } else {
            setSuccessFor(patientGender);
            successCounter++;
        }

        // Validate contact number
        contactNumberSuffix = patientContactNumberValue.substring(3)

        if (patientContactNumberValue == "") {
            setErrorFor(patientContactNumber, "Contact number cannot be blank!")
        } else if (!(isNum(contactNumberSuffix))) {
            setErrorFor(patientContactNumber, 'Contact number must be numberic!');
        } else if (!(patientContactNumberValue.startsWith("+639"))) {
            setErrorFor(patientContactNumber, "Contact number format is invalid. It must starts with +639!")
        } else if (patientContactNumberValue.length < 13) {
            setErrorFor(patientContactNumber, "Contact number length is invalid!")
        } else {
            setSuccessFor(patientContactNumber)
            successCounter++;
        }

        // Validate email address
        if (patientEmailAddressValue == "") {
            setErrorFor(patientEmailAddress, "Email cannot be blank!")
        } else if (!isEmail(patientEmailAddressValue)) {
            setErrorFor(patientEmailAddress, 'Invalid Email!')
        } else {
            setSuccessFor(patientEmailAddress);
            successCounter++;
        }

        if (patientHomeAddressValue == '') {
            setErrorFor(patientHomeAddress, 'Address cannot be blank!');
        } else {
            setSuccessFor(patientHomeAddress);
            successCounter++;
        }

        if (successCounter >= 9) {
            $('form#addForm').submit();
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'You left an invalid input'
            })
        }
    })

    // Edit modal save changes on click
    $('#editSaveChanges').click(function(event) {
        var successCounter = 0;

        let patientFirstNameValue = $("#edit_first_name").val()
        let patientLastNameValue = $("#edit_last_name").val()
        let patientMiddleNameValue = $("#edit_middle_name").val()
        let patientBirthDateValue = $("#edit_birth_date").val()
        let patientAgeValue = $("#edit_age").val()
        let patientGenderValue = $("#edit_gender").val()
        let patientContactNumberValue = $("#edit_contact_number").val()
        let patientEmailAddressValue = $("#edit_email_address").val()
        let patientHomeAddressValue = $("#edit_home_address").val()
        let patientOccupationValue = $("#edit_occupation").val()
        let patientAllergiesValue = $("#edit_allergies").val()

        event.preventDefault();

        // Validate first name
        if (patientFirstNameValue === '') {
            setErrorFor(edit_first_name, 'First name cannot be blank!');
        } else if (!(patientFirstNameValue.match(numberRegex))) {
            setErrorFor(edit_first_name, 'It must not contain numbers!')
        } else if (patientFirstNameValue.length <= 1) {
            setErrorFor(edit_first_name, 'Invalid Input!')
        } else {
            setSuccessFor(edit_first_name)
            successCounter++;
        }

        // Validate last name
        if (patientLastNameValue === '') {
            setErrorFor(edit_last_name, 'Last name cannot be blank!');
        } else if (!(patientLastNameValue.match(numberRegex))) {
            setErrorFor(edit_last_name, 'It must not contain numbers!')
        } else if (patientLastNameValue.length <= 1) {
            setErrorFor(edit_last_name, 'Invalid Input!')
        } else {
            setSuccessFor(edit_last_name)
            successCounter++;
        }

        // Validate middle name
        if (patientMiddleNameValue === '') {
            setErrorFor(edit_middle_name, 'Middle name cannot be blank!');
        } else if (!(patientMiddleNameValue.match(numberRegex))) {
            setErrorFor(edit_middle_name, 'It must not contain numbers!')
        } else if (patientMiddleNameValue.length <= 1) {
            setErrorFor(edit_middle_name, 'Invalid Input!')
        } else {
            setSuccessFor(edit_middle_name)
            successCounter++;
        }

        // Validate birth date
        if (patientBirthDateValue === '') {
            setErrorFor(edit_birth_date, 'Birth date cannot be blank!');
        } else {
            setSuccessFor(edit_birth_date)
            successCounter++;
        }

        // Validate age
        if (patientAgeValue == "") {
            setErrorFor(edit_age, 'Age cannot be blank!');
        } else if (!(isNum(patientAgeValue))) {
            setErrorFor(edit_age, 'Age must be numberic!');
        } else if (patientAgeValue != getAge(patientBirthDateValue)) {
            setErrorFor(edit_age, 'Age does not match the given birth date!');
        } else {
            setSuccessFor(edit_age);
            successCounter++;
        }

        // Validate gender
        if (patientGenderValue == "") {
            setErrorFor(edit_gender, 'Gender cannot be blank!');
        } else {
            setSuccessFor(edit_gender);
            successCounter++;
        }

        // Validate contact number
        contactNumberSuffix = patientContactNumberValue.substring(3)

        if (patientContactNumberValue == "") {
            setErrorFor(edit_contact_number, "Contact number cannot be blank!")
        } else if (!(isNum(contactNumberSuffix))) {
            setErrorFor(edit_contact_number, 'Contact number must be numberic!');
        } else if (patientContactNumberValue.length < 13) {
            setErrorFor(edit_contact_number, "Contact number length is invalid!")
        } else if (!(patientContactNumberValue.startsWith("+639"))) {
            setErrorFor(edit_contact_number, "Contact number format is invalid. It must starts with +639!")
        } else {
            setSuccessFor(edit_contact_number)
            successCounter++;
        }

        // Validate email address
        if (patientEmailAddressValue == "") {
            setErrorFor(edit_email_address, "Email cannot be blank!")
        } else if (!isEmail(patientEmailAddressValue)) {
            setErrorFor(edit_email_address, 'Invalid Email!')
        } else {
            setSuccessFor(edit_email_address);
            successCounter++;
        }

        if (patientHomeAddressValue == '') {
            setErrorFor(edit_home_address, 'Address cannot be blank!');
        } else {
            setSuccessFor(edit_home_address);
            successCounter++;
        }

        if (successCounter >= 9) {
            $('form#editForm').submit();
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'You left an invalid input'
            })
        }
    })

    $("#addNewTreatmentButton").click(function(event) {
        var successCounter = 0;
        event.preventDefault();

        // Validate treatment and procedure
        if (!(newTreatmentToothNoButtonIsClicked)) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Tooth number is required!'
            })
            return;
        } else if (!(procedureCheckButtonIsClicked)) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Procedure is required!'
            })
            return;
        }

        // Validate date
        if ($("#date").val() == "") {
            setErrorFor(date, "Date cannot be blank!")
        } else {
            setSuccessFor(date)
            successCounter++
        }

        // Validate total amount
        if ($("#total_amount").val() == "") {
            setErrorFor(total_amount, "Total amount cannot be blank!")
        } else {
            setSuccessFor(total_amount)
            successCounter++
        }

        if (successCounter >= 2) {
            $('#add_date').val($('#date').val());
            $('#add_total_amount').val($('#total_amount').val());
            $('#add_treatment_form').submit();

        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'You left an invalid input'
            })
        }
    })

    // Edit form close button on click
    $("#editFormCloseButton").click(function() {
        $("#editFormModalBody").html('<div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">FIRST NAME <b style="color: red;"> *</b></h6> <input id="edit_first_name" type="text" name="first_name" class="form-control" maxlength="26" required> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">LAST NAME <b style="color: red;"> *</b></h6> <input id="edit_last_name" type="text" name="last_name" class="form-control" maxlength="26" required> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">MIDDLE NAME <b style="color: red;"> *</b></h6> <input id="edit_middle_name" type="text" name="middle_name" class="form-control" maxlength="26" required> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">BIRTH DATE <b style="color: red;"> *</b></h6> <input id="edit_birth_date" type="date" name="birth_date" autocomplete="off" required> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">AGE <b style="color: red;"> *</b></h6> <input id="edit_age" type="number" name="age" class="form-control" required> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">GENDER <b style="color: red;"> *</b></h6> <select class="form-control" id="edit_gender" name="gender" required> <option value="" selected>Select gender</option> <option value="male">Male</option> <option value="female">Female</option> </select> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">CONTACT NUMBER <b style="color: red;"> *</b></h6><br> <input type="text" id="edit_contact_number" name="contact_no" maxlength="13" autocomplete="off" value="+639" required> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">EMAIL ADDRESS <b style="color: red;"> *</b></h6> <input id="edit_email_address" type="email" name="email_address" class="form-control" required> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">HOME ADDRESS <b style="color: red;"> *</b></h6> <input id="edit_home_address" type="text" name="home_address" class="form-control" required> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">OCCUPATION</h6> <input id="edit_occupation" type="text" name="occupation" class="form-control"> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> </div> <div class="form-group form-con"> <h6 class="modal-title pt-2" style="color: #292a2d;">ALLERGIES</h6> <input id="edit_allergies" type="text" name="allergies" class="form-control" aria-describedby="allergiesHelpBlock"> <i class="fa fa-check-circle"></i> <i class="fa fa-exclamation-circle"></i> <small>Error message</small> <p class="font-intalic text-muted"> For more than 1 allergy, please use comma to separate the items (e.g., peanuts, pollen, eggs, milk).</p></div>')
    })

    // Tooth number buttons for treatment history
    $(".treatment-history-tooth-btn").click(function() {
        let tooth_no = $(this).text();
        let patient_id = $('#viewTreatmentHistoryButton').data('patient_id');
        getTreatmentHistory(patient_id, tooth_no)
    });

    $('#addPaymentButton').click(function() {
        $('#viewModal').modal('hide');
    })

    $('#viewTreatmentHistoryButton').click(function() {
        resetTreatmentHistoryToothNoButtons();
        let id = $('#viewTreatmentHistoryButton').attr('patient-id');
        console.log('idpo')
        console.log(id)
        getTreatmentHistory(id);
        $('#viewModal').modal('hide');
        $('#treatmentHistoryModal').modal('show');
    })

    $('#plus-button').click(function() {
        var color = Colors.random();
        $('#profile_bg_color').val(color.rgb);
    })

    $('#addTreatmentButton').click(function() {
        console.log('clclcl')
        newTreatmentToothNoButtonIsClicked = false;
        procedureCheckButtonIsClicked = false;

        resetTreatmentHistoryToothNoButtons();

        var first_name = $(this).data('first-name');
        var last_name = $(this).data('last-name');
        var email = $(this).data('email');
        $('#patientSearchResultBox').html(
            '<table id="patientTableResult" class="table patientTableResult"><tbody id="patientTableBody"><tr id="patientTableRow-3" class="shadow-sm p-3 mb-5 bg-white rounded"><th class="patientResultTh" role="button" scope="row"><img class="h-8 w-8 rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + first_name + '' + last_name + '&amp;background=random&amp;bold=true"><span class="th-title ml-2 text-capitalize">' + first_name + '' + last_name + '</span><br><span class="td-email ml-2">' + email + '</span></th><td></td></tr></tbody></table>'
        );
        $('#viewModal').modal('hide');
    })

    $("#add_treatment_button").click(function() {
        event.preventDefault();

        // Validate patient
        if ($("#patientSearchBox").val() == "") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Patient is required!'
            })
            return
        }

        // Validate treatment and procedure
        if (!(newTreatmentToothNoButtonIsClicked)) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Tooth number is required!'
            })
            return;
        } else if (!(procedureCheckButtonIsClicked)) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Procedure is required!'
            })
            return;
        }

        // Validate date
        if ($("#date").val() == "") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Date cannot be blank!'
            })
            return;
        }

        // Validate total amount
        if ($("#total_amount").val() == "" || $("#total_amount").val() == "0.00") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Total amount cannot be blank!'
            })
            return;
        }

        $('#add_date').val($('#date').val());
        $('#add_total_amount').val($('#total_amount').val());
        $('#add_treatment_form').submit();
    })

    // View button on click
    $(document).on('click', ".btn-view", function() {
        hasFutureAppointments = false;
        hasPendingBalance = false;
        // $('#searchResult').addClass('show');
        // $('#searchResult').removeClass('show');
        let id = this.id;
        $('#addTreatmentButton').data('patient-id', id);
        $('#add_patient_id').val(id);
        $('#viewTreatmentHistoryButton').data('patient_id', id);

        $.ajax({
            dataType: 'json',
            url: '/get-patient/' + id,
            success: function(data) {
                $('#addTreatmentButton').data('first-name', data.first_name);
                $('#addTreatmentButton').data('last-name', data.last_name);
                $('#addTreatmentButton').data('email', data.email);
                viewPatient(data);
            }
        });

    });

    // Tooth number buttons
    $(".btn-purple").click(function() {
        newTreatmentToothNoButtonIsClicked = true;
        editTreatmentToothNoButtonIsClicked = true;
        let tooth_no = $(this).text();
        $('#add_tooth_no').val(tooth_no);
        $('#edit_tooth_no').val(tooth_no);
        $('.btn-warning').addClass('btn-light').removeClass('btn-warning');
        $(this).removeClass('btn-light');
        $(this).addClass('btn-warning');
    });

    // Edit button on click
    $(document).on('click', ".btn-light", function() {
        $("#viewModal").modal('hide');
        let id = this.id;

        $.ajax({
            dataType: 'json',
            url: '/get-patient/' + id,
            success: function(data) {
                editPatient(data);
            }
        });

    });

    // Deactivate patient (button action)
    $("#deactivatePatientButton").click(function(event) {
        if (hasPendingBalance) {
            Swal.fire({
                title: 'Oops...',
                text: 'The patient has a remaining balance. Once you deactivate the patient, all of the patient\'s remaining balance will be disregarded. Are you sure you want to deactivate the patient?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
                showCancelButton: true,
                showConfirmButton: true,
                cancelButtonText: 'No',
                confirmButtonText: 'Yes',
            }).then((result) => {
                if (result.value) {
                    $("#deactivatePatientForm")[0].submit();
                }
            });

            return;
        }

        if (hasFutureAppointments) {
            Swal.fire({
                title: 'Oops...',
                text: 'The patient has an upcoming appointment. Are you sure you want to deactivate the patient?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
                showCancelButton: true,
                showConfirmButton: true,
                cancelButtonText: 'No',
                confirmButtonText: 'Yes',
            }).then((result) => {
                if (result.value) {
                    $("#deactivatePatientForm")[0].submit();
                }
            });

            return;
        }

        $("#deactivatePatientForm")[0].submit();
    })

    // Activate patient (button action)
    $("#activatePatientButton").click(function() {
        $("#activatePatientForm")[0].submit();
    })

    // Search box
    // $('#searchBox').keyup(function(event) {
    //     var query = $(this).val();

    //     if (!(query == '')) {
    //         $.ajax({
    //             url: '/patients/search',
    //             method: 'post',
    //             data: {
    //                 "_token": $('meta[name="csrf-token"]').attr('content'),
    //                 "query": query,
    //                 "status": ""
    //             },
    //             dataType: 'json',
    //             success: function(data) {
    //                 if (data.length) {
    //                     let html = renderTable(data);
    //                     $('#main-content').html(html);
    //                     $('#main-content').prepend('<p>Search results for "' + query + '":</p>');
    //                 } else {
    //                     $('#main-content').html('<p>No search results for "' + query + '".</p>');
    //                 }

    //             }
    //         })
    //     } else {
    //         fetchPatients();
    //     }
    // })


    // Fetch patients
    fetchPatients();

    getProcedures();
})