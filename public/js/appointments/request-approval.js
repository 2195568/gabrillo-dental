 $(document).ready(function() {
     function init() {
         getOldAppointmentRequests();
         getNewAppointmentRequests();
         getProcedures();
     }

     function renderOldAppointmentTable(data) {
         let token = $('meta[name="csrf-token"]').attr('content');

         let html = '<table class="table table-hover"> <thead><tr class="p-3 mb-5 rounded"><td>Basic Info</td><td style="width:30%; text-align: center;">Description</td><td style="text-align: center;">Patient Type</td><td style="text-align: center;">Schedule</td><td style="text-align: center;">Action</td></tr></thead><tbody>';

         for (i = 0; i < data.length; i++) {
             profile_bg_color = ''

             if (data[i].profile_bg_color == '') {
                 profile_bg_color = 'random';
             } else {
                 profile_bg_color = data[i].profile_bg_color;
             }

             html += '<tr class="shadow p-3 mb-5 bg-white rounded"> <td><p><img class="h-8 w-8 rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + data[i].first_name + '+' + data[i].last_name + '&background=' + profile_bg_color + '&bold=true"><b>' + data[i].first_name + ' ' + data[i].last_name + '</b></p><br> <p id="emailAddress"><b>@</b>' + data[i].email + '</p> <p class="fa fa-phone ml-5"> ' + data[i].contact_no + '</p> </td> <td class="text-center" style="width:80px;">' + data[i].description + '</td> <td class="text-center">Old</td> <td class="text-center"><p class="fa fa-calendar"><b class="ml-2">' + data[i].date + '</b></p><br><p class="fa fa-clock-o"><b>' + data[i].time_start + '</b></p></td> <td class="text-center">' +
                 '<button type="submit" data-patient-id="' + data[i].patient_id + '" data-date="' + data[i].date + '" data-time_start="' + data[i].time_start + '" data-time_end="' + data[i].time_end + '" data-description="' + data[i].description + '" data-first-name="' + data[i].first_name + '" data-last-name="' + data[i].last_name + '" data-email="' + data[i].email + '"  class="btn btn-primary badge-pill accept" style="width:80px;">Accept</button> <button data-patient-id="' + data[i].patient_id + '" data-email="' + data[i].email + '" data-date="' + data[i].date + '" data-patient_type="old" data-time="' + data[i].time_start + '" class="btn btn-danger badge-pill reject" style="width:80px;">Reject</button> </td> </tr>'
         }

         html += '</tbody> </table>';

         return html
     }

     function renderNewAppointmentTable(data) {
         let token = $('meta[name="csrf-token"]').attr('content');

         let html = '<table class="table table-hover"> <thead><tr class="p-3 mb-5 rounded"><td>Basic Info</td><td style="width:30%; text-align: center;">Description</td><td style="text-align: center;">Patient Type</td><td style="text-align: center;">Schedule</td><td style="text-align: center;">Action</td></tr></thead><tbody>';

         for (i = 0; i < data.length; i++) {
             profile_bg_color = ''

             if (data[i].profile_bg_color == '') {
                 profile_bg_color = 'random';
             } else {
                 profile_bg_color = data[i].profile_bg_color;
             }

             html += '<tr class="shadow p-3 mb-5 bg-white rounded"> <td><p><img class="h-8 w-8 rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + data[i].first_name + '+' + data[i].last_name + '&background=' + profile_bg_color + '&bold=true"><b>' + data[i].first_name + ' ' + data[i].last_name + '</b></p><br> <p id="emailAddress"><b>@</b>' + data[i].email + '</p> <p class="fa fa-phone ml-5"> ' + data[i].contact_no + '</p> </td> <td class="text-center" style="width:80px;">' + data[i].description + '</td> <td class="text-center">New</td> <td class="text-center"><p class="fa fa-calendar"><b class="ml-2">' + data[i].date + '</b></p><br><p class="fa fa-clock-o"><b>' + data[i].time_start + '</b></p></td> <td class="text-center">' +
                 '<button type="submit" data-patient_type="new" data-patient-id="' + data[i].patient_id + '" data-date="' + data[i].date + '" data-time="' + data[i].time_start + '" data-time_end="' + data[i].time_end + '" data-description="' + data[i].description + '" data-first-name="' + data[i].first_name + '" data-last-name="' + data[i].last_name + '" data-middle-name="' + data[i].middle_name + '" data-email="' + data[i].email + '" data-address="' + data[i].address + '" data-contact_no="' + data[i].contact_no + '" data-occupation="' + data[i].occupation + '" data-birth-date="' + data[i].birth_date + '" data-age="' + data[i].age + '" data-gender="' + data[i].gender + '" data-allergies="' + data[i].allergies + '" data-profile_bg_color="' + data[i].profile_bg_color + '" data-reference_id="' + data[i].reference_id + '"  class="btn btn-primary badge-pill review" style="width:80px;">Review</button></td> </tr>'
         }

         html += '</tbody> </table>';

         return html
     }

     // Retrieve request records from the database
     function getOldAppointmentRequests() {
         $.ajax({
             dataType: 'json',
             url: '/old-appointment-requests',
             success: function(data) {
                 if (data.length > 0) {
                     let html = renderOldAppointmentTable(data);
                     $("#tab1").html(html);
                 }
             }
         })
     }

     // Retrieve request records from the database
     function getNewAppointmentRequests() {
         $.ajax({
             dataType: 'json',
             url: '/new-appointment-requests',
             success: function(data) {
                 if (data.length > 0) {
                     let html = renderNewAppointmentTable(data);
                     $("#tab2").html(html);
                 }
             }
         })
     }

     function renderProcedures(data) {
         let html = '';
         let currentCategoryId = '';

         for (i = 0; i < data.length; i++) {
             if (currentCategoryId == '') {
                 currentCategoryId = data[i].procedure_category_id;
                 html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend><hr>';
             }

             if (data[i].procedure_category_id == currentCategoryId) {
                 html += '<div class="form-check"><input class="form-check-input" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>'
             } else {
                 html += '</fieldset></div>';
                 html += '<div class="col-md-4"><fieldset><legend>' + data[i].category + '</legend>';
                 html += '<hr><div class="form-check"><input class="form-check-input" name="procedures[]" type="checkbox" value="' + data[i].procedure_id + '" data-amount-charged="' + data[i].amount_charged + '"> <label class="form-check-label" for="flexCheckDefault">' + data[i].name + '</label></div>';

                 currentCategoryId = data[i].procedure_category_id;
             }
         }

         html += '</div>';

         // let html = '<fieldset><legend>' + data.category + '</legend> <hr> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="1"> <label class="form-check-label" for="flexCheckDefault"> Tooth Extraction </label> </div> <div class="form-check"> <input class="form-check-input" name="procedures[]" type="checkbox" value="Pasta"> <label class="form-check-label" for="flexCheckChecked"> Pasta </label> </div> </fieldset>';

         // for (i = 0; i < data.length; i++) {
         //     html += '<option ' + 'id="' + data[i].amount_charged + '" data-procedure-id="' + data[i].id + '">' + data[i].name + '</option>';
         // }

         return html;
     }

     // Retrieve procedures records from the database
     function getProcedures() {
         $.ajax({
             dataType: 'json',
             url: '/procedures/index',
             success: function(data) {
                 let html = renderProcedures(data);
                 $(".procedures_container").html(html);
                 $("#edit-select-procedures").html(html);
             }
         })
     }

     $(document).on('click', '.accept', function() {
         let patientId = $(this).data('patient-id')
         let date = $(this).data('date')
         let time_start = $(this).data('time_start')
         let time_end = $(this).data('time_end')
         let description = $(this).data('description')
         let first_name = $(this).data('first-name')
         let last_name = $(this).data('last-name')
         let email = $(this).data('email')

         $('#editPatientSearchResultBox').html(
             '<table id="patientTableResult" class="table patientTableResult"><tbody id="patientTableBody"><tr id="patientTableRow-3" class="shadow-sm p-3 mb-5 bg-white rounded"><th class="patientResultTh" role="button" scope="row"><img class="h-8 w-8 rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + first_name + '+' + last_name + '&amp;background=random&amp;bold=true"><span class="th-title ml-2 text-capitalize">' + first_name + '' + last_name + '</span><br><span class="td-email ml-2">' + email + '</span></th></tr></tbody></table>'
         );
         $('#editPatientSearchBox').hide();

         $('#date').val(date)
         $('#time').val(time_start)
         $('#time_end').val(time_end)
         $('#description').val(description)
         $('#patient_id').val(patientId)
         $('#email').val(email)
         $('#first_name').val(first_name)
         $('#last_name').val(last_name)

         $('#addModal').modal('toggle');
     })

     $(document).on('click', '.reject', function() {
         let patientId = $(this).data('patient-id')
         let email = $(this).data('email')
         let firstName = $(this).data('first_name')
         let lastName = $(this).data('last_name')
         let date = $(this).data('date')
         let time = $(this).data('time')
         let patient_type = $(this).data('patient_type')

         $('#reject_patient_id').val(patientId)
         $('#reject_patient_type').val(patient_type)
         $('#reject_email').val(email)
         $('#reject_first_name').val(firstName)
         $('#reject_last_name').val(lastName)
         $('#reject_date').val(date)
         $('#reject_time').val(time)

         $('#rejectModal').modal('toggle');
     })

     $(document).on('click', '.reject-review', function() {
         $('#rejectModal').modal('toggle');
     })

     $(document).on('click', '.review', function() {
         let email = $(this).data('email')
         let firstName = $(this).data('first-name')
         let lastName = $(this).data('last-name')
         let date = $(this).data('date')
         let time = $(this).data('time')
         let time_end = $(this).data('time_end')
         let description = $(this).data('description')
         let middle_name = $(this).data('middle-name')
         let address = $(this).data('address')
         let contact_no = $(this).data('contact_no')
         let occupation = $(this).data('occupation')
         let birth_date = $(this).data('birth-date')
         let age = $(this).data('age')
         let gender = $(this).data('gender')
         let profile_bg_color = $(this).data('profile_bg_color')
         let reference_id = $(this).data('reference_id')

         $('#review_first_name').val(firstName)
         $('#review_last_name').val(lastName)
         $('#review_middle_name').val(middle_name)
         $('#review_birth_date').val(birth_date)
         $('#review_age').val(age)
         $('#review_gender').val(gender)
         $('#review_address').val(address)
         $('#review_contact_no').val(contact_no)
         $('#review_email').val(email)
         $('#review_occupation').val(occupation)

         $('#reject_email').val(email)
         $('#reject_first_name').val(firstName)
         $('#reject_last_name').val(lastName)
         $('#reject_date').val(date)
         $('#reject_time').val(time)

         $('#review_date').val(date)
         $('#review_time').val(time)
         $('#review_time_end').val(time_end)
         $('#review_description').val(description)

         $('#review_profile_bg_color').val(profile_bg_color)
         $('#review_reference_id').val(reference_id)

         $('#reviewModal').modal('toggle');
     })

     $(".tab-slider--body").hide();
     $(".tab-slider--body:first").show();

     $(".tab-slider--nav li").click(function() {
         $(".tab-slider--body").hide();
         var activeTab = $(this).attr("rel");
         $("#" + activeTab).fadeIn();
         if ($(this).attr("rel") == "tab2") {
             $('.tab-slider--tabs').addClass('slide');
         } else {
             $('.tab-slider--tabs').removeClass('slide');
         }
         $(".tab-slider--nav li").removeClass("active");
         $(this).addClass("active");
     });


     init()
 });