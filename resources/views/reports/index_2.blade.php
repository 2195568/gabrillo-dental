<!DOCTYPE html>
<html lang="en">

<head>
    <!-- CSS only -->
    <link href="https://netdna.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <!-- JavaScript only -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js">
    </script>

    <style>
        * {
            box-sizing: border-box;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        /* Style the header */
        header {
            background-color: #bf5af2;
            padding: 30px;
            text-align: center;
            font-size: 28px;
            color: white;
        }

        /* Create two columns/boxes that floats next to each other */
        nav {
            float: left;
            width: 30%;
            height: 300px;
            /* only for demonstration, should be removed */
            background: #ccc;
            padding: 20px;
        }

        /* Style the list inside the menu */
        nav ul {
            list-style-type: none;
            padding: 0;
        }

        article {
            float: left;
            padding: 20px;
            width: 70%;
            background-color: #f1f1f1;
            height: 300px;
            /* only for demonstration, should be removed */
        }

        /* Clear floats after the columns */
        section::after {
            content: "";
            display: table;
            clear: both;
        }

        /* Style the footer */
        footer {
            background-color: #777;
            padding: 10px;
            text-align: center;
            color: white;
        }

        /* Sorting in Monthly Income Salary*/
        table {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th,
        td {
            padding: 5px 10px;
            border: 1px solid black;
        }

        thead {
            background: #ddd;
        }

        table#demo2.js-sort-0 tbody tr td:nth-child(1),
        table#demo2.js-sort-1 tbody tr td:nth-child(2),
        table#demo2.js-sort-2 tbody tr td:nth-child(3),
        table#demo2.js-sort-3 tbody tr td:nth-child(4),
        table#demo2.js-sort-4 tbody tr td:nth-child(5),
        table#demo2.js-sort-5 tbody tr td:nth-child(6),
        table#demo2.js-sort-6 tbody tr td:nth-child(7),
        table#demo2.js-sort-7 tbody tr td:nth-child(8),
        table#demo2.js-sort-8 tbody tr td:nth-child(9),
        table#demo2.js-sort-9 tbody tr td:nth-child(10) {
            background: #dee;
        }

        /* End */

        /* Responsive layout - makes the two columns/boxes stack on top of each other instead of next to each other, on small screens */
        @media (max-width: 600px) {

            nav,
            article {
                width: 100%;
                height: auto;
            }
        }
    </style>

    <style>
        .chartMenu {
            width: 100vw;
            height: 40px;
            background: #1A1A1A;
            color: rgba(255, 26, 104, 1);
        }

        .chartMenu p {
            padding: 10px;
            font-size: 20px;
        }

        .chartBox {
            width: 1200px;
            padding: 20px;
            border-radius: 20px;
            border: solid 3px rgba(255, 26, 104, 1);
            background: white;
            align-items: center;
        }
    </style>

    <style>
        .chartBoxP1 {
            width: 1325px;
            height: 700px;
            padding: 20px;
            border-radius: 20px;
            border: solid 3px rgba(255, 26, 104, 1);
        }

        .chartBoxP2 {
            width: 850px;
            height: 600px;
            padding: 20px;
            border-radius: 20px;
            border: solid 3px rgba(255, 26, 104, 1);
        }

        .chartBoxP3 {
            width: 1200px;
            padding: 20px;
            border-radius: 20px;
            border: solid 3px rgba(255, 26, 104, 1);
        }
    </style>
</head>

<body>
    <h2>Reports Tab</h2>
    <!-- Top Procedures -->
    <header>
        <h2>Top Procedures</h2>
    </header>
    <section class="d-flex justify-content-center">
        <div class="chartBoxP1">
            <div class="filter d-flex justify-content-end">
                <input onchange="filterMonth()" type="text" class="form-control" name="datepicker" id="datepicker" style="width: 200px;" value="pickdate" />
            </div>
            
            <canvas id="procedureChart"></canvas>

            <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
            <script>
                var ctx = document.getElementById('procedureChart').getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['01/01/2021', '01/02/2021', '01/03/2021', '01/04/2021', '01/05/2021', '01/06/2021', '01/07/2021', '01/08/2021', '01/09/2021', '01/10/2021'],
                        datasets: [{
                            label: 'Cleaning',
                            data: [4, 8, 6, 5, 2, 3, 4, 7, 3, 9, 5],
                            backgroundColor: ['rgba(255, 99, 132)'],
                            borderColor: ['rgba(255, 99, 132, 1)'],
                            borderWidth: 1
                        }, {
                            label: 'Extraction',
                            data: [6, 5, 2, 5, 8, 7, 5, 4, 7, 8, 2],
                            backgroundColor: ['rgba(54, 162, 235)'],
                            borderColor: ['rgba(54, 162, 235, 1)'],
                            borderWidth: 1
                        }, {
                            label: 'Braces',
                            data: [1, 3, 3, 1, 1, 1, 2, 4, 5, 5, 4],
                            backgroundColor: ['rgba(255, 206, 86)'],
                            borderColor: ['rgba(255, 206, 86, 1)'],
                            borderWidth: 1
                        }, {
                            label: 'Feelings',
                            data: [3, 3, 2, 4, 5, 4, 3, 1, 5, 6, 3],
                            backgroundColor: ['rgba(75, 192, 192)'],
                            borderColor: ['rgba(75, 192, 192, 1)'],
                            borderWidth: 1
                        }, {
                            label: 'Dentures',
                            data: [6, 4, 5, 7, 3, 5, 4, 8, 9, 10, 7],
                            backgroundColor: ['rgba(153, 102, 255)'],
                            borderColor: ['rgba(153, 102, 255, 1)'],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
            </script>


            <script type="text/javascript">
                $("#datepicker").datepicker({
                    format: "mm-yyyy",
                    startView: "months",
                    minViewMode: "months"
                });

                function filterMonth() {
                    const month = [...topData];
                    console.log(month);
                    const choosemonth = document.getElementById('datepicker');

                    const indexmonth = month.indexOf(choosemonth.value);
                }
            </script>
        </div>
    </section>

    <!-- Patient Status -->
    <header>
        <h2>Active and Inactive Patients</h2>
    </header>
    <section class="d-flex justify-content-center">
        <div class="chartBoxP2">
            <style>
                #doughnutChart {
                    margin: 0 auto;
                    height: 40vh;
                    width: 74vh;
                }
            </style>

            <div class="chartLegend">
                <p><span class="circle"></span></p>
            </div>
            <div class="chart-container">
                <canvas id="doughnutChart"></canvas>
            </div>

            <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
            <script>
                var ctx = document.getElementById('doughnutChart').getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ['Inactive', 'Active'],
                        datasets: [{
                            label: '# of Votes',
                            data: [27, 73],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(0, 128, 0, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(0, 128, 0, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
            </script>
        </div>
    </section>

    <!-- Patient Volume -->
    <header>
        <h2>Patient Volume</h2>
    </header>
    <section class="d-flex justify-content-center">
        <div class="chartBox">

            <section class="d-flex justify-content-end">
                <select id="filter" onchange="update()">
                    <option>Filter</option>
                    <option value="1">Day</option>
                    <option value="2">Month</option>
                    <option value="3">Year</option>
                </select>
            </section>
            <br>
            <section class="d-flex justify-content-end">
                <div class="selectBox">
                    <input onchange="filterData()" type="hidden" class="form-control" id="startdate" value="2021-07-10" style="width: 250px;">
                    <br>
                    <input onchange="filterData()" type="hidden" class="form-control" id="enddate" value="2021-07-15" style="width: 250px;">
                    <input onchange="filterMonth2()" type="hidden" class="form-control" id="monthcal" style="width: 200px;" value="pickdate" />
                    <input onchange="yearfilter()" type="hidden" class="form-control" id="yearcal" style="width: 200px;" value="pickdate" />
                </div>
            </section>

            <canvas id="myChart">

            </canvas>
    </section>




    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        // displays the data of a whole month below
        //replace data from database
        //populate using queries
        var year = ['2020', '2021', '2022'];
        const months = ['01-2021', '02-2021', '03-2021', '04-2021', '05-021', '06-2021', '07-2021', '08-2021',
            '09-021', '10-2021', '11-2021', '12-2021',
        ];
        //populate using queries
        const appointments = [
            [16, 13, 11, 15, 12, 10, 11, 17, 18, 10, 17, 21, 11, 20, 11, 10, 21, 10, 12, 18, 24, 17, 21, 11, 12,
                23, 25, 25, 16, 16,
            ],
            [12, 11, 12, 15, 24, 17, 13, 23, 19, 15, 23, 10, 12, 14, 15, 23, 10, 25, 16, 11, 17, 20, 18, 16, 16,
                14, 25, 12, 23, 24,
            ],
            [18, 17, 16, 19, 13, 25, 16, 12, 20, 10, 17, 15, 25, 25, 19, 13, 16, 25, 12, 22, 23, 23, 15, 19, 18,
                17, 10, 10, 17, 10,
            ],
            [24, 21, 10, 24, 18, 21, 12, 15, 21, 11, 21, 12, 14, 16, 13, 21, 11, 13, 15, 23, 25, 11, 19, 23, 15,
                19, 21, 25, 16, 10,
            ],
            [22, 10, 15, 22, 19, 14, 23, 10, 21, 11, 13, 22, 23, 13, 25, 23, 10, 10, 18, 14, 13, 14, 17, 21, 22,
                25, 23, 12, 14, 22,
            ],
            [12, 10, 14, 16, 18, 20, 15, 24, 16, 10, 13, 21, 24, 25, 11, 21, 17, 21, 15, 11, 10, 17, 12, 11, 22,
                24, 11, 11, 17, 23,
            ],
            [20, 18, 23, 19, 24, 18, 20, 13, 17, 19, 25, 11, 18, 21, 14, 15, 20, 22, 14, 18, 23, 12, 24, 23, 19,
                10, 13, 11, 10, 18,
            ],
            [15, 17, 24, 23, 25, 21, 19, 19, 25, 16, 10, 18, 19, 19, 22, 21, 13, 13, 20, 10, 19, 11, 21, 24, 18,
                23, 20, 15, 21, 17,
            ],
            [16, 25, 25, 13, 16, 23, 10, 22, 22, 17, 10, 21, 15, 24, 17, 24, 25, 18, 21, 16, 24, 11, 12, 24, 21,
                15, 20, 15, 19, 13,
            ],
            [14, 16, 18, 24, 21, 18, 20, 17, 12, 19, 11, 14, 21, 23, 19, 11, 25, 19, 12, 11, 12, 13, 18, 18, 19,
                20, 19, 20, 16, 21,
            ],
            [11, 25, 13, 23, 12, 24, 14, 18, 13, 18, 22, 17, 16, 18, 18, 23, 22, 11, 23, 16, 11, 18, 10, 15, 17,
                17, 20, 14, 25, 22,
            ],
            [23, 16, 17, 10, 10, 23, 10, 19, 22, 14, 13, 14, 20, 14, 24, 24, 22, 10, 10, 19, 16, 13, 11, 16, 12,
                19, 17, 16, 10, 25,
            ],
        ];

        $("#monthcal").datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months"
        });

        $("#yearcal").datepicker({
            format: "yyyy",
            startView: "years",
            minViewMode: "years"
        });

        function filterMonth2() {
            const month = [...months];

            const choosemonth = document.getElementById('monthcal');
            console.log('Month');
            console.log(month);
            console.log(String(choosemonth.value));

            const indexmonth = month.indexOf(String(choosemonth.value));


            //////////////////////
            const dates2 = [...months];
            console.log(dates2);
            var dat = months.indexOf(choosemonth.value);
            console.log(dat);
            //  const startdate = 1;
            //  const enddate =1;


            // const indexstartdate = dates2.indexOf(startdate.value);
            // const indexendddate = dates2.indexOf(enddate.value);

            // const filterDate = dates2.slice(indexstartdate, indexendddate + 1);

            // //datapoints
            const datapoints22 = [...appointments];
            const filterDatapoints = datapoints22[dat]
            console.log(datapoints22[dat]);
            myChart.config.data.datasets[0].data = filterDatapoints;

            myChart.config.data.labels = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12',
                '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28',
                '29', '30'
            ];
            myChart.update();

        }
        const monthsu = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
            'October', 'November', 'December',
        ];

        function yearfilter() {
            var totalappointments = sum();
            const datapointsSum = [...totalappointments];
            const filterDatapoints = datapointsSum
            console.log(filterDatapoints);
            myChart.config.data.datasets[0].data = filterDatapoints;

            myChart.config.data.labels = monthsu;
            myChart.update();
        }

        //add all the appointments per month
        function sum() {
            var all = [];
            for (var i = 0; i < appointments.length; i++) {
                var total = 0;
                for (var j = 0; j < appointments[i].length; j++) {
                    console.log(appointments[i][j]);
                    total += appointments[i][j];
                    console.log(total);
                }
                all.push(total);
            }
            return all;
        }

        // displays the data of a whole month above
        function update() {
            clear();
            var select = document.getElementById('filter');
            var value = select.options[select.selectedIndex].value;
            console.log(select);
            if (select.value == "1") {
                console.log(select);
                var a = document.getElementById('startdate');
                var b = document.getElementById('enddate');
                a.type = "date";
                b.type = "date";
            } else if (select.value == "2") {
                console.log('test');
                var a = document.getElementById('monthcal');
                a.type = "text";
            } else if (select.value == "3") {
                var a = document.getElementById('yearcal');
                a.type = "text";

            } else if (select.value == "4") {

            }
        }

        function clear() {
            var a = document.getElementById('startdate');
            var b = document.getElementById('enddate');
            var c = document.getElementById('monthcal');
            var d = document.getElementById('yearcal');


            a.type = "hidden";
            b.type = "hidden";
            c.type = "hidden";
            d.type = "hidden";
        }

        const dates = ['2021-07-10', '2021-07-11', '2021-07-12', '2021-07-13', '2021-07-14', '2021-07-15'];
        const datapoints = [7, 8, 9, 11, 12, 13];
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: dates,
                datasets: [{
                    label: '# of Votes',
                    data: datapoints,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        function filterData() {
            const dates2 = [...dates];
            console.log(dates2);
            const startdate = document.getElementById('startdate');
            const enddate = document.getElementById('enddate');


            const indexstartdate = dates2.indexOf(startdate.value);
            const indexendddate = dates2.indexOf(enddate.value);

            const filterDate = dates2.slice(indexstartdate, indexendddate + 1);

            //datapoints
            const datapoints2 = [...datapoints];
            const filterDatapoints = datapoints2.slice(indexstartdate, indexendddate + 1);
            myChart.config.data.datasets[0].data = filterDatapoints;

            myChart.config.data.labels = filterDate;
            myChart.update();
        }
    </script>
    </div>

    <!-- Monthly Income Summary -->
    <header>
        <h2>Monthly Income Summary</h2>
    </header>

    <!-- CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.css">

    <!-- Javascript -->
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script>

    </script>
    <section class="d-flex justify-content-center">
        <div class="container text-center">

            <table data-toggle="table" data-classes="table table-bordered table-hover table-condensed" data-striped="false" data-sort-name="appointment" data-sort-order="desc">
                <thead>
                    <tr>
                        <th class="col-sm-2" data-field="Product_Name" data-sortable="true">Date</th>
                        <th class="col-xs-1" data-field="appointment" data-sortable="true">Appointments</th>
                        <th class="col-xs-1" data-field="uincome" data-sortable="true">Unearned Income</th>
                        <th class="col-xs-1" data-field="eincome" data-sortable="true">Earned Income</th>
                        <th class="col-sm-2" data-field="tincome" data-sortable="true">Total Income</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2021</td>
                        <td>5</td>
                        <td>25</td>
                        <td>730.1381054</td>
                        <td>755.1381054</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2022</td>
                        <td>4</td>
                        <td>42</td>
                        <td>707.1099259</td>
                        <td>732.1099259</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2023</td>
                        <td>5</td>
                        <td>14</td>
                        <td>1872.821815</td>
                        <td>1897.821815</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2027</td>
                        <td>6</td>
                        <td>21</td>
                        <td>398.8481313</td>
                        <td>423.8481313</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2032</td>
                        <td>3</td>
                        <td>15</td>
                        <td>664.5478301</td>
                        <td>689.5478301</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2030</td>
                        <td>5</td>
                        <td>22</td>
                        <td>1553.734981</td>
                        <td>1578.734981</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2025</td>
                        <td>6</td>
                        <td>20</td>
                        <td>981.6239602</td>
                        <td>1006.62396</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2038</td>
                        <td>3</td>
                        <td>21</td>
                        <td>1247.35273</td>
                        <td>1272.35273</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2040</td>
                        <td>8</td>
                        <td>11</td>
                        <td>149.9553074</td>
                        <td>174.9553074</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2027</td>
                        <td>9</td>
                        <td>14</td>
                        <td>623.573498</td>
                        <td>648.573498</td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>01/21/2029</td>
                        <td>7</td>
                        <td>16</td>
                        <td>781.5045838</td>
                        <td>806.5045838</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>GRAND TOTAL
                        </th>
                        <th>
                        </th>
                        <th>
                        </th>
                        <th>1111
                        </th>
                        <th>1111
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>

        <script>
            function runningFormatter(value, row, index) {
                return index;
            }
        </script>
    </section>
</body>

</html>