@extends('master')
@section('title', 'Gabrillo Dental Clinic')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ url('/css/reports.css') }}" />
<!-- CSS only -->

<!-- JavaScript only -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

<!-- nav -->
<!-- Wrapper -->

<div id="appointmentSecondaryNav" class="secondary-nav">
    <div class="mr-auto p-2 d-flex justify-content-center">
        <a class="active mx-5" href="#" id="button1" name="button1">Top Procedures</a>
        <a class="mx-1" href="#" id="button2" name="button2">Patient Status</a>
        <a class="mx-1" href="#" id="button3" name="button3">Patient Volume</a>
        <a class="mx-1" href="#" id="button4" name="button4">Monthly Income</a>
    </div>
</div>


<!-- content 1 -->
<!-- Top Procedures -->
<div id="top_procedures" style="display:none; text-align: center;">
    <header>
        <h2>Top Procedures</h2>
    </header>

    <section class="d-flex justify-content-center">
        <div class="chartBoxP1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-9">

                    </div>

                    <div class="col-md-3">
                        <select name="year" id="year" class="form-control">
                            <option value="">Select Year</option>
                            @foreach ($year_list as $row)
                            <option value="{{ $row->year }}">{{ $row->year }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="panel-body">
                    <canvas id="procedureChart"></canvas>
                </div>
            </div>
        </div>
    </section>
    <script>
        $('#year').change(function() {
            const years = $(this).val();
            console.log("ready");
            if (years != '') {
                console.log("ready");
                getTopProcedure(years, 'Top Procedures:');
            }
        });

        function getTopProcedure(years, top) {
            $.ajax({
                url: '/getTopProceduresname/' + years,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    console.log("success get Procedures name");
                    console.log("The top procedures are");
                    console.log(data);
                    updateChart(data);
                }
            });
        }
    </script>
    <script>
        var names = [];
        var monthchecker = [];
        var monthcounter = [];
        var datapoints3 = [
            [],
            [],
            [],
            [],
            []
        ];
        $.ajax({
            url: '/getTopProceduresname/2021',
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log("success get Procedures name");
                console.log("The top procedures are");
                console.log(data);
                for (var i = 0; i < data[1].length; i++) {
                    names.push(data[1][i][0].name);
                }
                updateChart(data);
            }

        });

        const yearLabel = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        const procedureDataPoints = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        var ctx = document.getElementById('procedureChart').getContext('2d');
        var procedureChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: yearLabel,
                datasets: [{
                    label: names[0],
                    data: datapoints3[0],
                    backgroundColor: ['rgba(255, 99, 132)'],
                    borderColor: ['rgba(255, 99, 132, 1)'],
                    borderWidth: 1
                }, {
                    label: names[1],
                    data: datapoints3[1],
                    backgroundColor: ['rgba(54, 162, 235)'],
                    borderColor: ['rgba(54, 162, 235, 1)'],
                    borderWidth: 1
                }, {
                    label: names[2],
                    data: datapoints3[2],
                    backgroundColor: ['rgba(255, 206, 86)'],
                    borderColor: ['rgba(255, 206, 86, 1)'],
                    borderWidth: 1
                }, {
                    label: names[3],
                    data: datapoints3[3],
                    backgroundColor: ['rgba(75, 192, 192)'],
                    borderColor: ['rgba(75, 192, 192, 1)'],
                    borderWidth: 1
                }, {
                    label: names[4],
                    data: datapoints3[4],
                    backgroundColor: ['rgba(153, 102, 255)'],
                    borderColor: ['rgba(153, 102, 255, 1)'],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        function updateChart(data) {
            console.log("Update chart");
            names = [];
            for (var i = 0; i < data[1].length; i++) {
                names.push(data[1][i][0].name);
            }
            var a = [];
            var b = [];
            var c = [];
            var d = [];
            var e = [];
            var ii = [];
            for (var index = 0; index < 5; index++) {
                ii.push(data[2][i]);
            }
            for (var month = 0; month < 12; month++) {
                for (var i = 0; i < data[0].length; i++) {
                    var counter = 0;
                    if (monthchecker.includes(data[0][i].Month)) {} else {
                        counter++;
                        switch (data[0][i].procedure_id) {
                            case data[2][0]:
                                a.push(data[0][i].number_of_treatments);
                                break;
                            case data[2][1]:
                                b.push(data[0][i].number_of_treatments);
                                break;
                            case data[2][2]:
                                c.push(data[0][i].number_of_treatments);
                                break;
                            case data[2][3]:
                                d.push(data[0][i].number_of_treatments);
                                break;
                            case data[2][4]:
                                e.push(data[0][i].number_of_treatments);
                                break;
                        }
                    }
                    if (counter == 5) {
                        monthchecker.push(data[0][i].Month);
                        break
                    }
                }
            }

            console.log(names);
            procedureChart.data.datasets[0].label = names[0];
            procedureChart.data.datasets[1].label = names[1];
            procedureChart.data.datasets[2].label = names[2];
            procedureChart.data.datasets[3].label = names[3];
            procedureChart.data.datasets[4].label = names[4];

            console.log(a);
            procedureChart.data.datasets[0].data = a;
            console.log(b);
            procedureChart.data.datasets[1].data = b;
            console.log(c);
            procedureChart.data.datasets[2].data = c;
            console.log(d);
            procedureChart.data.datasets[3].data = d;
            console.log(e);
            procedureChart.data.datasets[4].data = e;
            procedureChart.update();
        }
    </script>
</div>


<!-- content 2 -->
<!--  Patient Status -->
<div id="patient_status" style="display:none; text-align: center;">
    <header>
        <h2>Active and Inactive Patients</h2>
    </header>
    <section class="d-flex justify-content-center">
        <div class="chartBoxP2">
            <div id="piechart"></div>
        </div>
    </section>
    <script>
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var active, inactive = 0;
            $.ajax({
                dataType: 'json',
                url: '/reports2',
                success: function(data) {
                    console.log('Active/Inactive Data');

                    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                    var data = google.visualization.arrayToDataTable([
                        ['Patient Status', 'Values'],
                        [data[0].account_status, data[0].total_account_status],
                        [data[1].account_status, data[1].total_account_status],
                    ]);

                    var options = {
                        'width': 650,
                        'height': 500
                    };
                    chart.draw(data, options);
                }
            });
        }
    </script>
</div>

<!-- content 3 -->
<!-- Patient Volume -->
<div id="patient_volume" style="display:none; text-align: center;">
    <header>
        <h2>Patient Volume</h2>
    </header>
    <section class="d-flex justify-content-center">
        <div class="chartBox">

            <section class="d-flex justify-content-end">
                <select id="filter" onchange="update()">
                    <option>Filter</option>
                    <option value="1">Day</option>
                    <option value="2">Month</option>
                    <option value="3">Year</option>
                </select>
            </section>

            <br>

            <section class="d-flex justify-content-end">
                <div class="selectBox">
                    <form id="changeday" action="getDayRange()" method="get">
                        @csrf
                        <input type="hidden" class="form-control" id="startdate" name="start" style="width: 250px;">
                        <br>
                        <input type="hidden" class="form-control" name="end" id="enddate" style="width: 250px;">
                        {{-- <button type="button" id='datesubmit'>Button</button> --}}
                    </form>
                    {{-- action="/reports/getAppointments" --}}
                    <form id="changemonth" method="get">
                        @csrf
                        <input onchange="filterMonth2()" name="monthcal" type="hidden" class="form-control" id="monthcal" style="width: 200px;" value="pickdate" />

                    </form>
                    {{-- action="/reports/getAppointments" --}}
                    <form id="changeyear" method="get">
                        @csrf
                        <input onchange="yearfilter()" type="hidden" name="yearcal" class="form-control" id="yearcal" style="width: 200px;" value="pickdate" />
                    </form>
                </div>
            </section>


            <canvas id="myChart">
            </canvas>
    </section>

    <script>
        var group = [];
        var count = [];
        $("#enddate").change(function() {
            datechange();
        });

        $("#startdate").change(function() {
            datechange();
        });

        //update graphs 
        function datechange() {
            group = [0, ];
            count = [0, ];
            var end = document.getElementById('enddate').value;
            var start = document.getElementById('startdate').value;
            console.log(start);
            console.log(end);
            $.ajax({
                dataType: 'json',
                url: '/reportsday/' + start + '/' + end + '/appointments',
                success: function(data) {
                    console.log('appointments');
                    console.log(data)
                    appointmentsperday(data);

                    filterData();
                }
            });
        }

        var cat1 = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11',
            '12',
            '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27',
            '28',
            '29', '30', '31',
        ];

        var cat2 = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11',
            '12',
            '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27',
            '28',
            '29', '30',
        ];

        var feb = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11',
            '12',
            '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27',
            '28',
        ];


        var month;
        var cat;
        //appointment counting
        function appointmentsperday(data) {
            for (var i = 0; i < data.length; i++) {
                console.log(data[i].date);
                appointmentscounter(data[i].date)
                //getCat(data[i].date)
            }
        }

        //appointments counter
        function appointmentsperyear(data) {
            for (var i = 0; i < data.length; i++) {
                appointmentscounter2(data[i].date)
            }
        }

        function appointmentscounter2(data) {
            var split = data.split("-");
            console.log('split middle');
            console.log(split[1]);
            var builder = split[0] + '-' + split[1]
            if (group.indexOf(builder) !== -1) {
                count[group.indexOf(builder)] += 1;
            } else {
                group.push(builder);
                count.push(1);
            }
        }

        //count the appointments of each day
        function appointmentscounter(data) {
            if (group.indexOf(data) !== -1) {
                count[group.indexOf(data)] += 1;
            } else {
                group.push(data);
                count.push(1);
            }
        }

        //get the category, might be unneeded
        function getCat(data) {
            switch (data) {
                case '01':
                case '03':
                case '05':
                case '07':
                case '08':
                case '10':
                case '12':
                    return cat1;
                    // code block
                case '04':
                case '06':
                case '09':
                case '11':
                    return cat2;


                default:
                    return feb;
            }
            return feb;
        }
        var year = ['2020', '2021', '2022'];

        const monthsu = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
            'October', 'November', 'December',
        ];

        const months = ['01-2021', '02-2021', '03-2021', '04-2021', '05-021', '06-2021', '07-2021', '08-2021',
            '09-021', '10-2021', '11-2021', '12-2021',
        ];

        //populate using queries
        var appointments = [
            [1, 1, 1, 1, 1, 1, ],
            [2, 2, 2, 2, 2, 2, ],
        ];

        $("#monthcal").datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months"
        });

        $("#yearcal").datepicker({
            format: "yyyy",
            startView: "years",
            minViewMode: "years"
        });

        $("#monthcal").change(function() {
            group = [];
            count = [];
            var monthcal = document.getElementById('monthcal').value;
            console.log('month calendar change');

            $.ajax({
                dataType: 'json',
                url: '/reportsday/' + monthcal + '/appointments',
                success: function(data) {
                    console.log('success');
                    console.log('appointments');
                    console.log(data);
                    appointmentsperday(data);
                    filterMonth2();
                }
            });
        });

        function filterMonth2() {
            var choosemonth = document.getElementById('monthcal').value;
            var split = choosemonth.split("-");
            var datapoints22 = count;
            myChart.config.data.datasets[0].data = datapoints22;
            myChart.config.data.labels = group;
            myChart.update();
        }

        $("#yearcal").change(function() {
            group = [];
            count = [];
            var yearcal = document.getElementById('yearcal').value;
            console.log('month calendar change');

            $.ajax({
                dataType: 'json',
                url: '/reportsyear/' + yearcal + '/appointments',
                success: function(data) {
                    console.log('success');
                    console.log('appointments');
                    console.log(data);
                    appointmentsperyear(data);
                    yearfilter();
                    console.log('Group');
                    console.log(group);
                    console.log('Count');
                    console.log(count);

                }
            });
        });

        function yearfilter() {
            myChart.config.data.datasets[0].data = count;
            myChart.config.data.labels = group;
            myChart.update();
        }

        //add all the appointments per month
        function sum() {
            var all = [];
            for (var i = 0; i < appointments.length; i++) {
                var total = 0;
                for (var j = 0; j < appointments[i].length; j++) {
                    console.log(appointments[i][j]);
                    total += appointments[i][j];
                    console.log(total);
                }
                all.push(total);
            }
            return all;
        }

        // displays the data of a whole month above
        function update() {
            clear();
            var select = document.getElementById('filter');
            var value = select.options[select.selectedIndex].value;
            console.log(select);
            if (select.value == "1") {
                console.log(select);
                var a = document.getElementById('startdate');
                var b = document.getElementById('enddate');
                a.type = "date";
                b.type = "date";
            } else if (select.value == "2") {
                console.log('test');
                var a = document.getElementById('monthcal');
                a.type = "text";
            } else if (select.value == "3") {
                var a = document.getElementById('yearcal');
                a.type = "text";
            } else if (select.value == "4") {}
        }

        function clear() {
            var a = document.getElementById('startdate');
            var b = document.getElementById('enddate');
            var c = document.getElementById('monthcal');
            var d = document.getElementById('yearcal');

            a.type = "hidden";
            b.type = "hidden";
            c.type = "hidden";
            d.type = "hidden";
        }

        const dates = ['2021-07-10', '2021-07-11', '2021-07-12', '2021-07-13', '2021-07-14', '2021-07-15'];
        const datapoints = [7, 8, 9, 11, 12, 13];
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: dates,
                datasets: [{
                    label: 'Data',
                    data: datapoints,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        function filterData() {


            const dates2 = [...dates];
            console.log(dates2);
            const startdate = document.getElementById('startdate');
            const enddate = document.getElementById('enddate');

            const indexstartdate = dates2.indexOf(startdate.value);
            const indexendddate = dates2.indexOf(enddate.value);
            const filterDate = dates2.slice(indexstartdate, indexendddate + 1);

            //datapoints
            var datapoints2 = group;
            var filterDatapoints = count;
            myChart.config.data.datasets[0].data = filterDatapoints;

            myChart.config.data.labels = group;
            myChart.update();
        }
    </script>
</div>

<!-- content 4 -->
<!-- Monthly Income Summary -->
<div id="monthly_income" style="display:none; text-align: center;">
    <header>
        <h2>Monthly Income Summary</h2>
    </header>
    <style>
        #table_div {
            width: 1350px;
        }
    </style>

    <section class="d-flex justify-content-center">

        <div class="chartBoxP4">
            <section class="d-flex justify-content-end">
                <div class="selectBox">

                    <form id="incomeyearform" method="get" class="income">
                        @csrf
                        <input type="text" name="incomeyear" id="incomeyear" style="width: 200px;" value="pickdate" />

                    </form>
                </div>
            </section>
            <div id="table_div"></div>
        </div>
    </section>

    <script>
        $("#incomeyear").datepicker({
            format: "yyyy",
            startView: "years",
            minViewMode: "years"
        });

        $("#incomeyear").change(function() {
            group = [];
            count = [];
            var incomeyear = document.getElementById('incomeyear').value;

            $.ajax({
                dataType: 'json',
                url: '/incomeyear/' + incomeyear + '/',
                success: function(data) {
                    console.log('success');
                    console.log(data);
                    setLabels(data);
                    updateTable();
                }
            });
        });


        var labels = [];
        var datapts = [];

        function setLabels(income) {
            console.log('Set Labels function');
            console.log(income);
            labels = [];
            datapts = [];
            for (var i = 0; i < income.length; i++) {
                if (income[i].Month == 1) {
                    labels.push("January");
                } else if (income[i].Month == 2) {
                    labels.push("February");
                } else if (income[i].Month == 3) {
                    labels.push("March");
                } else if (income[i].Month == 4) {
                    labels.push("April");
                } else if (income[i].Month == 5) {
                    labels.push("May");
                } else if (income[i].Month == 6) {
                    labels.push("June");
                } else if (income[i].Month == 7) {
                    labels.push("July");
                } else if (income[i].Month == 8) {
                    labels.push("August");
                } else if (income[i].Month == 9) {
                    labels.push("September");
                } else if (income[i].Month == 10) {
                    labels.push("October");
                } else if (income[i].Month == 11) {
                    labels.push("November");
                } else if (income[i].Month == 12) {
                    labels.push("December");
                }
                datapts.push(income[i].Income);
            }
            console.log(labels);
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['table']
        });
        google.charts.setOnLoadCallback(drawTable);

        function updateTable() {
            datatable = new google.visualization.DataTable();
            tableincome = new google.visualization.Table(document.getElementById('table_div'));
            datatable.addColumn('string', 'Date');
            datatable.addColumn('number', 'Total Income');
            datatable.addRows(labels.length);

            for (var i = 0; i < labels.length; i++) {
                datatable.setCell(i, 0, labels[i]);
                datatable.setCell(i, 1, datapts[i]);
            }

            tableincome.draw(datatable, {
                showRowNumber: true,
                width: '100%',
                height: '100%'
            });
        }
        var datatable;
        var tableincome;

        function drawTable() {
            datatable = new google.visualization.DataTable();
            tableincome = new google.visualization.Table(document.getElementById('table_div'));
            datatable.addColumn('string', 'Date');
            //data.addColumn('number', 'Appointment');
            datatable.addColumn('number', 'Total Income');
            datatable.addRows([
                ['01/21/2021', {
                    v: 730.1381054,
                    f: '₱730.1381054'
                }, ],
                ['01/21/2022', {
                    v: 707.1099259,
                    f: '₱707.1099259'
                }, ],
                ['01/21/2023', {
                    v: 1897.821815,
                    f: '₱1897.821815'
                }],
                ['01/21/2027', {
                    v: 423.8481313,
                    f: '₱423.8481313'
                }],
                ['01/21/2027', {
                    v: 689.5478301,
                    f: '₱689.5478301'
                }],
                ['01/21/2027', {
                    v: 1578.734981,
                    f: '₱1578.734981'
                }],
                ['01/21/2027', {
                    v: 1006.62396,
                    f: '₱1006.62396'
                }],
                ['01/21/2027', {
                    v: 1272.35273,
                    f: '₱1272.35273'
                }],
            ]);
            tableincome.draw(datatable, {
                showRowNumber: true,
                width: '100%',
                height: '100%'
            });
        }
    </script>
</div>

<script>
    document.getElementById("button1").addEventListener("click", function() {
        var displayDiv = document.getElementById('top_procedures');
        document.getElementById("patient_status").style.display = "none";
        document.getElementById("patient_volume").style.display = "none";
        document.getElementById("monthly_income").style.display = "none";
        var displayValue = (displayDiv.style.display === "block") ? "none" : "block";
        this.innerHTML = (displayValue === "block") ? "Top Procedures" : "Top Procedures";
        displayDiv.style.display = displayValue;
    });

    document.getElementById("button2").addEventListener("click", function() {
        var displayDiv = document.getElementById('patient_status');
        document.getElementById("top_procedures").style.display = "none";
        document.getElementById("patient_volume").style.display = "none";
        document.getElementById("monthly_income").style.display = "none";
        var displayValue = (displayDiv.style.display === "block") ? "none" : "block";
        this.innerHTML = (displayValue === "block") ? "Patient Status" : "Patient Status";
        displayDiv.style.display = displayValue;
    });

    document.getElementById("button3").addEventListener("click", function() {
        var displayDiv = document.getElementById('patient_volume');
        document.getElementById("top_procedures").style.display = "none";
        document.getElementById("patient_status").style.display = "none";
        document.getElementById("monthly_income").style.display = "none";
        var displayValue = (displayDiv.style.display === "block") ? "none" : "block";
        this.innerHTML = (displayValue === "block") ? "Patient Volume" : "Patient Volume";
        displayDiv.style.display = displayValue;
    });

    document.getElementById("button4").addEventListener("click", function() {
        var displayDiv = document.getElementById('monthly_income');
        document.getElementById("top_procedures").style.display = "none";
        document.getElementById("patient_status").style.display = "none";
        document.getElementById("patient_volume").style.display = "none";
        var displayValue = (displayDiv.style.display === "block") ? "none" : "block";
        this.innerHTML = (displayValue === "block") ? "Monthly Income" : "Monthly Income";
        displayDiv.style.display = displayValue;
    });
</script>

<!-- <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script> -->
<script src="{{ asset('js/patient.js') }}"></script>
<script src="{{ asset('js/reports.js') }}"></script>
{{-- <script src="{{ asset('css/reports.css') }}"></script> --}}

@endsection