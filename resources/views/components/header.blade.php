<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css" integrity="sha384-wESLQ85D6gbsF459vf1CiZ2+rr+CsxRY0RpiF1tLlQpDnAgg6rwdsUF1+Ics2bni" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous"/>
    <link rel ="stylesheet" href="{{ asset('css/treatment.css')}}"/>
</head>

<body>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
   
    <div >
        <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand text-white"><img src ='\images\logo.png' alt = "Logo"  id = "logo"/>

            <div class="container-fluid ">   
              <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                <ul class="navbar-nav ">
                  <li class="nav-item">
                    <a class="nav-link active text-white" aria-current="page" href="dashboard">Dashboard</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="patients_records_list">Patients</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="">Appointments</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="treatments">Treatments</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="">Payments</a>
                  </li>
                </ul>
              </div>
              <i class="fas fa-bell"></i>
              <div class="btn-group">
              <ul class="navbar-nav" >
                  <div class="btn-group dropstart navbar-collapse" id="navbarNavDarkDropdown" >
                      <li class="nav-item dropdown">
                      <img src ='\images\prof_pic.png' alt = "profile pic" width = "100%" role="button" id = "navbarDarkDropdownMenuLink"data-bs-toggle="dropdown" data-bs-display="static" aria-expanded="false" style =" float:right;"/>
                        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink"> 
                          <li><a class="dropdown-item" href="#">Profile</a></li>
                          <li><a class="dropdown-item" href="#">Settings</a></li>
                          <li><a class="dropdown-item" href="#">Log Out</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
              </div>
            </div>
          </nav>
       
    </div>
</body>
