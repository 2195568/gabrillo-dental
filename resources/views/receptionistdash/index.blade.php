@extends('master')
@section('title', 'Gabrillo Dental Clinic')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ url('/css/admindash.css') }}" />
<div class="content">
    <div id="secondary-nav" class="secondary-nav d-flex justify-content-between nopadding">
        <div class="mr-auto pt-2 pb-1">
            <a class="active mx-5" href="#">Dashboard Overview</a>
        </div>
    </div>

    <div id="main-content" style="background-color: #F3F6FB;">

        <div class="row">
            <div class="d-flex justify-content-start col-lg greet-con">
                <h2 id="greetings">Goodmorning  {{ Auth::user()->name }}!</h2>
                <br>
            </div>

            <div class="d-flex justify-content-end col-lg date-con">
                <h5 id="date-today"></h5>
            </div>
        </div>



        <div class="row first-row">

            <div class="col-lg calendar-con">
                <div class="list-group">
                    <div class="title-bg d-flex justify-content-center">
                        <h2 class="header2"> {{$count}} Appointment/s for today</h2>
                    </div>
                    @foreach($data as $i)
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between btn" data-toggle="modal" data-target="#viewModal" data-id="{{ $i->id }}">
                            <h5 class="mb-1 name"> {{$i->first_name }} {{$i->last_name}}</h5>
                        </div>
                        <p class="mb-1">{{$i->description}}</p>
                        <small class="text-info"> {{ date('g:i', strtotime($i->time_start)) }}</small>
                    </a>
                    @endforeach 

                </div>
            </div>

            <div class="col-lg treatments-con">
            
            <h1>Appointment Requests<h1>
            <div class="col active-">
            <div id="active-patients">

                <div class="card">
                    <div class="card-body d-flex justify-content-center align-items-center flex-column">
                        <h1 id="active-data">{{$newpatientreq}} <i class="fas fa-user-check fa-lg"></i></h1>
                        <h6 class="header-active">New Patient Requests</h6>
                    </div>
                </div>

            </div>
        </div>

        <div class="col inactive-con">
            <div id="inactive-patients">

                <div class="card">
                    <div class="card-body d-flex justify-content-center align-items-center flex-column">
                        <h1 id="inactive-data" data-toggle="modal">{{$oldpatientreq}}  <i class="fas fa-user-times fa-lg"></i></h1>
                        <h6 class="header-inactive">Old Patient Requests</h6>
                    </div>
                    <div class="card-footer text-light text-center">
                    <a href="{{ url('/appointments/request-approval') }}" style=" text-decoration: none; color: white; font-size: 24px;"> View More</a>
                    </div>
                </div>

            </div>
        </div>
    
            </div>
        </div>
    </div>

        <div class="col inactive-con">
            <div id="inactive-patients">

</div>

<!-- View Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <div class="d-inline-flex flex-row">
                    <i style="color: #7764CA" class="fa fa-user-circle fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;" id="exampleModalLabel">View patient</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="d-flex justify-content-around">
                    <div class="d-flex flex-column align-items-center justify-content-start justify-content-between">
                        <div class="d-flex flex-column align-items-center justify-content-around shadow p-3 mb-5 bg-white rounded">
                            <div class="col-sm-4 bg-c-lite-green user-profile">
                                <div>
                                    <div class="m-b-25" id="view_patient_img"></div>
                                </div>
                            </div>
                            <div>
                                <h5 id="view_patient_name"></h5>
                            </div>
                            <span id="view_patient_email" class="text-wrap"></span>
                            <button id="viewTreatmentHistoryButton" data-toggle="modal" data-target="#treatmentHistoryModal" type="button" class="btn btn-primary" data-patient-id='0'>View treatment history</button>
                        </div>
                        <div class="d-flex flex-column w-100 shadow p-3 mb-5 bg-white rounded">
                            <h5>Balance:</h5>
                            <span id="view_patient_balance"></span>
                        </div>
                    </div>
                    <div class="w-75 shadow p-3 mb-5 bg-white rounded ml-5">
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Gender</th>
                                <th scope="col">Birthday</th>
                                <th scope="col">Phone Number</th>
                                <th scope="col">Age</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td id="view_patient_gender"></td>
                                <td id="view_patient_birthday"></td>
                                <td id="view_patient_phone_number"></td>
                                <td id="view_patient_age"></td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Address</th>
                              <!--  <th scope="col">Occupation</th> -->
                               
                                </tr>   
                            </thead>
                            <tbody>
                                <tr>
                                <td  id="view_patient_address"></td>
                                <!--<td  id="view_patient_occupation"></td>-->
                                
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Patient Status</th>
                                <th scope="col">Allergies</th>
                               <!-- <th scope="col">Account Creation</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td  id="view_patient_patient_status"><span class="badge rounded-pill bg-success"></span></td>
                                <td  id="view_patient_allergies"></td>
                            <!--    <td  id="view_patient_registered_date">Baguio</td> -->
                                </tr>
                            </tbody>
                        </table>
                     <!--   <button id="addPaymentButton" type="button" data- class="btn btn-primary" data-toggle="modal" data-target="#addPaymentModal">Add payment</button>
                        <button id="addTreatmentButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTreatmentModal" data-patient-id="0" data-first-name="." data-last-name="." data-email=".">Add treatment</button> -->
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

            <!-- Treatment History Modal -->
<div class="modal fade" id="treatmentHistoryModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header border-0">
                <div class="d-flex">
                    <i style="color: #7764CA" class="fa fa-history fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;">Treatment History</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <!-- Tooth numbering system -->
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="d-flex justify-content-between">
                        <div class="col-s titleL">
                            <h6 class="txt-grey ">Upper Left</h6>
                        </div>
                        <div class="col-s titleR">
                            <h6 class="txt-grey ">Upper Right</h6>
                        </div>
                    </div>
                    
                    <div class="d-flex flew-row justify-content-center">
                        <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">18</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">48</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">17</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">47</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">16</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">46</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">15</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">45</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">14</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">44</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">13</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">43</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">12</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">42</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">11</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">41</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="vl"></div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">21</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">31</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">22</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">32</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">23</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">33</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">24</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">34</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">25</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">35</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">26</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">36</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">27</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">37</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">28</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">38</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                </div>
                
                <div class="d-flex justify-content-between">
                    <div class="col-s titleL">
                        <h6 class="txt-grey ">Lower Left</h6>
                    </div>
                    <div class="col-s titleR">
                        <h6 class="txt-grey ">Lower Right</h6>
                    </div>
                </div>
                
            </div>    
            
            <!-- Records -->
            <div class="d-flex">
                <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                <h6 class="modal-title pt-2" style="color: black;">Records</h6>
            </div>
            
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="container overflow-auto" style="height: 200px;">
                        <div id="treatmentsHistoryContainer" class="row">
                        </div>
                    </div>
                </div>
                <div id="footer" >
                            <button id="closeModal" data-dismiss="modal"class="badge rounded-pill bg-danger" style="float:right; width: 10%; font-weght:40px; font-size:18px;" >Close</button>
                        </div>
            </form>
            </div>
        </div>
    </div>
</div>
</div>


<script src="{{ asset('js/appointments/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/appointments/disable.js') }}"></script>
<script src="{{ asset('js/receptdash.js') }}"></script>
<script src="/js/appointments/request-approval.js"></script>
<script>
    var m =  ["January","February","March","April","May","June","July","August","September","October","November","December"];
    n =  new Date();
y = n.getFullYear();
month = n.getMonth();
d = n.getDate();
document.getElementById("date-today").innerHTML = m[month] +" " + d + "," + y;
</script>

@if (Auth::user()->user_role_id == 3)
<script src="{{ asset('js/dashboard-calendar.js') }}"></script>
@else
<script src="{{ asset('js/dashboard-calendar.js') }}"></script>
@endif
@endsection
