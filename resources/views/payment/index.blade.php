@extends('master')
@section('title', 'Gabrillo Dental Clinic')
@section('content')
<div id="content">
<div class="secondary-nav d-flex justify-content-between nopadding">
    <div class="mr-auto p-2">
        <a class="active mx-5" href="/payments">Payments</a>
        @if (Auth::user()->user_role_id < 3)
        <a class="mx-5" href="/payments/logs">Logs</a>
        @endif
    </div>
    <div class="p-2">
        <!-- <input id="searchInput" class="mx-5 rounded bg-light" type="text" placeholder="Search " aria-label="Search "> -->
        <input id="searchBox" type="text" class="mx-5 rounded bg-light bg-secondary" style="border-color: white; background-color: #F1F4FB;" placeholder="Search" autocomplete="off">
        <div id="searchResult" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">
                <u>Profile</u>
            </a>
        </div>
    </div>
</div>

<div id="main-content" class="container-fluid" style="background-color: #F3F6FB;">

    {{-- $('#tablee').bootstrapTable({
    url: 'http://www.mergebranch.com:8000/getPatientData',

    pagination: true,
    search: true,
    columns: [{
        sortable: true,
      field: 'last_name',
      title: 'Last Name'
    }, {
      field: 'contact_no',
      title: 'Contact Number'
    }, {
      field: 'address',
      title: 'Address'
    },{
        field: 'new_appointment',
        title: 'New Appointment'
      },{
        field: 'last_appointment',
        title: 'Last Appointment'
      },{
        sortable: true,
        field: 'created_at',
        title: 'Created At'
      },{
        events: 'operateEvents',
        formatter: 'operateFormatter',
        field: 'operate',
        title: 'Action'
      }]
  }); --}}
  

    <table id="table2" data-toggle="table" 
        data-pagination="true" 
        data-pagination-loop="false" 
        data-search="true"
        data-visible-search="true" 
        data-pagination-pre-text="Previous"
        data-pagination-next-text="Next" 
        data-search-selector="#searchBox"
        data-unique-id="id"
        data-url="/paymentData">
        <thead>
            <tr>
                {{-- <th data-sortable="true" data-field="id">id</th> --}}
                {{-- <th data-sortable="true" data-field="first_name">Treatment ID</th> --}}
                <th data-sortable="true" data-field="treatment_date">Date Time</th>
                <th data-field="treatment_reference_id">Treatment Reference ID</th>
                <th data-field="patient_id">Patient ID</th>
                <th data-field="first_name">First Name</th>
                <th data-field="last_name">Last Name</th>
                <th data-sortable="true" data-field="treatment_total_amount" data-formatter="digitFormatter">Amount Charged</th>
                <th data-sortable="true" data-formatter="digitFormatter" data-field="total_payment">Total Payment</th>
                <th data-sortable="true" data-formatter="digitFormatter" data-field="treatment_balance">Balance</th>
                <th data-sortable="true" data-field="payment_status_name">Status</th>
                <th data-field="operate" data-formatter="operateFormatter">Action</th>
            </tr>
        </thead>
    </table>
    <script>
        var $table = $('#table2')


        function digitFormatter(value, row, index) {
                    console.log("digit formatter");
                    console.log(value);
                   
                    return [
                        "<p style='text-align:right'>₱ "+value+".00</p>",
                    ].join('')
                }




        function operateFormatter(value, row, index) {
            if (row.payment_status_name == 'paid') {
                return [
              
                    '<button type="button" class="btn pay-btn btn-primary" data-reference-id="' + row.reference_id + '" data-treatment-balance="' + row.treatment_balance + '" disabled>Pay</button>',


                 ].join('')
                
             } else{
                    return [
                
                        '<button type="button" class="btn pay-btn btn-primary" data-reference-id="' + row.reference_id + '" data-treatment-balance="' + row.treatment_balance + '">Pay</button>',
                    ].join('')
                
            }
            return [



            ].join('');
        }

        // window.operateEvents = {
        //     'click .like': function(e, value, row, index) {
        //         alert('You click like action, row: ' + JSON.stringify(row))
        //     },
            
        // }

    </script>

    {{-- table for data showing --}}
</div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/payments/update" method="post">
                    @csrf
                    <!-- Modal Header -->
                    <div class="modal-header border-0">
                        <div class="d-flex">
                            <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                            <h5 class="modal-title" style="color: #7764CA;">Edit payment</h5>
                        </div>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Treatment Reference ID</label>
                        <input id="edit-treatment-reference-input" type="text" name="treatment_reference_id" class="form-control" required>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Add Modal -->
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <form action="/payments/store" method="post">
                @csrf
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header border-0">
                            <div class="d-flex">
                                <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                                <h5 class="modal-title" style="color: #7764CA;">Add new payment</h5>
                            </div>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Treatment Reference ID</label>
                                <input id="treatment-reference-input" type="text" name="treatment_reference_id" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="text" style="text-align: right;"  name="amount" class="form-control" id="amount" placeholder="Amount"
                               required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="save-changes" class="btn btn-primary btn-pay">Pay</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        

        <script src="{{ asset('js/payment.js') }}"></script>
        <script src="{{ asset('js/calendardisabler.js') }}"></script>

<script src="{{ asset('js/payment.js') }}"></script>
</div>
@endsection
