@extends('master')
@section('title', 'Gabrillo Dental Clinic')
@section('content')
    <div id="content">
        <div class="secondary-nav d-flex justify-content-between nopadding">
            <div class="mr-auto p-2">
                <a class="active mx-5" href="/treatments">Treatments</a>
            </div>
            <div class="p-2">
                <!-- <input id="searchInput" class="mx-5 rounded bg-light" type="text" placeholder="Search " aria-label="Search "> -->
                <input id="searchBox" type="text" class="mx-5 rounded bg-light bg-secondary"
                    style="border-color: white; background-color: #F1F4FB;" placeholder="Search" minlength="13">
                @if (Auth::user()->user_role_id != 3)
                <button type="mx-5 button" id="plus-button" class="btn btn-default btn-circle btn-xl border"
                    data-toggle="modal" data-target="#addModal">
                    <i class="fa fa-plus" style="color: #7764CA;"></i>
                </button>
                @endif
                <div id="searchResult" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">
                        <u>Profile</u>
                    </a>
                </div>
            </div>
        </div>
        <script src="{{ asset('css/tooth-numbering.css') }}"></script>
        <div id="main-content" class="container-fluid" style="background-color: #F3F6FB;">

            {{-- $('#tablee').bootstrapTable({
            url: 'http://www.mergebranch.com:8000/getPatientData',
        
            pagination: true,
            search: true,
            columns: [{
                sortable: true,
              field: 'last_name',
              title: 'Last Name'
            }, {
              field: 'contact_no',
              title: 'Contact Number'
            }, {
              field: 'address',
              title: 'Address'
            },{
                field: 'new_appointment',
                title: 'New Appointment'
              },{
                field: 'last_appointment',
                title: 'Last Appointment'
              },{
                sortable: true,
                field: 'created_at',
                title: 'Created At'
              },{
                events: 'operateEvents',
                formatter: 'operateFormatter',
                field: 'operate',
                title: 'Action'
              }]
          }); --}}
          
        
            <table id="table2" data-toggle="table" 
                data-pagination="true" 
                data-pagination-loop="false" 
                data-search="true"
                data-visible-search="true" 
                data-pagination-pre-text="Previous"
                data-pagination-next-text="Next" 
                data-search-selector="#searchBox"
                data-url="/treatmentData">
                <thead>
                    <tr>
                        <th data-sortable="true" data-field="0.reference_id">Treatment Reference ID</th>
                        <th data-field="0.patient.first_name">First Name</th>
                        <th data-field="0.patient.last_name">Last Name</th>
                        <th data-sortable="true" data-field="0.date">Date</th>
                        <th data-field="0.tooth_no">Tooth</th>
                        <th data-sortable="true"  data-field="0.procedures" data-formatter="proceduresFormatter">Procedure/s</th>
                        <th data-sortable="true"  data-formatter="digitFormatter" data-field="0.total_amount">Total Amount</th>
                        @if (Auth::user()->user_role_id != 3)
                        <th data-field="operate" data-formatter="operateFormatter">Action</th>
                        @endif
                    </tr>
                </thead>
            </table>
            
            <script>
                var $table = $('#table2')

                function digitFormatter(value, row, index) {
                    console.log("digit formatter");
                    console.log(value);
                   
                    return [
                        "<p style='text-align:right'>₱ "+value+".00</p>",
              
                    ].join('')
                }


                function proceduresFormatter(value, row, index) {
                    var procedures = value[0];
                //     console.log("procedures");
                //    console.log(value);

                    var strProcedrues = "";

                   for(var i = 0; i < value.length; i++){
                    //    console.log("individual procedures");
                    //    console.log(value[i].name);

                       strProcedrues += " " + value[i].name + "<br>";

                   }
                    return [
                    "<p>"+strProcedrues+"</p>",
              
                    ].join('')
                }
               


                function operateFormatter(value, row, index) {
                    console.log('towww')
                    console.log(row);
                    return [
                        '<button id="' + row[0].id + '"class="btn btn-danger btn-sm rounded-0 mr-2" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a></button>',
                        '<button id="' +row[0].id + '" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a></button></td>',
        
                        // '<button id="' + row.id + '" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a></button>',
        
        
                    ].join('')
                }
        
                // window.operateEvents = {
                //     'click .like': function(e, value, row, index) {
                //         alert('You click like action, row: ' + JSON.stringify(row))
                //     },
                    
                // }
        
            </script>
        
            {{-- table for data showing --}}
        </div>

        <!-- Add Modal -->
        <div class="modal" id="addModal">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header border-0">
                        <div class="d-flex">
                            <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                            <h5 class="modal-title" style="color: #7764CA;">Add new treatment</h5>
                        </div>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <!-- Patient -->
                        <div class="d-flex">
                            <h6 class="modal-title pt-2" style="color: black;">Patient <b style="color: red;"> *</b></h6>
                        </div>

                        <div class="shadow p-3 mb-5 bg-white rounded">
                            <div class="form-group">
                                <input type="text" class="form-control" id="patientSearchBox" placeholder="Search" autocomplete="off">
                                <div id="patientSearchResultBox" class="d-flex justify-content-center mt-3" style="overflow: auto; max-height: 300px;">
                                </div>
                            </div>
                           
                        </div>
                        
                        <!-- Tooth numbering system -->
                        <div class="d-flex">
                            <!-- <img class="img-responsive img-thumbnail img-fluid btn btn-xs" width="50px" src="{{ asset('img/tooth.svg') }}"></img> -->
                            <h6 class="modal-title pt-2" style="color: black;">Tooth number <b style="color: red;"> *</b></h6>
                        </div>

                        <div class="shadow p-3 mb-5 bg-white rounded">
                            <div class="d-flex justify-content-between">
                                <div class="col-s titleL">
                                    <h6 class="txt-grey ">Upper Left</h6>
                                </div>
                                <div class="col-s titleR">
                                    <h6 class="txt-grey ">Upper Right</h6>
                                </div>
                            </div>

                            <div id="tooth_buttons" class="d-flex flew-row justify-content-center">
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/molarDown.svg') }}" class="mb-1" class="mb-1" alt=""
                                        width="30%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">18</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">48</button>
                                    <img src="{{ asset('img/molar.svg') }}" class="mb-1" alt="" width="30%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/molarDown.svg') }}" class="mb-1" alt="" width="30%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">17</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">47</button>
                                    <img src="{{ asset('img/molar.svg') }}" class="mb-1" alt="" width="30%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/premolarDown.svg') }}" class="mb-1" alt="" width="20%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">16</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">46</button>
                                    <img src="{{ asset('img/premolar.svg') }}" class="mb-1" alt="" width="20%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">15</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">45</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">14</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">44</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/canineDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">13</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">43</button>
                                    <img src="{{ asset('img/canine.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">12</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">42</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">11</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">41</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="vl"></div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">21</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">31</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">22</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">32</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/canineDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">23</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">33</button>
                                    <img src="{{ asset('img/canine.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">24</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">34</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">25</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">35</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/premolarDown.svg') }}" class="mb-1" alt="" width="20%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">26</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">36</button>
                                    <img src="{{ asset('img/premolar.svg') }}" class="mb-1" alt="" width="20%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/molarDown.svg') }}" class="mb-1" alt="" width="30%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">27</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">37</button>
                                    <img src="{{ asset('img/molar.svg') }}" class="mb-1" alt="" width="30%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/molarDown.svg') }}" class="mb-1" alt="" width="30%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">28</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">38</button>
                                    <img src="{{ asset('img/molar.svg') }}" class="mb-1" alt="" width="30%">
                                </div>
                            </div>

                            <div class="d-flex justify-content-between">
                                <div class="col-s titleL">
                                    <h6 class="txt-grey ">Lower Left</h6>
                                </div>
                                <div class="col-s titleR">
                                    <h6 class="txt-grey ">Lower Right</h6>
                                </div>
                            </div>

                        </div>

                        <!-- Procedure -->
                        <div class="d-flex">
                            <h6 class="modal-title pt-2" style="color: black;">Procedure <b style="color: red;"> *</b></h6>
                        </div>

                        <form id="add_treatment_form" action="/treatments/store" method="post">
                            @csrf
                            <input type="hidden" name='tooth_no' id="add_tooth_no">
                            <input type="hidden" name='patient_id' id="add_patient_id" minlength="13">
                            <input type="hidden" name='date' id="add_date">
                            <input type="hidden" name='total_amount' id="add_total_amount">

                            <div class="shadow p-3 mb-5 bg-white rounded">
                                <div class="container overflow-auto" style="height: 200px;">
                                    <div class="row procedures_container">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- Date/Total amount -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="d-flex">
                                    <h6 class="modal-title pt-2" style="color: black;">Date <b style="color: red;"> *</b></h6>
                                </div>

                                <div class="shadow p-3 mb-5 bg-white rounded">
                                    <div class="form-group form-con">
                                        <input name="treatmentEditDate" type="date" class="form-control" id="date" placeholder="Date">
                                        <i class="fa fa-check-circle"></i>
                                        <i class="fa fa-exclamation-circle"></i>
                                        <small>Error message</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex">
                                    <h6 class="modal-title pt-2" style="color: black;">Total amount <b style="color: red;"> *</b></h6>
                                </div>

                                <div class="shadow p-3 mb-5 bg-white rounded">
                                    <div class="form-group form-con">

                                        <input type="text" style="text-align: right;" name="Amount" id="total_amount"
                                            class="form-control" placeholder="Total value"
                                            onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)"
                                            onchange="RoundtoDecimal(this)" maxlength="6" min="0" max="999999" />
                                            <i class="fa fa-check-circle"></i>
                                            <i class="fa fa-exclamation-circle"></i>
                                            <small>Error message</small>

                                        <script>
                                            function maxLengthCheck(object) {
                                                if (object.value.length > object.maxLength)
                                                    object.value = object.value.slice(0, object.maxLength)
                                            }

                                            function isNumeric(evt) {
                                                var theEvent = evt || window.event;
                                                var key = theEvent.keyCode || theEvent.which;
                                                key = String.fromCharCode(key);
                                                var regex = /[0-9]|\./;
                                                if (!regex.test(key)) {
                                                    theEvent.returnValue = false;
                                                    if (theEvent.preventDefault) theEvent.preventDefault();
                                                }
                                            }

                                            function RoundtoDecimal(ctrl) {
                                                if (ctrl.value.length > 0) {
                                                    var num = parseFloat(ctrl.value);
                                                    ctrl.value = num.toFixed(2);
                                                }
                                            }

                                        </script>
                                    </div>
                                    <!-- <input id="patientSearchBox" type="text" class="rounded bg-light bg-secondary" style="border-color: #EEF0F2; background-color: #F1F4FB;" placeholder="Search">     -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer border-0">
                        <button id="add_submit_button" type="button" class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </div>
        </div>

        <!-- Edit Modal -->
        <div class="modal" id="editModal">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header border-0">
                        <div class="d-flex">
                            <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                            <h5 class="modal-title" style="color: #7764CA;">Edit treatment</h5>
                        </div>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <!-- Patient -->
                        <div class="d-flex">
                            <h6 class="modal-title pt-2" style="color: black;">Patient <b style="color: red;"> *</b></h6>
                        </div>

                        <div class="shadow p-3 mb-5 bg-white rounded">
                            <div class="form-group">
                                <input type="text" class="form-control" id="editPatientSearchBox" placeholder="Search" autocomplete="off">
                                <div id="editPatientSearchResultBox" class="d-flex justify-content-center">
                                </div>
                            </div>
                        </div>
                        
                        <!-- Tooth numbering system -->
                        <div class="d-flex">
                            <!-- <img class="img-responsive img-thumbnail img-fluid btn btn-xs" width="50px" src="{{ asset('img/tooth.svg') }}"></img> -->
                            <h6 class="modal-title pt-2" style="color: black;">Tooth number <b style="color: red;"> *</b></h6>
                        </div>

                        <div class="shadow p-3 mb-5 bg-white rounded">
                            <div class="d-flex justify-content-between">
                                <div class="col-s titleL">
                                    <h6 class="txt-grey ">Upper Left</h6>
                                </div>
                                <div class="col-s titleR">
                                    <h6 class="txt-grey ">Upper Right</h6>
                                </div>
                            </div>

                            <div class="d-flex flew-row justify-content-center">
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/molarDown.svg') }}" class="mb-1" class="mb-1" alt=""
                                        width="30%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">18</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">48</button>
                                    <img src="{{ asset('img/molar.svg') }}" class="mb-1" alt="" width="30%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/molarDown.svg') }}" class="mb-1" alt="" width="30%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">17</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">47</button>
                                    <img src="{{ asset('img/molar.svg') }}" class="mb-1" alt="" width="30%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/premolarDown.svg') }}" class="mb-1" alt="" width="20%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">16</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">46</button>
                                    <img src="{{ asset('img/premolar.svg') }}" class="mb-1" alt="" width="20%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">15</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">45</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">14</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">44</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/canineDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">13</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">43</button>
                                    <img src="{{ asset('img/canine.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">12</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">42</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">11</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">41</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="vl"></div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">21</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">31</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">22</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">32</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/canineDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">23</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">33</button>
                                    <img src="{{ asset('img/canine.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">24</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">34</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/incisorDown.svg') }}" class="mb-1" alt="" width="15%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">25</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">35</button>
                                    <img src="{{ asset('img/incisor.svg') }}" class="mb-1" alt="" width="15%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/premolarDown.svg') }}" class="mb-1" alt="" width="20%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">26</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">36</button>
                                    <img src="{{ asset('img/premolar.svg') }}" class="mb-1" alt="" width="20%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/molarDown.svg') }}" class="mb-1" alt="" width="30%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">27</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">37</button>
                                    <img src="{{ asset('img/molar.svg') }}" class="mb-1" alt="" width="30%">
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('img/molarDown.svg') }}" class="mb-1" alt="" width="30%">
                                    <button type="button" class="btn btn-purple btn-light mb-1">28</button>
                                    <button type="button" class="btn btn-purple btn-light mb-1">38</button>
                                    <img src="{{ asset('img/molar.svg') }}" class="mb-1" alt="" width="30%">
                                </div>
                            </div>

                            <div class="d-flex justify-content-between">
                                <div class="col-s titleL">
                                    <h6 class="txt-grey ">Lower Left</h6>
                                </div>
                                <div class="col-s titleR">
                                    <h6 class="txt-grey ">Lower Right</h6>
                                </div>
                            </div>

                        </div>

                        <!-- Procedure -->
                        <div class="d-flex">
                            <h6 class="modal-title pt-2" style="color: black;">Procedure <b style="color: red;"> *</b></h6>
                        </div>

                        <form id="edit_treatment_form" action="/treatments/update" method="post">
                            @csrf
                            <input type="hidden" name='tooth_no' id="edit_tooth_no">
                            <input type="hidden" name='treatment_id' id="edit_treatment_id">
                            <input type="hidden" name='patient_id' id="edit_patient_id">
                            <input type="hidden" name='date' id="edit_date">
                            <input type="hidden" name='total_amount' id="edit_total_amount">

                            <div class="shadow p-3 mb-5 bg-white rounded">
                                <div class="container overflow-auto" style="height: 200px;">
                                    <div class="row procedures_container">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- Date/Total amount -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="d-flex">
                                    <h6 class="modal-title pt-2" style="color: black;">Date <b style="color: red;"> *</b></h6>
                                </div>

                                <div class="shadow p-3 mb-5 bg-white rounded">
                                    <div class="form-group form-con">
                                        <input name="treatmentDate" type="date"class="form-control" id="edit_date_display" placeholder="Date">
                                        <div id="patientSearchResultBox" class="d-flex justify-content-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex">
                                    <h6 class="modal-title pt-2" style="color: black;">Total amount <b style="color: red;"> *</b></h6>
                                </div>

                                <div class="shadow p-3 mb-5 bg-white rounded">
                                    <div class="form-group form-con">
                                        <input type="text" style="text-align: right;" name="Amount" id="edit_total_amount_display"
                                        class="form-control" placeholder="Total value"
                                        onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)"
                                        onchange="RoundtoDecimal(this)" maxlength="6" min="0" max="999999" />

                                    <script>
                                        function maxLengthCheck(object) {
                                            if (object.value.length > object.maxLength)
                                                object.value = object.value.slice(0, object.maxLength)
                                        }

                                        function isNumeric(evt) {
                                            var theEvent = evt || window.event;
                                            var key = theEvent.keyCode || theEvent.which;
                                            key = String.fromCharCode(key);
                                            var regex = /[0-9]|\./;
                                            if (!regex.test(key)) {
                                                theEvent.returnValue = false;
                                                if (theEvent.preventDefault) theEvent.preventDefault();
                                            }
                                        }

                                        function RoundtoDecimal(ctrl) {
                                            if (ctrl.value.length > 0) {
                                                var num = parseFloat(ctrl.value);
                                                ctrl.value = num.toFixed(2);
                                            }
                                        }

                                    </script>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button id="edit_submit_button" type="button" class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </div>
        </div>

        <form id="deleteTreatmentForm" action="/treatments/delete" method="POST">
            @csrf
            <input type="hidden" name="patientName" id="deletePatientName">
            <input type="hidden" name="id" id="deleteTreatmentId">
            <input type="hidden" name="treatment_reference_id" id="treatment_reference_id">
        </form>

        @if (Auth::user()->user_role_id == 3)
        <script src="{{ asset('js/dentist-treatment.js') }}"></script>
        @else
        <script src="{{ asset('js/treatment.js') }}"></script>
        @endif

        <script src="{{ asset('js/calendardisabler.js') }}"></script>
    </div>
    </div>
@endsection
