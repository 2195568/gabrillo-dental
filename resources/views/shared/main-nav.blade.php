<div id="main-header" class="main-header mh-100 h-25 d-flex justify-content-around w-100">
        <ul class="navbar-header">
            <div id="logo" class="col d-flex align-items-center justify-content-center">
                <a href="/dashboard">
                    <img src="{{ asset('img/logo.png') }}" width="120" height="80" alt="logo">
                </a>
            </div>
        </ul>
        <ul id="main-nav-list" class="align-items-center main-header list-inline font-weight-bold">
            
            <li>
                <a id="dashboardNavItem" class="btn btn-default text-white" href="/dashboard">Dashboard</a>
            </li>
            @if (Auth::user()->user_role_id < 2)
            <li>
                <a id="usersNavItem" class="btn btn-default text-white" href="/users">Users</a>
            </li>
            @endif
            <li class="">
                <a id="patientsNavItem" class="btn btn-default text-white" href="/patients">Patients</a>
            </li>
            <li class="">
                <a id="appointmentsNavItem" class="btn btn-default text-white" href="/appointments">Appointments</a>
            </li>
            <li class="nav-item ">
                <a id="treatmentsNavItem" class="btn btn-default text-white" href="/treatments">Treatments</a>
            </li>
            @if (Auth::user()->user_role_id != 3)
            <li class="nav-item ">
                <a id="paymentsNavItem" class="btn btn-default text-white" href="/payments">Payments</a>
            </li>
            @endif
            <li class="nav-item ">
                <a id="reportsNavItem" class="btn btn-default text-white" href="/reports">Reports</a>
            </li>
        </ul>
        <ul class="navbar-header">
            <div>
                <div class="dropdown">   
                    <button  class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img id="profileImage" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" width="80" height="70"/>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <p class="pl-3 text-muted">Manage account</p>
                        <a class="dropdown-item" href=" /user/profile">
                            <u>Profile</u>
                        </a>
                        <form id="logoutForm" method="POST" action="{{ route('logout') }}">
                            {{ csrf_field() }}
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logoutForm').submit()">
                                <u>Logout</u>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </ul>
    </div>