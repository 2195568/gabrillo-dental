<html>

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/login.css')}}"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
        integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>

    <title>GABRILLO DENTAL CLINIC</title>

</head>

<body>
    <nav class="navbar navbar-light fixed-top">
        <a class="navbar-brand"><img src="{{ asset('img/logo-desk.png')}}" alt="logo" width="45px">
            <b class="navtitle">GABRILLO DENTAL CLINIC</b>
        </a>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li>
                    <a class="color-me" href="#aboutus"><b>ABOUT US</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="conform rounded boarder-info justify-content-start">
        <div style="margin-left: 20px;margin-right: 20px">
            <div class="row" style="margin-top: 30px;">
                <div class="col-lg">
                    <h2 class="title text-dark">WELCOME TO GABRILLO DENTAL CLINIC</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg">
                    <form method="POST" action="{{ route('login') }}">
                         @csrf
                        <div class="form-group">
                            <label for="label formGroupExampleInput">Email</label>
                            <input name="email" type="text" class="form-control" id="formGroupExampleInput" placeholder="Enter email...">
                        </div>
                        <div class="form-group">
                            <label for="label formGroupExampleInput2">Password</label>
                            <input name="password" type="password" class="form-control" id="formGroupExampleInput2" placeholder="Enter password...">
                        </div>
                        @if ($errors->any())
                        <ul>
                                @foreach ($errors->all() as $error)
                                    <li style="color: red;">{{ $error }}</li>
                                @endforeach
                        </ul>
                        @endif
                        <div style="margin-left: 41%; margin-top: 30%">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!--
<img class="vector1" src="/images/Asset 1.svg" alt="svg">
<img class="svg1" src="/images/LGroupM.svg" alt="svg">
<img class="svg2" src="/images/BIgC.svg" alt="">
-->

    <img class="try" src="{{ asset('img/doctor.svg') }}" alt="">
    <img class="svg2" src="{{ asset('img/BIgC.svg') }}" alt="">

</body>

</html>