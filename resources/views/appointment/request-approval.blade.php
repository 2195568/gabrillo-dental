@extends('master')
@section('title', 'Gabrillo Dental Clinic')
@section('content')

@if(!(empty(Session::get('success'))))
<div class="msg mb-5 auto-hide" style="position: fixed; bottom: 0; right: 0; z-index: 9999;">
    The appointment of {{ Session::get('patientName') }} has been accepted! 
</div>
<!-- <div class="alert alert-success">
</div> -->
@endisset
<div id="content">
<div id="appointmentSecondaryNav" class="secondary-nav d-flex justify-content-between nopadding">
    <div class="mr-auto p-2">
        <a class="mx-5" href="/appointments">Appointments</a>
        <a class="active mx-1" href="/appointments/request-approval">Request approval</a>
    </div>
    <div class="p-2">
        <!-- <input id="searchInput" class="mx-5 rounded bg-light" type="text" placeholder="Search " aria-label="Search "> -->
        <!-- <input type="text" class="mx-5 rounded bg-light bg-secondary" style="border-color: white; background-color: #F1F4FB;" placeholder="Search"> -->
        <!-- <button type="mx-5 button" id="plus-button" class="btn btn-default btn-circle btn-xl border" data-toggle="modal" data-target="#addModal">
                <i class="fa fa-plus" style="color: #7764CA;"></i>
            </button> -->
        <!-- <input class="mx-5 rounded bg-light border-0" type="date"> -->
    </div>
</div>


<div style='background-color: #F3F6FB;' id='wrap'>
  <div class="mx-auto " style="width: 1200px;">
  <!-- <h3 class="text-center text-primary"><b>Appointment Approval</b><h3>
    <div class="col-md text-center fs-6" stlye="width:400px">
      <input type="date" id="picker" class="mx-auto" style="width: 200px;">
    </div> -->
    <!-- <br> -->
    <!-- <br> -->
    <div id="main-content" class="container-fluid" style="background-color: #F3F6FB;">
    <div class="container">
            <div class="tab-slider--nav ">
                    @if(empty(Session::get('tab2')))
                    <ul class="tab-slider--tabs ">
                        <li class="tab-slider--trigger active" rel="tab1">Old Patient</li>
                        <li class="tab-slider--trigger" rel="tab2">New Patient</li>
                    </ul>
                    @else
                    <ul class="tab-slider--tabs slide">
                        <li class="tab-slider--trigger" rel="tab1">Old Patient</li>
                        <li class="tab-slider--trigger active" rel="tab2">New Patient</li>
                    </ul>
                    @endif
                
            </div>
            <div class="tab-slider--container">
                <div id="tab1" class="tab-slider--body">
                    <div class="d-flex flex-column justify-content-center pl-3">
                        <img src="{{ asset('img/no-data.png') }}" style="height: 500px;">
                    </div>
                </div>
                <div id="tab2" class="tab-slider--body">
                    <div class="d-flex flex-column justify-content-center pl-3">
                        <img src="{{ asset('img/no-data.png') }}" style="height: 500px;">
                    </div>
                    <!-- <h2>Second Tab</h2>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras mattis consectetur purus sit amet fermentum. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p> -->
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <div class="d-flex flex-column justify-content-center">
                <div class="d-flex flex-column justify-content-center pl-3">
                </div>
            </div>
        </div>
    </div>
    

<!-- Accept Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <form id="appointment-accept-form" action="/appointments/request-approval/accept" method="post">
        @csrf
        <input type="hidden" name='id' id="patient_id">
        <input type="hidden" name='email' id="email">
        <input type="hidden" name='first_name' id="first_name">
        <input type="hidden" name='last_name' id="last_name">
        <input type="hidden" name='time_end' id="time_end">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                        <h5 class="modal-title" style="color: #7764CA;">Add new appointment</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!-- Patient -->
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                        <h6 class="modal-title pt-2" style="color: black;">Patient</h6>
                    </div>
                    
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group">
                            <input type="text" class="form-control" id="editPatientSearchBox" placeholder="Search" autocomplete="off">
                            <div id="editPatientSearchResultBox" class="d-flex justify-content-center">
                            </div>
                        </div>
                    </div>

                    <!-- Procedure -->
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                        <h6 class="modal-title pt-2" style="color: black;">Procedure <span style="color: red;">(For others, please specify in the description box below.)</span></h6>
                    </div>
                    
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="container overflow-auto" style="height: 200px;">
                            <div class="row procedures_container">
                            </div>
                        </div>
                    </div>

                    <!-- Date -->
                    <div class="form-group">
                        <h6>Date</h6>
                        <input id="date" name="date" type="date" class="form-control" id="appointmentDate" readonly>    
                        
                    </div>
                    <div class="form-group">
                        <h6>Time</h6>
                        <input id="time" type='time' name="time_start" class="datetimepicker form-control" id="appointmentTimeStart" readonly>

                    </div>

                    <div class="form-group">
                        <h6>Description<b style="color: red;"> *</b></h6>
                        <textarea class="form-control" name="description" id="description"
                            maxlength="200" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="save-changes" class="btn btn-primary">Add new appointment</button>
                </div>
            </div>
        </div>
    </form>

</div>

<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <form id="appointment-accept-form" action="/appointments/request-approval/reject" method="post">
        @csrf
        <input type="hidden" name='id' id="reject_patient_id">
        <input type="hidden" name='patient_type' id="reject_patient_type">
        <input type="hidden" name='first_name' id="reject_first_name">
        <input type="hidden" name='last_name' id="reject_last_name">
        <input type="hidden" name='date' id="reject_date">
        <input type="hidden" name='time' id="reject_time">
        <input type="hidden" name='email' id="reject_email">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
            <div class="modal-header border-0">
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                        <h5 class="modal-title" style="color: #7764CA;">Reject appointment</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <h6>Message<b style="color: red;"> *</b></h6>
                        <textarea class="form-control" name="message" maxlength="500" required></textarea>
                    </div>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" id="save-changes" class="btn btn-primary">Send</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Review Modal -->
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <form id="appointment-accept-form" action="/appointments/request-approval/review/accept" method="post">
        @csrf
        <input type="hidden" name="profile_bg_color" id="review_profile_bg_color">
        <input type="hidden" name="reference_id" id="review_reference_id">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
            <div class="modal-header border-0">
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                        <h5 class="modal-title" style="color: #7764CA;">Review appointment</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">

                    <!-- Patient Information -->
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                        <h6 class="modal-title pt-2" style="color: black;">Patient Information</h6>
                    </div>
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group">
                            <h6>First Name<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_first_name" name="first_name" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Last Name<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_last_name" name="last_name" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Middle Name<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_middle_name" name="middle_name" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Birth Date<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_birth_date" name="birth_date" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Age<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_age" name="age" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Gender<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_gender" name="gender" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Address<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_address" name="address" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Contact No<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_contact_no" name="contact_no" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Email<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_email" name="email" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Occupation<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_occupation" name="occupation" readonly>
                        </div>
                    </div>

                    <!-- Appointment -->
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                        <h6 class="modal-title pt-2" style="color: black;">Appointment</h6>
                    </div>

                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group">
                            <h6>Date<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_date" name="date" readonly>
                        </div>
                        <div class="form-group">
                            <h6>Time<b style="color: red;"> *</b></h6>
                            <input class="form-control" id="review_time" name="time_start" readonly>
                            <input type="hidden" id="review_time_end" name="time_end" readonly>
                        </div>
                    </div>

                    <!-- Procedure -->
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                        <h6 class="modal-title pt-2" style="color: black;">Procedure <span style="color: red;">(For others, please specify in the description box below.)</span></h6>
                    </div>
                    
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="container overflow-auto" style="height: 200px;">
                            <div class="row procedures_container">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                            <h6>Description<b style="color: red;"> *</b></h6>
                            <textarea class="form-control" id="review_description" name="description" readonly></textarea>
                        </div>
                    
                </div>
                    <div class="modal-footer">
                        <button type="submit" id="save-changes" class="btn btn-primary">Accept</button>
                        <button type="button" class="btn btn-danger reject-review" data-dismiss="modal">Reject</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

</div>
</div>
</div>
<script src="/js/appointments/request-approval.js"></script>
@endsection



