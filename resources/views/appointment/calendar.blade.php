<div id="appointmentSecondaryNav" class="secondary-nav d-flex justify-content-between nopadding">
    <div class="mr-auto p-2">
        <a class="active mx-5" href="/appointments">Appointments</a>
        @if (Auth::user()->user_role_id != 3)
        <a class="mx-1" href="/appointments/request-approval">Request approval</a>
        @endif
    </div>
    <div class="p-2">
        <!-- <input id="searchInput" class="mx-5 rounded bg-light" type="text" placeholder="Search " aria-label="Search "> -->
        @if (Auth::user()->user_role_id != 3)
        <input id="searchBox" type="text" class="mx-5 rounded bg-light bg-secondary" style="border-color: white; background-color: #F1F4FB;" placeholder="Search" maxlength="13" autocomplete="off">
        <button type="mx-5 button" id="plus-button" class="btn btn-default btn-circle btn-xl border" data-toggle="modal"
            data-target="#addModal">
            <i class="fa fa-plus" style="color: #7764CA;"></i>
        </button>
        @endif
        <div id="searchResult" class="dropdown-menu" style="height:200px; overflow-y: scroll;"
            aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">
                <u>Profile</u>
            </a>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <form id="editAppointmentForm" action="/appointments/update" method="POST">
                @csrf
                <input type="hidden" name='patient_id' id="edit_patient_id">
                <input type="hidden" name='id' id="editAppointmentId">
                <input type="hidden" name='time_start' id="edit_time_start">
                <input type="hidden" name='time_end' id="edit_time_end">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="editAppointmentHeading">Edit appointment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <!-- Patient -->
                        <div class="shadow p-3 mb-5 bg-white rounded">
                            <div class="d-flex">
                                <h6 class="modal-title pt-2" style="color: black;">Patient<b style="color: red;"> *</b></h6>
                            </div>
                            <div class="form-group form-com">
                                <input type="text" class="form-control" id="editPatientSearchBox" placeholder="Search" autocomplete="off">
                                <div id="editPatientSearchResultBox" class="d-flex justify-content-center mt-3" style="overflow: auto; max-height: 300px;">
                                </div>
                            </div>
                        </div>

                        <!-- Procedure -->
                        <div class="shadow p-3 mb-5 bg-white rounded">
                            <div class="d-flex">
                                <h6 class="modal-title pt-2" style="color: black;">Procedure <span style="color: red;">(For others, please specify in the description box below.)</span></h6>
                            </div>
                            <div class="container overflow-auto" style="height: 200px;">
                                <div class="row procedures_container">
                                </div>
                            </div>
                        </div>

                        <!-- Date -->
                        <div class="shadow p-3 mb-5 bg-white rounded">
                            <h6 class="modal-title pt-2" style="color: black;">Date<b style="color: red;"> *</b></h6>
                            <div class="form-group form-con">
                                <input name="date" type="date" class="form-control" id="editAppointmentDate">
                            </div>
                        </div>

                        <!-- Time -->
                        <div class="shadow p-3 mb-5 bg-white rounded">
                            <div class="form-group form-con">
                                <h6 class="modal-title pt-2" style="color: black;">Time<b style="color: red;"> *</b></h6>
                                <select class="form-control" id="edit_time">
                                    <option selected disabled>Select time</option>
                                    <option value="08:00-09:00">8:00 AM</option>
                                    <option value="09:00-10:00">9:00 AM</option>
                                    <option value="10:00-11:00">10:00 AM</option>
                                    <option value="11:00-12:00">11:00 AM</option>
                                    <option value="13:00-14:00">1:00 PM</option>
                                    <option value="14:00-15:00">2:00 PM</option>
                                    <option value="15:00-16:00">3:00 PM</option>
                                    <option value="16:00-17:00">4:00 PM</option>
                                    <option value="17:00-18:00">5:00 PM</option>
                                </select>
                            </div>
                        </div>


                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group form-con">
                            <h6>Description<b style="color: red;"> *</b></h6>
                            <textarea name="description" class="form-control" id="editAppointmentDescription" maxlength="200" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    @if (Auth::user()->user_role_id != 3)
                    <button type="submit" id="deleteAppointmentSubmit" class="btn btn-danger"
                        data-dismiss="modal">Delete</button>
                    <button type="submit" id="editSaveChanges" class="btn btn-primary">Submit</button>
                    @endif
                </div>
            </form>
            <!-- Delete appointment form -->
            <form id="deleteAppointmentForm" action="/appointments/delete" method="post">
                @csrf
                <input type="hidden" name='id' id="deleteAppointmentId">
                <input type="hidden" name='title' id="deleteAppointmentTitle">
            </form>
        </div>
    </div>
</div>


<!-- Add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <form id="addAppointmentForm" action="/appointments/store" method="post">
        @csrf
        <input type="hidden" name='id' id="add_patient_id">
        <input type="hidden" name='time_start' id="add_time_start">
        <input type="hidden" name='time_end' id="add_time_end">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                        <h5 class="modal-title" style="color: #7764CA;">Add new appointment</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <!-- Patient -->
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="d-flex">
                            <h6 class="modal-title pt-2" style="color: black;">Patient<b style="color: red;"> *</b></h6>
                        </div>
                        <div class="form-group form-con">
                            <input type="text" class="form-control" id="patientSearchBox" placeholder="Search" autocomplete="off">
                            <div id="patientSearchResultBox" class="d-flex justify-content-center mt-3" style="overflow: auto; max-height: 300px;">
                            </div>
                            <i class="fa fa-check-circle"></i>
                            <i class="fa fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                    </div>

                    <!-- Procedure -->
                    <div class="d-flex">
                        <h6 class="modal-title pt-2" style="color: black;">Procedure <span style="color: red;">(For others, please specify in the description box below.)</span></h6>
                    </div>
                    
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="container overflow-auto" style="height: 200px;">
                            <div class="row procedures_container">
                            </div>
                        </div>
                    </div>

                    <!-- Date -->
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group form-con">
                            <h6>Date<b style="color: red;"> *</b></h6>
                            <input name="date" type="date" id="appointmentDate">    
                            <i class="fa fa-check-circle"></i>
                            <i class="fa fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                    </div>

                    <!-- Time -->
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group form-con">
                            <h6 class="modal-title pt-2" style="color: black;">Time<b style="color: red;"> *</b></h6>
                            <select class="form-control" id="add_time">
                                <option selected disabled>Select time</option>
                                <option value="08:00-09:00">8:00 AM</option>
                                <option value="09:00-10:00">9:00 AM</option>
                                <option value="10:00-11:00">10:00 AM</option>
                                <option value="11:00-12:00">11:00 AM</option>
                                <option value="13:00-14:00">1:00 PM</option>
                                <option value="14:00-15:00">2:00 PM</option>
                                <option value="15:00-16:00">3:00 PM</option>
                                <option value="16:00-17:00">4:00 PM</option>
                                <option value="17:00-18:00">5:00 PM</option>
                            </select>
                            <i class="fa fa-check-circle"></i>
                            <i class="fa fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                    </div>

                    <!-- Description -->
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group form-con">
                            <h6>Description<b style="color: red;"> *</b></h6>
                            <textarea class="form-control" name="description" id="appointmentDescription" maxlength="200" required></textarea>
                            <i class="fa fa-check-circle"></i>
                            <i class="fa fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="save-changes" class="btn btn-primary">Submit</button>
                </div>
            </div>  
        </div>
    </form>

</div>

<!-- Calendar -->
<!-- <div style='background-color: #F3F6FB;' id='wrap'>
    <div style='background-color: #F3F6FB;' id='calendar'></div>
    <div style='background-color: #F3F6FB;' style='clear:both'></div>
</div> -->
<div id="main-content" class="container-fluid" style="background-color: #F3F6FB;">
    <div id="searchResultContainer" class="container-fluid"></div>
    <div style='background-color: #F3F6FB;' id='wrap'>
        <div style='background-color: #F3F6FB;' id='calendar'></div>
        <div style='background-color: #F3F6FB;' style='clear:both'></div>
    </div>  
</div>

<script src="{{ asset('js/appointments/fullcalendar.min.js') }}"></script>
<!-- <script src="{{ asset('js/appointments/disable.js') }}"></script> -->

@if (Auth::user()->user_role_id == 3)
<script src="{{ asset('js/dentist-calendar.js') }}"></script>
@elseif (Auth::user()->user_role_id == 4)
<script src="{{ asset('js/receptionist-calendar.js') }}"></script>
@else
<script src="{{ asset('js/calendar.js') }}"></script>
@endif
