<!doctype html>
<html lang="en">

<head>
    <title>Gabrillo Dental Clinic</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- css -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="css/website.css">
    <link rel="stylesheet" href="css/website-svg.css">

    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/apple-touch-icon.png') }}">
   <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
   <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }} ">
   <link rel="mask-icon" href="{{ asset('/safari-pinned-tab.svg') }}" color="#5bbad5">
   <link rel="manifest" href="{{ asset('/site.webmanifest') }}">
    <script src="svg-inject.min.js"></script>
</head>

<body>
    <!-- Nav Bar -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top nav nav-pills nav-justified">
        <h1 class="navbar-brand"><img src="img/landinglogo.png" alt="logo" width="45px">
            <b>GABRILLO DENTAL CLINIC</b>
        </h1>

        <a class="flex-md-fill text-sm-center nav-link nav-item active" href="#pills-home" data-toggle="pill"><b>HOME</b></a>

        <a class="flex-md-fill text-sm-center nav-link nav-item" href="#pills-appointment" data-toggle="pill"><b>APPOINTMENT</b></a>

        <a class="flex-md-fill text-sm-center nav-link nav-item" href="#pills-aboutUs" data-toggle="pill"><b>ABOUT US</b></a>
        <a class="flex-md-fill text-sm-center nav-link nav-item" href="/login"><b>LOGIN</b></a>
    </nav>


    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <section id="home">
                <div class="row">
                    <div class="col-lg-7">
                        <img id="vector1" src="img/landing/home/Vector1.svg" alt="svg">
                    </div>
                    <div class="col-sm">
                        <h2 id="textHome1">WELCOME TO GABRILLO DENTAL CLINIC</h2>
                        <h6>Gabrillo Dental clinic is a dental clinic located at Villasis, Pangasinan.
                            The clinic is managed by Dr.Gabrillo and her staff.
                        </h6>
                    </div>
                </div>
                <img id="homeSVG1" src="img/landing/home/Home1.svg" alt="svg">
                <img id="homeSVG2" src="img/landing/home/Home2.svg" alt="svg">
                <img id="homeSVG3" src="img/landing/home/Home3.svg" alt="svg">

            </section>
        </div>

        <div class="tab-pane fade" id="pills-appointment" role="tabpanel" aria-labelledby="pills-profile-tab">
            <section id="appointments">
                <div class="row">
                    <div class="col-lg-7">
                        <img id="vector2" src="img/landing/appointments/Vector2.svg" alt="svg">
                    </div>
                    <div class="col-sm">
                        <h2 id="textAppointments1">SET AN APPOINTMENT</h2>
                        <h6 data-aos="fade">The clinic provide dental services such as tooth restorations, braces, fillings, and other more!</h6>
                        <a href="/online-appointment/patient-type" id="appButton" class="btn btn-primary">SET APPOINTMENT</a>
                    </div>
                </div>
            </section>
        </div>


        <div class="tab-pane fade" id="pills-aboutUs" role="tabpanel" aria-labelledby="pills-contact-tab">
            <section id="aboutus">

                <div class="d-flex justify-content-center row upper">
                    <div class="d-flex flex-column justify-content-center">
                    <h1 id="upperTitle">ABOUT US</h1>
                    <h5 id="upperTitle2">The story of the Gabrillo Dental Clinic.</h5>
                    </div>
                </div>

                <div class="d-flex justify-content-center row lower">
                    <div class="col-md-5">
                        <h6 id="lowerDesc">Gabrillo Dental Clinic (GDC) was founded by Doctor Maria Najiva D. Gabrillo in
                            1991, which provides dental services and dental treatment for all types of dental patients.
                            It is well known in Villasis, Pangasinan, with its specialization in orthodontics and oral implantology.
                            We take pride in helping our patient be the best version of themselves in dental health. We will always
                            strive to improve our capabilities and provide for our valued patients.
                        </h6>
                    </div>
                </div>

            </section>
        </div>

    </div>



    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init({
            offset: 400
        });
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>