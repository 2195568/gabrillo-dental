<!doctype html>
<html lang="en">

<head>
    <title>Gabrillo Dental Clinic</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- css -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="css/website.css">
    <link rel="stylesheet" href="css/website-svg.css">

    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/apple-touch-icon.png') }}">
   <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
   <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }} ">
   <link rel="mask-icon" href="{{ asset('/safari-pinned-tab.svg') }}" color="#5bbad5">
   <link rel="manifest" href="{{ asset('/site.webmanifest') }}">
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="svg-inject.min.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
</head>

<body>
    <!-- Nav Bar -->
    <div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top container-fluid">
        <a class="navbar-brand" href="#home"><img src="img/landinglogo.png" alt="logo" width="45px" data-aos="fade" data-aos-delay="4000" data-aos-duration="2000">
            <b data-aos="fade" data-aos-delay="200" data-aos-duration="2000">GABRILLO DENTAL CLINIC</b>
        </a>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li>
                    <a class="color-me" href="#home"><b>HOME</b></a>
                </li>
                <li>
                    <a class="color-me" href="#appointments"><b>APPOINTMENTS</b></a>
                </li>
                <li>
                    <a class="color-me" href="#aboutus"><b>ABOUT US</b></a>
                </li>
                <li>
                    <a class="color-me" href="/login"><b>LOGIN</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <!-- Home Content -->
    <section id="home" class="container-fluid">
        <div class="row">
            <div class="col-lg-7">
                <img id="vector1" src="img/landing/home/Vector1.svg" alt="svg" data-aos="slide-right" data-aos-delay="300" data-aos-duration="2000">
            </div>
            <div class="col-sm">
                <h2 id="textHome1" data-aos="slide-left" data-aos-delay="300" data-aos-duration="2000">WELCOME TO GABRILLO DENTAL CLINIC</h2>
                <h6>Gabrillo Dental clinic is a dental clinic located at Villasis, Pangasinan.
                    The clinic is managed by Dr.Gabrillo and her staff.
                </h6>
            </div>
        </div>
        <img id="homeSVG1" src="img/landing/home/Home1.svg" alt="svg" data-aos="fade" data-aos-delay="400" data-aos-duration="4000">
        <img id="homeSVG2" src="img/landing/home/Home2.svg" alt="svg" data-aos="fade" data-aos-delay="300" data-aos-duration="3500">
        <img id="homeSVG3" src="img/landing/home/Home3.svg" alt="svg" data-aos="fade" data-aos-delay="100" data-aos-duration="3500">
        <img id="homeSVG4" src="img/landing/home/Home4.svg" alt="svg" data-aos="fade" data-aos-delay="200" data-aos-duration="2000">
    </section>

    <!-- Appointments Content -->
    <section id="appointments">
        <div class="row">
            <div class="col-lg-7">
                <img id="vector2" src="img/landing/appointments/Vector2.svg" alt="svg" data-aos="fade-right" data-aos-delay="100">
            </div>
            <div class="col-sm">
                <h2 id="textAppointments1">SET AN APPOINTMENT</h2>
                <h6 data-aos="fade-left" data-aos-delay="100">The clinic provide dental services such as tooth restorations, braces, fillings, and other more!</h6>
                <a href="/online-appointment/patient-type" id="appButton" class="btn btn-primary">SET APPOINTMENT</a>
            </div>
        </div>
        <img id="appSVG1" src="img/landing/appointments/App1.svg" alt="svg" data-aos="fade" data-aos-delay="700" data-aos-duration="3500">
        <!-- <img id="appSVG2" src="img/landing/appointments/App2.svg" alt="svg" data-aos="fade" data-aos-delay="500" data-aos-duration="3500"> -->
        <img id="appSVG3" src="img/landing/appointments/App3.svg" alt="svg" data-aos="fade" data-aos-delay="890" data-aos-duration="3000">
        <img id="homeSVG5" src="img/landing/home/Home5.svg" alt="svg" data-aos="fade" data-aos-delay="400" data-aos-duration="2000">
    </section>

     <!-- AboutUsS Content -->
    <section id="aboutus">
        <div class="row justify-content-center">
            <div class="col-6 d-flex justify-content-center">
                <div id="mapCon"><img  id="map" src="img/map.png" alt="map"></div>
            </div>
            <div class="col-6">
                <div class="row d-flex justify-content-center">
                    <div class="col d-flex justify-content-center">
                        <h1 id="aboutUsHeader">ABOUT US</h1>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div id="aboutUsText" class="col-6">
                        <h5>Gabrillo Dental Clinic (GDC) was founded by Doctor Maria Najiva D. Gabrillo in 1991, which provides dental services and dental treatment for all types of dental patients.
                            It is well known in Villasis, Pangasinan, with its specialization in orthodontics and oral implantology.We, The Gabrillo dental clinic vowes to provide the best service to its
                            patients, to bring the best out of people in dental ways. We care about our patients and will continue to improve our services and practices.</h5>
                    </div>
                </div>
            </div>
        </div>
        <img id="aboutSVG1" src="img/landing/aboutus/aboutUs1.svg" alt="svg">
        <img id="aboutSVG2" src="img/landing/aboutus/aboutUs2.svg" alt="svg">
    </section>
    </div>
    <script>
        AOS.init({
            offset: 400
        });
    </script>
</body>
</html>