<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <title>Appointment</title>
   <link rel="stylesheet" href="/styles/fontawesome-free-5.15.4-web/css/all.css">
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/new-patient.css') }}">
   <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/apple-touch-icon.png') }}">
   <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
   <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }} ">
   <link rel="mask-icon" href="{{ asset('/safari-pinned-tab.svg') }}" color="#5bbad5">
   <link rel="manifest" href="{{ asset('/site.webmanifest') }}">
   <!-- datepicker -->
   <link href="http://code.jquery.com/ui/1.9.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />
   <!--time picker-->
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

   <!--date picker -->
   <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
   <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
   <!-- timepicker -->
   <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
   <!-- Sweet alert -->
   <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>

<body>
   <nav class="navbar navbar-expand-lg bg-light navbar-light fixed-top">
      <a class="navbar-brand"><img src="{{ asset('img/landinglogo.png') }}" alt="logo" width="45px">
         <b id="gdc">GABRILLO DENTAL CLINIC</b>
      </a>
   </nav>

   <div class="container border">
      <div class="patientCon">
         <div class="titleCon">
            <h3 id="patientTitle">PATIENT INFORMATION</h3>
         </div>
         <div class="d-flex justify-content-start">
            <h6 class="secondaryTitle"><i>To send an application for a scheduled appointment, please fill out the form.</i></h6>
         </div>

         <form action="/online-appointment/submit" method="POST" class="form" id="form">
            @csrf
            <input type="hidden" name="patient_type" value="new">
            <input type="hidden" id="time_start" name="time_start">
            <input type="hidden" id="time_end" name="time_end">
            <input type="hidden" id="profile_bg_color" name="profile_bg_color">
            <div class="row">
               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientFirstName"><b>FIRST NAME</b><b class="asterisk">*</b></label>
                     <input type="text" name="first_name" id="patientFirstName" maxlength="20" />
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>

               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientLastName"><b>LAST NAME</b><b class="asterisk">*</b></label>
                     <input type="text" name="last_name" id="patientLastName" maxlength="20" />
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientMiddleName"><b>MIDDLE NAME</b><b class="asterisk">*</b></label>
                     <input type="text" name="middle_name" id="patientMiddleName" maxlength="20" />
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>
               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientBirthDate"><b>BIRTHDATE</b><b class="asterisk">*</b></label>
                     <input type='date' name="birth_date" class="datetimepicker" id="patientBirthDate">
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientAge"><b>AGE</b><b class="asterisk">*</b></label>
                     <input type="number" name="age" id="patientAge" maxlength="10" />
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>

               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientGender"><b>GENDER</b><b class="asterisk">*</b></label>
                     <select name="gender" id="patientGender">
                        <option selected value='' disabled>Select gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                     </select>
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientContactNumber"><b>CONTACT NUMBER</b><b class="asterisk">*</b></label>
                     <input type="text" name="contact_no" id="patientContactNumber" maxlength="11" />
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>

               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientEmailAddress"><b>EMAIL</b><b class="asterisk">*</b></label>
                     <input type="text" name="email" id="patientEmailAddress" maxlength="50" />
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientHomeAddress"><b>HOME ADDRESS</b><b class="asterisk">*</b></label>
                     <input type="text" name="address" id="patientHomeAddress" maxlength="50" />
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>

               <div class="col-sm-5">
                  <div class="form-con">
                     <label class="d-flex justify-content-start" for="patientOccupation"><b>OCCUPATION</b></label>
                     <input type="text" name="occupation" id="patientOccupation" maxlength="10" />
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-lg-10">
                  <label class="d-flex justify-content-start" for="patientAllergies"><b>Allergies</b></label>
                  <div class="d-flex justify-content-start">
                     <h6 class="secondaryTitle">
                           <i>
                           For more than 1 allergy, please use comma to separate the items (e.g., peanuts, pollen, eggs, milk).
                           If none, enter word "None".
                           </i>
                        </h6>
                  </div>
                  <div class="form-con">
                     <input id="patientAllergies" type='text' name="allergies">
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                     
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-lg-10">
                  <label class="d-flex justify-content-start" for="desc"><b>DESCRIPTION</b><b class="asterisk">*</b></label>
                  <div class="d-flex justify-content-start">
                        <h6 class="secondaryTitle">
                           <i>
                              Enter a brief description of what you are feeling or the purpose of the appointment.
                           </i>
                        </h6>
                     </div>
                  <div class="form-con">
                     <textarea id="description" name="description" rows="3"></textarea>
                     <i class="fas fa-check-circle"></i>
                     <i class="fas fa-exclamation-circle"></i>
                     <small>Error message</small>
                  </div>
               </div>
            </div>

            <!-- schedules -->
            <div class="vacantCon">
               <div class="titleCon">
                  <h3 id="avail">CHOOSE SCHEDULE</h3>
               </div>
               <div class="d-flex justify-content-start">
                  <h6 class="secondaryTitle"><i>Refer to the vacant schedules below.</i></h6>
               </div>
               <div class="row">
                  <div class="col-sm-3">
                     <div class="form-con">
                        <label class="d-flex justify-content-start" for="fname"><b>Date:</b><b class="asterisk"> *</b></label>
                        <input type="text" id="datepicker" name="date" readonly='true' required>
                        <i class="fas fa-check-circle"></i>
                        <i class="fas fa-exclamation-circle"></i>
                        <small>Error message</small>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-con">
                        <label class="d-flex justify-content-start"><b>Time Start:</b><b class="asterisk"> *</b></label>
                        <input type="text" id="stime" name="time_start" readonly='true'disabled required/>
                        <i class="fas fa-check-circle"></i>
                        <i class="fas fa-exclamation-circle"></i>
                        <small>Error message</small>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-con">
                        <label class="d-flex justify-content-start"><b>Time End:</b><b class="asterisk"> *</b></label>
                        <input type="text" id="otime" name="time_end" readonly='true' disabled required/>
                        <i class="fas fa-check-circle"></i>
                        <i class="fas fa-exclamation-circle"></i>
                        <small>Error message</small>
                     </div>
                  </div>
               </div>
               <div class="availCon">
                  <table class="table table-striped">
                     <thead id="schedules-head" class="thead-dark">
                        <tr>
                           <th scope="col">#</th>
                           <th scope="col">Available Schedules</th>
                           <th scope="col"></th>
                        </tr>
                     </thead>
                     <tbody id="schedules-body">
                     </tbody>
                  </table>
               </div>
            </div>
            </form>
            <div class="row">
               <button id="sweetalert">Submit</button>
            </div>
            <div class="d-flex justify-content-start">
               <h6 class="secondaryTitle"><i>After a successful submition. An email will be sent to your email address<br> for the confirmation and finalizations of the appointment</i></h6>
            </div>
      </div>
   </div>


   </div>
   </div>

   @isset($success)
   <script>
      Swal.fire({
               icon: 'success',
               html: 'Application has been sent! We will email you soon once your appointment has been processed!',
               focusConfirm: false,
               confirmButtonText: 'Okay',
         }).then(function() {
               $('form#form').submit();
               window.location = "/online-appointment";
         });
   </script>
   @endisset

   <script src="{{ asset('js/online-appointment-new-patient.js') }}"></script>
   <script src="scriptPlugins.js"></script>
</body>

</html>