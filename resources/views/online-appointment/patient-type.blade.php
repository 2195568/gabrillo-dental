<!DOCTYPE html>
<html lang="en">

<head>
    <title>Online Appointment</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/patient-type.css') }}">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
</head>

<body>
@if ($errors->any())
<div class="msg mb-5 auto-hide alert-danger"" style="position: fixed; bottom: 0; right: 0; z-index: 9999;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
</div>
@endif

@if (session('err'))
<div class="msg mb-5 auto-hide alert-danger" style="position: fixed; bottom: 0; right: 0; z-index: 9999;">
    {{ session('err') }}
</div>
<!-- <div class="alert alert-success">
</div> -->
@endif

@if (session('status'))
<div class="msg mb-5 auto-hide" style="position: fixed; bottom: 0; right: 0; z-index: 9999;">
    {{ session('status') }}
</div>
<!-- <div class="alert alert-success">
</div> -->
@endif
    <nav class="navbar navbar-expand-lg bg-light navbar-light fixed-top">
        <a class="navbar-brand"><img src="{{ asset('img/landinglogo.png') }}" alt="logo" width="45px">
            <b>GABRILLO DENTAL CLINIC</b>
        </a>
    </nav>

    <div class="container">
        <div class="row content">
            <div class="col colNew">
                <div class="d-flex flex-column align-items-center ">
                    <h1 class="header">NEW PATIENT</h1>
                    <h6>A new patient in The Gabrillo Dental Clinic</h6>
                    <a class="btn btn-success btn-block" href="/online-appointment/new-patient-form" role="button">NEW PATIENT</a>
                </div>
            </div>

            <div class="col-md-1 colLine d-flex justify-content-center">
                <div class="vline"></div>
            </div>

            <div class="col colOld">
                <div class="d-flex flex-column align-items-center">
                    <h1 class="header">OLD PATIENT</h1>
                    <h6>A former patient whom had previous treatments in the clinic</h6>
                    <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#oldModal">OLD PATIENT</button>
                  
                    <div class="modal fade" id="oldModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Enter Patient ID</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/get-patient-by-slug">
                                        <div class="form-group">
                                            <label>Patient ID</label>
                                            <input type="number" class="form-control" name="slug" id="patientID" required>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success">Enter</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
