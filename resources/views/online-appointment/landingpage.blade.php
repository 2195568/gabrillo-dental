<!doctype html>
<html lang="en">

<head>
   <title>Gabrillo Dental Clinic</title>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" href="css/landing.css">
</head>

<body>
   <header>
      <nav>
         <a id="header" class="navbar-brand font-weight-bold">
            <img id="logo" src="img/landinglogo.png" alt="logo" style="width:40px;">Gabrillo Dental Clinic
         </a>
      </nav>
   </header>
   <div class="row-lg" style="height: 42px;">
      <div class="col-md text-right">
         <label for="">Date today:</label>
         <input type='date' id='hasta' value='<?php echo date('Y-m-d'); ?>'>
      </div>
   </div>

   <section class="MainContent">
      <div class="row-lg">
         <h5 class="bold" id="schedules" style=" color: #C37DF3;">SCHEDULES</h5>
      </div>
      <div class="maincon row px-lg-5">
         <div class="con1 col-lg-5 row-height px-lg-5 border bg-light">
            <div class="row">
               <div class="col-sm-2">
                  <div class="d-flex-column">
                     <div class="hour">
                        <div class="d-flex flex-column"></div>
                        <h6 class="p-2">7am</h6>
                        <h6 class="p-2">8am</h6>
                        <h6 class="p-2">9am</h6>
                        <h6 class="p-2">10am</h6>
                        <h6 class="p-2">11am</h6>
                        <h6 class="p-2">12pm</h6>
                        <h6 class="p-2">1pm</h6>
                        <h6 class="p-2">2pm</h6>
                        <h6 class="p-2">3pm</h6>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="coltable">
                     <table class="table table-bordered table-sm">
                        <tbody>
                           <tr id="unavail">
                              <th scope="row">
                                 <p style="visibility: hidden;">text</p>
                              </th>
                           </tr>
                           <tr id="avail">
                              <th scope="row">
                                 <p style="visibility: hidden;">text</p>
                              </th>
                           </tr>
                           <tr id="unavail">
                              <th scope="row">
                                 <p style="visibility: hidden;">text</p>
                              </th>
                           </tr>
                           <tr id="avail">
                              <th scope="row">
                                 <p style="visibility: hidden;">text</p>
                              </th>
                           </tr>
                           <tr id="unavail">
                              <th scope="row">
                                 <p style="visibility: hidden;">text</p>
                              </th>
                           </tr>
                           <tr id="avail">
                              <th scope="row">
                                 <p style="visibility: hidden;">text</p>
                              </th>
                           </tr>
                           <tr id="unavail">
                              <th scope="row">
                                 <p style="visibility: hidden;">text</p>
                              </th>
                           </tr>
                           <tr id="avail">
                              <th scope="row">
                                 <p style="visibility: hidden;">text</p>
                              </th>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="row">
                     <div class="col d-flex flex-row">
                        <div class="box avail"></div>
                        <p>Available</p>
                     </div>
                     <div class="col d-flex flex-row">
                        <div class="box notAvail"></div>
                        <p>Not Available</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="con2 col-lg-4 row-height px-lg-5 border bg-light">
            <h5 class="bold" style=" color: #C37DF3;">PATIENT INFORMATION</h5>
            <form action="/appointment-requests/store" method="POST">
               @csrf
               <div class="form-group row">
                  <label for="name" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-9">
                     <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                  </div>
               </div>
               <div class="form-group row">
                  <label for="email" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-9">
                     <input type="text" name="email" class="form-control" id="email" placeholder="Email">
                  </div>
               </div>
               <div class="form-group row">
                  <label for="contact_no" class="col-sm-2 col-form-label">Contact No.</label>
                  <div class="col-sm-9">
                     <input type="text" name="contact_no" class="form-control" id="conno" placeholder="number">
                  </div>
               </div>
               <div class="form-group row">
                  <label for="type" class="col-sm-2 col-form-label">Type</label>
                  <div class="col-sm-9">
                     <select class="form-control" name="patient_type" id="type">
                        <option disabled selected>Select patient type</option>
                        <option value="new">New Patient</option>
                        <option value="old">Old Patient</option>
                     </select>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="description"class="col-sm-2 col-form-label">Description</label>
                  <div class="col-sm-9">
                     <textarea class="form-control" name="description"  id="description" rows="3"></textarea>
                  </div>
               </div>
            </div>
            <div class="con3 col row-height px-lg-5 border bg-light">
               <h5 class="bold" style=" color: #C37DF3">PREFERRED TIME & DATE</h5>
               <label for="">Date:</label>
               <input type="date" name="date" id="">
               <label for="">Time start:</label>
               <input type="time" name="time_start" id="">
               <label for="">Time end:</label>
               <input type="time" name="time_end" id="">
               <div class="row btnSubmit justify-content-end">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </form>
            </div>
      </div>

   </section>
   <!-- Optional JavaScript -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>