@extends('master')
@section('title', 'Gabrillo Dental Clinic')
@section('content')
<div id="content">
    <div class="secondary-nav d-flex justify-content-between nopadding">
        <div class="mr-auto p-2">
            <a class="active mx-5" href="/users">Users</a>
        </div>
        <div class="p-2">
            <!-- <input id="searchInput" class="mx-5 rounded bg-light" type="text" placeholder="Search " aria-label="Search "> -->
            <input id="searchBox" type="text" class="mx-5 rounded bg-light bg-secondary" style="border-color: white; background-color: #F1F4FB;" placeholder="Search" autocomplete="off">
            <button type="mx-5 button" id="plus-button" class="btn btn-default btn-circle btn-xl border" data-toggle="modal" data-target="#addModal">
                <i class="fa fa-plus" style="color: #7764CA;"></i>
            </button>
            <div id="searchResult" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">
                    <u>Profile</u>
                </a>
            </div>
        </div>
    </div>

    <div id="main-content" class="container-fluid" style="background-color: #F3F6FB;">

    {{-- $('#tablee').bootstrapTable({
            url: 'http://www.mergebranch.com:8000/getPatientData',
        
            pagination: true,
            search: true,
            columns: [{
                sortable: true,
              field: 'last_name',
              title: 'Last Name'
            }, {
              field: 'contact_no',
              title: 'Contact Number'
            }, {
              field: 'address',
              title: 'Address'
            },{
                field: 'new_appointment',
                title: 'New Appointment'
              },{
                field: 'last_appointment',
                title: 'Last Appointment'
              },{
                sortable: true,
                field: 'created_at',
                title: 'Created At'
              },{
                events: 'operateEvents',
                formatter: 'operateFormatter',
                field: 'operate',
                title: 'Action'
              }]
          }); --}}


        <table id="table2" data-toggle="table" data-pagination="true" data-pagination-loop="false" data-search="true" data-visible-search="true" data-pagination-pre-text="Previous" data-pagination-next-text="Next" data-search-selector="#searchBox" data-url="/userData">
            <thead>
                <tr>
                    <th data-sortable="true" data-field="id">ID</th>
                    <th data-sortable="true" data-field="name">Name</th>
                    <th data-field="email">Email Address</th>
                    <th data-field="role">Role</th>
                    <th data-field="operate" data-formatter="operateFormatter">Action</th>
                </tr>
            </thead>
        </table>
        <script>
            var $table = $('#table2')

            function operateFormatter(value, row, index) {
                
                return [
                    '<button id="' + row.id + '"class="btn btn-danger btn-sm rounded-0 mr-2" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a></button>',
                    '<button id="' + row.id + '" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a></button></td>',
                ].join('')
            }
        </script>

        {{-- table for data showing --}}

    </div>

    <!-- Delete Form -->
    <form id="deleteForm" action="/users/destroy" method="POST">
        @csrf
        <input type="hidden" name="id" id="deleteFormId">
    </form>

    <!-- Add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <form action="/users/store" method="post" id="add_user_form">
            @csrf
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New user</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input id="patient-input" type="text" name="name" class="form-control" id="add_name" maxlength="15" required>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Roles</label>
                            <select name="role" class="form-control" id="add-select-roles" required>
                                <option value="" selected>Select role</option>
                                <option value="1">Admin</option>
                                <option value="2">Admin dentist</option>
                                <option value="3">Dentist</option>
                                <option value="4">Receptionist</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input id="patient-input" type="email" name="email" class="form-control" id="add_email" maxlength="30" required>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input id="add_password" type="password" name="password" class="form-control" maxlength="30" required>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input id="add_confirm_password" type="password" name="confirm" class="form-control" maxlength="30" required>
                        </div>
                        <div>
                            <span id="passwordmessage"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="save-changes" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="editForm" action="/users/update" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="editPatientHeading">Edit user</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input id="edit_id" type="hidden" name="id" class="form-control" maxlength="30">
                            <label>Name</label>
                            <input id="edit_name" type="text" name="name" class="form-control" maxlength="30" required>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Roles</label>
                            <select name="role" class="form-control" id="edit-select-roles" required>
                                <option selected disabled>Select role</option>
                                <option value="1">Admin</option>
                                <option value="2">Admin dentist</option>
                                <option value="3">Dentist</option>
                                <option value="4">Receptionist</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input id="edit_email" type="email" name="email" class="form-control" id="edit_email" maxlength="30" required>
                        </div>
                        <div class="form-group">
                            <label>Old Password</label>
                            <input id="edit_old_password" type="password" name="old_password" class="form-control" maxlength="30" required>
                        </div>
                        <div class="form-group">
                            <label>New Password</label>
                            <input id="edit_cnew_password" type="password" name="new_password" class="form-control" onChange="verifyPassword()" maxlength="30" minlength="6" required>
                        </div>
                        <div class="form-group">
                            <label>Confirm New Password</label>
                            <input id="edit_confirm_new_password" type="password" name="confirm_new_password" class="form-control" onChange="verifyPassword()" maxlength="30" minlength="6" required>
                        </div>
                    </div>
            <div class="modal-footer">
                <button type="submit" id="editSaveChanges" class="btn btn-primary">Submit</button>
            </div>
        </form>
        </div>
    </div>

    <script src="{{ asset('js/user.js') }}"></script>
</div>
@endsection