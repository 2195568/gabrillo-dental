@extends('master')
@section('title', 'Gabrillo Dental Clinic')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ url('/css/admindash.css') }}" />
<div class="content">
    <div id="secondary-nav" class="secondary-nav d-flex justify-content-between nopadding">
        <div class="mr-auto pt-2 pb-1">
            <a class="active mx-5" href="#">Dashboard Overview</a>
        </div>
    </div>

    <div id="main-content" style="background-color: #F3F6FB;">

        <div class="row">
            <div class="d-flex justify-content-start col-lg greet-con">
                <h2 id="greetings">Goodmorning  {{ Auth::user()->name }}!</h2>
            </div>

            <div class="d-flex justify-content-end col-lg date-con">
                <h5 id="date-today"></h5>
            </div>
        </div>



        <div class="row first-row">

            <div class="col-lg calendar-con">
                <div class="list-group">
                    <div class="title-bg d-flex justify-content-center">
                        <h2 class="header2">Today's Appointments</h2>
                    </div>
                    @foreach($data as $i)
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between" id ="btn"data-toggle="modal" data-target="#viewModal" data-id="{{ $i->id }}">
                            <h5 class="mb-1 name">{{$i->first_name }} {{$i->last_name}}</h5>
                        </div>
                        <p class="mb-1">{{$i->description}}</p>
                        <small class="text-info"> {{ date('g:i', strtotime($i->time_start)) }}</small>
                    </a>
                    @endforeach 

                    
    
                </div>
            </div>

<!-- View Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <div class="d-inline-flex flex-row">
                    <i style="color: #7764CA" class="fa fa-user-circle fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;" id="exampleModalLabel">View patient</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="d-flex justify-content-around">
                    <div class="d-flex flex-column align-items-center justify-content-start justify-content-between">
                        <div class="d-flex flex-column align-items-center justify-content-around shadow p-3 mb-5 bg-white rounded">
                            <div class="col-sm-4 bg-c-lite-green user-profile">
                                <div>
                                    <div class="m-b-25" id="view_patient_img"></div>
                                </div>
                            </div>
                            <div>
                                <h5 id="view_patient_name"></h5>
                            </div>
                            <span id="view_patient_email" class="text-wrap"></span>
                            <button id="viewTreatmentHistoryButton" data-toggle="modal" data-target="#treatmentHistoryModal" type="button" class="btn btn-primary" data-patient-id='0'>View treatment history</button>
                        </div>
                        <div class="d-flex flex-column w-100 shadow p-3 mb-5 bg-white rounded">
                            <h5>Balance:</h5>
                            <span id="view_patient_balance"></span>
                        </div>
                    </div>
                    <div class="w-75 shadow p-3 mb-5 bg-white rounded ml-5">
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Gender</th>
                                <th scope="col">Birthday</th>
                                <th scope="col">Phone Number</th>
                                <th scope="col">Age</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td id="view_patient_gender"></td>
                                <td id="view_patient_birthday"></td>
                                <td id="view_patient_phone_number"></td>
                                <td id="view_patient_age"></td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Address</th>
                              <!--  <th scope="col">Occupation</th> -->
                               
                                </tr>   
                            </thead>
                            <tbody>
                                <tr>
                                <td  id="view_patient_address"></td>
                                <!--<td  id="view_patient_occupation"></td>-->
                                
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Patient Status</th>
                                <th scope="col">Allergies</th>
                               <!-- <th scope="col">Account Creation</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td  id="view_patient_patient_status"><span class="badge rounded-pill bg-success"></span></td>
                                <td  id="view_patient_allergies"></td>
                            <!--    <td  id="view_patient_registered_date">Baguio</td> -->
                                </tr>
                            </tbody>
                        </table>
                     <!--   <button id="addPaymentButton" type="button" data- class="btn btn-primary" data-toggle="modal" data-target="#addPaymentModal">Add payment</button>
                        <button id="addTreatmentButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTreatmentModal" data-patient-id="0" data-first-name="." data-last-name="." data-email=".">Add treatment</button> -->
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

            <div class="col-lg treatments-con">
            
                <div class="list-group">
                    <div class="title-bg d-flex justify-content-center">
                        <h2 class="header2">Top Treatments</h2>
                    </div>
                    @foreach($procedurecount as $i)
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 name">{{$i-> name}}</h5>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <p class="mb-1">Total Treatments:</p>
                            <h6>{{$i-> count}}</h6>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


    <div class="row second-row">

        <div class="col pay-con">
            <div id="payment-dues">
                <div class="card">
                    <div class="card-body justify-content-center align-items-center flex-column">
                        <h1 id="payment-data"  data-toggle="modal" >{{$unpaidcount}} <i class="fas fa-money-check-alt fa-lg"></i></h1>
                        <h6 class="header-payment">Unpaid Treatments</h6>
                    </div>
                    <div class="card-footer text-light text-center">
                    <a href="{{ url('payments') }}" style=" text-decoration: none; color: white;"> View More</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="col active-">
            <div id="active-patients">

                <div class="card">
                    <div class="card-body d-flex justify-content-center align-items-center flex-column">
                        <h1 id="active-data">{{$activepatients}} <i class="fas fa-user-check fa-lg"></i></h1>
                        <h6 class="header-active">Active Patients</h6>
                    </div>
                    <div class="card-footer text-light text-center" >
                       <a href="{{ url('/patients') }}" style=" text-decoration: none; color: white;"> View More</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="col inactive-con">
            <div id="inactive-patients">

                <div class="card">
                    <div class="card-body d-flex justify-content-center align-items-center flex-column">
                        <h1 id="inactive-data" data-toggle="modal">{{$inactivepatients}}  <i class="fas fa-user-times fa-lg"></i></h1>
                        <h6 class="header-inactive">Inactive Patients</h6>
                    </div>
                    <div class="card-footer text-light text-center">
                    <a href="{{ url('/inactive-patients') }}" style=" text-decoration: none; color: white;"> View More</a>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

</div>

            <!-- Treatment History Modal -->
<div class="modal fade" id="treatmentHistoryModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header border-0">
                <div class="d-flex">
                    <i style="color: #7764CA" class="fa fa-history fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;">Treatment History</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <!-- Tooth numbering system -->
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="d-flex justify-content-between">
                        <div class="col-s titleL">
                            <h6 class="txt-grey ">Upper Left</h6>
                        </div>
                        <div class="col-s titleR">
                            <h6 class="txt-grey ">Upper Right</h6>
                        </div>
                    </div>
                    
                    <div class="d-flex flew-row justify-content-center">
                        <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">18</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">48</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">17</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">47</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">16</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">46</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">15</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">45</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">14</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">44</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">13</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">43</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">12</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">42</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">11</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">41</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="vl"></div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">21</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">31</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">22</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">32</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">23</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">33</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">24</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">34</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">25</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">35</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">26</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">36</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">27</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">37</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">28</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">38</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                </div>
                
                <div class="d-flex justify-content-between">
                    <div class="col-s titleL">
                        <h6 class="txt-grey ">Lower Left</h6>
                    </div>
                    <div class="col-s titleR">
                        <h6 class="txt-grey ">Lower Right</h6>
                    </div>
                </div>
                
            </div>    
            
            <!-- Records -->
            <div class="d-flex">
                <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                <h6 class="modal-title pt-2" style="color: black;">Records</h6>
            </div>
            
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="container overflow-auto" style="height: 200px;">
                        <div id="treatmentsHistoryContainer" class="row">
                        </div>
                    </div>
                </div>
                <div id="footer" >
                            <button id="closeModal" data-dismiss="modal"class="badge rounded-pill bg-danger" style="float:right; width: 10%; font-weght:40px; font-size:18px;" >Close</button>
                        </div>
            </form>
            </div>
        </div>
    </div>
</div>

<div class="container" style="padding: 0%,10px,0%;" width="800px">
            <div class="row">
                <div class="col-md-12">
                    <div class="modal fade" id="paymentstoday">
                        <div class="modal-dialog modal-lg rounded-0">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div>
                                        <h2 id="ttle" >Total Payments</h2>
                                        <table class="content-table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Amount Charged</th>
                                                    <th>Balance</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($payments as $i)
                                                <tr>
                                                    <td>{{$i->id}}</td>
                                                    <td>{{$i->first_name }} {{$i->last_name}}</td>
                                                    <td>{{$i->total_amount}}</td>
                                                    <th>{{$i->balance}}</th>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <button class="allbutton" >All Payments</button>
                                        <button class="closebutton" id="btn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container" style="padding: 0%,10px,0%;" width="800px">
            <div class="row">
                <div class="col-md-12">
                    <div class="modal fade" id="totalpatients">
                        <div class="modal-dialog modal-lg rounded-0">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div>
                                        <h2 id="ttle">Total Patients</h2>
                                        <table class="content-table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Contact No.</th>
                                                    <th>Balance</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($patients as $i)
                                                <tr>
                                                    <td>{{$i->id}}</td>
                                                    <td>{{$i->first_name }} {{$i->last_name}}</td>
                                                    <td>{{$i->contact_no}}</td>
                                                    <td>{{$i->balance}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <button class="allbutton">All patients</button>
                                        <button class="closebutton" id="btn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="padding: 0%,10px,0%;" width="800px">
            <div class="row">
                <div class="col-md-12">
                    <div class="modal fade" id="inactivePatients">
                        <div class="modal-dialog modal-lg rounded-0">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div>
                                        <h2 id="ttle">Total Patients</h2>
                                        <table class="content-table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Contact No.</th>
                                                    <th>Balance</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listInactivePatients as $i)
                                                <tr>
                                                    <td>{{$i->id}}</td>
                                                    <td>{{$i->first_name }} {{$i->last_name}}</td>
                                                    <td>{{$i->contact_no}}</td>
                                                    <td>{{$i->balance}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <button class="allbutton">All patients</button>
                                        <button class="closebutton" id="btn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script src="{{ asset('js/appointments/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/appointments/disable.js') }}"></script>
<script src="{{ asset('js/dentistdash.js') }}"></script>
<script>
    var m =  ["January","February","March","April","May","June","July","August","September","October","November","December"];
    n =  new Date();
y = n.getFullYear();
month = n.getMonth();
d = n.getDate();
document.getElementById("date-today").innerHTML = m[month] +" " + d + "," + y;



$(document).on('click', '#btn[data-id]', function () {
    var id = $(this).attr('data-id');
     
        $.ajax({
            dataType: 'json',
            url: '/get-patient/' + id,
            success: function(patient) {

                var firstName = patient.first_name,
                lastName = patient.last_name,
                email = patient.email,
                balance = patient.balance,
                gender = patient.gender,
                birthDate = patient.birth_date,
                address = patient.address,
                age = patient.age,
                contactNo = patient.contact_no,
               // occupation = patient.occupation,
                allergies = patient.allergies,
                //registeredDate = patient.created_at,
                id = patient.id,
                profile_bg_color = patient.profile_bg_color,
                patientStatus = '';
    
            if (patient.account_status == 'active') {
                patientStatus = '<span class = "badge rounded-pill bg-success"> Active </span>';
            } else {
                patientStatus = '<span class="badge rounded-pill bg-danger">Inactive</span>';
            }
 
            
            $('#view_patient_img').html('<img class="img-responsive rounded-full object-cover" style="float: left;" src="https://ui-avatars.com/api/?name=' + firstName + '+' + lastName  + profile_bg_color + '&amp;bold=true" alt="User profile image">');
            $('#view_patient_name').text(firstName + ' ' + lastName);
            $('#view_patient_email').text(email);
            $('#view_patient_balance').text(balance);
            $('#view_patient_gender').text(gender);
            $('#view_patient_birthday').text(birthDate);
            $('#view_patient_phone_no').text(contactNo);
            $('#view_patient_address').text(address);
            $('#view_patient_age').text(age);
            $('#view_patient_allergies').text(allergies);
          //  $('#view_patient_occupation').text(occupation);
            $('#view_patient_patient_status').html(patientStatus);
          //  $('#view_patient_registered_date').text(registeredDate);


            }
        });


        $('#viewTreatmentHistoryButton').click(function() {
            getTreatmentHistory(id);

        })

        // Tooth number buttons for treatment history
    $(".treatment-history-tooth-btn").click(function() {
        let tooth_no = $(this).text();
        let patient_id = $('#viewTreatmentHistoryButton').data('patient_id');
        console.log(patient_id);
        getTreatmentHistory(patient_id, tooth_no);
    });

    function getTreatmentHistory(patient_id, tooth_number = 'all') {
        $.ajax({
            dataType: 'json',
            url: '/treatments/' + patient_id + '/' + tooth_number + '/getTreatments',
            success: function(data) {              
                let html = renderTreatmentHistoryTable(data);
                $("#treatmentsHistoryContainer").html(html);
            }
        })
    }

    // Renders treatment history table
    function renderTreatmentHistoryTable(data) {      
        let html =
            '<table class="table ">' +
            '<thead>' +
            '<tr class="p-3 mb-5 rounded">' +
            '<td class="text-center">Treatment ID</td>' +
            '<td class="text-center">Name</td>' +
            '<td class="text-center">Date</td>' +
            '<td class="text-center">Tooth No</td>' +
            '<td class="text-center">Procedure/s</td>' +
            '<td class="text-center">Total Amount</td>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="treatmentBodyTable">';

        if (data.length > 0) {
            for (i = 0; i < data.length; i++) {
                html += "<tr class='shadow-sm p-3 mb-5 bg-white rounded'>";
                html += "<td class='text-center'>" + data[i].reference_id + "1</td>";
                html += "<th class='text-center' scope='row'>" + data[i].patient.first_name + ' ' + data[i].patient.last_name + '</th>';
                html += "<td class='text-center'>" + data[i].date + "</td>";
                html += "<td class='text-center'>" + data[i].tooth_no + "</td>";


                $("button:contains(" + data[i].tooth_no + ")").removeClass('btn-light').addClass('btn-warning').attr('disabled', false);

                let procedures = '';


                for (let val of data[i].procedures) {
                    procedures += ' - ' + val.name + '<br>';
                }


                procedures = $.trim(procedures);


                html += "<td class='text-center'><div>" + procedures + "</div></td>";
                html += "<td class='text-center'>" + data[i].total_amount + "</td>";

            }
            html += '</tbody></table>';
        } else {
            html = '<div class="d-flex justify-content-center"><div class="d-flex flex-column justify-content-center"><div class="d-flex flex-column justify-content-center pl-3"><img src="img/no-data.png" style="height: 500px;"></div></div></div>';
            return html;
        }

        return html;
    }

    $('#closeModal').on('click', function() {
        $('.modal').modal('hide');
    });
});
</script>

@if (Auth::user()->user_role_id == 3)
<script src="{{ asset('js/dashboard-calendar.js') }}"></script>
@else
<script src="{{ asset('js/dashboard-calendar.js') }}"></script>
@endif
@endsection
