@if (Auth::check())
    @extends('master')
    @section('title', 'Gabrillo Dental Clinic')
    @section('content')
    <div id="content">
    @include('dashboard.index')
    </div>
    @endsection     
@else
    <script>window.location = "/login";</script>
@endif