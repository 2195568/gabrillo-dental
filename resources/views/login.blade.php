<html>

<head>

    <link rel="stylesheet" href="{{ asset('css/login.css')}}"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <title>Gabrillo Dental Clinic</title>

</head>

<body>
    <nav class="navbar navbar-light fixed-top">
        <a class="navbar-brand"><img src="{{ asset('img/logo-desk.png')}}" alt="logo" width="45px">
            <b class="navtitle">GABRILLO DENTAL CLINIC</b>
        </a>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li>
                    <a class="color-me" href="#aboutus"><b>ABOUT US</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="conform rounded boarder-info justify-content-start">
        <div style="margin-left: 20px;margin-right: 20px">
            <div class="row" style="margin-top: 30px;">
                <div class="col-lg">
                    <h2 class="title text-dark">WELCOME TO GABRILLO DENTAL CLINIC</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg">
                    <form method="POST" action="/login">
                        @csrf
                        <div class="form-group">
                            <label for="label formGroupExampleInput">Username</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Enter username">
                        </div>
                        <div class="form-group">
                            <label for="label formGroupExampleInput2">Password</label>
                            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Enter password">
                        </div>
                        <div style="margin-left: 41%; margin-top: 30%">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!--
<img class="vector1" src="/images/Asset 1.svg" alt="svg">
<img class="svg1" src="/images/LGroupM.svg" alt="svg">
<img class="svg2" src="/images/BIgC.svg" alt="">
-->

    <img class="try" src="{{ asset('img/doctor.svg') }}" alt="">
    <img class="svg2" src="{{ asset('img/BIgC.svg') }}" alt="">

</body>

</html>