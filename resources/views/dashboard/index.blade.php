@extends('master')
@section('title', 'Gabrillo Dental Clinic')
@section('content')
<div id="content">
<!-- Secondary Nav -->
<div class="content">
    <div id="secondary-nav" class="secondary-nav d-flex justify-content-between nopadding">
        <div class="mr-auto pt-2 pb-1">
            <a class="active mx-5" href="#">Dashboard Overview</a>
        </div>
    </div>
    <!-- Main Content -->
    <div class="fluid-container" style="background-color: #F3F6FB;">
            <div class="container" style="background-color: grey;">
                <div class="d-flex">
                    <div>
                        <div class="graph-container">
                            <h2 class="bx--graph-header" style="color: white;">Appointment Statistics</h2>
                            <svg class="img-fluid"></svg>
                            <div class="graph-tooltip"></div>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-end">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">NEW PATIENTS THIS MONTH</h5>
                                <p class="h1">26</p>
                                <button type="button" class="btn btn-primary btn-sm">More</button>
                            </div>
                        </div>
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">TODAY'S APPOINTMENT</h5>
                                <p class="h1">4</p>
                                <button type="button" class="btn btn-primary btn-sm">More</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex w-100 justify-content-lg-around">
                    <div class="d-flex w-100">
                        <div>
                            <div class="card" style="width: 18rem;">
                                <div class="card-body">
                                    <h5 class="card-title">PATIENTS</h5>
                                    <p class="h1">103</p>
                                    <button type="button" class="btn btn-primary btn-sm">More</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">TREATMENTS</h5>
                                <ol type="1">
                                    <li>1. Consultation</li>
                                    <li>2. Scaling</li>
                                    <li>3. Root Canal</li>
                                    <li>4. Bleaching</li>
                                    <li>5. Tooth Extraction</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end w-100">
                        <div>
                            <p class="fw-bold">Gabrillo Dental Clinic</p>
                            <p>Cosue Bldg., Mc Arthur Highway</p>
                            <p>Villasis, Pangasinan</p>
                            <p>09123456789 - (075) 632 -1234</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src='https://cdnjs.cloudflare.com/ajax/libs/d3/4.7.3/d3.min.js'></script>
        <script src="js/dashboard.js"></script>
    </div>  
</div>

</div>
@endsection

