@extends('master')
@section('title', 'Gabrillo Dental Clinic | Patient')
@section('content')
<div id="content">
<div class="secondary-nav d-flex justify-content-between nopadding">
    <div class="mr-auto p-2">
        <a class="mx-5" href="/patients">Active patients</a>
         <a class="active mx-1" href="/inactive-patients">Inactive patients</a>
    </div>
    <div class="p-2">
        <!-- <input id="searchInput" class="mx-5 rounded bg-light" type="text" placeholder="Search " aria-label="Search "> -->
        <input id="searchBox" type="text" class="mx-5 rounded bg-light bg-secondary searchInput" style="border-color: #EEF0F2; background-color: #F1F4FB;" placeholder="Search">
        @if (Auth::user()->user_role_id != 3)
        <button type="mx-5 button" id="plus-button" class="btn btn-default btn-circle btn-xl border" data-toggle="modal" data-target="#addModal">
                <i class="fa fa-plus" style="color: #7764CA;"></i>
        </button>
        @endif
        <div id="searchResult" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">
                <u>Profile</u>
            </a>
        </div>
    </div>
</div>

<div id="main-content" class="container-fluid" style="background-color: #F3F6FB;">

    {{-- $('#tablee').bootstrapTable({
    url: 'http://www.mergebranch.com:8000/getPatientData',

    pagination: true,
    search: true,
    columns: [{
        sortable: true,
      field: 'last_name',
      title: 'Last Name'
    }, {
      field: 'contact_no',
      title: 'Contact Number'
    }, {
      field: 'address',
      title: 'Address'
    },{
        field: 'new_appointment',
        title: 'New Appointment'
      },{
        field: 'last_appointment',
        title: 'Last Appointment'
      },{
        sortable: true,
        field: 'created_at',
        title: 'Created At'
      },{
        events: 'operateEvents',
        formatter: 'operateFormatter',
        field: 'operate',
        title: 'Action'
      }]
  }); --}}

    <table id="tablee" data-toggle="table"    data-pagination="true" 
    data-pagination-loop="false" 
    data-search="true"
    data-visible-search="true" 
    data-pagination-pre-text="Previous"
    data-pagination-next-text="Next" 
    data-search-selector="#searchBox"

        data-url="/getInactive">
        <thead>
            <tr>
                <th data-sortable="true" data-field="slug">Id</th>
                <th data-sortable="true" data-field="first_name">First Name</th>
                <th data-sortable="true" data-field="last_name">Last Name</th>
                <th data-field="email">Email </th>
                <th data-field="contact_no">Contact No</th>
                <th data-field="address">Address</th>
                <th data-field="contact_no">Contact No</th>
                <th data-field="new_appointment">New Appointment</th>
                <th data-field="last_appointment">Last Appointment</th>
                <th data-sortable="true" data-field="created_at">Created At</th>
                <th data-field="operate" data-formatter="operateFormatter">Action</th>
            </tr>
        </thead>
    </table>
    <script>
        var $table = $('#tablee')

        function operateFormatter(value, row, index) {
            console.log(value);
            return [
                '<button id="' + row.id +
                '" class="btn btn-view btn-sm rounded-0 mr-3" style="background-color: #7764CA; color: white;" type="button" data-toggle="tooltip" data-placement="top" title="View patient"><i class="fa fa-user-circle"></i></a></button>',
                ,

                // '<button id="' + row.id + '" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a></button>',


            ].join('')
        }

        // window.operateEvents = {
        //     'click .like': function(e, value, row, index) {
        //         alert('You click like action, row: ' + JSON.stringify(row))
        //     },
            
        // }

    </script>

    {{-- table for data showing --}}
</div>


<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="editForm" action="/patients/update" method="post" novalidate>
                @csrf
                <input type="hidden" name='id' id="editPatientId">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="editPatientHeading">Edit patient</h5>
                    <button type="button" id="editFormCloseButton" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="editFormModalBody" class="modal-body">
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">FIRST NAME <b style="color: red;"> *</b></h6>
                    <input id="edit_first_name" type="text" name="first_name" class="form-control" maxlength="26" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">LAST NAME <b style="color: red;"> *</b></h6>
                    <input id="edit_last_name" type="text" name="last_name" class="form-control" maxlength="26" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">MIDDLE NAME <b style="color: red;"> *</b></h6>
                    <input id="edit_middle_name" type="text" name="middle_name" class="form-control" maxlength="26" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">BIRTH DATE <b style="color: red;"> *</b></h6>
                    <input id="edit_birth_date" type='date' name="birth_date" autocomplete="off" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">AGE <b style="color: red;"> *</b></h6>
                    <input id="edit_age" type='number' name="age" class="form-control" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">GENDER <b style="color: red;"> *</b></h6>
                    <select class="form-control" id="edit_gender" name="gender" required>
                        <option value="" selected>Select gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">CONTACT NUMBER <b style="color: red;"> *</b></h6><br>
                    <input type="text" id="edit_contact_number" name="contact_no" maxlength="13" autocomplete="off" value="+639" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">EMAIL ADDRESS <b style="color: red;"> *</b></h6>
                    <input id="edit_email_address" type='email' name="email_address" class="form-control" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">HOME ADDRESS <b style="color: red;"> *</b></h6>
                    <input id="edit_home_address" type='text' name="home_address" class="form-control" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">OCCUPATION</h6>
                    <input id="edit_occupation" type='text' name="occupation" class="form-control">
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">ALLERGIES</h6>
                    <input id="edit_allergies" type='text' name="allergies" class="form-control" aria-describedby="allergiesHelpBlock">
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                    <p class="font-intalic text-muted">
                        For more than 1 allergy, please use comma to separate the items (e.g., peanuts, pollen, eggs, milk). 
                    </p>
                </div>
            </div>
                <div class="modal-footer border-0">
                    @if (Auth::user()->user_role_id < 3)
                    <button type="submit" id="activatePatientButton" class="btn btn-success" data-dismiss="modal">Activate</button>
                    @endif
                    <button type="submit" id="editSaveChanges" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            <!-- Deactivate patient form -->
            <form id="activatePatientForm" action="/patients/activate" method="post">
                @csrf
                <input type="hidden" name='id' id="activatePatientId">
                <input type="hidden" name='name' id="activatePatientName">
            </form>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form id="addForm" action="/patients/store" method="post" novalidate>
    @csrf
    <input type="hidden" name="profile_bg_color" id="profile_bg_color">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <div class="d-flex">
                    <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;">Add new patient</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">FIRST NAME <b style="color: red;"> *</b></h6>
                    <input type="text" name="first_name" class="form-control" id="patientFirstName" maxlength="26" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">LAST NAME <b style="color: red;"> *</b></h6>
                    <input type="text" name="last_name" class="form-control" id="patientLastName" maxlength="26" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">MIDDLE NAME <b style="color: red;"> *</b></h6>
                    <input type="text" name="middle_name" class="form-control" id="patientMiddleName" maxlength="26" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">BIRTH DATE <b style="color: red;"> *</b></h6>
                    <input type='date' name="birth_date" id="patientBirthDate" autocomplete="off" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">AGE <b style="color: red;"> *</b></h6>
                    <input type='text' name="age" class="form-control" id="patientAge" maxlength="3" required>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                     <h6 class="modal-title pt-2" style="color: #292a2d;">GENDER <b style="color: red;"> *</b></h6>
                    <select class="form-control" name="gender" id="patientGender" required>
                        <option value="" selected>Select gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                    <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                    <h6 class="modal-title pt-2" style="color: #292a2d;">CONTACT NUMBER <b style="color: red;"> *</b></h6>
                        <input type='text' name="contact_no" id="patientContactNumber" maxlength="13" autocomplete="off" value="+639" required>
                        <i class="fa fa-check-circle"></i>
                        <i class="fa fa-exclamation-circle"></i>
                        <small>Error message</small>
                </div>
                <div class="form-group form-con">
                        <h6 class="modal-title pt-2" style="color: #292a2d;">EMAIL ADDRESS <b style="color: red;"> *</b></h6>
                        <input type='email' name="email_address" class="form-control" id="patientEmailAddress" maxlength="320"required>
                        <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                        <h6 class="modal-title pt-2" style="color: #292a2d;">HOME ADDRESS <b style="color: red;"> *</b></h6>
                        <input type='text' name="home_address" class="form-control" id="patientHomeAddress" maxlength="35" required>
                        <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                        <h6 class="modal-title pt-2" style="color: #292a2d;">OCCUPATION</h6>
                        <input type='text' name="occupation" class="form-control" id="patientOccupation" maxlength="35">
                        <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                </div>
                <div class="form-group form-con">
                        <h6 class="modal-title pt-2" style="color: #292a2d;">ALLERGIES</h6>
                        <input type='text' name="allergies" class="form-control" id="patientAllergies" maxlength="49" aria-describedby="allergiesHelpBlock" >
                        <i class="fa fa-check-circle"></i>
                    <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small>
                    <p class="font-intalic text-muted">
                            For more than 1 allergy, please use comma to separate the items (e.g., peanuts, pollen, eggs, milk). 
                    </p>
                </div>
            </div>
            <div class="modal-footer border-0">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" id="save-changes" class="btn btn-primary">Add</button>
            </div>
        </div>
    </div>
</form>
</div>

<!-- View Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <div class="d-inline-flex flex-row">
                    <i style="color: #7764CA" class="fa fa-user-circle fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;" id="exampleModalLabel">View patient</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="d-flex justify-content-around">
                    <div class="d-flex flex-column align-items-center justify-content-start justify-content-between">
                        <div class="d-flex flex-column align-items-center justify-content-around shadow p-3 mb-5 bg-white rounded">
                            <div class="col-sm-4 bg-c-lite-green user-profile">
                                <div>
                                    <div class="m-b-25" id="view_patient_img"></div>
                                    <!-- <h5 class="f-w-600">Foo Test</h5>
                                    <p class="text-center"><small>2191711@slu.edu.ph</small></p> -->
                                </div>
                            </div>
                            <div>
                                <h5 id="view_patient_name">Foo test</h5>
                            </div>
                            <span id="view_patient_email" class="text-wrap">2195568@slu.edu.ph</span>
                            <button id="viewTreatmentHistoryButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#treatmentHistoryModal" data-patient-id='0'>View treatment history</button>
                        </div>
                        <div class="d-flex flex-column w-100 shadow p-3 mb-5 bg-white rounded">
                            <h5>Balance:</h5>
                            <i class="fa-solid fa-peso-sign"></i><span id="view_patient_balance">0.00</span>
                        </div>
                    </div>
                    <div class="w-75 shadow p-3 mb-5 bg-white rounded ml-5">
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Gender</th>
                                <th scope="col">Birthday</th>
                                <th scope="col">Phone Number</th>
                                <th scope="col">Age</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td id="view_patient_gender">Male</td>
                                <td id="view_patient_birthday">Feb 24th, 1997</td>
                                <td id="view_patient_phone_number">0917550921</td>
                                <td id="view_patient_age">81 years old</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Address</th>
                                <th scope="col">Occupation</th>
                                <th scope="col">Allergies</th>
                                </tr>   
                            </thead>
                            <tbody>
                                <tr>
                                <td  id="view_patient_address">Jl. Diponegoro No. 21, Baguio City</td>
                                <td  id="view_patient_occupation">Software Developer</td>
                                <td  id="view_patient_allergies">Egg, sea foods</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Patient Status</th>
                                @if (Auth::user()->user_role_id < 3)
                                <th scope="col">Account Creation</th>
                                @endif
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td  id="view_patient_patient_status"><span class="badge rounded-pill bg-success">Active</span></td>
                                @if (Auth::user()->user_role_id < 3)
                                <td  id="view_patient_registered_date">Baguio</td>
                                @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer border-0">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" id="save-changes" class="btn btn-primary">Save changes</button>
            </div> -->
        </div>
    </div>
</div>

<!-- Add Payment Modal -->
<div class="modal fade" id="addPaymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form action="/payments/store" method="post">
    @csrf
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header border-0">
                <div class="d-flex">
                    <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;">Add new payment</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                 <div class="form-group">
                    <label>Treatment Reference ID</label>
                    <input id="treatment-reference-input" type="text" name="treatment_reference_id" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Amount</label>
                    <input type="number" name="amount" class="form-control" id="amount" required>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="save-changes" class="btn btn-primary">Pay</button>
            </div>
        </div>
    </div>
</form>
</div>

<!-- Add Treatment Modal -->
<div class="modal fade" id="addTreatmentModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header border-0">
                <div class="d-flex">
                    <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;">Add new treatment</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <!-- Patient -->
            <div class="d-flex">
                <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                <h6 class="modal-title pt-2" style="color: black;">Patient</h6>
            </div>
            
            <div class="shadow p-3 mb-5 bg-white rounded">
                <div class="form-group">
                    <div id="patientSearchResultBox" class="d-flex justify-content-center">
                    </div>
                </div>
            </div>
                
                <!-- Tooth numbering system -->
                <div class="d-flex">
                    <!-- <img class="img-responsive img-thumbnail img-fluid btn btn-xs" width="50px" src="{{ asset('img/tooth.svg') }}"></img> -->
                    <h6 class="modal-title pt-2" style="color: black;">Tooth number <b style="color: red;"> *</b></h6>
                </div>
                
                <div id="toothNumberContainer" class="shadow p-3 mb-5 bg-white rounded">
                    <div class="d-flex justify-content-between">
                        <div class="col-s titleL">
                            <h6 class="txt-grey ">Upper Left</h6>
                        </div>

                        <div class="col-s titleR">
                            <h6 class="txt-grey ">Upper Right</h6>
                        </div>
                    </div>
                    
                    <div class="d-flex flew-row justify-content-center">
                        <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" class="mb-1" alt="" width="30%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">18</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">48</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">17</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">47</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">16</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">46</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">15</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">45</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">14</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">44</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">13</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">43</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">12</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">42</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">11</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">41</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="vl"></div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">21</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">31</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">22</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">32</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">23</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">33</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">24</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">34</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">25</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">35</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">26</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">36</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">27</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">37</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">28</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">38</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <!-- <i class="fa fa-exclamation-circle"></i>
                    <small>Error message</small> -->
                </div>
                
                <div class="d-flex justify-content-between">
                    <div class="col-s titleL">
                        <h6 class="txt-grey ">Lower Left</h6>
                    </div>
                    <div class="col-s titleR">
                        <h6 class="txt-grey ">Lower Right</h6>
                    </div>
                </div>
                
            </div>    
            
            <!-- Procedure -->
            <div class="d-flex">
                <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                <h6 class="modal-title pt-2" style="color: black;">Procedure <b style="color: red;"> *</b></h6>
            </div>
            
            <form id="add_treatment_form" action="/treatments/store" method="post">
                @csrf
                <input type="hidden" name='tooth_no' id="add_tooth_no">
                <input type="hidden" name='patient_id' id="add_patient_id">
                <input type="hidden" name='date' id="add_date">
                <input type="hidden" name='total_amount' id="add_total_amount">

                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="container overflow-auto" style="height: 200px;">
                        <div class="row procedures_container">
                        </div>
                    </div>
                </div>
            </form>

            <!-- Date/Total amount -->
            <div class="row">
                <div class="col-md-6">
                        <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                        <h6 class="modal-title pt-2" style="color: black;">Date <b style="color: red;"> *</b></h6>
                    </div>
                    
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group form-con">
                            <input type="date" class="form-control" id="date" placeholder="Date">
                            <i class="fa fa-check-circle"></i>
                            <i class="fa fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                        <h6 class="modal-title pt-2" style="color: black;">Total amount <b style="color: red;"> *</b></h6>
                    </div>
                    
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group form-con">
                            <input type="number" step=".01" class="form-control" id="total_amount" style="text-align: right;" placeholder="Total value" onchange="leadingZeros(this)" onkeyup="leadingZeros(this)" onclick="leadingZeros(this)">
                            <i class="fa fa-check-circle"></i>
                            <i class="fa fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button id="addNewTreatmentCloseButton" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="addNewTreatmentButton" type="submit" class="btn btn-primary">Add</button>
            </div>

        </div>
    </div>
</div>

<!-- Treatment History Modal -->
<div class="modal fade" id="treatmentHistoryModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header border-0">
                <div class="d-flex">
                    <i style="color: #7764CA" class="fa fa-history fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;">Treatment History</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <!-- Tooth numbering system -->
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="d-flex justify-content-between">
                        <div class="col-s titleL">
                            <h6 class="txt-grey ">Upper Left</h6>
                        </div>
                        <div class="col-s titleR">
                            <h6 class="txt-grey ">Upper Right</h6>
                        </div>
                    </div>
                    
                    <div class="d-flex flew-row justify-content-center">
                        <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">18</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">48</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">17</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">47</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">16</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">46</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">15</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">45</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">14</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">44</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">13</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">43</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">12</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">42</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">11</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">41</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="vl"></div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">21</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">31</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">22</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">32</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">23</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">33</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">24</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">34</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">25</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">35</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">26</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">36</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">27</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">37</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">28</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">38</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                </div>
                
                <div class="d-flex justify-content-between">
                    <div class="col-s titleL">
                        <h6 class="txt-grey ">Lower Left</h6>
                    </div>
                    <div class="col-s titleR">
                        <h6 class="txt-grey ">Lower Right</h6>
                    </div>
                </div>
                
            </div>    
            
            <!-- Records -->
            <div class="d-flex">
                <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                <h6 class="modal-title pt-2" style="color: black;">Records</h6>
            </div>
            
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="container overflow-auto" style="height: 200px;">
                        <div id="treatmentsHistoryContainer" class="row">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@if (Auth::user()->user_role_id == 4)
<script src="{{ asset('js/receptionist-patient.js') }}"></script>
@elseif (Auth::user()->user_role_id == 3)
<script src="{{ asset('js/dentist-patient.js') }}"></script>
@else
<script src="{{ asset('js/patient.js') }}"></script>
@endif
</div>
@endsection
