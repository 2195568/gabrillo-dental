@extends('master')

@section('title', 'Gabrillo Dental Clinic | Patient')
@section('content')
    <div id="content">
        <div class="secondary-nav d-flex justify-content-between nopadding">
            <div class="mr-auto p-2">
                <a class="active mx-5" href="/patients">Active patients</a>
                <a class="mx-1" href="/inactive-patients">Inactive patients</a>
            </div>
            <div class="p-2">
                <!-- <input id="searchInput" class="mx-5 rounded bg-light" type="text" placeholder="Search " aria-label="Search "> -->
                <input id="searchBox" type="text" class="mx-5 rounded bg-light bg-secondary searchInput"
                    style="border-color: #EEF0F2; background-color: #F1F4FB;" placeholder="Search" minlength="13">
                <button type="mx-5 button" id="plus-button" class="btn btn-default btn-circle btn-xl border"
                    data-toggle="modal" data-target="#addModal">
                    <i class="fa fa-plus" style="color: #7764CA;"></i>
                </button>
                <div id="searchResult" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">
                        <u>Profile</u>
                    </a>
                </div>
            </div>
        </div>

        <div id="main-content" class="container-fluid" style="background-color: #F3F6FB;">
            <div class="d-flex justify-content-center">
                <div class="d-flex flex-column justify-content-center">
                    <div class="d-flex flex-column justify-content-center pl-3">
                        <img src="img/no-data.png" style="height: 500px;">
                    </div>
                </div>
            </div>
        </div>

        <!-- Edit Modal -->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="/patients/update" method="post">
                        @csrf
                        <input type="hidden" name='id' id="editPatientId">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editPatientHeading">Edit patient</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>First Name</label>
                                <input id="edit_first_name" type="text" name="first_name" class="form-control"
                                    id="patientFirstName" maxlength="30" required>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input id="edit_last_name" type="text" name="last_name" class="form-control"
                                    id="patientLastName" maxlength="30" required>
                            </div>
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input id="edit_middle_name" type="text" name="middle_name" class="form-control"
                                    id="patientMiddleName" maxlength="30" required>
                            </div>
                            <div class="form-group">
                                <label>Birth date</label>
                                <input id="edit_birth_date" type='date' name="birth_date"
                                    class="datetimepicker form-control" id="patientBirthDate" required>
                            </div>
                            <div class="form-group">
                                <label>Age</label>
                                <input type='text' name="age" id="patientAge" class="form-control"
                                    onkeypress="return isNumeric(event)" type="number" min="5" max="120" required />

                                <script>
                                    function isNumeric(evt) {
                                        var theEvent = evt || window.event;
                                        var key = theEvent.keyCode || theEvent.which;
                                        key = String.fromCharCode(key);
                                        var regex = /[0-9]|\./;
                                        if (!regex.test(key)) {
                                            theEvent.returnValue = false;
                                            if (theEvent.preventDefault) theEvent.preventDefault();
                                        }
                                    }

                                </script>
                            </div>
                            <div class="form-group">
                                <label for="sel1">Gender:</label>
                                <select class="form-control" id="edit_gender" name="gender">
                                    <option selected disabled>Select gender</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Contact number</label>
                                <input type='text' class="form-control" name="contact_number" name="Amount"
                                    id="patientContactNumber" class="form-control" placeholder="09xxxxxxxxx"
                                    onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" type="number"
                                    maxlength="11" min="0" max="09999999999" />

                                <script>
                                    function maxLengthCheck(object) {
                                        if (object.value.length > object.maxLength)
                                            object.value = object.value.slice(0, object.maxLength)
                                    }

                                    function isNumeric(evt) {
                                        var theEvent = evt || window.event;
                                        var key = theEvent.keyCode || theEvent.which;
                                        key = String.fromCharCode(key);
                                        var regex = /[0-9]|\./;
                                        if (!regex.test(key)) {
                                            theEvent.returnValue = false;
                                            if (theEvent.preventDefault) theEvent.preventDefault();
                                        }
                                    }

                                </script>
                            </div>
                            <div class="form-group">
                                <label>Email address</label>
                                <input id="edit_email_address" type='email' name="email_address" class="form-control"
                                    id="patientEmailAddress" maxlength="40">
                            </div>
                            <div class="form-group">
                                <label>Home address</label>
                                <input type='text' name="home_address" class="form-control" id="patientHomeAddress"
                                    maxlength="200" minlength="11">
                            </div>
                            <div class="form-group">
                                <label>Occupation</label>
                                <input type='text' name="occupation" class="form-control" id="patientOccupation"
                                    maxlength="30">
                            </div>
                            <div class="form-group">
                                <label>Allergies</label>
                                <input id="edit_allergies" type='text' name="allergies" class="form-control"
                                    id="patientAllergies" aria-describedby="allergiesHelpBlock" maxlength="300">
                                <small id="allergiesHelpBlock" class="form-text text-muted">
                                    For more than 1 allergy, please use comma to separate the items (e.g., peanuts, pollen,
                                    eggs, milk).
                                </small>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" id="deactivatePatientButton" class="btn btn-danger"
                                data-dismiss="modal">Deactivate</button>
                            <button type="submit" id="editSaveChanges" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                    <!-- Deactivate patient form -->
                    <form id="deactivatePatientForm" action="/patients/deactivate" method="post">
                        @csrf
                        <input type="hidden" name='id' id="deactivatePatientId">
                        <input type="hidden" name='name' id="deactivatePatientName">
                    </form>
                </div>
            </div>
        </div>

        <!-- Add Patient Modal -->
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <form action="/patients/store" method="post">
                @csrf
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New patient</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="hidden" name="profile_bg_color" id="profile_bg_color">
                                <input type="text" name="first_name" class="form-control" id="patientFirstName"
                                    maxlength="30" required>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="last_name" class="form-control" id="patientLastName" maxlength="30"
                                    required>
                            </div>
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input type="text" name="middle_name" class="form-control" id="patientMiddleName"
                                    maxlength="30" required>
                            </div>
                            <div class="form-group">
                                <label>Birth date</label>
                                <input type='date' name="birth_date" class="datetimepicker form-control"
                                    id="patientBirthDate" required>
                            </div>
                            <div class="form-group">
                                <label>Age</label>
                                <input type='number' name="age" id="patientAge" class="form-control"
                                    onkeypress="return isNumeric(event)" type="number" maxlength="2" min="2" max="120"
                                    required />
                            </div>
                            <div class="form-group">
                                <label for="sel1">Gender:</label>
                                <select class="form-control" id="patientGender">
                                    <option selected disabled>Select gender</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Contact number</label>
                                <input type='number' class="form-control" name="contact_number" name="Amount"
                                    id="patientContactNumber" class="form-control" placeholder="09xxxxxxxxx"
                                    oninput="maxLengthCheck(this)" type="number"
                                    maxlength="11" min="0" max="09999999999" />


                            </div>
                            <div class="form-group">
                                <label>Email address</label>
                                <input type='email' name="email_address" class="form-control" id="patientEmailAddress"
                                    maxlength="40">
                            </div>
                            <div class="form-group">
                                <label>Home address</label>
                                <input type='text' name="home_address" class="form-control" id="patientHomeAddress"
                                    maxlength="200" minlength="11">
                            </div>
                            <div class="form-group">
                                <label>Occupation</label>
                                <input type='text' name="occupation" class="form-control" id="patientOccupation"
                                    maxlength="30">
                            </div>
                            <div class="form-group">
                                <label>Allergies</label>
                                <input type='text' name="allergies" class="form-control" id="patientHomeAddress"
                                    aria-describedby="allergiesHelpBlock" maxlength="200">
                                <small id="allergiesHelpBlock" class="form-text text-muted">
                                    For more than 1 allergy, please use comma to separate the items (e.g., peanuts, pollen,
                                    eggs, milk).
                                </small>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- Add Payment Modal -->
        <div class="modal fade" id="addPaymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <form action="/payments/store" method="post">
                @csrf
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header border-0">
                            <div class="d-flex">
                                <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                                <h5 class="modal-title" style="color: #7764CA;">Add new payment</h5>
                            </div>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Treatment Reference ID</label>
                                <input id="treatment-reference-input" type="text" name="treatment_reference_id"
                                    class="form-control" onkeypress="return isNumeric(event)" " maxlength=" 9" required>


                                <script>
                                    function isNumeric(evt) {
                                        var theEvent = evt || window.event;
                                        var key = theEvent.keyCode || theEvent.which;
                                        key = String.fromCharCode(key);
                                        var regex = /[0-9]|\./;
                                        if (!regex.test(key)) {
                                            theEvent.returnValue = false;
                                            if (theEvent.preventDefault) theEvent.preventDefault();
                                        }
                                    }

                                </script>
                            </div>
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="number" name="amount" class="form-control" id="amount" placeholder="Amount"
                                    onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)"
                                    onchange="RoundtoDecimal(this)" maxlength="10" min="0" max="999999" required>
                                <script>
                                    function maxLengthCheck(object) {
                                        if (object.value.length > object.maxLength)
                                            object.value = object.value.slice(0, object.maxLength)
                                    }

                                    function isNumeric(evt) {
                                        var theEvent = evt || window.event;
                                        var key = theEvent.keyCode || theEvent.which;
                                        key = String.fromCharCode(key);
                                        var regex = /[0-9]|\./;
                                        if (!regex.test(key)) {
                                            theEvent.returnValue = false;
                                            if (theEvent.preventDefault) theEvent.preventDefault();
                                        }
                                    }

                                    function RoundtoDecimal(ctrl) {
                                        if (ctrl.value.length > 0) {
                                            var num = parseFloat(ctrl.value);
                                            ctrl.value = num.toFixed(2);
                                        }
                                    }

                                </script>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" id="save-changes" class="btn btn-primary">Pay</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

<!-- View Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <div class="d-inline-flex flex-row">
                    <i style="color: #7764CA" class="fa fa-user-circle fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;" id="exampleModalLabel">View patient</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="d-flex justify-content-around">
                    <div class="d-flex flex-column align-items-center justify-content-start justify-content-between">
                        <div class="d-flex flex-column align-items-center justify-content-around shadow p-3 mb-5 bg-white rounded">
                            <div class="col-sm-4 bg-c-lite-green user-profile">
                                <div>
                                    <div class="m-b-25" id="view_patient_img"></div>
                                    <!-- <h5 class="f-w-600">Foo Test</h5>
                                    <p class="text-center"><small>2191711@slu.edu.ph</small></p> -->
                                </div>
                            </div>
                            <span id="view_patient_email" class="text-wrap">2195568@slu.edu.ph</span>
                            <button id="viewTreatmentHistoryButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#treatmentHistoryModal" data-patient-id='0'>View treatment history</button>
                        </div>
                        <div class="d-flex flex-column w-100 shadow p-3 mb-5 bg-white rounded">
                            <h5>Balance:</h5>
                            <span id="view_patient_balance">0.00</span>
                        </div>
                    </div>
                    <div class="w-75 shadow p-3 mb-5 bg-white rounded ml-5">
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Gender</th>
                                <th scope="col">Birthday</th>
                                <th scope="col">Phone Number</th>
                                <th scope="col">Age</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td id="view_patient_gender">Male</td>
                                <td id="view_patient_birthday">Feb 24th, 1997</td>
                                <td id="view_patient_phone_number">0917550921</td>
                                <td id="view_patient_age">81 years old</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Address</th>
                                <th scope="col">Occupation</th>
                                <th scope="col">Allergies</th>
                                </tr>   
                            </thead>
                            <tbody>
                                <tr>
                                <td  id="view_patient_address">Jl. Diponegoro No. 21, Baguio City</td>
                                <td  id="view_patient_occupation">Software Developer</td>
                                <td  id="view_patient_allergies">Egg, sea foods</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless border-bottom">
                            <thead>
                                <tr>
                                <th scope="col">Patient Status</th>
                                <th scope="col">Account Creation</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td  id="view_patient_patient_status"><span class="badge rounded-pill bg-success">Active</span></td>
                                <td  id="view_patient_registered_date">Baguio</td>
                                </tr>
                            </tbody>
                        </table>
                        <button id="addPaymentButton" type="button" data- class="btn btn-primary" data-toggle="modal" data-target="#addPaymentModal">Add payment</button>
                        <button id="addTreatmentButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTreatmentModal" data-patient-id="0" data-first-name="." data-last-name="." data-email=".">Add treatment</button>
                    </div>
                </div>
            </div>
        </div>

<!-- Add Treatment Modal -->
<div class="modal fade" id="addTreatmentModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header border-0">
                <div class="d-flex">
                    <i style="color: #7764CA" class="fa fa-plus-square fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;">Add new treatment</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <!-- Tooth numbering system -->
                <div class="d-flex">
                    <!-- <img class="img-responsive img-thumbnail img-fluid btn btn-xs" width="50px" src="{{ asset('img/tooth.svg') }}"></img> -->
                    <h6 class="modal-title pt-2" style="color: black;">Tooth number</h6>
                </div>
                
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="d-flex justify-content-between">
                        <div class="col-s titleL">
                            <h6 class="txt-grey ">Upper Left</h6>
                        </div>
                        <div class="col-s titleR">
                            <h6 class="txt-grey ">Upper Right</h6>
                        </div>
                    </div>
                    
                    <div class="d-flex flew-row justify-content-center">
                        <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" class="mb-1" alt="" width="30%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">18</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">48</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">17</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">47</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">16</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">46</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">15</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">45</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">14</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">44</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">13</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">43</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">12</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">42</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">11</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">41</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="vl"></div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">21</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">31</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">22</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">32</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">23</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">33</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">24</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">34</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">25</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">35</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">26</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">36</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">27</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">37</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">28</button>
                        <button type="button" class="btn new-treatment-tooth-no-btn btn-light mb-1">38</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                </div>
                
                <div class="d-flex justify-content-between">
                    <div class="col-s titleL">
                        <h6 class="txt-grey ">Lower Left</h6>
                    </div>
                    <div class="col-s titleR">
                        <h6 class="txt-grey ">Lower Right</h6>
                    </div>
                </div>
                
            </div>    
            
            <!-- Procedure -->
            <div class="d-flex">
                <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                <h6 class="modal-title pt-2" style="color: black;">Procedure</h6>
            </div>
            
            <form id="add_treatment_form" action="/treatments/store" method="post">
                @csrf
                <input type="hidden" name='tooth_no' id="add_tooth_no">
                <input type="hidden" name='patient_id' id="add_patient_id">
                <input type="hidden" name='date' id="add_date">
                <input type="hidden" name='total_amount' id="add_total_amount">

                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="container overflow-auto" style="height: 200px;">
                        <div class="row procedures_container">
                        </div>
                    </div>
                </div>
            </form>

            
            <!-- Patient -->
            <div class="d-flex">
                <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                <h6 class="modal-title pt-2" style="color: black;">Patient</h6>
            </div>
            
            <div class="shadow p-3 mb-5 bg-white rounded">
                <div class="form-group">
                    <input type="text" class="form-control" id="patientSearchBox" placeholder="Search" autocomplete="off">
                    <div id="patientSearchResultBox" class="d-flex justify-content-center">
                    </div>
                </div>
            </div>

            <!-- Date/Total amount -->
            <div class="row">
                <div class="col-md-6">
                        <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                        <h6 class="modal-title pt-2" style="color: black;">Date</h6>
                    </div>
                    
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group">
                            <input type="date" class="form-control" id="date" placeholder="Date">
                            <div id="patientSearchResultBox" class="d-flex justify-content-center">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="d-flex">
                        <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                        <h6 class="modal-title pt-2" style="color: black;">Total amount</h6>
                    </div>
                    
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="form-group">
                            <input type="number" step=".01" class="form-control" id="total_amount" style="text-align: right;" placeholder="Total value" onchange="leadingZeros(this)" onkeyup="leadingZeros(this)" onclick="leadingZeros(this)">
                        </div>
                        <!-- <input id="patientSearchBox" type="text" class="rounded bg-light bg-secondary" style="border-color: #EEF0F2; background-color: #F1F4FB;" placeholder="Search">     -->
                    </div>
                </div>
            </div>
        </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button id="add_submit_button" type="button" class="btn btn-primary" data-dismiss="modal">Add</button>
            </div>

        </div>
    </div>
</div>

<!-- Treatment History Modal -->
<div class="modal fade" id="treatmentHistoryModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header border-0">
                <div class="d-flex">
                    <i style="color: #7764CA" class="fa fa-history fa-2x mr-3"></i>
                    <h5 class="modal-title" style="color: #7764CA;">Treatment History</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <!-- Tooth numbering system -->
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="d-flex justify-content-between">
                        <div class="col-s titleL">
                            <h6 class="txt-grey ">Upper Left</h6>
                        </div>
                        <div class="col-s titleR">
                            <h6 class="txt-grey ">Upper Right</h6>
                        </div>
                    </div>
                    
                    <div class="d-flex flew-row justify-content-center">
                        <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">18</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">48</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">17</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">47</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">16</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">46</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">15</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">45</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">14</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">44</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">13</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">43</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">12</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">42</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">11</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">41</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="vl"></div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">21</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">31</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">22</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">32</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/canineDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">23</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">33</button>
                        <img src="{{ asset('img/canine.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">24</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">34</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/incisorDown.svg')}}" class="mb-1" alt="" width="15%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">25</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">35</button>
                        <img src="{{ asset('img/incisor.svg')}}" class="mb-1" alt="" width="15%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/premolarDown.svg')}}" class="mb-1" alt="" width="20%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">26</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">36</button>
                        <img src="{{ asset('img/premolar.svg')}}" class="mb-1" alt="" width="20%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">27</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">37</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{ asset('img/molarDown.svg')}}" class="mb-1" alt="" width="30%">
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">28</button>
                        <button disabled type="button" class="btn btn-light treatment-history-tooth-btn mb-1">38</button>
                        <img src="{{ asset('img/molar.svg')}}" class="mb-1" alt="" width="30%">
                    </div>
                </div>
                
                <div class="d-flex justify-content-between">
                    <div class="col-s titleL">
                        <h6 class="txt-grey ">Lower Left</h6>
                    </div>
                    <div class="col-s titleR">
                        <h6 class="txt-grey ">Lower Right</h6>
                    </div>
                </div>
                
            </div>    
            
            <!-- Records -->
            <div class="d-flex">
                <i style="color: #7764CA" class="fa fa-tools fa-2x mr-3"></i>
                <h6 class="modal-title pt-2" style="color: black;">Records</h6>
            </div>
            
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <div class="container overflow-auto" style="height: 200px;">
                        <div id="treatmentsHistoryContainer" class="row">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/patient.js') }}"></script>
</div>
@endsection
