<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProcedureController;
use App\Http\Controllers\RequestApprovalController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\TreatmentController;
use App\Http\Controllers\DashController;
use App\Http\Controllers\AdminDashController;
use Illuminate\Http\Request;
use App\Models\ProcedureCategory;
use App\Models\Treatment;
use App\Models\Patient;
use App\Models\Procedure;
use App\Models\Payment;
use App\Models\Appointment;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect('/online-appointment');
});

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/dashboard', function () {
    echo Auth::user;
});

Route::get('/log', function () {
    return view('login');
});

// User routes
Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('/users', function () {
        return view('user.index');
    });
    Route::get('/users/index', [UserController::class, 'index']);
    Route::post('/users/store', [UserController::class, 'store']);
    Route::post('/users/destroy', [UserController::class, 'destroy']);
    Route::get('/users/{id}/show', [UserController::class, 'show']);
    Route::post('/users/update', [UserController::class, 'update']);
    Route::post('/users/search', [UserController::class, 'search']);
});

Route::get('/reports', function () {
    return view('reports.index');
});
Route::get('/dashboard/admin', function () {
    return view('/admindash.index');
});

Route::group(['middleware' => ['auth', 'dentist']], function () {
    Route::post('/appointments/store', [AppointmentController::class, 'store']);
    
    Route::get('/appointment/index', [AppointmentController::class, 'index']);
    Route::post('/appointments/update', [AppointmentController::class, 'update']);
    Route::post('/appointments/delete', [AppointmentController::class, 'destroy']);
    Route::post('/appointments/request-approval/accept', [AppointmentController::class, 'requestApprovalAccept']);
    Route::post('/appointments/request-approval/reject', [AppointmentController::class, 'requestApprovalReject']);
    Route::post('/appointments/request-approval/review/accept', [AppointmentController::class, 'requestApprovalReviewAccept']);

    Route::get('/dashboard/dentist', function () {
        return view('dentistdash.index');
    });
    // Payment routes
    Route::get('/payments', function () {
        return view('payment.index');
    });
});

// Appointment routes
Route::get('/appointments', function () {
    return view('appointment.index');
});
Route::get('/events', [AppointmentController::class, 'events']);
Route::get('/event/{id}', [AppointmentController::class, 'getEventById']);
Route::get('/appointments/request-approval', function () {
    return view('appointment.request-approval');
});
Route::post('/appointments/search', [AppointmentController::class, 'search']);

Route::group(['middleware' => ['auth', 'receptionist']], function () {
    Route::get('/payments/logs/index', [PaymentController::class, 'logs']);
    Route::post('/payments/update', [PaymentController::class, 'update']);
    Route::get('/payments/logs', function() {
        return view('payment.logs');
    });
});



// data for table
Route::get('/userData', [UserController::class, 'getData']);
Route::get('/logsData', [PaymentController::class, 'getLogs']);
Route::get('/treatmentData', [TreatmentController::class, 'getData']);
Route::get('/paymentData', [PaymentController::class, 'getData']);

Route::group(['middleware' => ['auth']], function () {
    // Treatment routes
    Route::get('/treatments', function () {
        return view('treatment.index');
    });
  
    Route::get('/treatments/index', [TreatmentController::class, 'index']);
    Route::post('/treatments/{tooth}/{procid}/{cost}/{date}/{patientid}/save', [TreatmentController::class, 'saveMulti']);
    Route::get('/treatments/{toothno}/{procid}/{cost}/{date}/{id}/save', [TreatmentController::class, 'savemulti']);
    Route::get('/treatments/{id}/show', [TreatmentController::class, 'show']);
    Route::get('/treatments/{id}/procedures', [TreatmentController::class, 'showSelectedProcedures']);
    Route::post('/treatments', [TreatmentController::class, 'saveProcedure']);
    Route::post('/treatments/update', [TreatmentController::class, 'update']);
    Route::post('/treatments/delete', [TreatmentController::class, 'destroy']);
    Route::post('/treatments/search', [TreatmentController::class, 'search']);
    Route::post('/treatments/store', [TreatmentController::class, 'store']);
    Route::get('/treatments/{id}/getProcedures', [TreatmentController::class, 'getProceduresByTreatmentId']);
    Route::get('/treatments/{patient_id}/{tooth_number}/getTreatments', [TreatmentController::class, 'getTreatmentsByPatientId']);
});

// Payments routes
Route::get('/payments/index', [PaymentController::class, 'index']);
Route::post('/payments/store', [PaymentController::class, 'store']);
Route::post('/payments/delete', [PaymentController::class, 'destroy']);
Route::get('/payments/{id}/show', [PaymentController::class, 'show']);

// Appointment request routes
Route::get('/old-appointment-requests', [RequestApprovalController::class, 'oldAppointmentRequests']);
Route::get('/new-appointment-requests', [RequestApprovalController::class, 'newAppointmentRequests']);
Route::post('/appointment-requests/store', [RequestApprovalController::class, 'store']);

// Patient routes

Route::get('/getInactive', [PatientController::class, 'getInactive']);
Route::get('/getPatientData', [PatientController::class, 'getData']);
Route::get('/patients', function () {
    return view('patient.index');
});
Route::post('/patients/store', [PatientController::class, 'store']);
Route::get('/patients/{status}', [PatientController::class, 'patients']);
Route::get('/inactive-patients', function () {
    return view('patient.inactive-patients');
});
// Route::get('/patients/inactive', [PatientController::class, 'inactivePatients']);
Route::get('/patients/{id}/appointments', [PatientController::class, 'appointments']);
Route::get('/get-patient/{id}', [PatientController::class, 'show']);
Route::get('/get-patient-with-appointments/{id}', [PatientController::class, 'showPatientWithAppointments']);
Route::post('/patients/update', [PatientController::class, 'update']);
Route::post('/patients/deactivate', [PatientController::class, 'deactivatePatient']);
Route::post('/patients/activate', [PatientController::class, 'activatePatient']);
Route::post('/patients/search', [PatientController::class, 'search']);

// Procedure routes
Route::get('/procedures/index', [ProcedureController::class, 'index']);

// Website and Online Appointment routes
Route::get('/online-appointment/{date}/schedules', [AppointmentController::class, 'getAppointmentsByDate']);
Route::get('/online-appointment', function () {
    return view('online-appointment.index2');
});
Route::get('/online-appointment/patient-type', function () {
    return view('online-appointment.patient-type');
});
Route::get('/online-appointment/new-patient-form', function () {
    return view('online-appointment.new-patient-form');
});
Route::get('/online-appointment/old-patient-form', [PatientController::class, 'getPatientBySlug']);
Route::get('/online-appointment/new-patient-form', [AppointmentController::class, 'newPatientForm']);
Route::get('/online-appointment/new-patient-form/check-email-availability/{email}', [AppointmentController::class, 'checkEmailAvailability']);
Route::get('/online-appointment/new-patient-form/update/{referenceId}', [AppointmentController::class, 'updateNewPatientAppointmentGet']);
Route::post('/online-appointment/new-patient-form/update', [AppointmentController::class, 'updateNewPatientAppointmentPost']);
Route::post('/online-appointment/submit', [AppointmentController::class, 'onlineAppointmentSubmit']);
Route::post('/online-appointment/cancel-appointment', [AppointmentController::class, 'cancelAppointment']);
Route::get('/online-appointment/confirm/{reference_id}/{patient_type}', [AppointmentController::class, 'confirmAppointment']);

Route::get('/landing', function () {
    return view('online.landingpage');
});
Route::get('/get-patient-by-slug', [PatientController::class, 'getPatientBySlug']);

// Test routings
Route::get('/tests', function (Request $request) {
    $old = DB::table('old_patient_appointment_requests')->where('reference_id', '7OQwN')->first();

    print_r(json_encode($old) );

});

//dashboard routings
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {

    switch(Auth::user()->user_role_id){
        case 1:
            return redirect('dashboard/admin');
            break;
        case 2:
            return redirect('dashboard/admin');
            break;
        case 3:
            return redirect('dashboard/dentist');
            break;
        case 4:
            return redirect('dashboard/receptionist');
            break;
    }
})->name('dashboard');
Route::get('dashboard/admin', [DashController::class, 'getAdminDashData']);
Route::get('dashboard/receptionist', [DashController::class, 'getReceptionistDashData']);
Route::get('dashboard/dentist', [DashController::class, 'getTodaysAppointmentData']);
//end of dashboard routings

    // return json_encode($result);

// });

//reports
Route::get('/reportsday/{start}/{end}/appointments', [ReportsController::class, 'getAppointmentsByday']);
Route::get('/reportsday/{monthcal}/appointments', [ReportsController::class, 'getAppointmentsByMonth']);
Route::get('/reportsyear/{yearcal}/appointments', [ReportsController::class, 'getAppointmentsByYear']);
Route::get('/procedures/{year1}/appointments', [ReportsController::class, 'getProceduresByYear']);
Route::get('/incomeyear/{yearcal}/', [ReportsController::class, 'getIncome']);

Route::post('/reports/getAppointmentsMonth', [ReportsController::class, 'appointmentsMonth']);
Route::post('/reports/getAppointmentsYear', [ReportsController::class, 'appointmentsYear']);

//Active and Inactive
Route::get('/reports2', [ReportsController::class, 'pieGraph2']);

Route::get('/reports', [ReportsController::class, 'index']);
Route::get('/getTopProceduresname/{year}', [ReportsController::class, 'getTopProceduresName']);



